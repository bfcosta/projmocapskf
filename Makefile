CXX			=	g++
CXX2JS		=	em++
INC_DIRS	=	-I/usr/include/eigen3 -I./lib -I./tools -I./sbox
CXXFLAGS	=	-O2 -Wall -g $(INC_DIRS)
LDFLAGS		=	-lGL -lGLU -lglut -L/usr/lib/nvidia-384
CXX2JS_DIR		=	emscripten
CXX2JS_CFLAGS	=	-I/usr/include/eigen3 -I./lib -I./$(CXX2JS_DIR) -Wno-gnu-static-float-init -Wno-deprecated
CXX2JS_LDFLAGS	=	-s WASM=1 -s NO_EXIT_RUNTIME=1 -s ALLOW_MEMORY_GROWTH=1 -s SINGLE_FILE=1 
CXX2JS_LDFLAGS	+=	-s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]'
BUILD_DIR	=	objs
TOL_DIR		=	tools
TST_DIR		=	utest
BOX_DIR		=	sbox
LIB_DIR		=	lib
SOURCE_DIR	=	$(LIB_DIR) $(TST_DIR) $(BOX_DIR) $(TOL_DIR)
SOURCES		=	$(wildcard $(addsuffix /*.cpp,$(SOURCE_DIR)))
LIB_SOURCES	=	$(wildcard $(LIB_DIR)/*.cpp)
BVH_SOURCES	=	$(filter-out $(LIB_DIR)/asfreader.cpp, $(wildcard $(LIB_DIR)/*.cpp))
DEPS		=	$(wildcard $(addsuffix /*.h,$(SOURCE_DIR)))
OBJECTS		=	$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(notdir $(SOURCES)))
LIB_OBJECTS	=	$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(notdir $(LIB_SOURCES)))
BVH_OBJECTS	=	$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(notdir $(BVH_SOURCES)))
CXX2JS_OBJECTS	=	$(patsubst %.cpp,$(CXX2JS_DIR)/$(BUILD_DIR)/%.o,$(notdir $(BVH_SOURCES)))
CXX2JS_OBJECTS	+=	$(patsubst %.c,$(CXX2JS_DIR)/$(BUILD_DIR)/%.o,$(notdir $(wildcard $(CXX2JS_DIR)/*.c)))

EXEC1	=	$(TOL_DIR)/reader
EXEC2	=	$(BOX_DIR)/curvesimpl
EXEC3	=	$(BOX_DIR)/matstat
EXEC4	=	$(BOX_DIR)/mograph
EXEC5	=	$(TOL_DIR)/skfmocap
EXEC6	=	$(TST_DIR)/pcaut
EXEC7	=	$(BOX_DIR)/pcamocap
EXEC8	=	$(BOX_DIR)/kynanalizer
EXEC9	=	$(TST_DIR)/kfselectorut
EXEC10	=	$(TST_DIR)/experiment01
EXEC11	=	$(TOL_DIR)/dumpanim
EXEC12	=	$(TOL_DIR)/dumplinear
EXEC13	=	$(BOX_DIR)/show2d
EXEC14	=	$(TST_DIR)/experiment02
EXEC15	=	$(TOL_DIR)/dumpmixed
EXEC16	=	$(TOL_DIR)/saveproj
EXEC17	=	$(TOL_DIR)/runtemp
EXEC18	=	$(TOL_DIR)/impvbvhproj
EXEC19	=	$(TOL_DIR)/proj2us
EXEC20	=	$(TOL_DIR)/xml2bvh
EXEC21	=	$(TOL_DIR)/proj2bvh
EXEC22	=	$(TOL_DIR)/savehmap
EXEC23	=	$(TOL_DIR)/savedistmat
EXEC24	=	$(TOL_DIR)/stress
EXEC25	=	$(TOL_DIR)/rbfptest
EXEC26	=	$(TOL_DIR)/impvuciproj
EXEC27	=	$(TOL_DIR)/rbfpbvhsmoother
EXEC28	=	$(CXX2JS_DIR)/projection.js
EXEC29	=	$(CXX2JS_DIR)/multiproj.js

all: core
sandbox: $(EXEC2) $(EXEC3) $(EXEC4) $(EXEC7) $(EXEC8) $(EXEC13)
test: $(EXEC6) $(EXEC9) $(EXEC10) $(EXEC14)
tools: player util core
core: $(EXEC16) $(EXEC17) $(EXEC18) $(EXEC19) $(EXEC24) $(EXEC26)
player: $(EXEC1) $(EXEC5)
util: $(EXEC11) $(EXEC12) $(EXEC15) $(EXEC25) $(EXEC27)
cpp: sandbox test core player util
js:	$(EXEC28) $(EXEC29)

# objects
$(BUILD_DIR)/impvuciproj.o $(BUILD_DIR)/proj2us.o $(BUILD_DIR)/rbfptest.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp
$(BUILD_DIR)/renderhelper.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(TOL_DIR)/%.h
$(BUILD_DIR)/lle.o $(BUILD_DIR)/pca.o $(BUILD_DIR)/saliency.o: $(BUILD_DIR)/%.o: $(BOX_DIR)/%.cpp $(BOX_DIR)/%.h
$(BUILD_DIR)/rbf.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h
$(CXX2JS_DIR)/$(BUILD_DIR)/rbf.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h
$(CXX2JS_DIR)/$(BUILD_DIR)/phelper.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(CXX2JS_DIR)/%.cpp $(CXX2JS_DIR)/%.h
$(CXX2JS_DIR)/$(BUILD_DIR)/projection.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(CXX2JS_DIR)/%.cpp
$(CXX2JS_DIR)/$(BUILD_DIR)/glu2emsdk.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(CXX2JS_DIR)/%.c

HDR01 = $(LIB_DIR)/animation.h
HDR02 = $(LIB_DIR)/asfreader.h
HDR03 = $(LIB_DIR)/bvhreader.h
HDR04 = $(LIB_DIR)/drawer.h
HDR05 = $(LIB_DIR)/mocapreader.h
HDR06 = $(LIB_DIR)/kfinterpolator.h
HDR07 = $(LIB_DIR)/kf2dinterpolator.h
HDR08 = $(LIB_DIR)/kfselector.h
HDR09 = $(BOX_DIR)/pca.h
HDR10 = $(CXX2JS_DIR)/phelper.h
HDR11 = $(TOL_DIR)/renderhelper.h
HDR12 = $(BOX_DIR)/saliency.h $(BOX_DIR)/lle.h
HDR13 = $(LIB_DIR)/skanalizer.h
HDR14 = $(LIB_DIR)/skeleton.h

HDR15 = $(HDR04) $(HDR14) $(LIB_DIR)/rbf.h
HDR16 = $(HDR02) $(HDR03) $(HDR05)
HDR17 = $(HDR01) $(HDR15) $(HDR16) 

$(BUILD_DIR)/reader.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR11)
$(BUILD_DIR)/show2d.o: $(BUILD_DIR)/%.o: $(BOX_DIR)/%.cpp $(HDR11)
$(BUILD_DIR)/skeleton.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR04)
$(CXX2JS_DIR)/$(BUILD_DIR)/skeleton.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR04)

$(BUILD_DIR)/skanalizer.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR04) $(HDR14)
$(CXX2JS_DIR)/$(BUILD_DIR)/skanalizer.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR04) $(HDR14)
$(CXX2JS_DIR)/$(BUILD_DIR)/multiproj.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(CXX2JS_DIR)/%.cpp $(HDR10)
$(BUILD_DIR)/animation.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR15)
$(CXX2JS_DIR)/$(BUILD_DIR)/animation.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR15)
$(BUILD_DIR)/asfreader.o $(BUILD_DIR)/bvhreader.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR05)
$(CXX2JS_DIR)/$(BUILD_DIR)/bvhreader.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR05)

$(BUILD_DIR)/stress.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR03) $(HDR05) $(HDR06) $(HDR13) $(HDR15)
$(BUILD_DIR)/xml2bvh.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR01) $(HDR15)
$(BUILD_DIR)/proj2bvh.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR01) $(HDR03) $(HDR05) $(HDR15)

$(BUILD_DIR)/curvesimpl.o $(BUILD_DIR)/kynanalizer.o: $(BUILD_DIR)/%.o: $(BOX_DIR)/%.cpp $(BOX_DIR)/%.h $(HDR16)
$(BUILD_DIR)/matstat.o $(BUILD_DIR)/mograph.o: $(BUILD_DIR)/%.o: $(BOX_DIR)/%.cpp $(BOX_DIR)/%.h $(HDR16)
$(BUILD_DIR)/dumpanim.o $(BUILD_DIR)/dumpmixed.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR17)
$(BUILD_DIR)/dumplinear.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR15) $(HDR16)
$(BUILD_DIR)/skfmocap.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR11) $(HDR17)

$(BUILD_DIR)/experiment01.o $(BUILD_DIR)/experiment02.o: $(BUILD_DIR)/%.o: $(TST_DIR)/%.cpp $(HDR06) $(HDR07) $(HDR08) $(HDR16)
$(BUILD_DIR)/impvbvhproj.o $(BUILD_DIR)/rbfpbvhsmoother.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR13) $(HDR17)

$(BUILD_DIR)/kf2dinterpolator.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR05) $(HDR06) $(HDR13) $(HDR15)
$(CXX2JS_DIR)/$(BUILD_DIR)/kf2dinterpolator.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: \
$(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR05) $(HDR06) $(HDR13) $(HDR15)

$(BUILD_DIR)/kfinterpolator.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR05) $(HDR13) $(HDR15)
$(CXX2JS_DIR)/$(BUILD_DIR)/kfinterpolator.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR05) $(HDR13) $(HDR15)

$(BUILD_DIR)/kfselector.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR07) $(HDR19)
$(CXX2JS_DIR)/$(BUILD_DIR)/kfselector.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR07) $(HDR19)

$(BUILD_DIR)/kfselectorut.o: $(BUILD_DIR)/%.o: $(TST_DIR)/%.cpp $(HDR13) $(HDR15) $(HDR18)
$(BUILD_DIR)/multireader.o: $(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR13) $(HDR15) $(HDR18)
$(CXX2JS_DIR)/$(BUILD_DIR)/multireader.o: $(CXX2JS_DIR)/$(BUILD_DIR)/%.o: $(LIB_DIR)/%.cpp $(LIB_DIR)/%.h $(HDR13) $(HDR15) $(HDR18)

$(BUILD_DIR)/pcamocap.o: $(BUILD_DIR)/%.o: $(BOX_DIR)/%.cpp $(HDR04) $(HDR09) $(HDR11) $(HDR14) $(HDR16)
$(BUILD_DIR)/pcaut.o: $(BUILD_DIR)/%.o: $(TST_DIR)/%.cpp $(HDR09) $(HDR12) $(HDR16)
$(BUILD_DIR)/runtemp.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR03) $(HDR08) $(HDR19)
$(BUILD_DIR)/savedistmat.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR03) $(HDR19)
$(BUILD_DIR)/savehmap.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR13) $(HDR21)
$(BUILD_DIR)/saveproj.o: $(BUILD_DIR)/%.o: $(TOL_DIR)/%.cpp $(HDR03) $(HDR08) $(HDR20)

## all libs

$(EXEC6): $(BUILD_DIR)/pca.o $(BUILD_DIR)/saliency.o $(BUILD_DIR)/pcaut.o $(BUILD_DIR)/lle.o $(LIB_OBJECTS)
$(EXEC7): $(BUILD_DIR)/pca.o $(BUILD_DIR)/saliency.o $(BUILD_DIR)/pcamocap.o $(BUILD_DIR)/renderhelper.o $(LIB_OBJECTS)
$(EXEC1) $(EXEC5) $(EXEC11) $(EXEC12) $(EXEC15): \
	$(TOL_DIR)/%: $(addprefix $(BUILD_DIR)/,%.o) $(LIB_OBJECTS) $(BUILD_DIR)/renderhelper.o
$(EXEC2) $(EXEC3) $(EXEC4) $(EXEC8): \
	$(BOX_DIR)/%: $(addprefix $(BUILD_DIR)/,%.o) $(LIB_OBJECTS)
$(EXEC9) $(EXEC10) $(EXEC14): \
	$(TST_DIR)/%: $(addprefix $(BUILD_DIR)/,%.o) $(LIB_OBJECTS)
$(EXEC18) $(EXEC27): \
	$(TOL_DIR)/%: $(addprefix $(BUILD_DIR)/,%.o) $(LIB_OBJECTS)

## no asf

$(EXEC16) $(EXEC17) $(EXEC21) $(EXEC22) $(EXEC23) $(EXEC24) $(EXEC25): \
	$(TOL_DIR)/%: $(addprefix $(BUILD_DIR)/,%.o) $(BVH_OBJECTS)

## standalone

$(EXEC13): $(BUILD_DIR)/show2d.o $(BUILD_DIR)/renderhelper.o
$(EXEC19) $(EXEC26): \
	$(TOL_DIR)/%: $(addprefix $(BUILD_DIR)/,%.o)

$(EXEC1) $(EXEC2) $(EXEC3) $(EXEC4) $(EXEC5) $(EXEC6) $(EXEC7) $(EXEC8) \
$(EXEC9) $(EXEC10) $(EXEC11) $(EXEC12) $(EXEC13) $(EXEC14) $(EXEC15) \
$(EXEC16) $(EXEC17) $(EXEC18) $(EXEC19) $(EXEC21) $(EXEC22) $(EXEC23) \
$(EXEC24) $(EXEC25) $(EXEC26) $(EXEC27):
	$(CXX) -o $@ $^ $(LDFLAGS)

## javascript

$(EXEC28): $(CXX2JS_OBJECTS) $(CXX2JS_DIR)/$(BUILD_DIR)/projection.o
	$(CXX2JS)  -o $@ $^ $(CXX2JS_LDFLAGS) -s EXPORTED_FUNCTIONS='["_getBvhProjection","_getNumberOfFrames","_getRbfSmoothedProjection","_getImprovedProjection","_getPoseError","_getSkfSkeleton","_getMocapSkeleton","_addMocapInMultiProjection","_delMocapInMultiProjection","_getMultiRbfProjection","_getMocapUnitRbfProjection"]'

$(EXEC29): $(CXX2JS_OBJECTS) $(CXX2JS_DIR)/$(BUILD_DIR)/multiproj.o $(CXX2JS_DIR)/$(BUILD_DIR)/phelper.o
	$(CXX2JS)  -o $@ $^ $(CXX2JS_LDFLAGS) -s EXPORTED_FUNCTIONS='["_add2DB","_clearDB","_computeKFs","_setKFs","_getRBFP","_getRBFPOpt","_getPoseError","_unprojectPose"]'

## use pugixml library

$(EXEC20): $(BUILD_DIR)/xml2bvh.o $(BUILD_DIR)/skeleton.o $(BUILD_DIR)/animation.o $(BUILD_DIR)/rbf.o
	$(CXX) -o $@ $^ $(LDFLAGS) -lpugixml

$(OBJECTS):
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(CXX2JS_DIR)/$(BUILD_DIR)/projection.o $(CXX2JS_DIR)/$(BUILD_DIR)/multiproj.o $(CXX2JS_DIR)/$(BUILD_DIR)/phelper.o $(CXX2JS_OBJECTS):
	$(CXX2JS) $(CXX2JS_CFLAGS) -o $@ -c $<

.PHONY: clean debug

clean:
	rm -f *~ $(CXX2JS_DIR)/$(BUILD_DIR)/*.o $(BUILD_DIR)/*.o $(EXEC1) \
	$(EXEC2) $(EXEC3) $(EXEC4) $(EXEC5) $(EXEC6) $(EXEC7) $(EXEC8) \
	$(EXEC9) $(EXEC10) $(EXEC11) $(EXEC12) $(EXEC13) $(EXEC14) \
	$(EXEC15) $(EXEC16) $(EXEC17) $(EXEC18) $(EXEC19) $(EXEC20) \
	$(EXEC21) $(EXEC22) $(EXEC23) $(EXEC24) $(EXEC25) $(EXEC26) \
	$(EXEC27) $(EXEC28) $(EXEC29)

debug:
	@echo CXX = $(CXX)
	@echo CXXFLAGS = $(CXXFLAGS)
	@echo LDFLAGS = $(LDFLAGS)
	@echo BUILD_DIR = $(BUILD_DIR)
	@echo SOURCE_DIR = $(SOURCE_DIR)
	@echo SOURCES = $(SOURCES)
	@echo LIB_SOURCES = $(LIB_SOURCES)
	@echo BVH_SOURCES = $(BVH_SOURCES)
	@echo DEPS = $(DEPS)
	@echo OBJECTS = $(OBJECTS)
	@echo LIB_OBJECTS = $(LIB_OBJECTS)
	@echo BVH_OBJECTS = $(BVH_OBJECTS)
	@echo CXX2JS_OBJECTS = $(CXX2JS_OBJECTS)
