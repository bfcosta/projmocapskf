#ifndef KYNANALIZER_H
#define KYNANALIZER_H

#include <Eigen/Dense>
#include <mcsession.h>

/**
 * @class
 * @brief
 */
class MocapKynematicAnalizer: public MocapSession {
	public:
		/**
		 * @brief
		 */
		MocapKynematicAnalizer(MocapReader * r = NULL): MocapSession(r) {}
		Eigen::MatrixXd getVelocityNaive();
		Eigen::MatrixXd getVelocity();
		Eigen::MatrixXd getAcceleration();
		Eigen::MatrixXd getCurvature();
		Eigen::MatrixXd getJerkness();
		void printKynematicsDescriptors(unsigned int jid);
};

#endif
