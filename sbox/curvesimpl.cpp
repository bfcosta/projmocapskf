#include <curvesimpl.h>
#include <asfreader.h>
#include <bvhreader.h>
//#include <matstat.h>
#include <iostream>
#include <algorithm>
#include <libgen.h>
#include <cstdlib>

/**
 * @brief build an array with interpolated skeleton poses, given inital and final frames.
 * @param lower frame interpolation index
 * @param upper frame interpolation index
 * @return array with interpolated frames
 */
std::vector<Skeleton> CurveSimplification::getInterpolatedFrames(unsigned int f0, unsigned int f1) {
	assert(f0 < rd->getNumberOfFrames());
	assert(f1 < rd->getNumberOfFrames());
	Skeleton s0, s1;
	Animation anim;
	std::vector<Skeleton> ret;
	unsigned int aux;

	if (f0 > f1) {
		aux = f0;
		f0 = f1;
		f1 = aux;
	}
	s0 = rd->getFrameSkeleton(f0);
	anim.setSkeleton(s0);
	anim.addNewKeyFrame(f0, s0);
	s1 = rd->getFrameSkeleton(f1);
	anim.addNewKeyFrame(f1, s1);
	anim.setContext();
	for (unsigned int i = f0; i <= f1; ++i) {
		anim.setController(i);
		anim.animate();
		ret.push_back(anim.getSkeleton());
	}
	return ret;
}

/**
 * @brief get a distance metric measure between two skeleton poses defined as frame ids.
 * @param fid0 frame id
 * @param fid1 frame id
 * @return error distance dependent on distance definition
 */
double CurveSimplification::getPoseDistance(unsigned int fid0, unsigned int fid1) {
	Skeleton s0, s1;
	double dist;

	assert((fid0 < rd->getNumberOfFrames()) && (fid1 < rd->getNumberOfFrames()));
	s0 = rd->getFrameSkeleton(fid0);
	s1 = rd->getFrameSkeleton(fid1);
	dist = sahelper->getPoseDistance(s0, s1);
	std::cout << "d(" << fid0 << ", " << fid1 << ") = " << dist << std::endl;
	return dist;
}

/**
 * @brief get the frame with most distant skeleton pose inside frame interval
 * @param f0 lower frame id
 * @param f1 upper frame id
 * @param error distance metric for elected frame (output)
 * @return frame with largest summed error from inital and final given frames
 */
unsigned int CurveSimplification::getMostDissimilarFrameFromExtremes(unsigned int f0, unsigned int f1, double & error) {
	assert(f0 < rd->getNumberOfFrames());
	assert(f1 < rd->getNumberOfFrames());
	unsigned int aux = f0 + f1;
	double dd, dp;
	Skeleton s0, s1, s2;

	error = 0;
	if (f0 > f1) {
		aux = f0;
		f0 = f1;
		f1 = aux;
	}
	s0 = rd->getFrameSkeleton(f0);
	s1 = rd->getFrameSkeleton(f1);
	for (unsigned int i = f0; i <= f1; ++i) {
		s2 = rd->getFrameSkeleton(i);
		dd = 0;
		dp = sahelper->getPoseDistance(s0, s2);
		dd = dp * dp;
		dp = sahelper->getPoseDistance(s1, s2);
		dd = dd + dp * dp;
		std::cout << i << "\t" << dd << std::endl;
		if (dd > error) {
			error = dd;
			aux = i;
		}
	}
	return aux;
}

/**
 * @brief get the frame with most distant skeleton pose inside frame interval
 * @param f0 lower frame id
 * @param f1 upper frame id
 * @param error distance metric for elected frame (output)
 * @return frame with largest distance from interpolated frame
 */
unsigned int CurveSimplification::getMostDissimilarFrameFromInterp(unsigned int f0, unsigned int f1, double & error) {
	std::vector<Skeleton> pose = getInterpolatedFrames(f0, f1);
	Skeleton sk;
	double dist;
	unsigned int ret = f0;

	error = 0;
	for (unsigned int i=0; i < pose.size(); ++i) {
		sk = rd->getFrameSkeleton(i + f0);
		dist = sahelper->getPoseDistance(sk, pose.at(i));
		//std::cout << i << "\t" << dist << std::endl;
		if (dist > error) {
			error = dist;
			ret = i + f0;
		}
	}
	return ret;
}

/**
 * @brief get the frame with most distant skeleton pose during animation
 * @param anim animation handler
 * @param error distance metric for elected frame (output)
 * @return frame with largest distance from animation
 */
unsigned int CurveSimplification::getMostDissimilarFrameFromAnimation(Animation & anim, double & error) {
	Skeleton sk, isk;
	double dist;
	unsigned int ret = rd->getNumberOfFrames();

	error = 0;
	for (unsigned int i = 0; i < rd->getNumberOfFrames(); ++i) {
		sk = rd->getFrameSkeleton(i);
		anim.setController(i);
		anim.animate();
		isk = anim.getSkeleton();
		dist = sahelper->getPoseDistance(sk, isk);
		//std::cout << i << "\t" << dist << std::endl;
		if (dist > error) {
			error = dist;
			ret = i;
		}
	}
	return ret;	
}

/**
 * @brief return an ordered array of frame indices as a sugestion for keyframe interpolation. Parameters
 * for division are maximum number of subdivisions or minimum error, the first which applies.
 * @param vsize maximum number of subdivisions
 * @param eer minimum expected error, ignored if not strictly positive
 * @return vector with keyframe indices
 */
std::vector<unsigned int> CurveSimplification::getSubdivisionVectorLocally(unsigned int vsize, double eer) {
	std::vector<unsigned int> ret, frameheap;
	std::vector<double> interrheap, kferr;
	std::vector<bool> pickheap;
	double err;
	unsigned int ld, l0 = 0;
	unsigned int l1 = rd->getNumberOfFrames() - 1;

	// add extremities
	ret.push_back(l0);
	ret.push_back(l1);
	kferr.push_back(0);
	kferr.push_back(0);
	// make new subdivision and put in buffer
	ld = getMostDissimilarFrameFromInterp(l0, l1, err);
	frameheap.push_back(ld);
	interrheap.push_back(err);
	pickheap.push_back(false);
	while (vsize > 0) {
		// get divisor with largest error
		err = 0;
		ld = 0;
		kferr.push_back(err);
		for (unsigned int i=0; i < frameheap.size(); ++i) {
			if ((interrheap.at(i) > err) && (!pickheap.at(i))) {
				err = interrheap.at(i);
				ld = i;
				kferr.back() = err;
			}
		}
		pickheap.at(ld) = true;
		// add to output vector keeping it ordered
		ret.push_back(frameheap.at(ld));
		for (unsigned int i = ret.size() - 1; i > 0; --i) {
			if (ret.at(i) < ret.at(i-1)) {
				unsigned int aux = ret.at(i);
				ret.at(i) = ret.at(i-1);
				ret.at(i-1) = aux;
				double tmp = kferr.at(i);
				kferr.at(i) = kferr.at(i-1);
				kferr.at(i-1) = tmp;
			}
		}
		if ((eer > 0) && (interrheap.at(ld) <= eer)) vsize = 0; 
		else --vsize;
		//std::cout << "KF vector =";
		//for (unsigned int i=0; i < ret.size(); ++i) std::cout << " " << ret.at(i);
		//std::cout << std::endl;
		// find next subdivision boundaries
		for (unsigned int i=0; i < ret.size(); ++i) {
			if (frameheap.at(ld) == ret.at(i)) {
				l0 = ret.at(i-1);
				l1 = ret.at(i+1);
				break;
			}
		}
		// make new subdivision and put in buffer
		//std::cout << "New divisors are " << l0 << ", " << frameheap.at(ld) << " and " << l1 << std::endl;
		l0 = getMostDissimilarFrameFromInterp(l0, frameheap.at(ld), err);
		frameheap.push_back(l0);
		interrheap.push_back(err);
		pickheap.push_back(false);
		l1 = getMostDissimilarFrameFromInterp(frameheap.at(ld), l1, err);
		frameheap.push_back(l1);
		interrheap.push_back(err);
		pickheap.push_back(false);
	}
	std::cout << "KF error =";
	for (unsigned int i=0; i < kferr.size(); ++i) std::cout << " " << kferr.at(i);
	std::cout << std::endl;
	return ret;
}

/**
 * @brief return an ordered array of frame indices as a sugestion for keyframe interpolation. Parameters
 * for division are maximum number of subdivisions or minimum error, the first which applies.
 * @param vsize maximum number of subdivisions
 * @param eer minimum expected error, ignored if not strictly positive
 * @return vector with keyframe indices
 */
std::vector<unsigned int> CurveSimplification::getSubdivisionVectorGlobally(unsigned int vsize, double eer) {
	Animation anim;
	Skeleton s0, s1;
	unsigned int frid;
	double error;
	std::vector<unsigned int> ret;

	frid = 0;
	ret.push_back(frid);
	s0 = rd->getFrameSkeleton(frid);
	anim.setSkeleton(s0);
	anim.addNewKeyFrame(frid, s0);
	frid = rd->getNumberOfFrames() - 1;
	ret.push_back(frid);
	while (vsize != 0) {
		--vsize;
		s1 = rd->getFrameSkeleton(frid);
		anim.addNewKeyFrame(frid, s1);
		anim.setContext();
		frid = getMostDissimilarFrameFromAnimation(anim, error);
		ret.push_back(frid);
		if ((eer != 0) && (error < eer)) break;
	}
	std::sort(ret.begin(), ret.end());
	return ret;
}

/**
 * @brief return the pose distance between keyframe interpolation and current mocap session
 * @param kframes array of keyframes
 * @param global flag to activate global interpolation
 * @return array of pose distances
 */
std::vector<double> CurveSimplification::getSubdivisionDistance(std::vector<unsigned int> const & kframes, 
	bool global) {
	Animation anim;
	Skeleton s0, s1;
	unsigned int frid;
	double err;
	std::vector<double> ret;
	
	if (global) {
		for (unsigned int i=0; i < kframes.size(); ++i) {
			frid = kframes.at(i);
			s0 = rd->getFrameSkeleton(frid);
			anim.setSkeleton(s0);
			anim.addNewKeyFrame(frid, s0);
		}
		anim.setContext();
		for (unsigned int i=0; i < rd->getNumberOfFrames(); ++i) {
			s0 = rd->getFrameSkeleton(i);
			anim.setController(i);
			anim.animate();
			s1 = anim.getSkeleton();
			err = sahelper->getPoseDistance(s0, s1);
			ret.push_back(err);
		}
		
	} else {
		for (unsigned int i=1; i < kframes.size(); ++i) {
			frid = kframes.at(i);
			s0 = rd->getFrameSkeleton(frid);
			anim.setSkeleton(s0);
			anim.addNewKeyFrame(frid, s0);
			frid = kframes.at(i - 1);
			s0 = rd->getFrameSkeleton(frid);
			anim.setSkeleton(s0);
			anim.addNewKeyFrame(frid, s0);
			anim.setContext();
			for (unsigned int j=kframes.at(i - 1); j < kframes.at(i); ++j) {
				s0 = rd->getFrameSkeleton(j);
				anim.setController(j);
				anim.animate();
				s1 = anim.getSkeleton();
				err = sahelper->getPoseDistance(s0, s1);
				ret.push_back(err);			
			}			
			anim.clearAnimation();
		}
		ret.push_back(0);	
	}
	
	return ret;
}

/**
 * @brief compares and returns the frame id of an animation handler interval which best 
 * fits a given skeleton pose.
 * @param anim animation handler
 * @param sk skeleton pose
 * @param f0 frame id of interval begin (inclusive)
 * @param f1 frame id of interval end (inclusive)
 * @return frame id with best fit to skeleton pose
 */
unsigned int CurveSimplification::getBestFitFrameId(Animation & anim, 
	Skeleton & sk, unsigned int f0, unsigned int f1) {
	
	unsigned int ret = f0;
	Skeleton s1;
	double err, minerr;

	anim.setController(f0);
	anim.animate();
	s1 = anim.getSkeleton();
	minerr = sahelper->getPoseDistance(sk, s1);
	for (unsigned int i = f0 + 1; i <= f1; ++i) {
		anim.setController(i);
		anim.animate();
		s1 = anim.getSkeleton();
		err = sahelper->getPoseDistance(sk, s1);
		if (minerr > err) {
			ret = i;
			minerr = err;
		}
	}
	return ret;
}

/**
 * @brief return a best fit parameterization between keyframe interpolation and current mocap session poses
 * @param kframes array of keyframes
 * @param global flag to activate global interpolation
 * @return array of frame ids (poses)
 */
std::vector<unsigned int> CurveSimplification::getSubdivisionParameterization(
			std::vector<unsigned int> const & kframes, bool global) {
	
	Animation anim;
	Skeleton s0;
	unsigned int frid, fref;
	std::vector<unsigned int> ret;
	
	if (global) {
		// build animation
		for (unsigned int i=0; i < kframes.size(); ++i) {
			frid = kframes.at(i);
			s0 = rd->getFrameSkeleton(frid);
			anim.setSkeleton(s0);
			anim.addNewKeyFrame(frid, s0);
		}
		anim.setContext();
		// look for best fit
		fref = 1;
		for (unsigned int i=0; i < rd->getNumberOfFrames(); ++i) {
			if (i > kframes.at(fref)) ++fref;
			s0 = rd->getFrameSkeleton(i);
			frid = getBestFitFrameId(anim, s0, kframes.at(fref - 1), kframes.at(fref));
			ret.push_back(frid);
		}
		
	} else {
		for (unsigned int i=1; i < kframes.size(); ++i) {
			// build animation
			frid = kframes.at(i);
			s0 = rd->getFrameSkeleton(frid);
			anim.setSkeleton(s0);
			anim.addNewKeyFrame(frid, s0);
			frid = kframes.at(i - 1);
			s0 = rd->getFrameSkeleton(frid);
			anim.setSkeleton(s0);
			anim.addNewKeyFrame(frid, s0);
			anim.setContext();
			// look for best fit
			fref = i;
			for (unsigned int j=kframes.at(i - 1); j < kframes.at(i); ++j) {
				s0 = rd->getFrameSkeleton(j);
				frid = getBestFitFrameId(anim, s0, kframes.at(fref - 1), kframes.at(fref));
				ret.push_back(frid);
			}			
			anim.clearAnimation();
		}
		ret.push_back(kframes.back());
	}
	
	return ret;
}

/**
 * @brief get a 2D projection of many skeletons by successively approximating
 * a random sample to the skeleton distance distribution.
 * @param fref original skeleton frame reference
 * @param train number of approximation iterations
 * @param stepsize fraction of delta approximation
 * @return projected skeleton into 2D
 */
Eigen::MatrixXd CurveSimplification::getForceApproachProjection(std::vector<unsigned int> const & fref, 
	unsigned int train, double stepsize) {
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(fref.size(), 3);
	Eigen::MatrixXd ospdist = Eigen::ArrayXXd::Zero(fref.size(), fref.size());
	Eigen::VectorXd v;
	Skeleton sk0, sk1;
	double delta, min0, max0 = RAND_MAX;
	double min1, max1, x0, x1;
	
	// initialize 2D data with random vectors
	assert(fref.size() > 2);
	std::srand(time(NULL));		
	for (unsigned int i=0; i < ret.rows(); ++i) {
		do {
			x0 = (double) std::rand() / max0;
		} while (x0 == 0);
		x1 = (double) std::rand() / max0;
		ret(i,0) = std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		//ret(i,0) = std::rand() / max0;
		do {
			x0 = (double) std::rand() / max0;
		} while (x0 == 0);
		x1 = (double) std::rand() / max0;
		ret(i,1) = std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		//ret(i,1) = std::rand() / max0;
		ret(i,2) = 0;
	}
	// discover skeleton space distances and normalize it
	sk0 = rd->getFrameSkeleton(fref.at(0));
	sk1 = rd->getFrameSkeleton(fref.at(1));
	min0 = max0 = std::sqrt(sahelper->getPoseDistance(sk0, sk1));
	for (unsigned int i=0; i < ospdist.rows(); ++i)
		for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
			sk0 = rd->getFrameSkeleton(fref.at(i));
			sk1 = rd->getFrameSkeleton(fref.at(j));
			ospdist(i,j) = std::sqrt(sahelper->getPoseDistance(sk0, sk1));
			//ospdist(j,i) = ospdist(i,j);
			if (ospdist(i,j) > max0) max0 = ospdist(i,j);
			else if (ospdist(i,j) < min0) min0 = ospdist(i,j);
		}
	for (unsigned int i=0; i < ospdist.rows(); ++i)
		for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
			ospdist(i,j) = (ospdist(i,j) - min0) / (max0 - min0);
			ospdist(j,i) = ospdist(i,j);
			
		}
	
	// training session
	for (unsigned int k=0; k < train; ++k) {
		// discover delta and update
		for (unsigned int i=0; i < ospdist.rows(); ++i)
			for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
				v = ret.row(i) - ret.row(j);
				delta = stepsize * (ospdist(i,j) - std::sqrt(v.dot(v)));
				v *= delta;
				ret.row(i) += v;
			}
		// normalize projection coordinates
		min0 = max0 = ret(0,0);
		min1 = max1 = ret(0,1);
		for (unsigned int i=1; i < ret.rows(); ++i) {
			if (min0 > ret(i,0)) min0 = ret(i,0);
			else if (max0 < ret(i,0)) max0 = ret(i,0);
			if (min1 > ret(i,1)) min1 = ret(i,1);
			else if (max1 < ret(i,1)) max1 = ret(i,1);
		}
		for (unsigned int i=0; i < ret.rows(); ++i) {
			ret(i,0) = (ret(i,0) - min0) / (max0 - min0);
			ret(i,1) = (ret(i,1) - min1) / (max1 - min1);
		}
	}
	// put reference frame at last column
	for (unsigned int i=0; i < fref.size(); ++i) 
		ret(i,2) = (double) fref.at(i);
	return ret;
}

/**
 * @brief show for each frame the most distant pose inside the motion capture session
 * @param rd motion capture session
 */
void CurveSimplification::discoverAntagonisticPoses() {
	std::vector<Skeleton> pose;
	Skeleton s;

	// read poses
	for (unsigned int i=0; i < rd->getNumberOfFrames(); ++i) {
		s = rd->getFrameSkeleton(i);
		pose.push_back(s);
	}
	// discover antagonistic
	for (unsigned int i=0; i < pose.size(); ++i) {
		double tmp, err = 0;
		unsigned int anti = 0;
		for (unsigned int j=0; j < pose.size(); ++j) {
			if (i != j) {
				tmp = sahelper->getPoseDistance(pose.at(i), pose.at(j));
				if (tmp > err) {
					err = tmp;
					anti = j;
				}
			}
		}
		std::cout << "Pose " << i << ": antipose " << anti << std::endl;
	}
}

/**
 * @brief find best fit between mocap frames and linear interpolation domain
 */
void CurveSimplification::printInterpBestFit(unsigned int f0, unsigned int f1) {
	std::vector<Skeleton> interp = getInterpolatedFrames(f0, f1);
	std::vector<Skeleton> pose;
	std::vector<unsigned int> bfit;
	Skeleton s; 
	double tmp, min;

	// get all poses
	for (unsigned int i = f0; i <= f1; ++i) {
		s = rd->getFrameSkeleton(i);
		pose.push_back(s);
	}
	// get best fit
	assert(pose.size() == interp.size());
	bfit.resize(pose.size());
	for (unsigned int i = 0; i < pose.size(); ++i) {
		min = sahelper->getPoseDistance(pose.at(i), interp.at(i));
		bfit.at(i) = i + f0;
		for (unsigned int j = 0; j < interp.size(); ++j) {
			tmp = sahelper->getPoseDistance(pose.at(i), interp.at(j));
			if (min > tmp) {
				min = tmp;
				bfit.at(i) = j + f0;
			}
		}
	}
	std::cout << "Best fit" << std::endl;
	for (unsigned int i=0; i < bfit.size(); ++i) {
		std::cout << (i + f0) << "\t" << bfit.at(i) << std::endl;
	}

	// get least squares cubic parameter
	Eigen::MatrixXf xmat = Eigen::ArrayXXf::Zero(bfit.size(), 4);
	Eigen::VectorXf yvet = Eigen::ArrayXf::Zero(bfit.size());
	for (unsigned int i=0; i < bfit.size(); ++i) {
		yvet(i) = bfit.at(i);
		//xmat(i, 0) = (i + f0)*(i + f0)*(i + f0)*(i + f0);
		xmat(i, 0) = (i + f0)*(i + f0)*(i + f0);
		xmat(i, 1) = (i + f0)*(i + f0);
		xmat(i, 2) = i + f0;
		xmat(i, 3) = 1;
	}
	Eigen::MatrixXf xlxmat = xmat.transpose() * xmat;
	Eigen::VectorXf beta = xlxmat.inverse() * xmat.transpose() * yvet;
	std::cout << "least squares polynomial = " << beta.transpose() << std::endl;
}

int main(int argc, char ** argv) {
	MocapReader *reader = NULL;
	EuclideanSkeletonAnalizer esa;
	//QuaternionSkeletonAnalizer esa;
	SkeletonAnalizer * sa = &esa;
	CurveSimplification cs(sa);
	
	if ((argc < 2) || (argc > 5)) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	if (ext.compare("asf") == 0) {
		reader = new ASFReader();
		reader->readFile(fname);
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				reader->readFile(fname);				
			}
		}
	} else if (ext.compare("bvh") == 0) {
		reader = new BVHReader();
		reader->readFile(fname);
	}
	cs.setMocapSession(reader);
	
	unsigned int nframes = reader->getNumberOfFrames() / 20;
	std::vector<unsigned int> sub = cs.getSubdivisionVectorGlobally(nframes, 0);
	//std::cout << "subdivision is";
	for (unsigned int i=0; i < sub.size(); ++i) std::cout << " " << sub.at(i);
	std::cout << std::endl;
	
	std::cout << "skeleton space 2d projection" << std::endl;
	Eigen::MatrixXd pose2d = cs.getForceApproachProjection(sub);
	std::cout << pose2d << std::endl;
	
	//std::vector<double> error = cs.getSubdivisionDistance(sub, true);
	//double serr = 0;
	//for (unsigned int i=0; i < error.size(); ++i) serr += error.at(i);
	//std::cout << "total error = " << serr << std::endl;

	//std::vector<unsigned int> param = cs.getSubdivisionParameterization(sub, true);
	//std::cout << "error and parameterization" << std::endl;
	//for (unsigned int i=0; i < param.size(); ++i) 
	//	std::cout << i << "\t" << error.at(i) << "\t" << param.at(i) << std::endl;
	
	delete reader;
	return 0;
}
