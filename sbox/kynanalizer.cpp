#include <kynanalizer.h>
#include <asfreader.h>
#include <bvhreader.h>
//#include <matstat.h>
#include <iostream>
//#include <algorithm>
//#include <libgen.h>
//#include <cstdlib>

/**
 * @brief calculate and return a joint velocity for each frame
 * @param motion capture session
 * @return matrix of joint speed for each frame, except first. Frames are rows and joints are columns.
 */
Eigen::MatrixXd MocapKynematicAnalizer::getVelocityNaive() {
	Eigen::MatrixXd vel;
	std::vector<Skeleton> pose;
	Skeleton s0, s1;
	Eigen::Vector4d v;
	double frate = 1000.0 / rd->getFrameInterval();
	unsigned int nframes, njoints;

	nframes = rd->getNumberOfFrames();
	for (unsigned int i=0; i < nframes; ++i) {
		s0 = rd->getFrameSkeleton(i);
		pose.push_back(s0);
	}
	s0 = pose.at(0);
	njoints = s0.getJointMatrices().size();
	vel = Eigen::ArrayXXd::Zero(nframes - 1, njoints);
	for (unsigned int i=1; i < pose.size(); ++i) {
		s1 = pose.at(i);
		for (unsigned int j=0; j < njoints; ++j) {
			v = frate * (s1.getJointPosition(j) - s0.getJointPosition(j));
			vel(i - 1, j) = std::sqrt(v.dot(v));
		}
		s0 = s1;
	}
	return vel;
}

/**
 * @brief calculate and return a joint velocity for each frame
 * @return matrix of joint speed for each frame, except first and last. Frames are rows and joints are columns.
 */
Eigen::MatrixXd MocapKynematicAnalizer::getVelocity() { 
	Eigen::MatrixXd vel;
	std::vector<Skeleton> pose;
	Skeleton s0, s1;
	Eigen::Vector4d v;
	double frate = 1000.0 / rd->getFrameInterval();
	unsigned int nframes, njoints;

	nframes = rd->getNumberOfFrames();
	for (unsigned int i=0; i < nframes; ++i) {
		s0 = rd->getFrameSkeleton(i);
		pose.push_back(s0);
	}
	s0 = pose.at(0);
	njoints = s0.getJointMatrices().size();
	vel = Eigen::ArrayXXd::Zero(nframes - 2, njoints);
	for (unsigned int i=2; i < pose.size(); ++i) {
		s1 = pose.at(i);
		for (unsigned int j=0; j < njoints; ++j) {
			v = 0.5 * frate * (s1.getJointPosition(j) - s0.getJointPosition(j));
			vel(i - 2, j) = std::sqrt(v.dot(v));
		}
		s0 = s1;
	}
	return vel;
}

/**
 * @brief calculate and return the joint acceleration for each frame
 * @return matrix ofd joint speed for each frame, except first and last. Frames are rows and joints are columns.
 */
Eigen::MatrixXd MocapKynematicAnalizer::getAcceleration() { 
	Eigen::MatrixXd acc;
	std::vector<Skeleton> pose;
	Skeleton s0, s1, s2;
	Eigen::Vector4d v;
	double frate = 1000.0 / rd->getFrameInterval();
	unsigned int nframes, njoints;

	nframes = rd->getNumberOfFrames();
	for (unsigned int i=0; i < nframes; ++i) {
		s0 = rd->getFrameSkeleton(i);
		pose.push_back(s0);
	}
	assert(pose.size() > 2);
	s0 = pose.at(0);
	s1 = pose.at(1);
	njoints = s0.getJointMatrices().size();
	acc = Eigen::ArrayXXd::Zero(nframes - 2, njoints);
	for (unsigned int i=2; i < pose.size(); ++i) {
		s2 = pose.at(i);
		for (unsigned int j=0; j < njoints; ++j) {
			v = frate * frate * (s2.getJointPosition(j) - 2 * s1.getJointPosition(j) + s0.getJointPosition(j));
			acc(i - 2, j) = std::sqrt(v.dot(v));
		}
		s0 = s1;
		s1 = s2;
	}
	return acc;
}

/**
 * @brief calculate and return the curvature of each frame
 * @return matrix of frame's curvature, except first and last. Frames are rows and joints are columns.
 */
Eigen::MatrixXd MocapKynematicAnalizer::getCurvature() {
	Eigen::MatrixXd curv;
	std::vector<Skeleton> pose;
	Skeleton s0, s1, s2;
	Eigen::Vector4d v, a, t;
	double frate = 1000.0 / rd->getFrameInterval();
	unsigned int nframes, njoints;

	// get all poses
	nframes = rd->getNumberOfFrames();
	for (unsigned int i=0; i < nframes; ++i) {
		s0 = rd->getFrameSkeleton(i);
		pose.push_back(s0);
	}
	// build velocity, acceleration and curvature vector
	assert(pose.size() > 2);
	s0 = pose.at(0);
	s1 = pose.at(1);
	njoints = s0.getJointMatrices().size();
	curv = Eigen::ArrayXXd::Zero(nframes - 2, njoints);
	for (unsigned int i=2; i < pose.size(); ++i) {
		s2 = pose.at(i);
		for (unsigned int j=0; j < njoints; ++j) {
			v = 0.5 * frate * (s2.getJointPosition(j) - s0.getJointPosition(j));
			a = frate * frate * (s2.getJointPosition(j) - 2 * s1.getJointPosition(j) + s0.getJointPosition(j));
			t = a.cross3(v);
			curv(i - 2, j) = std::sqrt(t.dot(t)) * std::pow(1 / v.dot(v), 1.5);
		}
		s0 = s1;
		s1 = s2;
	}
	return curv;
}

/**
 * @brief calculate and return the jerk of each frame (how the acceleration changes)
 * @return matrix of frame's jerkness, except two first and two last. Frames are rows and joints are columns.
 */
Eigen::MatrixXd MocapKynematicAnalizer::getJerkness() {
	Eigen::MatrixXd jerkness;
	std::vector<Skeleton> pose;
	Skeleton s0, s1, s3, s4;
	Eigen::Vector4d v;
	double frate = 1000.0 / rd->getFrameInterval();
	unsigned int nframes, njoints;

	nframes = rd->getNumberOfFrames();
	for (unsigned int i=0; i < nframes; ++i) {
		s0 = rd->getFrameSkeleton(i);
		pose.push_back(s0);
	}
	assert(pose.size() > 4);
	s0 = pose.at(0);
	s1 = pose.at(1);
	njoints = s0.getJointMatrices().size();
	jerkness = Eigen::ArrayXXd::Zero(nframes - 4, njoints);
	for (unsigned int i=2; i < pose.size() - 2; ++i) {
		s3 = pose.at(i + 1);
		s4 = pose.at(i + 2);
		for (unsigned int j=0; j < njoints; ++j) {
			v = 0.5 * frate * frate * frate * (s4.getJointPosition(j) - 2 * s3.getJointPosition(j) + 
					2 * s1.getJointPosition(j) - s0.getJointPosition(j));
			jerkness(i - 2, j) = std::sqrt(v.dot(v));
		}
		s0 = s1;
		s1 = pose.at(i);
	}
	return jerkness;
}

/**
 * @brief print Acceleration, Velocity, Curvature and Jerkness for stdout gnuplot analysis
 * @param jid joint id to be tracked
 */
void MocapKynematicAnalizer::printKynematicsDescriptors(unsigned int jid) {
	Eigen::MatrixXd vel = getVelocity();
	Eigen::MatrixXd acc = getAcceleration();
	Eigen::MatrixXd curv = getCurvature();
	Eigen::MatrixXd jerk = getJerkness();
	unsigned int i = 0;

	std::cout << "Frame id\tVelocity\tAcceleration\tCurvature\tJerkness" << std::endl;
	std::cout << (i+1) << "\t" << vel(i, jid) << "\t" << acc(i, jid) << "\t" 
		<< curv(i, jid) << "\t0" << std::endl;
	for (; i < jerk.rows(); ++i) 
		std::cout << (i+2) << "\t" << vel(i + 1, jid) << "\t" << acc(i + 1, jid) << "\t" 
			<< curv(i + 1, jid) << "\t" << jerk(i, jid) << std::endl;
	std::cout << (i+2) << "\t" << vel(i + 1, jid) << "\t" << acc(i + 1, jid) << "\t" 
		<< curv(i + 1, jid) << "\t0" << std::endl;
}

int main(int argc, char ** argv) {
	MocapReader *reader = NULL;
	MocapKynematicAnalizer mka;
	
	if ((argc < 2) || (argc > 5)) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	if (ext.compare("asf") == 0) {
		reader = new ASFReader();
		reader->readFile(fname);
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				reader->readFile(fname);				
			}
		}
	} else if (ext.compare("bvh") == 0) {
		reader = new BVHReader();
		reader->readFile(fname);
	}
	mka.setMocapSession(reader);
	mka.printKynematicsDescriptors(1);
	if (reader) delete reader;
	return 0;
}
