#ifndef MCSESSION_H
#define MCSESSION_H

#include <mocapreader.h>

/**
 * @class
 * @brief
 */
class MocapSession {
	protected:
		MocapReader * rd;
	
	public:
		/**
		 * @brief
		 * @param reader
		 */
		void setMocapSession(MocapReader * reader) {
			rd = reader;
		}

		/**
		 * @brief
		 * @param reader
		 */
		MocapSession(MocapReader * reader = NULL) {
			setMocapSession(reader);
		}

		/**
		 * @brief
		 */
		~MocapSession() {}
};

#endif
