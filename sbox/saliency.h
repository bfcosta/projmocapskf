#ifndef SALIENCY_H
#define SALIENCY_H

#include <Eigen/Dense>

/**
 * @class Saliency
 * @brief Useful tools for curvature and saliency calculations
 * 
 * This class provides useful methods for motion saliency calculation.
 * Motion saliency is defined as the data curvature, basically a function
 * given by its first and second derivative against time. For generality,
 * data is displayed in matrix format where each row is a time measure and
 * each column is one data dimension.
 * 
 * @see http://mathworld.wolfram.com/Curvature.html
 */
class Saliency {
	public:
		Saliency() {}
		~Saliency() {}
		Eigen::MatrixXd getCurvatureMatrix(Eigen::MatrixXd & mat);
		Eigen::MatrixXd getNonlinearNormalizationMatrix(Eigen::MatrixXd & mat);
		Eigen::VectorXi getMaxSaliency(Eigen::MatrixXd & smat);
		Eigen::MatrixXd getGaussianWeightedAverageMatrix(Eigen::MatrixXd & mat, 
			double sigma);
		int getKernelSize(double sigma, double threshold);
};

#endif
