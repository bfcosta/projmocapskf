#ifndef MOGRAPH_H
#define MOGRAPH_H

#include <Eigen/Dense>
#include <vector>
#include <skeleton.h>

class Mograph {
	protected:
		std::vector<Skeleton> seq0;
		std::vector<Skeleton> seq1;
		std::vector<double> weight;
		std::vector<Eigen::Vector2i> minima;
		Eigen::MatrixXd dist;

		Eigen::MatrixXd convolve(Eigen::MatrixXd const & srcmat, Eigen::VectorXd const & kernel);
		double evalGaussFunc(double mu, double sigma, double x);
		Eigen::VectorXd getGaussVector(unsigned int size);
		std::vector<double> getHierarchyWeigths(std::vector<unsigned int> const & par);
		double getPoseDistance(const Skeleton & s0, const Skeleton & s1);
		void setWeights(std::vector<double> const & w);
		std::vector<Eigen::Vector2i> findLocalMinima(Eigen::MatrixXd const & mat);

	public:
		Mograph(const std::vector<Skeleton> & s0, const std::vector<Skeleton> & s1);
		void calculateErrorMatrix();
		void printLocalMinima(double thres = 0);
};

#endif
