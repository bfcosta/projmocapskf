#ifndef CURVESIMPL_H
#define CURVESIMPL_H

#include <Eigen/Dense>
#include <vector>
#include <mcsession.h>
#include <skanalizer.h>
#include <animation.h>

/**
 * @class
 * @brief
 */
class CurveSimplification: public MocapSession {
	protected:
		SkeletonAnalizer *sahelper;
		std::vector<Skeleton> getInterpolatedFrames(unsigned int f0, unsigned int f1);
		unsigned int getMostDissimilarFrameFromExtremes(unsigned int f0, unsigned int f1, double & error);
		unsigned int getMostDissimilarFrameFromInterp(unsigned int f0, unsigned int f1, double & error);
		unsigned int getMostDissimilarFrameFromAnimation(Animation & anim, double & error);
		unsigned int getBestFitFrameId(Animation & anim, Skeleton & sk, unsigned int f0, unsigned int f1);

	public:
		/**
		 * @brief
		 * @param sa
		 * @param r
		 */
		CurveSimplification(SkeletonAnalizer * sa, MocapReader * r = NULL): MocapSession(r) {
			sahelper = sa;
		}

		/**
		 * @brief
		 */
		~CurveSimplification() {}
		double getPoseDistance(unsigned int fid0, unsigned int fid1);
		std::vector<unsigned int> getSubdivisionVectorLocally(unsigned int vsize, double eer);
		std::vector<unsigned int> getSubdivisionVectorGlobally(unsigned int vsize, double eer);
		std::vector<double> getSubdivisionDistance(std::vector<unsigned int> const & kframes, bool global);
		std::vector<unsigned int> getSubdivisionParameterization(
			std::vector<unsigned int> const & kframes, bool global);
		void discoverAntagonisticPoses();
		Eigen::MatrixXd getForceApproachProjection(std::vector<unsigned int> const & fref, 
			unsigned int train = 50, double stepsize = 0.125);
		void printInterpBestFit(unsigned int f0, unsigned int f1);
};

#endif
