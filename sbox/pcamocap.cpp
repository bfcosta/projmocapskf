#include <bvhreader.h>
#include <asfreader.h>
#include <skeleton.h>
#include <iostream>
#include <libgen.h>
#include <cstdlib>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <renderhelper.h>
#include <pca.h>

Skeleton *mcskel, *anskel;
MocapReader *mocap;
Pca *pca;
Eigen::MatrixXd mcdata, pcadata;
bool play, mcshow, anshow;
int height;
Eigen::Vector3d eye, center, up;
int readfr;
unsigned int nf, fi;
double zn,zf;

/**
 * \param value frame id to be rendered
 * 
 * loads given frame id into the rendered skeleton
 */	
void getFrame(int value) {
	Skeleton fs = mocap->getFrameSkeleton(mcdata.row(value));

	mcskel->setSkeleton(fs);
	fs = mocap->getFrameSkeleton(pcadata.row(value));
	anskel->setSkeleton(fs);
	glutPostRedisplay();
    if (play) {
        readfr = value;
        //std::cout << "frame = " << readfr << std::endl;
        glutTimerFunc(fi, getFrame, (readfr + 1) % nf);
    } else glutTimerFunc(fi, getFrame, readfr % nf);
	readfr = value;
}

/**
 * glut reshape handling
 */	
void reshape(int w, int h) {
    height = h;
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glViewport(0, 0, w, h); 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective (45.0, ((float) w)/ h, zn, zf);
    gluLookAt(eye(0), eye(1), eye(2), 
		center(0), center(1), center(2), 
		up(0), up(1), up(2));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

/**
 * glut display handling
 */	
void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//drawGround(100);
	drawAxis();
	if ((mcskel != NULL) && (mcshow)) mcskel->draw(GL_RENDER);
	if ((anskel != NULL) && (anshow)) anskel->draw(GL_RENDER);
	glutSwapBuffers();
}

/**
 * \param key ascii value for the pressed key
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * glut keyboard handler
 */	
void keyboard(unsigned char key, int x, int y) {
	
	switch (key) {
		case 27: // esc
			exit(0);
			break;
        case 49:
            mcshow = !mcshow;
            break;
        case 50:
            anshow = !anshow;
            break;
		case 80:
		case 112: // P ou p
            std::cout << "play" << std::endl;
            play = true;
            break;
		default:
            play = false;
            std::cout << "stop" << std::endl;
	}	
}

void handleArrowKeys(int key, int x, int y) {
    switch(key) {
        case GLUT_KEY_UP:
        case GLUT_KEY_RIGHT:
            play = false;
            readfr = (readfr + 1) % nf;
            std::cout << "frame = " << readfr << std::endl;
            break;
        case GLUT_KEY_DOWN:
        case GLUT_KEY_LEFT:
            play = false;
            if (readfr == 0) readfr = nf - 1;
            else readfr = (readfr - 1) % nf;
            std::cout << "frame = " << readfr << std::endl;
            break;
        default:
        break;
    }
}

int main(int argc, char ** argv) {
	Skeleton s0, s1;

	if (argc < 2) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	if (ext.compare("bvh") == 0) {
		mocap = new BVHReader();
		mocap->readFile(fname);
		mcdata = mocap->getEulerAnglesDOFMatrix();
		pca = new Pca(mcdata);
		pca->doAnalysis();
		pcadata = pca->getPcaRebuiltData();
		s0 = mocap->getFrameSkeleton(mcdata.row(0));
		s1 = mocap->getFrameSkeleton(pcadata.row(0));
		//s = mocap->getFrameSkeleton(0);
		
	} else if (ext.compare("asf") == 0) {
		mocap = new ASFReader();
		mocap->readFile(fname);
		s0 = mocap->getSkeleton();
		s1 = mocap->getSkeleton();
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				mocap->readFile(fname);
				mcdata = mocap->getEulerAnglesDOFMatrix();
				pca = new Pca(mcdata);
				pca->doAnalysis();
				pcadata = pca->getPcaRebuiltData();
				s0 = mocap->getFrameSkeleton(mcdata.row(0));
				s1 = mocap->getFrameSkeleton(pcadata.row(0));
				//s = mocap->getFrameSkeleton(0);
			}
		}
	}
	mcskel = &s0;
	s1.setColor(0,1,1);
	anskel = &s1;
	Eigen::MatrixXd dofs = mocap->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd skpos = dofs.block(0, 0, dofs.rows(), 3);
	setMocapPerspective(skpos);
	//setDefaultPerspective(mcskel->getCenter());
	play = false;
    mcshow = true;
    anshow = true;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(400, 400);
	glutCreateWindow("Animation stub");
	readfr = 0;
	nf = mocap->getNumberOfFrames();
	fi = mocap->getFrameInterval();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(handleArrowKeys);
	glutTimerFunc(fi, getFrame, readfr % nf);
    glutMainLoop();
    delete mocap;
    delete pca;
	return 0;
}
