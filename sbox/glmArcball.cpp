#include <glmArcball.h>
#include <iostream>
//#include <glm/gtx/string_cast.hpp>
#include <GL/gl.h>
#include <GL/glu.h>
/*
	get arcball vector from screen coords (I - C).

	C = center
	E = camera eye
	I = intersection on sphere or plane
	P = projected point of I on near plane
	R = sphere radius
	T = tangent's circle center

	sphere:  (I - C)^2 = R^2
	normal plane on sphere center:   I*(C - E) = - (C - E)*C
	normal plane on sphere tangent's circle: I*(T - E) = - (T - E)*T
	where I = E + (P - E)*t
*/
glm::dvec3 glmArcball::getABVec(int xs, int ys) {
	double a,b,c,delta,t;
	glm::dvec3 pp, pshere;

	gluUnProject(xs , ys, 0, &mview[0][0], &pm[0][0], &vp[0], &pp.x, &pp.y, &pp.z);
	a = glm::dot(pp - eye, pp - eye); 
	b = glm::dot(pp - eye, eye - center); // precisely b = 2*(p - eye)*(eye - center)
	c = ceye_radius2 - radius * radius; // precisely c = (eye - center)*(eye - center) - radius ^ 2
	delta = b*b - a*c; // precisely delta = 4*(b*b - a*c)
	if (delta <= 0) {
		// third try: mix of both earlier strategies,
		// get nearest tangent on sphere at tangents sphere's plane
		t = teye_radius2 / glm::dot(pp - eye, tcenter - eye);
		c = radius * std::sqrt(1 - radius * radius / ceye_radius2); // scale factor to hit tangent's circle
		pshere = tcenter + glm::normalize(eye + (pp - eye)*t - tcenter) * c;
	} else {
		// get the minimum, as a>0 ...
		t = (0 - b - std::sqrt(delta)) / a;
		pshere = eye + (pp - eye)*t;
	}
	return glm::normalize(pshere - center);
}

/*
	default constructor for floating point
*/
glmArcball::glmArcball(glm::vec3 const & sc, glm::vec3 const & e, float sr) {

	center = glm::dvec3(sc.x, sc.y, sc.z);
	eye = glm::dvec3(e.x, e.y, e.z);
	radius = (double) sr;
	ceye_radius2 = glm::dot(eye - center, eye - center);
	tcenter = center - (center - eye) * radius * radius / ceye_radius2;
	teye_radius2 = glm::dot(tcenter - eye, tcenter - eye);
	trto = glm::translate(glm::dmat4(1.0), center * (-1.0));
	trout = glm::translate(glm::dmat4(1.0), center);
}

/*
	default constructor for double precision
*/
glmArcball::glmArcball(glm::dvec3 const & sc, glm::dvec3 const & e, double sr) :
	center(sc), eye(e), radius(sr) {

	ceye_radius2 = glm::dot(eye - center, eye - center);
	tcenter = center - (center - eye) * radius * radius / ceye_radius2;
	teye_radius2 = glm::dot(tcenter - eye, tcenter - eye);
	trto = glm::translate(glm::dmat4(1.0), center * (-1.0));
	trout = glm::translate(glm::dmat4(1.0), center);
}

/*
	get initial vector and save rotation context
*/
void glmArcball::start(int xs, int ys) {

	msave =  mrot * msave;
	inivet = getABVec(xs, ys);
	//std::cout << "radius = " << radius << std::endl;
	//std::cout << "initvet = " << glm::to_string(inivet) << std::endl;
}

/*
	Compute rotation matrix for inital and current vectors.
*/
void glmArcball::move(int xs, int ys) {
	double cosa;
	glm::dvec3 cross;

	currvet = getABVec(xs, ys);
	if (!((currvet.x == inivet.x) && (currvet.y == inivet.y) && (currvet.z == inivet.z))) {
		cosa =  glm::dot(currvet, inivet);
		cross = glm::normalize(glm::cross(inivet, currvet));
#if GLM_VERSION >= 95
		mrot = glm::rotate(glm::dmat4(1.0), glm::acos(cosa), cross);
#else
		mrot = glm::rotate(glm::dmat4(1.0), glm::degrees(glm::acos(cosa)), cross);
#endif
		marcb = trout * mrot * msave * trto;
		//std::cout << "eye pos = " << glm::to_string(cross) << std::endl;
		//std::cout << "rot vec = " << glm::to_string(cross) << " angle = " << glm::acos(cosa) << std::endl;
	}
}

/*
	return current arcball modelview matrix
*/
double * glmArcball::getMultMatrixd() {
	return &marcb[0][0];
}

/*
	reset arcball calculus back to initial configuration
*/
void glmArcball::reset() {
	mrot = glm::dmat4(1.0);
	marcb = glm::dmat4(1.0);
	msave = glm::dmat4(1.0);
	glGetDoublev(GL_MODELVIEW_MATRIX, &mview[0][0]);
	glGetDoublev(GL_PROJECTION_MATRIX, &pm[0][0]);
	glGetIntegerv(GL_VIEWPORT, &vp[0]);
}

/*
	get point on world coordinates from (x,y,z) on screen coordinates 
	considering arcball transformations. wh stands for window height.
*/
glm::vec3 glmArcball::getProjectedPoint(int x, int y, int z, int wh) {
	glm::dvec3 pp;

	gluUnProject(x , wh - y - 1, z, &marcb[0][0], &pm[0][0], &vp[0], &pp.x, &pp.y, &pp.z);
	return glm::vec3((float) pp.x, (float) pp.y, (float) pp.z);
	//return getNewPointPosition(pp);
}

/*
	get new locaation from world coordinate point p considering
	arcball transformations
*/
glm::vec3 glmArcball::getNewPointPosition(glm::dvec3 const & p) {
	glm::dvec4 pn = glm::dvec4(p.x, p.y, p.z, 1.0) * marcb;
	pn = pn * (1 / pn.w);
	return glm::vec3((float) pn.x, (float) pn.y, (float) pn.z);
}

/*
	get camera position considering arcball transformations
*/
glm::vec3 glmArcball::getEyePosition() {
	glm::dvec4 p = glm::dvec4((double) eye.x, (double) eye.y, (double) eye.z, 1.0) * marcb;
	p = p * (1 / p.w);
	return glm::vec3((float) p.x, (float) p.y, (float) p.z);
}
