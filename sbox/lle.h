#ifndef LLE_H
#define LLE_H

#include <saliency.h>
#include <rbf.h>

/**
 * @class LLE
 * @brief Local Linear Embedding projection for motion capture
 * 
 * This class provides the Local Linear Embedding dimensionality reduction
 * technique for keyframe extracion from motion capture data. The keyframe
 * selection comes from the saliency of raw motion capture data, which is 
 * projected by LLE to a dimension reduced space where the search of keyframe
 * is made. The reduced space is projected back into the original space
 * dimension with RBF interpolation for calculating a defined error measure.
 * 
 * @see https://www.cs.nyu.edu/~roweis/lle/algorithm.html
 * @see http://onlinelibrary.wiley.com/doi/10.1002/cav.1471/abstract
 */
class LLE : public Saliency {
	protected:
		/// original data in matrix format.
		Eigen::MatrixXd dof;
		/// saliency map of original data
		Eigen::MatrixXd sdof;
		/// saliency's map LLE projection
		Eigen::MatrixXd llesp;

	public:
		LLE(Eigen::MatrixXd & mat) {
			dof = mat;
		}
		~LLE() {}
		Eigen::MatrixXd getDistanceMatrix(Eigen::MatrixXd & mat);
		Eigen::MatrixXd getLLEWeightMatrix(Eigen::MatrixXd & data, unsigned int knn);
		Eigen::MatrixXd getEmbeddingCoordinateMatrix(Eigen::MatrixXd & wmat, 
			unsigned int dim);
		RbfSimpleInterp getRbfKfInterpolation(Eigen::MatrixXd & kmat);
		Eigen::VectorXi getIKFSelection(Eigen::VectorXi & seedv, unsigned int itnum);
		Eigen::VectorXi getInitialKeyframesSet();
		Eigen::VectorXi getKnnIndices(Eigen::VectorXd const & dvec, unsigned int knn);
};

#endif
