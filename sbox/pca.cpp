#include <pca.h>
#include <vector>
#include <iostream>
//#include <ctime>

/**
 * @brief set sum of eigenvalues percentage approximation
 * @param val PCA gain percentage
 */
void Pca::setApproxValue(double val) {
	aprval = val;	
}

/**
 * @brief get a motion capture data matrix built on principal 
 * components dimensions
 * @return a motion capture data matrix with reduced dimensions
 */
Eigen::MatrixXd Pca::getPcaProjectedData() {
	return curvemat;
}

/**
 * @brief get a motion capture data matrix rebuilt from principal 
 * components in its original space
 * @return a motion capture data matrix with ordinary dimensions
 */
Eigen::MatrixXd Pca::getPcaRebuiltData() {
	return pcadata;
}

/**
 * @brief get the input data matrix
 * @return input data matrix
 */
Eigen::MatrixXd Pca::getInputData() {
	return mcdata;
}
/**
 * @brief calculate average data vector
 */
void Pca::calculateAverageDataVector() {
	avg = Eigen::ArrayXd::Zero(mcdata.cols());
	
	for (unsigned int i=0; i < mcdata.rows(); ++i) 
		avg += mcdata.row(i);
	avg = avg / mcdata.rows();	
}

/**
 * @brief centralize mocap data
 */
void Pca::centralizeData() {

	Eigen::VectorXd trvec = Eigen::ArrayXd::Zero(mcdata.cols());
	trdata = Eigen::ArrayXXd::Zero(mcdata.rows(), mcdata.cols());
	for (unsigned int k=0; k < mcdata.rows(); ++k) {
		trvec = mcdata.row(k);
		trdata.row(k) = trvec - avg;
	}	
}

/**
 * @brief get number of principal components which are relevant to analysis
 * @return total of principal components
 */
unsigned int Pca::getPrincipalComponentsNumber() {
	double ret, tsum, sev;
    //
	Eigen::VectorXd evabs = eigValues.cwiseAbs();
	sev = evabs.sum() * aprval;
	ret = 0;
	tsum = 0;
	for (unsigned int i=0; i < evabs.size(); ++i)
		if (tsum < sev) {
			tsum += evabs(i);
			++ret;
		} else break;
	return ret;
}

/**
 * @brief get number of principal components which are set to analysis
 * @return total of principal components
 */
unsigned int MMS::getPrincipalComponentsNumber() {
	return npc;
}

/**
 * @brief choose significant eigenvectors to produce analysis
 */
void Pca::chooseEigenVectors() {
    
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(trdata, Eigen::ComputeFullV);
    
    eigVectors = svd.matrixV();
    eigValues = svd.singularValues();
    msev = getPrincipalComponentsNumber();
	msev = 7; // [7; 10] according to paper DOI:10.1002/cav.380
	// choose eigenvector span matrix with best choosen
	bestEigVec = Eigen::MatrixXd::Zero(eigVectors.rows(), msev);
	for (unsigned int i=0; i < msev; ++i) 
		bestEigVec.col(i) = eigVectors.col(i);
}

/**
 * @brief find reduced motion curves and reproject motion to its original space
 */
void Pca::projectAndRebuildData() {
	Eigen::VectorXd trvec = Eigen::ArrayXd::Zero(avg.size());
	
	// find reduced motion (Di) curves
	curvemat = trdata * bestEigVec;
	// reproject reduced motion curve to original space
	pcadata = curvemat * bestEigVec.transpose();
	for (unsigned int i=0; i < pcadata.rows(); ++i) {
		trvec = pcadata.row(i);
		pcadata.row(i) = avg + trvec;
	}
}

/**
 * @brief calculate covariance matrix
 */
void Pca::doAnalysis() {
	
	calculateAverageDataVector();
	centralizeData();
	chooseEigenVectors();
	projectAndRebuildData();
}

/**
 * @brief send stdout significant debug information
 */
void Pca::debug() {
	//std::cout << "eigenvalues" << std::endl;
	//for (unsigned int i=0; i < eigValues.size(); ++i) {
	//	if (eigValues(i) < 1.0) break;
	//	std::cout << "value = " << eigValues(i) << std::endl;
	//	std::cout << "vector = " << eigVectors.col(i).transpose() << std::endl;
	//}
	std::cout << "original eigen values = " << eigValues.size() << 
		" rebuilt eigen values = " << msev << std::endl;
	//Eigen::VectorXi fids = getKeyframesByMMS();
	//std::cout << "keyframes" << std::endl;
	//std::cout << fids.transpose() << std::endl;
}

/**
 * @brief get saliency curve for each DOF
 * @param mat curvature matrix by DOFs or principal components
 * @return matrix of saliency curve by frame id
 */
Eigen::MatrixXd MMS::getSaliencyByDOF(Eigen::MatrixXd & mat) {
	Eigen::MatrixXd sig01, sig02, sig11, sig12, sig21, sig22;
	double sigma = 1.0;
	
	sig01 = getGaussianWeightedAverageMatrix(mat, sigma);
	sig02 = getGaussianWeightedAverageMatrix(mat, 2*sigma);
	sig01 -= sig02;
	sig02 = sig01.cwiseAbs();
	sig11 = getGaussianWeightedAverageMatrix(mat, 2*sigma);
	sig12 = getGaussianWeightedAverageMatrix(mat, 6*sigma);
	sig11 -= sig12;
	sig12 = sig11.cwiseAbs();
	sig21 = getGaussianWeightedAverageMatrix(mat, 3*sigma);
	sig22 = getGaussianWeightedAverageMatrix(mat, 6*sigma);
	sig21 -= sig22;
	sig22 = sig21.cwiseAbs();
	Eigen::MatrixXd sfmap = Eigen::MatrixXd::Zero(sig01.rows(), 3 * sig01.cols());
	sfmap.block(0, 0, sig01.rows(), sig01.cols()) = sig02;
	sfmap.block(0, sig01.cols(), sig01.rows(), sig01.cols()) = sig12;
	sfmap.block(0, 2 * sig01.cols(), sig01.rows(), sig01.cols()) = sig22;
	sfmap = getNonlinearNormalizationMatrix(sfmap);
	sig02 = sfmap.block(0, 0, sig01.rows(), sig01.cols());
	sig12 = sfmap.block(0, sig01.cols(), sig01.rows(), sig01.cols());
	sig22 = sfmap.block(0, 2 * sig01.cols(), sig01.rows(), sig01.cols());
	sig01 = sig02 + sig12 + sig22;
	return sig01;
}

/**
 * @brief returns the chosen keyframes given by Mulstiscale Motion Saliency
 * as described in Halit & Carpin paper. Need a previous 'doAnalysis' call.
 * @return vector of keyframes
 * @see http://yoksis.bilkent.edu.tr/pdf/files/10.1002-cav.380.pdf
 */
Eigen::VectorXi MMS::getKeyframesByMMS() {
	//std::clock_t start, stop;	
	//double duration;
	
	// measure time
	//start = std::clock();
	Eigen::MatrixXd cmat = getCurvatureMatrix(curvemat);
	//stop = std::clock();
	//duration = (stop - start) / (double) CLOCKS_PER_SEC;
	//std::cout << "curvature build time = " << duration << std::endl;
	// measure time
	//start = stop;	
	Eigen::MatrixXd sal = getSaliencyByDOF(cmat);
	//duration = (stop - start) / (double) CLOCKS_PER_SEC;
	//std::cout << "saliency build time = " << duration << std::endl;
	// measure time
	//start = stop;
	Eigen::VectorXi fids = getKeyframesByDOFSaliency(sal);
	//duration = (stop - start) / (double) CLOCKS_PER_SEC;
	//start = stop;
	//std::cout << "keyframe election time = " << duration << std::endl;
	return fids;
}

/**
 * @brief get selected keyframes for the given saliency DOFs map.
 * @param sal DOF saliency map matrix
 * @return vector with keyframe ids
 */
Eigen::VectorXi MMS::getKeyframesByDOFSaliency(Eigen::MatrixXd & sal) {
	Eigen::VectorXi frids;
	std::vector<unsigned int> in, out;
	std::vector<double> weight, sumw;
	double smean, fid;
	bool onlist, loopend;
	
	// get average
	smean = 0;
	for (unsigned int i=0; i < sal.rows(); ++i) {
		for (unsigned int j=0; j < sal.cols(); ++j) 
			smean += sal(i,j);
	}
	smean /= (sal.rows() * sal.cols());
	// get list of upper mean saliency
	onlist = false;
	for (unsigned int i=0; i < sal.rows(); ++i) {
		loopend = true;
		for (unsigned int j=0; j < sal.cols(); ++j) {
			if (sal(i,j) > smean) {
				if (onlist) {
					out.at(out.size() - 1) = i;
				} else {
					onlist = !onlist;
					in.push_back(i);
					out.push_back(i);
				}
				loopend = false;
				break;
			}
		}
		if (loopend) onlist = false;
	}
	// elect best cluster representative
	for (unsigned int k=0; k < in.size(); ++k) {
		for (unsigned int i = in.at(k); i <= out.at(k); ++i) {
			weight.push_back(0);
			for (unsigned int j=0; j < sal.cols(); ++j)
				if (sal(i,j) > smean) 
					weight.at(weight.size() - 1) += eigValues(j);
		}
	}
	for (unsigned int k=0; k < in.size(); ++k) {
		sumw.push_back(0);
		for (unsigned int i = in.at(k), j = 0; i <= out.at(k); ++i, ++j) 
			sumw.at(sumw.size() - 1) += weight.at(j);
	}
	frids = Eigen::VectorXi::Zero(sumw.size());
	for (unsigned int k=0; k < in.size(); ++k) {
		fid = 0;
		for (unsigned int i = in.at(k), j = 0; i <= out.at(k); ++i, ++j) 
			fid += i * weight.at(j);
		fid /= sumw.at(k);
		frids(k) = round(fid) + 2; // curvature skew to dof matrix
	}
	return frids;
}
