#include <mograph.h>
#include <cassert>
#include <mocapreader.h>
#include <asfreader.h>
#include <bvhreader.h>
#include <cmath>
#include <iostream>

/**
 * @breif build a new Motion Graph handler
 * @param s0 skeleton sequence as a motion
 * @param s1 skeleton sequence as a motion
 * @ param w weights for each skeleton joint
 */
Mograph::Mograph(const std::vector<Skeleton> & s0, const std::vector<Skeleton> & s1) {
	assert((s0.size() != 0) && (s1.size() != 0));
	seq0 = s0; 
	seq1 = s1; 
	assert(seq0.at(0).getBoneMatrices().size() == seq1.at(0).getBoneMatrices().size());
	weight = getHierarchyWeigths(seq0.at(0).getIndexHierarchy());
}

/**
 * @brief calculate Y-axis rotation and translation
 */
double Mograph::getPoseDistance(const Skeleton & s0, const Skeleton & s1) {

	std::vector<Eigen::Vector4d> j0, j1;
	Eigen::Vector4d v0, v1;
	double wsum = 0, arcn = 0, arcd = 0;
	v0 << 0,0,0,0;
	v1 << 0,0,0,0;

	// step one: get rotation and translation transformation
	Skeleton sk0 = s0;
	Skeleton sk1 = s1;
	assert(sk0.getBoneMatrices().size() == sk1.getBoneMatrices().size());
	j0.resize(sk0.getBoneMatrices().size() + 1);
	j1.resize(sk0.getBoneMatrices().size() + 1);
	assert(j0.size() == weight.size());
	for (unsigned int i=0; i < j0.size(); ++i) {
		j0.at(i) = sk0.getJointPosition(i);
		j1.at(i) = sk1.getJointPosition(i);
	}
	for (unsigned int i=0; i < j0.size(); ++i) {
		v0 = v0 + weight.at(i) * j0.at(i);
		v1 = v1 + weight.at(i) * j1.at(i);
		arcn += weight.at(i) * (j0.at(i)(0) * j1.at(i)(2) - j1.at(i)(0) * j0.at(i)(2));
		arcd += weight.at(i) * (j0.at(i)(0) * j1.at(i)(0) + j0.at(i)(2) * j1.at(i)(2));
		wsum += weight.at(i);
	}
	arcn -= (v0(0) * v1(2) - v1(0) * v0(2)) / wsum;
	arcd -= (v0(0) * v1(0) + v0(2) * v1(2)) / wsum;
	double theta = atan2(arcn, arcd);
	double x0 = (v0(0) - v1(0) * cos(theta) - v1(2) * sin(theta)) / wsum;
	double z0 = (v0(2) + v1(0) * sin(theta) - v1(2) * cos(theta)) / wsum;
	Eigen::Matrix4d ry = Eigen::Affine3d(Eigen::AngleAxisd(theta, Eigen::Vector3d::UnitY())).matrix();
	Eigen::Matrix4d tr = Eigen::Affine3d(Eigen::Translation3d(x0, 0, z0)).matrix();
	// step 2: get distance
	wsum = 0;
	for (unsigned int i=0; i < j1.size(); ++i) {
	   	j1.at(i) = tr * ry * j1.at(i);
		v0 = j0.at(i) - j1.at(i);
		wsum += v0.dot(v0);
	}
	return wsum;
}

/**
 * @brief calculate the gauss function image for one random variable
 * @param mu average value
 * @param sigma standard deviation
 * @param x random variable
 * @return function image for given parameters
 */
double Mograph::evalGaussFunc(double mu, double sigma, double x) {
    double ret = -1.0 * (x - mu) * (x - mu) / ( 2 * sigma * sigma);
    ret = std::exp(ret) / (2 * M_PI * sigma * sigma);
    return ret;
}

/**
 * @brief build a vector to convolute a gaussian kernel only at main diagonal
 * @param size vector size
 * @return convolution gaussian kernel vector
 */
Eigen::VectorXd Mograph::getGaussVector(unsigned int size) {
    Eigen::VectorXd ret = Eigen::ArrayXd::Zero(size);
    double inc = 4.0 / (ret.size() - 1);

    for (unsigned int i=0; i < ret.size(); ++i) ret(i) = evalGaussFunc(0, 1, i * inc - 2.0);
    inc = 0;
    for (unsigned int i=0; i < ret.size(); ++i) inc += ret(i);
    for (unsigned int i=0; i < ret.size(); ++i) ret(i) /= inc;
    return ret;
}

/**
 * @brief apply convolution operator to a matrix only at its diagonal
 * @param srcmat matrix to be convolved
 * @param kernel convolution vector operator
 * @return convolved matrix
 */
Eigen::MatrixXd Mograph::convolve(Eigen::MatrixXd const & srcmat, Eigen::VectorXd const & kernel) {
    unsigned int hs = (kernel.size() - 1) / 2;
    Eigen::MatrixXd ret = srcmat;
    double sum;

    for (unsigned int i=hs; i < srcmat.rows() - hs; ++i)
        for (unsigned int j=hs; j < srcmat.cols() - hs; ++j) {
            sum = 0;
            for (unsigned int k=0; k < kernel.size(); ++k)
                sum += ret(i - hs + k, j - hs + k) * kernel(k);
            ret(i,j) = sum;
        }
    return ret;
}

/**
 * @brief find local minimum inside matrix values
 * @param mat input matrix
 * @return vector with reference to all local minimun elements
 */
std::vector<Eigen::Vector2i> Mograph::findLocalMinima(Eigen::MatrixXd const & mat) {
	std::vector<Eigen::Vector2i> ret;
	Eigen::Vector2i v;

	for (unsigned int i=1; i < mat.rows() - 1; ++i)
		for (unsigned int j=1; j < mat.cols() - 1; ++j) {
			v(0) = i;
			v(1) = j;
			if ((mat(i,j) < mat(i+1,j)) && (mat(i,j) < mat(i-1,j)) && 
				(mat(i,j) < mat(i,j+1)) && (mat(i,j) < mat(i,j-1)) &&
				(mat(i,j) < mat(i+1,j+1)) && (mat(i,j) < mat(i+1,j-1)) &&
				(mat(i,j) < mat(i-1,j+1)) && (mat(i,j) < mat(i-1,j-1)))
				 ret.push_back(v);
		}
	return ret;
}

/**
 * @brief build error matrix between each pair of poses
 */
void Mograph::calculateErrorMatrix() {
	dist = Eigen::MatrixXd::Zero(seq0.size(), seq1.size());

	for (unsigned int i=0; i < seq0.size(); ++i) {
		for (unsigned int j=0; j < seq1.size(); ++j) {
			if (i != j) dist(i, j) = getPoseDistance(seq0.at(i), seq1.at(j));
		}
	}
	Eigen::VectorXd ker = getGaussVector(5);
	Eigen::MatrixXd tmp = dist;
	dist = convolve(tmp, ker);
	minima.clear();
	minima = findLocalMinima(dist);
}

/**
 * @brief print local minima values if below given threshold. If threshold equals zero,
 * all values are printed
 * @param thres maximum error value considered
 */
void Mograph::printLocalMinima(double thres) {
	double err;
	unsigned int x,y;

	std::cout << "Local minima" << std::endl;
	for (unsigned int i=0; i < minima.size(); ++i) {
		x = minima.at(i)(0);
		y = minima.at(i)(1);
		err = dist(x, y);
		if ((err <= thres) || (thres == 0)) 
			std::cout << "Element [" << x << ", " << y << "] = " << err << std::endl;
	}
	std::cout << std::endl;
}

/**
 * @brief get weight vector by hierarchy index vector. Elements have weight equal to their level, one is
 * the lowest level (leaf)
 * @param par skeleton hierarchy vector
 * @return array of weights
 */
std::vector<double> Mograph::getHierarchyWeigths(std::vector<unsigned int> const & par) {
	std::vector<double> w(par.size(), 1.0);
	for (unsigned int i=0; i < par.size(); ++i) 
		w.at( par.at(i) ) += 1;
	return w;
}

/**
 * @brief set weights to calculate pose distance
 * @param w joint weights
 */
void Mograph::setWeights(std::vector<double> const & w) {
	assert(seq0.at(0).getIndexHierarchy().size() == w.size());
	weight = w;
}


/**
 * @brief
 */
int main(int argc, char ** argv) {
    MocapReader *reader = NULL;
    
    if ((argc < 2) || (argc > 5)) {
        std::cout << "filename needed." << std::endl;
        exit(1);
    }   
    std::string fname(argv[1]);
    std::string bname(basename(argv[1]));
    std::string ext = bname.substr(bname.length() - 3, 3); 
    if (ext.compare("asf") == 0) {
        reader = new ASFReader();
        reader->readFile(fname);
        if (argc > 2) {
            fname = argv[2];
            bname = basename(argv[2]);
            ext = bname.substr(bname.length() - 3, 3); 
            if (ext.compare("amc") == 0) {
                reader->readFile(fname);    
            }
        }
    } else if (ext.compare("bvh") == 0) {
        reader = new BVHReader();
        reader->readFile(fname);
	}
	std::vector<Skeleton> pose;
	for (unsigned int i=0; i < reader->getNumberOfFrames(); ++i)
		pose.push_back(reader->getFrameSkeleton(i));
    Mograph mo(pose, pose);
	mo.calculateErrorMatrix();
	mo.printLocalMinima(0);
	return 0;
}
