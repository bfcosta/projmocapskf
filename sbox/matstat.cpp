#include <matstat.h>
#include <asfreader.h>
#include <bvhreader.h>
#include <iostream>
#include <cassert>
#include <cmath>

#define PZR  1.0e-6

/**
 * @brief creates a new matrix statistics extractor
 * @param isRot defines if matrix pool has rotation (true) or translation (false) matrix
 */
MatStat::MatStat(bool isRot) {
	isRotation = isRot;
	if (isRotation) statsz = 9; else statsz = 3;
}

/**
 * @brief reset configuration and clears all data
 * @param isRot defines if matrix pool has rotation (true) or translation (false) matrix
 */
void MatStat::reset(bool isRot) {
	isRotation = isRot;
	if (isRotation) statsz = 9; else statsz = 3;
	sample.clear();
	avg.clear();
	stdev.clear();
}

/**
 * @brief add new matrix to matrix sample pool
 * @param mat matrix added to pool
 */
void MatStat::addMatrix(Eigen::Matrix4d const & mat) {
	std::vector<double> tmp;

	tmp.resize(statsz);
	if (isRotation) {
		for (unsigned int i=0; i < 3; ++i)
			for (unsigned j=0; j < 3; ++j) tmp.at(3*i + j) = mat(i,j);
	} else {
		for (unsigned int i=0; i < statsz; ++i) tmp.at(i) = mat(i,3);
	}
	sample.push_back(tmp);
}

/**
 * @brief compute guassian parameters (mu and sigma) of each matrix element
 */
void MatStat::computeStats() {

	assert(sample.size() > 1);
	avg.resize(statsz);
	stdev.resize(statsz);
	for (unsigned int j=0; j < statsz; ++j) {
		avg.at(j) = 0;
		stdev.at(j) = 0;
	}
	// compute mu
	for (unsigned int i=0; i < sample.size(); ++i)
		for (unsigned int j=0; j < statsz; ++j) 
			avg.at(j) += sample.at(i).at(j);
	for (unsigned int j=0; j < statsz; ++j) avg.at(j) /= sample.size();
	// compute sigma
	for (unsigned int i=0; i < sample.size(); ++i)
		for (unsigned int j=0; j < statsz; ++j) 
			stdev.at(j) += pow(sample.at(i).at(j) - avg.at(j), 2.0);
	for (unsigned int j=0; j < statsz; ++j) stdev.at(j) /= (sample.size() - 1);
}

/**
 * @brief get matrix with average elements
 * @return matrix with average elements
 */
Eigen::Matrix4d MatStat::getAvgMatrix() {
	Eigen::Matrix4d ret = Eigen::Matrix4d::Identity();

	if (isRotation) {
		for (unsigned int i=0; i < 3; ++i)
			for (unsigned j=0; j < 3; ++j) ret(i, j) = avg.at(3*i + j);
		orthonormalize(ret);
	} else 
		for (unsigned int i=0; i < statsz; ++i) ret(i,3) = avg.at(i);
	return ret;
}

/**
 * @brief get matrix with sigma (standard deviation) elements
 * @return matrix with sigma (standard deviation) elements
 */
Eigen::Matrix4d MatStat::getStdMatrix() {
	Eigen::Matrix4d ret = Eigen::Matrix4d::Identity();

	if (isRotation) {
		for (unsigned int i=0; i < 3; ++i)
			for (unsigned j=0; j < 3; ++j) ret(i, j) = stdev.at(3*i + j);
	} else 
		for (unsigned int i=0; i < statsz; ++i) ret(i,3) = stdev.at(i);
	return ret;
}

/**
 * @brief return global axis average angles or shifts whether matrix is a rotation or translation one.
 * @return vector with ZYX rotation angles or translation shifts
 * @see http://nghiaho.com/?page_id=846
 */
std::vector<double> MatStat::getZYXDecomposition() {

	if (isRotation) {
		std::vector<double> ret;
		ret.resize(3);
		Eigen::Matrix4d rot = getAvgMatrix();
		ret.at(0) = atan2(rot(1,0), rot(0,0));
		ret.at(1) = atan2(-1 * rot(2,0), sqrt(rot(2,1) * rot(2,1) + rot(2,2) * rot(2,2)));
		ret.at(2) = atan2(rot(2,1), rot(2,2));
		return ret;
	} else 
		return avg;
}

/**
 * @brief build a suitable rotation matrix.
 * @param matrix to be orthonormalized
 */
void MatStat::orthonormalize(Eigen::Matrix4d & n) {
    Eigen::Vector4d u, v, w;
    double r;

    for (unsigned int i=0; i<10; ++i) {
        // normalize matrix
        n.col(0).normalize(); // x
        n.col(1).normalize(); // y
        n.col(2).normalize(); // z
        r = std::pow(n.col(0).dot(n.col(1)), 2) +
            std::pow(n.col(1).dot(n.col(2)), 2) +
            std::pow(n.col(2).dot(n.col(0)), 2);
        if (r < PZR) return;
        // find u, v, w
        u = n.col(1).cross3(n.col(2));
        v = n.col(2).cross3(n.col(0));
        w = n.col(0).cross3(n.col(1));
        n.col(0) = (n.col(0) + u)/2;
        n.col(1) = (n.col(1) + v)/2;
        n.col(2) = (n.col(2) + w)/2;
    }
    n.col(0).normalize();
    n.col(1).normalize();
    n.col(2).normalize();
}

/**
 * @brief get the euclidian distance between two matrices
 * @param m1 rotation/translation matrix
 * @param m2 rotation/translation matrix
 */
double MatStat::getMatEuclDist(Eigen::Matrix4d const & m1, Eigen::Matrix4d const & m2) {
	Eigen::Vector4d v1, v2;
	double r = std::sqrt(3.0);
	v1 << 1, 1, 1, r;
	v2 << 1, 1, 1, r;
	v1 = m1 * v1;
	v2 = m2 * v2;
	return fabs( v1.head(3).dot(v2.head(3)) / 3.0 );
}

/**
 *  * @brief dump bone angles for every frame
 */
int main (int argc, char ** argv) {
    MocapReader *reader = NULL;
    
    if (!((argc == 2) || (argc == 4))) {
        std::cout << "filename needed." << std::endl;
        exit(1);
    }   
    std::string fname(argv[1]);
    std::string bname(basename(argv[1]));
    std::string ext = bname.substr(bname.length() - 3, 3); 
    if (ext.compare("asf") == 0) {
        reader = new ASFReader();
        reader->readFile(fname);
        if (argc > 2) {
            fname = argv[2];
            bname = basename(argv[2]);
            ext = bname.substr(bname.length() - 3, 3); 
            if (ext.compare("amc") == 0) {
                reader->readFile(fname);    
            }
        }
    } else if (ext.compare("bvh") == 0) {
        reader = new BVHReader();
        reader->readFile(fname);
    }   

    Skeleton sk = reader->getSkeleton();
    std::vector<unsigned int> endeff = sk.getEndEffectorsIdx();
    std::cout << "End effectors =";
    for (unsigned int i=0; i < endeff.size(); ++i) 
        std::cout << " " << endeff.at(i);
    std::cout << std::endl;
    std::vector<MatStat> currstats;

    //std::cout << "initialize statistics descriptor" << std::endl;
    currstats.resize(sk.getBoneMatrices().size() + 1, MatStat(true));
    currstats.at(0).reset(false);
    //std::cout << "add data" << std::endl;
    for (unsigned int i=0; i < reader->getNumberOfFrames(); ++i) {
        sk = reader->getFrameSkeleton(i);
        currstats.at(0).addMatrix(sk.getJointMatrices().at(0));
        for (unsigned int j=1; j < currstats.size(); ++j) 
            currstats.at(j).addMatrix(sk.getBoneMatrices().at(j - 1));
    }   
    //std::cout << "compute statistics" << std::endl;
    for (unsigned int i=0; i < currstats.size(); ++i) 
        currstats.at(i).computeStats();
    //std::cout << "Root joint average" << std::endl;
    //std::cout << currstats.at(0).getAvgMatrix() << std::endl;
    //std::cout << "Root joint stdev" << std::endl;
    //std::cout << currstats.at(0).getStdMatrix() << std::endl;
    for (unsigned int i=1; i < currstats.size(); ++i) {
        //std::cout << "bone " << i << " average" << std::endl;
        //std::cout << currstats.at(i).getAvgMatrix() << std::endl;
        //std::cout << "bone " << i << " stdev" << std::endl;
        //std::cout << currstats.at(i).getStdMatrix() << std::endl;
        std::vector<double> angs = currstats.at(i).getZYXDecomposition();
        std::cout << "bone " << i << " average angles" << std::endl;
        std::cout << angs.at(0) << " " << angs.at(1) << " " << angs.at(2) << std::endl;
    }
}
