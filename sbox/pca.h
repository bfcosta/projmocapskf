#ifndef PCA_H
#define PCA_H

#include <saliency.h>

/**
 * @class Pca
 *
 * @brief Principal Component Analyser for motion capture
 *
 * This class takes a data matrix as input and tries to rebuilt it
 * using Principal Component Analysis for reducing its input dimension. In short,
 * the data is projected into the covariance matrix most significant eigen space
 * dimensions, which are used for other analysis later.
 * 
 * @see http://www.cse.psu.edu/~rtc12/CSE586Spring2010/lectures/pcaLectureShort_6pp.pdf
 */

class Pca : public Saliency {
	protected:
		/// PCA approximation factor.
		double aprval;
		/// number of most significant eigenvalues used.
		unsigned int msev;
		/// original data in matrix format.
		Eigen::MatrixXd mcdata;
		/// mocap data centered at average vector data.
		Eigen::MatrixXd trdata;
		/// projected mocap data into PCA eigen space.
		Eigen::MatrixXd curvemat;
		/// rebuilt mocap data from PCA eigen space
		Eigen::MatrixXd pcadata;
		/// average vector data.
		Eigen::VectorXd avg;
		/// covariance matrix eigen values
		Eigen::VectorXd eigValues;
		/// covariance matrix eigen vectors
		Eigen::MatrixXd eigVectors;
		/// significant eigenvectors for PCA
		Eigen::MatrixXd bestEigVec;
		
		void calculateAverageDataVector();
		void centralizeData();
		void chooseEigenVectors();
		void projectAndRebuildData();
		virtual unsigned int getPrincipalComponentsNumber();

	public:
		/**
		 * @brief assign a data matrix to PCA solver
		 * @param input data matrix
		 */
		void setDataMatrix(Eigen::MatrixXd & mat) {
			mcdata = mat;
		}
				
		/**
		 * @brief build new PCA solver
		 * @param mat input data matrix
		 * @param val PCA gain percentage
		 */
		Pca(Eigen::MatrixXd & mat, double val = 0.9): Saliency() {
			setDataMatrix(mat);
			setApproxValue(val);
		}
		
		/**
		 * @brief destroy all data
		 */
		virtual ~Pca() {}
		
		void debug();
		void doAnalysis();
		void setApproxValue(double val);
		Eigen::MatrixXd getPcaProjectedData();
		Eigen::MatrixXd getPcaRebuiltData();
		Eigen::MatrixXd getInputData();
};

class MMS: public Pca {
	protected:
		unsigned int npc;
		virtual unsigned int getPrincipalComponentsNumber();
	
	public:
		/**
		 * @brief build new Multiscale Motion Saliency solver
		 * @param mat input data matrix
		 * @param number of principal components in analysis
		 */
		MMS(Eigen::MatrixXd & mat, unsigned int n=7): 
			Pca(mat) {
			npc = n;
		}
		/**
		 * @brief destroy all data
		 */
		virtual ~MMS() {}
		Eigen::MatrixXd getSaliencyByDOF(Eigen::MatrixXd & mat);
		Eigen::VectorXi getKeyframesByDOFSaliency(Eigen::MatrixXd & sal);
		Eigen::VectorXi getKeyframesByMMS();
};
#endif
