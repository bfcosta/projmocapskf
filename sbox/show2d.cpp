#include <iostream>
#include <cstdlib>
#include <Eigen/Dense>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <renderhelper.h>

Eigen::MatrixXd mpos, fproj, crom;
Eigen::VectorXd cpos;
unsigned int framesec, totframes, currfid;
int mainwin;
bool play;
Eigen::Vector3d eye, center, up;
double zn,zf;
double fovy = 45.0;

void reshape(int w, int h) {
	glViewport(0, 0, w, h);
 	glMatrixMode(GL_PROJECTION);
 	glLoadIdentity();
	glOrtho (-0.5, 1.5, -0.5, 1.5, -1.0, 1.0);
 	glMatrixMode(GL_MODELVIEW);
 	glLoadIdentity();
}

void display() {
	unsigned int lframe = 0.5*totframes;
	
	if (currfid >= lframe) lframe = currfid - lframe;
	else lframe = 0;
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.85,0.85,0);
	// draw key frames
	for (unsigned int i=0; i < mpos.rows(); ++i)
		drawFilledCircle(mpos(i,0), mpos(i,1), 0, 0.02);
	glColor3f(1,0.5,0.5);
	// draw controller
	drawFilledCircle(cpos(0), cpos(1), 0, 0.02);
	// draw in-betweens
	if (fproj.rows() > 0) {
		glColor3f(0.2,0.8,1.0);
		for (unsigned int i=lframe; i < currfid; ++i) 
			drawFilledCircle(fproj(i,0), fproj(i,1), 0, 0.007);
	}
	// draw catmull rom of in-betweens
	if (crom.rows() > 0) {
		glColor3f(0.8,0.2,1.0);
		for (unsigned int i=lframe; i < currfid; ++i)
			drawFilledCircle(crom(i,0), crom(i,1), 0, 0.007);
	}
	glutSwapBuffers();
}

void getFrame(int value) {
	// controller
	double fp = (double) value;
	unsigned int r, f0, f1;
	Eigen::VectorXd v0, v1;

	r = 0;
	for (unsigned int i=0; i < mpos.rows(); ++i)
		if (mpos(i,2) >= fp) {
			r = i;
			break;
		}
	if (r == 0) 
		cpos = mpos.row(0).head(2); 
	else {
		if (fproj.rows() > 0)
			cpos = fproj.row(value).head(2);
		else {
			f0 = mpos(r - 1, 2);
			f1 = mpos(r, 2);
			v0 = mpos.row(r-1).head(2);
			v1 = mpos.row(r).head(2);
			fp = ((double) (value - f0)) / (f1 - f0);
			v1 = fp * (v1 - v0);
			cpos = v0 + v1;
		}
	}
	glutPostRedisplay();
	
	if (play) {
		currfid = value;
		if (totframes == (currfid + 1)) 
			glutTimerFunc(framesec, getFrame, (currfid) % totframes);
		else 
			glutTimerFunc(framesec, getFrame, (currfid + 1) % totframes);
	} else glutTimerFunc(framesec, getFrame, currfid % totframes);
}

void handleArrowKeys(int key, int x, int y) {
	switch(key) {
		case GLUT_KEY_UP:
		case GLUT_KEY_RIGHT:
			play = false;
			currfid = (currfid + 1) % totframes;
			std::cout << "frame = " << currfid << std::endl;
			break;
		case GLUT_KEY_DOWN:
		case GLUT_KEY_LEFT:
			play = false;
			if (currfid == 0) currfid = totframes - 1;
			else currfid = (currfid - 1) % totframes;
			std::cout << "frame = " << currfid << std::endl;
			break;
		default:
		break;
	}
}

void keyboard(unsigned char key, int x, int y) {
 	switch (key) {
  		case 27:
			glutDestroyWindow(mainwin);
   			exit(0);
   			break;
        case 80: 
        case 112:
			std::cout << "play" << std::endl;
			play = true;
            break;
        default:
			play = false;
			std::cout << "stop" << std::endl;
 	}
}

/**
 * @brief get the catmull rom interpolation of a set of vectors in a
 * timestamp, given a time interval and their respective values.
 * @param tt timestamp to calculate interpolation
 * @param t0 previous timestamp to the begin
 * @param t1 interval begin timestamp
 * @param t2 interval end timestamp
 * @param t3 next timestamp to the end
 * @param pproj rowwise matrix with the set of vectors, where each row
 * describe the vectors timestamp
 * @return interpolated vector
 */
Eigen::VectorXd getCatmullRomInterpolation(int tt, int t0, int t1, int t2, int t3, Eigen::MatrixXd & pproj) {
	Eigen::VectorXd a1, a2, a3, b1, b2;
	double r1, r2;

	// a1
	if (t1 == t0)
		a1 = pproj.row(t0);
	else {
		r1 = t1 - t0;
		r2 = (tt - t0) / r1;
		r1 = (t1 - tt) / r1;
		b1 = pproj.row(t0);
		b2 = pproj.row(t1);
		a1 = r1*b1 + r2*b2;
	}
	// a2
	r1 = t2 - t1;
	r2 = (tt - t1) / r1;
	r1 = (t2 - tt) / r1;
	b1 = pproj.row(t1);
	b2 = pproj.row(t2);
	a2 = r1*b1 + r2*b2;
	// a3
	if (t3 == t2)
		a3 = pproj.row(t2);
	else {
		r1 = t3 - t2;
		r2 = (tt - t2) / r1;
		r1 = (t3 - tt) / r1;
		b1 = pproj.row(t2);
		b2 = pproj.row(t3);
		a3 = r1*b1 + r2*b2;
	}
	// b1
	r1 = t2 - t0;
	r2 = (tt - t0) / r1;
	r1 = (t2 - tt) / r1;
	b1 = r1*a1 + r2*a2;
	// b2
	r1 = t3 - t1;
	r2 = (tt - t1) / r1;
	r1 = (t3 - tt) / r1;
	b2 = r1*a2 + r2*a3;
	// catmull rom
	r1 = t2 - t1;
	r2 = (tt - t1) / r1;
	r1 = (t2 - tt) / r1;
	a1 = r1*b1 + r2*b2;
	return a1;
}

/**
 * @brief get a catmull rom curve by control points
 * @param kfs key frames as control points
 * @param pproj pose projection
 * @return matrix describing a catmull rom curve
 */
Eigen::MatrixXd getCatmullRomCurve(Eigen::MatrixXd & kfs, Eigen::MatrixXd & pproj) {
	assert(kfs.rows() > 1);
	Eigen::MatrixXd ret = pproj;
	Eigen::VectorXi cpoints = Eigen::ArrayXi::Zero(kfs.rows());
	Eigen::VectorXd tmp;

	for (unsigned int i=0; i < cpoints.size(); ++i)
		cpoints(i) = kfs(i,2);
	for (int i=cpoints(0); i < cpoints(1); ++i) {
		tmp = getCatmullRomInterpolation(i, cpoints(0), cpoints(0), cpoints(1), cpoints(2), pproj);
		ret.row(i).head(2) = tmp.head(2);
	}
	for (int j=1; j < cpoints.size() - 2; ++j)
		for (int i=cpoints(j); i < cpoints(j+1); ++i) {
			tmp = getCatmullRomInterpolation(i, cpoints(j-1), cpoints(j), cpoints(j+1), cpoints(j+2), pproj);
			ret.row(i).head(2) = tmp.head(2);
		}
	int lidx = cpoints.size() - 1;
	for (int i=cpoints(lidx - 1); i < cpoints(lidx); ++i) {
		tmp = getCatmullRomInterpolation(i, cpoints(lidx - 2), cpoints(lidx - 1), cpoints(lidx), cpoints(lidx), pproj);
		ret.row(i).head(2) = tmp.head(2);
	}
	return ret;
}

/**
 * @brief get the most distant frame/projection of a 2D projection
 * and its given catmull rom curve.
 * @param kfs key frame matrix, describing curve's control points
 * @param pproj matrix of 2D projections
 * @param crom matrix of catmull rom approximation of 2D projection
 * @param maxerr
 * @return frame id with maximum distance between 2D projection and
 * catmull rom curve
 */
int getCatmullRomSCSKeyFrame(Eigen::MatrixXd & kfs, Eigen::MatrixXd & pproj, Eigen::MatrixXd & crom, double * maxerr) {
	Eigen::VectorXd error = Eigen::ArrayXd::Zero(crom.rows());
	Eigen::VectorXd tmp0, tmp1;
	double max = 0;
	int ret = 0;

	for (int i=0; i < error.size(); ++i) {
		tmp0 = pproj.row(i);
		tmp1 = crom.row(i);
		tmp1 -= tmp0;
		error(i) = tmp1.norm();
	}
	max = error(0);
	for (int i=1; i < error.size(); ++i)
		if (error(i) > max) {
			ret = i;
			max = error(i);
		}
	*maxerr = max;
	//std::cout << "new kf = " << ret << " error = " << max << std::endl;
	return ret;
}

/**
 * @brief add one new key frame to key frame matrix, making the catmull rom
 * curve resemble the 2D projection
 * @param kfs key frame matrix (updated)
 * @param pproj 2D projection matrix
 * @param crom catmull rom curve matrix
 */
void addKeyFrame(Eigen::MatrixXd & kfs, Eigen::MatrixXd & pproj, Eigen::MatrixXd & crom) {
	Eigen::MatrixXd nmat = Eigen::ArrayXXd::Zero(kfs.rows() + 1, kfs.cols());
	int i=0;
	double err;

	int kf = getCatmullRomSCSKeyFrame(mpos, fproj, crom, &err);
	while (kfs(i,2) < kf) {
		nmat.row(i) = kfs.row(i);
		++i;
	}
	nmat(i,0) = pproj(kf,0);
	nmat(i,1) = pproj(kf,1);
	nmat(i,2) = kf;
	while (i < kfs.rows()) {
		nmat.row(i+1) = kfs.row(i);
		++i;
	}
	crom = getCatmullRomCurve(nmat, pproj);
	kfs = nmat;
}

/**
 * @brief get an enriched key frames set which makes a catmull-rom curve
 * given by these key frames resembles its projection on 2D, given a threshold.
 * @param kfs initial key frames set
 * @param pproj 2D projection
 * @param ethresh error threshold
 * @return enriched key frame set
 */
Eigen::MatrixXd getScsCrCurve(Eigen::MatrixXd & kfs, Eigen::MatrixXd & pproj, double ethresh = 0.2) {
	Eigen::MatrixXd tmp = kfs;
	Eigen::MatrixXd ret = kfs;
	Eigen::MatrixXd curve = getCatmullRomCurve(ret, pproj);
	double err;
	int kf = getCatmullRomSCSKeyFrame(tmp, pproj, curve, &err);

	//std::cout << "err = " << err << " kf = " << kf << std::endl;
	while (err > ethresh) {
		//std::cout << "key frames" << std::endl << ret << std::endl;
		tmp = Eigen::ArrayXXd::Zero(ret.rows() + 1, ret.cols());
		int i=0;
		while (ret(i,2) < kf) {
			tmp.row(i) = ret.row(i);
			++i;
		}
		tmp(i,0) = pproj(kf,0);
		tmp(i,1) = pproj(kf,1);
		tmp(i,2) = kf;
		while (i < ret.rows()) {
			tmp.row(i+1) = ret.row(i);
			++i;
		}
		ret = tmp;
		curve = getCatmullRomCurve(ret, pproj);
		kf = getCatmullRomSCSKeyFrame(tmp, pproj, curve, &err);
		//std::cout << "err = " << err << " kf = " << kf << std::endl;
	}
	//std::cout << "key frames" << std::endl << ret << std::endl;
	return ret;
}

int main(int argc, char ** argv) {
	int w = 600, h = 600;
	
	// read files
	if (argc < 2) {
		std::cout << "syntax: " << argv[0] << 
			" [spatial keyframe file]" << std::endl;
		exit(1);
	}
	readMarkersPosition(argv[1], mpos, fproj);
	currfid = 0;
	mpos = getScsCrCurve(mpos, fproj, 0.15);
	crom = getCatmullRomCurve(mpos, fproj);
	//addKeyFrame(mpos, fproj, crom);
	//addKeyFrame(mpos, fproj, crom);

	if (fproj.rows() > 0) totframes = fproj.rows();
	else totframes = 20*mpos.rows();
	cpos = mpos.row(currfid).head(2);
	framesec = 20;
	// render animation
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (w, h);
	mainwin = glutCreateWindow("Spatial Key Frame - 2D projection");
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(handleArrowKeys);
	glutDisplayFunc(display);
	glutTimerFunc(framesec, getFrame, currfid % totframes);
	glutMainLoop();
	return 0;
}
