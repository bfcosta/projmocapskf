#ifndef MATSTAT_H
#define MATSTAT_H

#include <Eigen/Dense>
#include <vector>

class MatStat {
	protected:
	bool isRotation;
	unsigned int statsz;
	std::vector< std::vector<double> > sample;
	std::vector<double> avg;
	std::vector<double> stdev;
	void orthonormalize(Eigen::Matrix4d & n);

	public:
	MatStat(bool isRot);
	void reset(bool isRot);
	void addMatrix(Eigen::Matrix4d const & mat);
	void computeStats();
	Eigen::Matrix4d getAvgMatrix();
	Eigen::Matrix4d getStdMatrix();
	double getMatEuclDist(Eigen::Matrix4d const & m1, Eigen::Matrix4d const & m2);
	std::vector<double> getZYXDecomposition();
};

#endif
