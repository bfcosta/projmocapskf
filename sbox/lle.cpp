#include <lle.h>
#include <iostream>
#include <cmath>
#include <cassert>
//#include <ctime>

/**
 * @brief get distance matrix by data matrix where points are rowwise displayed.
 * @param mat data matrix
 * @return distance matrix
 */
Eigen::MatrixXd LLE::getDistanceMatrix(Eigen::MatrixXd & mat) {
    Eigen::MatrixXd dist = Eigen::ArrayXXd::Zero(mat.rows(), mat.rows());
    Eigen::VectorXd v0 = Eigen::ArrayXd::Zero(mat.cols());
    Eigen::VectorXd v1 = Eigen::ArrayXd::Zero(mat.cols());

    for (unsigned int i=0; i < mat.rows(); ++i) {
        for (unsigned int j=i+1; j < mat.rows(); ++j) {
            v0 = mat.row(i);
            v1 = mat.row(j);
            dist(i,j) = dist(j,i) = std::sqrt(v0.dot(v1));
        }
    }
    return dist;
}

/**
 * @brief get nearest neigbors index vector
 * @param dvec distance vector
 * @param knn number of nearest neighbors
 * @return index vector with nearest neighbors
 */
Eigen::VectorXi LLE::getKnnIndices(Eigen::VectorXd const & dvec, unsigned int knn) {
    Eigen::VectorXi ret = Eigen::ArrayXi::Zero(knn);
    double max = dvec(0);
    int maxidx = 0;

    for (int i=1; i < dvec.size(); ++i)
        if (max < dvec(i)) {
            max = dvec(i);
            maxidx = i;
        }
    for (unsigned int i=0; i < knn; ++i) {
        ret(i) = maxidx;
        for (int j=0; j < dvec.size(); ++j) {
            if (dvec(ret(i)) > dvec(j)) {
                bool chosen = false;
                // check if it was already chosen
                for (unsigned int k=0; k < i; ++k)
                    if (ret(k) == j) {
                        chosen = true;
                        break;
                    }
                if (!chosen) ret(i) = j;
            }
        }
    }
    return ret;
}


/**
 * @brief get local linear embedding weight reconstruction matrix for a given data matrix
 * @param data data matrix
 * @param knn size of nearest neighborhood
 * @return weight matrix
 */
Eigen::MatrixXd LLE::getLLEWeightMatrix(Eigen::MatrixXd & data, unsigned int knn) {
    Eigen::MatrixXd wmat = Eigen::ArrayXXd::Zero(data.rows(), data.rows());
    Eigen::MatrixXd dist = getDistanceMatrix(data);
    Eigen::MatrixXi nbmat = Eigen::ArrayXXi::Zero(data.rows(), knn);
    Eigen::VectorXd rdist = Eigen::ArrayXd::Zero(data.cols());
	//std::clock_t start, stop;
	//double duration;

    // get knn minimum values
    for (unsigned int i=0; i < dist.rows(); ++i) {
        rdist = dist.row(i);
        nbmat.row(i) = getKnnIndices(rdist, knn);
    }
    // calculate weights
	//std::cout << "calculate weights" << std::endl;
    Eigen::MatrixXd zmat = Eigen::ArrayXXd::Zero(knn, data.cols());
    Eigen::MatrixXd cmat = Eigen::ArrayXXd::Zero(knn, knn);
    Eigen::VectorXd ones = Eigen::ArrayXd::Constant(knn, 1.0);
    Eigen::VectorXd sol = Eigen::ArrayXd::Zero(knn);
    double sum = 1.0;
    for (unsigned int i=0; i < data.rows(); ++i) {
        for (unsigned int j=0; j < knn; ++j) {
            rdist = data.row(nbmat(i,j)) - data.row(i);
            zmat.row(j) = rdist;
        }
        cmat = zmat * zmat.transpose();
	    //start = std::clock();
        //sol = cmat.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(ones);
        Eigen::FullPivLU<Eigen::MatrixXd> lu(cmat);
        sol = lu.inverse() * ones;
	    //stop = std::clock();
    	//duration = (stop - start) / (double) CLOCKS_PER_SEC;
	    //std::cout << "jacobi svd line " << i << " duration =  " << duration << std::endl;
        sum = sol.sum();
        if (sum == 0) {
            //sum = 1.0;
            std::cout << "strange solution = " << sol.transpose() << std::endl;
            sol.normalize();
            sum = sol.cwiseAbs().sum();
        }
        for (unsigned int j=0; j < knn; ++j) {
            wmat(i, nbmat(i,j)) = sol(j) / sum;
        }
    }
    return wmat;
}

/**
 * @brief get embedding coordinates for LLE given weight matrix
 * @param wmat weighht matrix
 * @param dim dimension of embedding coordinates
 * @return matrix with eigenvectors displayed columnwise
 */
Eigen::MatrixXd LLE::getEmbeddingCoordinateMatrix(Eigen::MatrixXd & wmat, unsigned int dim) {
    Eigen::MatrixXd imat = Eigen::MatrixXd::Identity(wmat.rows(), wmat.cols());
    Eigen::MatrixXd iwmat = imat - wmat;
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(iwmat, Eigen::ComputeThinV);
    Eigen::MatrixXd vmat = svd.matrixV();
    assert((vmat.cols() - dim) > 0);
    Eigen::MatrixXd sol = vmat.block(0, vmat.cols() - dim - 1, vmat.rows(), dim);
    return sol;
}

/**
 * @brief get rbf interpolation function for saliency
 * @param kmat keyframes saliency matrix
 * @return rbf 
 */
RbfSimpleInterp LLE::getRbfKfInterpolation(Eigen::MatrixXd & kmat) {
	RbfSimpleInterp rbf(kmat.cols());
		
	for (unsigned int j=0; j < kmat.rows(); ++j)
		rbf.addMarker(kmat.row(j));
	rbf.prepare();		
	return rbf;	
}

/**
 * @brief get enhanced keyframe list given by iterate search procedure
 * @param seedv list of initial keyframes
 * @param itnum number of iterations
 * @return final vector of keyframes
 */
Eigen::VectorXi LLE::getIKFSelection(Eigen::VectorXi & seedv, unsigned int itnum) {
		
	assert(sdof.rows() == llesp.rows());
	Eigen::MatrixXd imat = Eigen::ArrayXXd::Zero(llesp.rows(), llesp.cols());
	Eigen::MatrixXd omat = Eigen::ArrayXXd::Zero(llesp.rows(), llesp.cols());
	Eigen::MatrixXd aimat = Eigen::ArrayXXd::Zero(dof.rows(), dof.cols());
	Eigen::MatrixXd aomat = Eigen::ArrayXXd::Zero(dof.rows(), dof.cols());
	Eigen::MatrixXd aaomat = Eigen::ArrayXXd::Zero(dof.rows(), dof.cols());
	Eigen::VectorXd err = Eigen::ArrayXd::Zero(llesp.rows());
	Eigen::VectorXi ikfv = Eigen::ArrayXi::Zero(llesp.rows());
	Eigen::VectorXi dvec = Eigen::ArrayXi::Zero(llesp.rows());
	int ci, co, nkf = seedv.size();
	//std::clock_t start, stop;
	//double duration;
	
	// measure time
	//start = std::clock();
	ikfv.head(nkf) = seedv;
	// iterate
	for (unsigned int i=0; i < itnum; ++i) {
		ci = co = 0;
		// separate input sets
		for (int j=0; j < sdof.rows(); ++j) 
			if (ikfv(ci) == (j+2)) { // curvature skew to dof matrix
				imat.row(ci) = llesp.row(j);
				aimat.row(ci) = dof.row(j+2); // curvature skew to dof matrix
				++ci;
			} else {
				omat.row(co) = llesp.row(j);
				aomat.row(co) = dof.row(j+2); // curvature skew to dof matrix
				dvec(co) = j;
				++co;
			}
		// find A*
		Eigen::MatrixXd tmat = imat.block(0,0, ci, imat.cols());
		RbfSimpleInterp rbf = getRbfKfInterpolation(tmat);
		for (unsigned int j=0; j < aimat.cols(); ++j) {
			Eigen::VectorXd b = aimat.col(j).head(ci);
			rbf.resolve(b);
			for (int k=0; k < co; ++k) 			
				aaomat(k,j) = rbf.getImage(omat.row(k));	
		}
		// calculate errors
		for (int j=0; j < co; ++j) {
			Eigen::VectorXd v0 = aomat.row(j);
			Eigen::VectorXd v1 = aaomat.row(j);
			v0 -= v1;
			v1 = v0.cwiseAbs();
			Eigen::VectorXd wvec = sdof.row(dvec(j));
			double sum = wvec.cwiseAbs().sum();
			wvec /= sum;
			err(i) = v1.dot(wvec);
		}
		// get min error
		int mini = 0;
		double minv = err(0);
		for (int j=1; j < co; ++j) 
			if (minv > err(j)) {
				mini = j;
				minv = err(j);			
			}
		mini = dvec(mini) + 2;  // curvature skew to dof matrix
		++nkf;
		// insert new kf and reorder vector
		bool inserted = false;
		for (int j=0; j < ci; ++j) 
			if ((ikfv(j) > mini) || inserted) {
				int t = ikfv(j);
				ikfv(j) = mini;
				mini = t;
				inserted = !inserted;
			}
		ikfv(ci) = mini;
	}
	//stop = std::clock();
	//duration = (stop - start) / (double) CLOCKS_PER_SEC;
	//std::cout << "total iterative KF election time = " << duration << std::endl;
	return ikfv.head(nkf);
}

/**
 * @brief get the initial keyframes set given by Chao Jin's paper
 * @return vector with keyframes
 * @see http://onlinelibrary.wiley.com/doi/10.1002/cav.1471/abstract
 */
Eigen::VectorXi LLE::getInitialKeyframesSet() {
	Eigen::MatrixXd sig01, sig02, sig11, sig12, sig21, sig22;
	double sigma = 1.0;
	// get curvature
	Eigen::MatrixXd cdof = getCurvatureMatrix(dof);
	//std::clock_t start, stop;
	//double duration;
	
	// get saliency map
	sig01 = getGaussianWeightedAverageMatrix(cdof, sigma);
	sig02 = getGaussianWeightedAverageMatrix(cdof, 2*sigma);
	sig01 -= sig02;
	sig02 = sig01.cwiseAbs();
	sig11 = getGaussianWeightedAverageMatrix(cdof, 2*sigma);
	sig12 = getGaussianWeightedAverageMatrix(cdof, 6*sigma);
	sig11 -= sig12;
	sig12 = sig11.cwiseAbs();
	sig21 = getGaussianWeightedAverageMatrix(cdof, 3*sigma);
	sig22 = getGaussianWeightedAverageMatrix(cdof, 6*sigma);
	sig21 -= sig22;
	sig22 = sig21.cwiseAbs();
	/*
	// was Nonlinear Normalization really used ??
	Eigen::MatrixXd sfmap = Eigen::MatrixXd::Zero(sig01.rows(), 3 * sig01.cols());
	sfmap.block(0, 0, sig01.rows(), sig01.cols()) = sig02;
	sfmap.block(0, sig01.cols(), sig01.rows(), sig01.cols()) = sig12;
	sfmap.block(0, 2 * sig01.cols(), sig01.rows(), sig01.cols()) = sig22;
	sfmap = getNonlinearNormalizationMatrix(sfmap); 
	sig02 = sfmap.block(0, 0, sig01.rows(), sig01.cols());
	sig12 = sfmap.block(0, sig01.cols(), sig01.rows(), sig01.cols());
	sig22 = sfmap.block(0, 2 * sig01.cols(), sig01.rows(), sig01.cols());
	*/
	sdof = sig02 + sig12 + sig22;
	// measure time
	//start = std::clock();
	// get weights for KNN in interval [0.1; 0.35]
	unsigned int nn = 0.1 * sdof.rows();
	Eigen::MatrixXd weights = getLLEWeightMatrix(sdof, nn);
	// get saliency map projection for 2D
	llesp = getEmbeddingCoordinateMatrix(weights, 10);
	Eigen::MatrixXd softsal2d = getGaussianWeightedAverageMatrix(llesp, sigma);
	// measure time
	//stop = std::clock();
	//duration = (stop - start) / (double) CLOCKS_PER_SEC;
	//start = stop;
	//std::cout << "LLE coordinates time = " << duration << std::endl;
	/*
	Eigen::VectorXi ret = getMaxSaliency(softsal2d);
	for (unsigned int i=0; i < ret.size(); ++i)
		ret(i) += 2; // curvature skew to dof matrix
	return ret;
	*/
	// get large values for saliency (1D norm) as initial keyframe set, up to 10% of keyframes
	Eigen::VectorXd sal1d = Eigen::ArrayXd::Zero(softsal2d.rows());
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(softsal2d.rows());
	for (unsigned int i=0; i < softsal2d.rows(); ++i)
		sal1d(i) = softsal2d.row(i).norm();	
	unsigned int kfidx = 0;
	// get local maxima saliency
	for (unsigned int i=1; i < sal1d.size() - 1; ++i)
		if ((sal1d(i) > sal1d(i-1)) && (sal1d(i) > sal1d(i+1)))
			ret(kfidx++) = i;
	unsigned int nkfs = 0.1 * sdof.rows(); // limit of number of keyframes to 10%
	// discard keyframes with small saliency
	while (kfidx > nkfs) {
		nn = 0;
		sigma = sal1d(ret(nn));
		for (unsigned int i=1; i < kfidx; ++i) 
			if (sigma < sal1d(ret(i))) {
				nn = i;
				sigma = sal1d(ret(i));
			}
		for (unsigned int i=nn; i < kfidx; ++i) 
			ret(i) = ret(i+1);
		--kfidx;
	}
	// curvature skew to dof matrix
	for (unsigned int i=0; i < kfidx; ++i) 
		ret(i) += 2; 
	return ret.head(kfidx);
}
