#include <saliency.h>
#include <iostream>
#include <cmath>
#include <cassert>

/**
 * @brief get curvature for given data matrix
 * @param data matrix
 * @return curvature matrix
 */
Eigen::MatrixXd Saliency::getCurvatureMatrix(Eigen::MatrixXd & mat) {
	assert(mat.rows() > 4);
	Eigen::MatrixXd dmat = Eigen::ArrayXXd::Zero(mat.rows() - 4, mat.cols());
	Eigen::MatrixXd ddmat = Eigen::ArrayXXd::Zero(mat.rows() - 4, mat.cols());
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(mat.rows() - 4, mat.cols());
	Eigen::VectorXd v0, v1, v2;

	// first and second derivatives
	for (unsigned int i=2; i < mat.rows() - 2; ++i) {
		// first derivative
		v0 = mat.row(i-1);
		v1 = mat.row(i+1);
		dmat.row(i-2) = (v1 - v0) / 2;
		// second derivative
		v0 = mat.row(i-2);
		v1 = mat.row(i);
		v2 = mat.row(i+2);
		ddmat.row(i-2) = (v2 + v0) / 4 - v1 / 2;
		// curvature formula C(Dij) = | D''ij | / (1 + D'ij ^2) ^ (3/2)
		for (unsigned int j=0; j < mat.cols(); ++j) {
			ret(i-2,j) = std::abs(ddmat(i-2,j)) / 
				std::pow((1 + dmat(i-2,j)*dmat(i-2,j)), 1.5);
		}
	}
	return ret;
}

/**
 * @brief get index of maximum saliency
 * @param smat saliency matrix
 * @return vector of maximun saliency index
 */
Eigen::VectorXi Saliency::getMaxSaliency(Eigen::MatrixXd & smat) {
	/*
	Eigen::MatrixXd dsmat = Eigen::ArrayXXd::Zero(smat.rows() - 4, smat.cols());
	Eigen::MatrixXd ddsmat = Eigen::ArrayXXd::Zero(smat.rows() - 4, smat.cols());
	Eigen::MatrixXd scurve = Eigen::ArrayXXd::Zero(smat.rows() - 4, smat.cols());
	Eigen::VectorXd v0, v1, v2;
	
	// calculate saliency
	for (unsigned int i=2; i < smat.rows() - 2; ++i) {
		// first derivative
		v0 = smat.row(i-1);
		v1 = smat.row(i+1);
		dsmat.row(i-2) = (v1 - v0) / 2;
		// second derivative
		v0 = smat.row(i-2);
		v1 = smat.row(i);
		v2 = smat.row(i+2);
		ddsmat.row(i-2) = (v2 + v0) / 4 - v1 / 2;
		// curvature formula C(Dij) = | D''ij | / (1 + D'ij ^2) ^ (3/2)
		for (unsigned int j=0; j < smat.cols(); ++j) {
			scurve(i-2,j) = std::abs(ddsmat(i-2,j)) / 
				std::pow((1 + dsmat(i-2,j)*dsmat(i-2,j)), 1.5);
		}
	}
	*/
	Eigen::MatrixXd scurve = getCurvatureMatrix(smat);
	// get max saliency
	unsigned int nmax = 0;
	for (unsigned int i=1; i < scurve.rows() - 1; ++i)
		for (unsigned int j=0; j < scurve.cols(); ++j) {
			if ((scurve(i,j) > scurve(i-1,j)) && 
				(scurve(i,j) > scurve(i+1,j))) {
				++nmax;
				break;
			}
		}
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(nmax);
	nmax = 0;
	for (unsigned int i=1; i < scurve.rows() - 1; ++i)
		for (unsigned int j=0; j < scurve.cols(); ++j) {
			if ((scurve(i,j) > scurve(i-1,j)) && 
				(scurve(i,j) > scurve(i+1,j))) {
				ret(nmax++) = i;
				break;
			}	
		}
	return ret;
}

/**
 * @brief return the one tail size of a 1D gaussian kernel
 * @param sigma standard deviation
 * @param threshold minimal probability which last kernel element should have
 * @return size of one tail kernel. To get full size, use 2*size + 1.
 */
int Saliency::getKernelSize(double sigma, double threshold) {
    int ws = -1; 
    double weight = 1.0;

    do {
        ++ws;
        weight = std::exp( -1*(ws)*(ws) / (2 * sigma));
    } while (weight > threshold);
    --ws;
    return ws; 
}

/**
 * @brief return the gaussian weighted average matrix, which is applied to
 * the matrix element column convoluted by the gaussian kernel
 * @param mat original data matrix
 * @param sigma standard deviation of kernel
 * @return gaussian weighted average matrix
 */
Eigen::MatrixXd Saliency::getGaussianWeightedAverageMatrix(Eigen::MatrixXd & mat, double sigma) {
	Eigen::MatrixXd gwamat = Eigen::MatrixXd::Zero(mat.rows(), mat.cols());
	double sum0, sum1, weight;

	for (int i=0;i < mat.rows(); ++i) 
		for (int j=0; j < mat.cols(); ++j) {
			sum0 = sum1 = 0;
			for (int k=i-sigma; k < i+sigma+1; ++k)
				if ((k >= 0) && (k < mat.rows())) {
					weight = std::exp( -1*(i - k)*(i - k) / (2 * sigma));
					sum0 += mat(k,j) * weight;
					sum1 += weight;
					
				}
			if (sum1 != 0) gwamat(i,j) = sum0 / sum1;
		}
	return gwamat;
}

/**
 * @brief return nonlinear normalization matrix.
 * @param mat matrix to be normalized
 * @return normalized matrix
 */
Eigen::MatrixXd Saliency::getNonlinearNormalizationMatrix(Eigen::MatrixXd & mat) {
	double maxh, minh;
	Eigen::VectorXd maxm = Eigen::ArrayXd::Zero(mat.cols());
	Eigen::VectorXd mavg = Eigen::ArrayXd::Zero(mat.cols());
	Eigen::VectorXi navg = Eigen::ArrayXi::Zero(mat.cols());
	Eigen::MatrixXd gmat = mat;
	
	// get global maximum and minimum
	maxh = minh = gmat(0,0);
	for (unsigned int i=0; i < gmat.rows(); ++i) 
		for (unsigned int j=0; j < gmat.cols(); ++j)
			if (gmat(i,j) > maxh) maxh = gmat(i,j);
			else if (gmat(i,j) < minh) minh = gmat(i,j);
	// normalize curves
	for (unsigned int i=0; i < gmat.rows(); ++i) 
		for (unsigned int j=0; j < gmat.cols(); ++j)
			gmat(i,j) = maxh * ((gmat(i,j) - minh) / (maxh - minh));
	// find average local maxima, excluding global maximum
	maxm = gmat.row(0);
	for (unsigned int i=1; i < gmat.rows(); ++i) 
		for (unsigned int j=0; j < gmat.cols(); ++j)
			if (gmat(i,j) > maxm(j)) maxm(j) = gmat(i,j);
			
	for (unsigned int i=1; i < gmat.rows() - 1; ++i) 
		for (unsigned int j=0; j < gmat.cols(); ++j)
			if ((gmat(i,j) > gmat(i-1,j)) && 
				(gmat(i,j) > gmat(i+1,j)) && 
				(gmat(i,j) != maxm(j))) {
				++navg(j);
				mavg(j) += gmat(i,j);
			}

	// calculate (Mi - mi)^2
	for (unsigned int j=0; j < mavg.size(); ++j) {
		if (navg(j) != 0) {
			mavg(j) /= navg(j);
			mavg(j) = (maxm(j) - mavg(j))*(maxm(j) - mavg(j));
		} else mavg(j) = 1.0;
	}
	for (unsigned int i=0; i < gmat.rows(); ++i) 
		for (unsigned int j=0; j < gmat.cols(); ++j)
			gmat(i,j) *= mavg(j);
	return gmat;
}

/**
 * @brief get saliency curve for the hole frame
 * @param mat curvature matrix by DOFs or principal components
 * @return vector of saliency curve by frame id
 *
Eigen::VectorXd Saliency::getSaliencyByFrame(Eigen::MatrixXd & mat) {
	Eigen::VectorXd sig01, sig02, sig11, sig12, sig21, sig22, sig31, sig32;
	double sigma = 10.0;
	
	sig01 = getGaussianWeightedAverageVector(mat, sigma);
	sig02 = getGaussianWeightedAverageVector(mat, 2*sigma);
	sig01 -= sig02;
	sig02 = sig01.cwiseAbs();
	sig11 = getGaussianWeightedAverageVector(mat, 1.5*sigma);
	sig12 = getGaussianWeightedAverageVector(mat, 3*sigma);
	sig11 -= sig12;
	sig12 = sig11.cwiseAbs();
	sig21 = getGaussianWeightedAverageVector(mat, 2*sigma);
	sig22 = getGaussianWeightedAverageVector(mat, 4*sigma);
	sig21 -= sig22;
	sig22 = sig21.cwiseAbs();
	sig31 = getGaussianWeightedAverageVector(mat, 2.5*sigma);
	sig32 = getGaussianWeightedAverageVector(mat, 5*sigma);
	sig31 -= sig32;
	sig32 = sig31.cwiseAbs();
	Eigen::MatrixXd sfmap = Eigen::MatrixXd::Zero(sig01.size(),4);
	sfmap.col(0) = sig02;
	sfmap.col(1) = sig12;
	sfmap.col(2) = sig22;
	sfmap.col(3) = sig32;
	sfmap = getNonlinearNormalizationMatrix(sfmap);
	sig01 = sfmap.rowwise().sum();
	return sig01;
}
* 
 *
 * @brief return the gaussian weighted average vetor, which is the matrix row
 * convoluted by the gaussian kernel
 * @param mat original data matrix
 * @param sigma standard deviation of kernel
 * @return gaussian weighted average vector
 *
Eigen::VectorXd Saliency::getGaussianWeightedAverageVector(Eigen::MatrixXd & mat, double sigma) {
	Eigen::VectorXd map = Eigen::VectorXd::Zero(mat.rows());
	Eigen::VectorXd v1;
	double sum0, sum1, weight;

	for (int i=0;i < mat.rows(); ++i) 
		for (int j=0; j < mat.cols(); ++j) {
			sum0 = sum1 = 0;
			for (int k=i-sigma; k < i+sigma+1; ++k)
				if ((k >= 0) && (k < mat.rows())) {
					v1 = mat.row(k);
					weight = std::exp( -1 * (i - k)*(i - k) / (2 * sigma));
					// norm or mean ? None
					sum0 += v1.mean() * weight;
					//sum0 += v1.norm() * weight;
					sum1 += weight;
				}
			if (sum1 != 0) map(i) = sum0 / sum1;
		}
	return map;
}

*/
