#ifndef GLMAB_H
#define GLMAB_H
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

class glmArcball {

	protected:

	glm::dvec3 center, tcenter, eye, inivet, currvet;
	glm::dmat4 mrot, msave, marcb, mview, pm, trto, trout;
	glm::ivec4 vp;
	double radius, ceye_radius2, teye_radius2;
	glm::dvec3 getABVec(int xs, int ys);

	public:

	glmArcball(glm::vec3 const & sc, glm::vec3 const & e, float sr);
	glmArcball(glm::dvec3 const & sc, glm::dvec3 const & e, double sr);
	// ~glmArcball() {}
	void start(int xs, int ys);
	void move(int xs, int ys);
	double * getMultMatrixd();
	glm::vec3 getProjectedPoint(int x, int y, int z, int wh);
	glm::vec3 getNewPointPosition(glm::dvec3 const & p);
	glm::vec3 getEyePosition();
	void reset();
};

#endif
