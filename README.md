# projMocapSkf

C++ implementation of Motion Capture dimension reduction to be used with Spatial Key Frame interpolation for reconstruction. This project is the coding part of the work published as [Enhancing Spatial Keyframe Animations with Motion Capture](https://doi.org/10.5220/0007296800310040) in GRAPP 2019 and subsequent publications. It features the dimension reduction of mocap files to be encoded in 2D space and later reconstructed using [Spatial Keyframe](https://www.coursera.org/lecture/interactive-computer-graphics/4-3-spatial-key-framing-gQPxL) as a back-projection algorithm. The code is organized in directories as follows:

- **emscripten**: contains CPP files for generating [WebAssembly](https://emscripten.org/docs/compiling/WebAssembly.html) applications to run as javascript functions in browsers. This is needed to be able to use this library with javascript applications such as this [Mocap Projection Browser](https://observablehq.com/@bfcosta/mocap-projection-browser)  fork. Use rule *make js* to build javascript files that provides the WebAssembly code. The [emscripten SDK](https://emscripten.org/docs/getting_started/downloads.html) is needed for building WebAssembly code.
- **lib**: the core library files. It contains code for reading motion capture data, selecting key frames and generating an interpolated version of this data for the given key frames, as well as the 2D projection of this data for the implemented projectors. The applications for doing these tasks are inside the *tools* directory.
- **sbox**: code mostly developed as a "proof of concept" to understand some ideas about motion capture data processing in the literature.
- **tools**: home of most of the important applications. This directory is a set of executables capable of reading motion capture data and processing it for generating the key frame selection and a 2D mocap projection. It also features code of useful debugging tools.
- **utest**: applications developed for testing current library.

## Build targets

This library is built using *make* command line an in the example above. 

```
$ make
```

It is possible to use other specific targets for building specific parts of the project. Here is a complete description of 
targets available for building.

- **all**: default behavior is to build just the core applications, which is the same as running *core* target.
- **sandbox**: build what is under *sbox* directory.
- **test**: build what is under *utest* diretcoty.
- **tools**: build everything that is under *tools* directory. The same as running *core*,*player* and *util* targets.
- **core**: build the needed applications under *tools* directory to process and generate data that was used to write the published work.
- **player**: build two command line players, one for playing single mocap files and another for playing the spatial key frame reconstruction of the file.
- **util**: build some other applications mostly useful for debugging purposes.
- **cpp**: build all CPP code, except what is under *emscripten* directory. 
- **js**: build all what is under *emscripten* directory. These are basically two applications that generate WebAssembly code in a single js file to be used as an imported javascript library in a browser.

## Applications

Here we describe the most important applications available. These are built using the *core* target.

- **saveproj**: this is the main application. It reads a BVH file, select a given percentage of key frames specified at the command line and outputs a chosen projection in a file, reporting the average error between the "keyframed" reconstruction and original mocap as well as the processing time. It uses spatial key frame interpolation for reconstructing mocap data. The projection file is a matrix where the first two columns are the projection coordinates and the third is a flag that reports if the frame (matrix line) is a key frame or not (having values 1 or 0 respectively).
- **runtemp**: it is basically the same application as *saveproj* but, instead of using spatial key frame interpolation, it uses common temporal interpolation schemes such as *Slerp* or *Hermite* interpolation. 
- **proj2us**: outputs a new projection file where the amount of key frames is given in the command line and these are chosen using Uniform Sampling (US) selection strategy. It requires a given original projection and the two first columns are not modified. Error measures for this new file must be taken using *impvbvhproj* application.
- **stress**: reports the stress measure for a given BVH file and its 2D projection in the same output format given by *saveproj* application.
- **impvbvhproj**: outputs a new projection file with an optimized version of an original projection and its related BVH file. Also reports original and optimized reconstruction error measures, as well as processing time and number of iterations that the algorithm took to build the optimized projection. It implements the core projection optimization algorithm.
- **impvuciproj**: this is basically the *impvbvhproj* application adapted to read data mostly in other formats rather than motion captured data. It is useful to deal with datasets such as the [UCI dataset](http://archive.ics.uci.edu/ml/index.php).

For debugging purposes, one might be interested in other applications in the *tools* directory. Some of these applications might be deprecated.

- **dumpanim**, **dumplinear** and **dumpmixed**: these applications output the reconstructed mocap interpolation in the BVH format which is able to be seen in many BVH players. The first uses spatial key frame interpolation for reconstruction and the second uses *Slerp*. The third reconstructs two mocap files together with their own projections using both projections as a shared space of poses.
- **rbfpbvhsmoother** and **rbfptest**: these applications implement the RBF projection for mocap files or other datasets as the [UCI dataset](http://archive.ics.uci.edu/ml/index.php). This is **deprecated**. Please consider using *saveproj* together with *impvbvhproj* application if using BVH files.
- **reader** and **skfmocap**: two players, the first reproduces a single mocap file and the second the spatial key frame interpolation of this file together with its 2D projection. This is **deprecated**. Please consider using *dumpanim* to produce the reconstructed mocap in the BVH format.

## Generating code documentation

This code was written using [Doxygen](http://www.doxygen.nl/index.html) as a reference tool for generating documentation. Running the command above will create a directory named *docs* where the complete library documentation is available.

```
$ doxygen mocapreader.dox
```
