#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>
#include <Eigen/Dense>
#include <bvhreader.h>
#include <kfinterpolator.h>
#include <skanalizer.h>

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset BVH input file: -b" << std::endl;
	std::cout << "\tset output matrix file: -m" << std::endl;
	std::cout << name << " -b [BHV input file] -m [output matrix file]" << std::endl;
}

/**
 * @brief save matrix
 * @param hmat matrix to be saved
 * @param fname filename for saving data
 */
void saveMatrix(Eigen::MatrixXd & hmat, std::string fname) {
    std::ofstream savedfile;

    savedfile.open(fname.c_str());
    savedfile << hmat << std::endl;
    savedfile.close();
}

int main(int argc, char ** argv) {
    int opt;
	std::string inbvh, outfile;
	bool emask[3] = {false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "bm")) != -1 ) {
        switch (opt) {
            case 'b':
				inbvh = argv[optind];
				emask[0] = true;
                break;
            case 'm':
				outfile = argv[optind];
				emask[1] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[2] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* BVH input file" << std::endl;
		if (!emask[1]) std::cout << "* output matrix file" << std::endl;
		exit(0);
	}
	if (!emask[2]) exit(0);
	//
	GeodesicSkeletonAnalizer skan;
	SkeletonAnalizer * skdist = &skan;
	MocapReader * mocap = new BVHReader();
	mocap->readFile(inbvh);
	LinearInterpolator lin(mocap, skdist, 0);
	unsigned int tf = mocap->getNumberOfFrames();
	Eigen::VectorXi fref = Eigen::ArrayXi::Zero(tf);
	for (unsigned int i=0; i < fref.size(); ++i)
		fref(i) = i;
	Eigen::MatrixXd dmat = lin.getDistanceMatrix(fref);
	saveMatrix(dmat, outfile);
	if (mocap != NULL) delete mocap;
	return 0;
}
