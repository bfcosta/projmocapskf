#include <iostream>
#include <cstdlib>
#include <Eigen/Dense>
#include <asfreader.h>
#include <bvhreader.h>
#include <animation.h>
#include <renderhelper.h>

Animation *skfanim = NULL;
// nao usado
Eigen::Vector3d eye, center, up;
double zn,zf;

void loadAnimation(Eigen::MatrixXd const & line, MocapReader * rd) {
	unsigned int id;
	Eigen::Vector4d v;
	Skeleton s;
	
	v << 0,0,0,1;
	for (unsigned int i=0; i < line.rows(); ++i) {
		v(0) = line(i,0);
		v(1) = line(i,1);
		id = (unsigned int) line(i,2);
		s = rd->getFrameSkeleton(id);	
		skfanim->addNewKeyFrame(v, s);
	}
	v(0) = line(0,0);
	v(1) = line(0,1);
	skfanim->setController(v);
	skfanim->setContext();
}

int main(int argc, char ** argv) {
	MocapReader *mocap = NULL;
	Eigen::MatrixXd mpos, fproj;
	unsigned int totframes;
	Skeleton ms;
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos;
	
	// read files
	if ((argc < 3) || (argc > 4)) {
		std::cout << "syntax: " << argv[0] << 
			" [mocap file [animation file]] [spatial keyframe file]" << std::endl;
		exit(1);
	}
    std::string fname(argv[1]);
    std::string bname(basename(argv[1]));
    std::string ext = bname.substr(bname.length() - 3, 3);	
	if (ext.compare("bvh") == 0) {
        mocap = new BVHReader();
        mocap->readFile(fname);
        readMarkersPosition(argv[2], mpos, fproj);
    } else if (ext.compare("asf") == 0) {
        mocap = new ASFReader();
        mocap->readFile(fname);
        if (argc > 2) {
            fname = argv[2];
            bname = basename(argv[2]);
            ext = bname.substr(bname.length() - 3, 3); 
            if (ext.compare("amc") == 0) {
                mocap->readFile(fname);
                readMarkersPosition(argv[3], mpos, fproj);
            }
        }
    }
    totframes = mocap->getNumberOfFrames();
	ms = mocap->getFrameSkeleton(0);
	skfanim = new Animation();
	skfanim->setSkeleton(ms);
	loadAnimation(mpos, mocap);
	//mocap->print();
	if (argc == 4) {
		std::cout << ":FULLY-SPECIFIED" << std::endl;
		std::cout << ":DEGREES" << std::endl;
	}
    for (unsigned int i=0; i < totframes; i++) {
		if (argc == 4) std::cout << i+1 << std::endl;
		ms = mocap->getFrameSkeleton(i);
		v0 = ms.getJointPosition(0);
		cpos = fproj.row(i);
		skfanim->setController(cpos);
		skfanim->animate();
		ms = skfanim->getSkeleton();
		ms.setRootPosition(v0);
		mocap->printPose(ms, std::cout);
	}
	if (skfanim != NULL) delete skfanim;
	if (mocap != NULL) delete mocap;
	return 0;
}
