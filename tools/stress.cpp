#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>
#include <Eigen/Dense>
#include <bvhreader.h>
#include <kfinterpolator.h>
#include <skanalizer.h>
#include <unistd.h>

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset BVH input file: -b" << std::endl;
	std::cout << "\tset input projection file: -p" << std::endl;
	std::cout << name << " -b [BHV input file] -p [input projection file]" << std::endl;
}

/**
 * @brief read projection matrix from file
 * @param fname file name to be read
 * @param rd mocap session handler
 * @return projection matrix
 */
Eigen::MatrixXd readProjection(std::string const & fname, 
	MocapReader * rd, unsigned int cols = 3) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	Eigen::Vector4d v;

	// read matrix
	infile.open(fname.c_str());
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				data.push_back(std::atof(buffer.c_str()));
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}

	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	
	return ret;
}

int main(int argc, char ** argv) {
    int opt;
	std::string inbvh, inproj;
	bool emask[3] = {false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "bp")) != -1 ) {
        switch (opt) {
            case 'b':
				inbvh = argv[optind];
				emask[0] = true;
                break;
            case 'p':
				inproj = argv[optind];
				emask[1] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[2] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* BVH input file" << std::endl;
		if (!emask[1]) std::cout << "* input projection file" << std::endl;
		exit(0);
	}
	if (!emask[2]) exit(0);
	//
	GeodesicSkeletonAnalizer skan;
	SkeletonAnalizer * skdist = &skan;
	MocapReader * mocap = new BVHReader();
	mocap->readFile(inbvh);
	LinearInterpolator lin(mocap, skdist, 0);
	unsigned int tf = mocap->getNumberOfFrames();
	Eigen::VectorXi fref = Eigen::ArrayXi::Zero(tf);
	for (unsigned int i=0; i < fref.size(); ++i)
		fref(i) = i;
	Eigen::MatrixXd dmat = lin.getDistanceMatrix(fref);
	Eigen::MatrixXd fproj = readProjection(inproj, mocap);
	// get stress
	double s0 = 0, s1 = 0, ldist;
	Eigen::VectorXd v0, v1;
	for (unsigned int i=0; i < dmat.rows(); ++i) {
		v0 = fproj.row(i).head(2);
		for (unsigned int j=i+1; j < dmat.rows(); ++j) {
			v1 = fproj.row(j).head(2);
			v1 -= v0;
			ldist = v1.norm();
			s0 += (ldist - dmat(i,j))*(ldist - dmat(i,j));
            s1 += ldist*ldist;
		}
	}
    s0 = s0 / s1;
	std::cout << "stress = " << s0 << std::endl;
	if (mocap != NULL) delete mocap;
	return 0;
}
