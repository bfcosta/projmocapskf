#include <iostream>
#include <cstdlib>
#include <Eigen/Dense>
#include <asfreader.h>
#include <bvhreader.h>
#include <animation.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <renderhelper.h>

Eigen::MatrixXd mpos, fproj;
Eigen::VectorXd cpos;
unsigned int framesec, totframes, currfid;
int mainwin, subwc, subwm, subwa;
bool play, mcshow, skfshow, zipshow;
Skeleton *mcskel, *skfskel, *zipskel;
Animation *skfanim = NULL, *zipanim = NULL;
MocapReader *mocap;
Eigen::Vector3d eye, center, up;
double zn,zf;
double fovy = 45.0;
const int views = 3; // number of windows constant => 2 || 3

void reshape(int w, int h) {
	// controller subwindow
	glutSetWindow(subwc);
	glutPositionWindow(0, 0);
	glutReshapeWindow(w/views, h);
	glViewport(0, 0, w/views, h);
 	glMatrixMode(GL_PROJECTION);
 	glLoadIdentity();
	glOrtho (-0.5, 1.5, -0.5, 1.5, -1.0, 1.0);
 	glMatrixMode(GL_MODELVIEW);
 	glLoadIdentity();

	// mocap subwindow
	glutSetWindow(subwm);
	glutPositionWindow(w/views, 0);
	glutReshapeWindow(w/views, h);
    glViewport(0, 0, w/views, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fovy, ((float) w)/ h, zn, zf);
    gluLookAt(eye(0), eye(1), eye(2), 
		center(0), center(1), center(2), 
		up(0), up(1), up(2));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	
	// animation subwindow
	if (views == 3) {
		glutSetWindow(subwa);
		glutPositionWindow(2*w/views, 0);
		glutReshapeWindow(w/views, h);
		glViewport(0, 0, w/views, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fovy, ((float) w)/ h, zn, zf);
		gluLookAt(eye(0), eye(1), eye(2),
			center(0), center(1), center(2),
			up(0), up(1), up(2));
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
}

void displayController() {
	glutSetWindow(subwc);
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.85,0.85,0);
	for (unsigned int i=0; i < mpos.rows(); ++i)
		drawFilledCircle(mpos(i,0), mpos(i,1), 0, 0.02);
	glColor3f(1,0.5,0.5);
	drawFilledCircle(cpos(0), cpos(1), 0, 0.02);
	if (fproj.rows() > 0) {
		glColor3f(0.5,0.5,1.0);
		for (unsigned int i=0; i < fproj.rows(); ++i) {
			if (i == currfid)
				drawFilledCircle(fproj(i,0), fproj(i,1), 0, 0.015);
			else
				drawFilledCircle(fproj(i,0), fproj(i,1), 0, 0.005);
		}
	}
	glutSwapBuffers();
}

void displayMocap() {
	glutSetWindow(subwm);
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//drawAxis();
	if ((skfskel != NULL) && (skfshow)) skfskel->draw(GL_RENDER);
	if ((zipskel != NULL) && (zipshow)) zipskel->draw(GL_RENDER);
	if ((mcskel != NULL) && (mcshow)) mcskel->draw(GL_RENDER);
	glutSwapBuffers();
}

void displayAnimation() {
	glutSetWindow(subwa);
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//drawAxis();
	if (skfskel != NULL) skfskel->draw(GL_RENDER);
	glutSwapBuffers();
}

void getFrame(int value) {
	// controller
	double fp = (double) value;
	unsigned int r, f0, f1;
	Eigen::VectorXd v0, v1;

	r = 0;
	for (unsigned int i=0; i < mpos.rows(); ++i)
		if (mpos(i,2) >= fp) {
			r = i;
			break;
		}
	if (r == 0) 
		cpos = mpos.row(0).head(2); 
	else {
		if (fproj.rows() > 0)
			cpos = fproj.row(value).head(2);
		else {
			f0 = mpos(r - 1, 2);
			f1 = mpos(r, 2);
			v0 = mpos.row(r-1).head(2);
			v1 = mpos.row(r).head(2);
			fp = ((double) (value - f0)) / (f1 - f0);
			v1 = fp * (v1 - v0);
			cpos = v0 + v1;
		}
	}
	//std::cout << "cpos = " << cpos.transpose() << std::endl;
	//std::cout << "frame id = " << value << std::endl;
	glutSetWindow(subwc);
	glutPostRedisplay();
	
	// skf animation
	Skeleton fs = mocap->getFrameSkeleton(value);
	mcskel->setSkeleton(fs);
	Eigen::Vector4d c;
	c << 0,0,0,1;
	c(0) = cpos(0);
	c(1) = cpos(1);
	skfanim->setController(c);
	skfanim->animate();
	v0 = fs.getJointPosition(0);
	fs = skfanim->getSkeleton();
	fs.setRootPosition(v0);
	skfskel->setSkeleton(fs);
	// zip animation
	zipanim->setController(value);
	zipanim->animate();
	fs = zipanim->getSkeleton();
	zipskel->setSkeleton(fs);
	
	glutSetWindow(subwm);
    glutPostRedisplay();
    if (views == 3) {
		glutSetWindow(subwa);
		glutPostRedisplay();
	}
    
	if (play) {
		currfid = value;
		//std::cout << "frame = " << currfid << std::endl;
		glutTimerFunc(framesec, getFrame, (currfid + 1) % totframes);
	} else glutTimerFunc(framesec, getFrame, currfid % totframes);
}

void loadAnimation(Eigen::MatrixXd const & line, MocapReader * rd) {
	unsigned int id;
	Eigen::Vector4d v;
	Skeleton s;
	
	v << 0,0,0,1;
	for (unsigned int i=0; i < line.rows(); ++i) {
		v(0) = line(i,0);
		v(1) = line(i,1);
		id = (unsigned int) line(i,2);
		s = rd->getFrameSkeleton(id);	
		skfanim->addNewKeyFrame(v, s);
		zipanim->addNewKeyFrame(id, s);
	}
	v(0) = line(0,0);
	v(1) = line(0,1);
	skfanim->setController(v);
	skfanim->setContext();
	id = (unsigned int) line(0,2);
	zipanim->setController(id);
	zipanim->setContext();
}

void keyboard(unsigned char key, int x, int y) {
 	switch (key) {
  		case 27:
			glutDestroyWindow(subwc);
			glutDestroyWindow(subwm);
			if (views == 3)
				glutDestroyWindow(subwa);
			glutDestroyWindow(mainwin);
   			exit(0);
   			break;
   		case 49:
			mcshow = !mcshow;
			break;
   		case 50:
			skfshow = !skfshow;
			break;
   		case 51:
			zipshow = !zipshow;
			break;
        case 80: 
        case 112:
			std::cout << "play" << std::endl;
			play = true;
            break;
        default:
			play = false;
			std::cout << "stop" << std::endl;
 	}
}

void handleArrowKeys(int key, int x, int y) {
	switch(key) {
		case GLUT_KEY_UP:
		case GLUT_KEY_RIGHT:
			play = false;
			currfid = (currfid + 1) % totframes;
			std::cout << "frame = " << currfid << std::endl;
			break;	
		case GLUT_KEY_DOWN:
		case GLUT_KEY_LEFT:
			play = false;
			if (currfid == 0) currfid = totframes - 1;
			else currfid = (currfid - 1) % totframes;
			std::cout << "frame = " << currfid << std::endl;
			break;
		default:
		break;
	}
}

int main(int argc, char ** argv) {
	Skeleton ms, as, zs;
	int w = 600, h = 600;
	
	// read files
	if ((argc < 3) || (argc > 4)) {
		std::cout << "syntax: " << argv[0] << 
			" [mocap file [animation file]] [spatial keyframe file]" << std::endl;
		exit(1);
	}
    std::string fname(argv[1]);
    std::string bname(basename(argv[1]));
    std::string ext = bname.substr(bname.length() - 3, 3);	
	if (ext.compare("bvh") == 0) {
        mocap = new BVHReader();
        mocap->readFile(fname);
        readMarkersPosition(argv[2], mpos, fproj);
    } else if (ext.compare("asf") == 0) {
        mocap = new ASFReader();
        mocap->readFile(fname);
        if (argc > 2) {
            fname = argv[2];
            bname = basename(argv[2]);
            ext = bname.substr(bname.length() - 3, 3); 
            if (ext.compare("amc") == 0) {
                mocap->readFile(fname);
                readMarkersPosition(argv[3], mpos, fproj);
            }
        }
    }
    ms = mocap->getFrameSkeleton(0);
	ms.setColor(0.17,0.36,0.93);
	ms.setThickness(2);
    mcskel = &ms;
	currfid = 0;
	cpos = mpos.row(currfid).head(2);
	framesec = mocap->getFrameInterval();
	totframes = mocap->getNumberOfFrames();
	Eigen::MatrixXd dofs = mocap->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd skpos = dofs.block(0, 0, dofs.rows(), 3);
	setMocapPerspective(skpos);
	//setDefaultPerspective(mcskel->getCenter());
	play = false;
	mcshow = true;
	skfshow = true;
	zipshow = false;
	//
	skfanim = new Animation();
	as = mocap->getFrameSkeleton(0);
	as.setColor(0.79,0.18,0.1);
	//as.setThickness(1.75);
	skfanim->setSkeleton(as);
	skfskel = &as;
	//
	zipanim = new Animation();
	zs = mocap->getFrameSkeleton(0);
	zs.setColor(0.43,0.12,0.89);
	//zs.setThickness(1.75);
	zipanim->setSkeleton(zs);
	zipskel = &zs;
	loadAnimation(mpos, mocap);
	// render animation
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(views*w, h);
	//glutInitWindowSize (w, h);
	mainwin = glutCreateWindow("Spatial KF");
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(handleArrowKeys);
	subwc = glutCreateSubWindow(mainwin, 0, 0, w, h);
	glutDisplayFunc(displayController);
	subwm = glutCreateSubWindow(mainwin, w, 0, w, h);
	glutDisplayFunc(displayMocap);
	if (views == 3) {
		subwa = glutCreateSubWindow(mainwin, 2*w, 0, w, h);
		glutDisplayFunc(displayAnimation);
	}
	glutTimerFunc(framesec, getFrame, currfid % totframes);
	glutMainLoop();
	if (skfanim != NULL) delete skfanim;
	return 0;
}
