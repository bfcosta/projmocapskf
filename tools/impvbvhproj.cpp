#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <Eigen/Dense>
#include <asfreader.h>
#include <bvhreader.h>
#include <animation.h>
#include <skanalizer.h>
#include <unistd.h>

Animation *skfanim = NULL;

/**
 * @brief rescale markers for [0,1] interval
 * @param proj marker matrix to be updated
 */
void rescaleMarkers(Eigen::MatrixXd & proj) {
	double minx, miny, maxx, maxy;
	minx = maxx = proj(0,0);
	miny = maxy = proj(0,1);
	for (unsigned int i=1; i < proj.rows(); ++i) {
		if (proj(i,0) < minx) minx = proj(i,0);
		else if (proj(i,0) > maxx) maxx = proj(i,0);
		if (proj(i,1) < miny) miny = proj(i,1);
		else if (proj(i,1) > maxy) maxy = proj(i,1);
	}
	maxx -= minx;
	maxy -= miny;
	if (maxy > maxx) maxx = maxy;
	for (unsigned int i=0; i < proj.rows(); ++i) {
		proj(i,0) = (proj(i,0) - minx) / maxx;
		proj(i,1) = (proj(i,1) - miny) / maxx;
	}
}

/**
 * @brief read projection matrix from file and load animation
 * @param fname file name to be read
 * @param rd mocap session handler
 * @param cols number of columns of given matrix
 * @return projection matrix
 */
Eigen::MatrixXd readProjMatrixAndLoadAnimation(std::string const & fname, 
	MocapReader * rd, unsigned int cols = 3) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	Eigen::Vector4d v;
	Skeleton s;

	// read matrix
	infile.open(fname.c_str());
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				data.push_back(std::atof(buffer.c_str()));
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}

	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	
	// load animation
	s = rd->getFrameSkeleton(0);
	skfanim = new Animation();
	skfanim->setSkeleton(s);
	v << 0,0,0,1;
	for (unsigned int i=0; i < ret.rows(); ++i) {
		if (ret(i,2) != 0) {
			v(0) = ret(i,0);
			v(1) = ret(i,1);
			s = rd->getFrameSkeleton(i);	
			skfanim->addNewKeyFrame(v, s);
		}
	}
	v(0) = ret(0,0);
	v(1) = ret(0,1);
	skfanim->setController(v);
	skfanim->setContext();
	return ret;
}

/**
 * @brief get the error vector of a reconstructed RBF/SKF animation for a given
 * frame projection matrix
 * @param proj frame projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @return error vector
 */
Eigen::VectorXd getErrorVector(Eigen::MatrixXd const & proj, MocapReader * rd, SkeletonAnalizer * skdist) {
	Eigen::VectorXd cerr = Eigen::ArrayXd::Zero(proj.rows());
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos;
	Skeleton sk0, sk1;
	
	cpos(2) = 0;
	for (unsigned int i=1; i < cerr.size() - 1; ++i) {
		sk0 = rd->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		// current error
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		skfanim->setController(cpos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		cerr(i) = skdist->getPoseDistance(sk0, sk1);
	}
	return cerr;
}

/**
 * @brief get the gradient matrix of a 2D pose projection with inverse projection
 * given by RBF/SKF.
 * @param proj pose projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @param cerr error vector
 * @param alpha gain parameter
 * @return gradient matrix
 */
Eigen::MatrixXd getGradientMatrix(Eigen::MatrixXd const & proj, MocapReader * rd, 
	SkeletonAnalizer * skdist, Eigen::VectorXd const & cerr, double alpha) {
		
	Eigen::MatrixXd error = Eigen::ArrayXXd::Zero(proj.rows(), 5);
	Eigen::MatrixXd grad = Eigen::ArrayXXd::Zero(proj.rows(), 2);
	Skeleton sk0, sk1;
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos, cbpos, capos;

	// get NSEW delta error
	cpos(2) = 0;
	capos(2) = 0;
	cbpos(2) = 0;
	sk0 = rd->getFrameSkeleton(0);
	v0 = sk0.getJointPosition(0);
	cpos.head(2) = proj.row(0).head(2);
	cpos(3) = 1;
	capos.head(2) = proj.row(1).head(2);
	capos(3) = 1;
	capos -= cpos;
	error(0,4) = capos.norm(); // circle radius
	capos = cpos;
	capos(0) += alpha*error(0,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(0,0) = skdist->getPoseDistance(sk0, sk1);
	error(0,0) -= cerr(0);
	// delta west error
	capos = cpos;
	capos(0) -= alpha*error(0,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(0,1) = skdist->getPoseDistance(sk0, sk1);
	error(0,1) -= cerr(0);
	// delta north error
	capos = cpos;
	capos(1) += alpha*error(0,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(0,2) = skdist->getPoseDistance(sk0, sk1);
	error(0,2) -= cerr(0);
	// delta south error
	capos = cpos;
	capos(1) -= alpha*error(0,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(0,3) = skdist->getPoseDistance(sk0, sk1);
	error(0,3) -= cerr(0);	
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		// delta east error
		sk0 = rd->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		cbpos.head(2) = proj.row(i-1).head(2);
		cbpos(3) = 1;
		capos.head(2) = proj.row(i+1).head(2);
		capos(3) = 1;
		cbpos -= cpos;
		capos -= cpos;
		error(i,4) = (cbpos.norm() + capos.norm()) / 2; // circle radius
		capos = cpos;
		capos(0) += alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,0) = skdist->getPoseDistance(sk0, sk1);
		error(i,0) -= cerr(i);
		// delta west error
		capos = cpos;
		capos(0) -= alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,1) = skdist->getPoseDistance(sk0, sk1);
		error(i,1) -= cerr(i);
		// delta north error
		capos = cpos;
		capos(1) += alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,2) = skdist->getPoseDistance(sk0, sk1);
		error(i,2) -= cerr(i);
		// delta south error
		capos = cpos;
		capos(1) -= alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,3) = skdist->getPoseDistance(sk0, sk1);
		error(i,3) -= cerr(i);
	}
	sk0 = rd->getFrameSkeleton(error.rows()-1);
	v0 = sk0.getJointPosition(0);
	cpos.head(2) = proj.row(error.rows()-1).head(2);
	cpos(3) = 1;
	capos.head(2) = proj.row(error.rows()-2).head(2);
	capos(3) = 1;
	capos -= cpos;
	error(error.rows()-1,4) = capos.norm(); // circle radius
	capos = cpos;
	capos(0) += alpha*error(error.rows()-1,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(error.rows()-1,0) = skdist->getPoseDistance(sk0, sk1);
	error(error.rows()-1,0) -= cerr(error.rows()-1);
	// delta west error
	capos = cpos;
	capos(0) -= alpha*error(error.rows()-1,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(error.rows()-1,1) = skdist->getPoseDistance(sk0, sk1);
	error(error.rows()-1,1) -= cerr(error.rows()-1);
	// delta north error
	capos = cpos;
	capos(1) += alpha*error(error.rows()-1,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(error.rows()-1,2) = skdist->getPoseDistance(sk0, sk1);
	error(error.rows()-1,2) -= cerr(error.rows()-1);
	// delta south error
	capos = cpos;
	capos(1) -= alpha*error(error.rows()-1,4);
	skfanim->setController(capos);
	skfanim->animate();
	sk1 = skfanim->getSkeleton();
	sk1.setRootPosition(v0);
	error(error.rows()-1,3) = skdist->getPoseDistance(sk0, sk1);
	error(error.rows()-1,3) -= cerr(error.rows()-1);	
	for (unsigned int i=0; i < error.rows(); ++i) {
		grad(i,0) = 0;
		grad(i,1) = 0;
		if ((error(i,0) < 0) || (error(i,1) < 0)) {
			grad(i,0) = error(i,1) - error(i,0);
			//if (error(i,0) <  error(i,1)) {
			//	grad(i,0) = -1*error(i,0);
			//} else {
			//	grad(i,0) = error(i,1);
			//}
		}
		if ((error(i,2) < 0) || (error(i,3) < 0)) {
			grad(i,1) = error(i,3) - error(i,2);
			//if (error(i,2) <  error(i,3)) {
			//	grad(i,1) = -1*error(i,2);
			//} else {
			//	grad(i,1) = error(i,3);
			//}
		}
	}
	for (unsigned int i=0; i < grad.rows(); ++i) {
		if (proj(i,2) == 1)
			grad.row(i) *= 0;
		if ((grad(i,0) != 0) || (grad(i,1) != 0)) {
			grad.row(i).normalize();
			grad.row(i) *= error(i,4);
		}
	}
	return grad;
}
/**
 * @brief improve a given projection matrix
 * @param proj pose projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @return improved frame projection matrix
 */
Eigen::MatrixXd improveProjection(Eigen::MatrixXd const & proj, MocapReader * rd, SkeletonAnalizer * skdist) {
	Eigen::MatrixXd grad;
	Eigen::VectorXd cerr;
	Eigen::MatrixXd ret = proj;
	double tini, total0, total1, alpha = 0.5;
	unsigned int i;
	
	cerr = getErrorVector(ret, rd, skdist);
	tini = total0 = cerr.sum();
	//std::cout << "original projection error = " << total0 << std::endl;
	for (i=0; i < 200; ++i) {
		grad = getGradientMatrix(ret, rd, skdist, cerr, alpha);
		ret.block(0, 0, ret.rows(), grad.cols()) += alpha * grad;
		cerr = getErrorVector(ret, rd, skdist);	
		total1 = cerr.sum();
		//std::cout << "delta projection error = " << total1 << std::endl;
		if (total1 == total0) break;
		total0 = total1;
	}
	total1 = 100 * (tini - total0) / tini;
	tini /= rd->getNumberOfDOFs() * rd->getNumberOfFrames();
	total0 /= rd->getNumberOfDOFs() * rd->getNumberOfFrames();
	std::cout << "original projection error = " << tini << std::endl;
	std::cout << "delta projection error = " << total0 << std::endl;
	std::cout << "total of iterations = " << i << std::endl;
	std::cout << "error gain = " << total1 << " %" << std::endl;
	return ret;
}

/**
 * @brief get the single dimension gradient matrix of a 2D pose projection with inverse projection
 * given by RBF/SKF.
 * @param proj pose projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @param cerr error vector
 * @param alpha gain parameter
 * @return gradient matrix
 */
Eigen::MatrixXd get1DGradientMatrix(Eigen::MatrixXd const & proj, MocapReader * rd,
	SkeletonAnalizer * skdist, Eigen::VectorXd const & cerr, double alpha) {

	Eigen::MatrixXd error = Eigen::ArrayXXd::Zero(proj.rows(), 3);
	Eigen::MatrixXd grad = Eigen::ArrayXXd::Zero(proj.rows(), 2);
	Skeleton sk0, sk1;
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos, cbpos, capos;

	cpos(2) = 0;
	capos(2) = 0;
	cbpos(2) = 0;
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		// delta east error
		sk0 = rd->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		cbpos.head(2) = proj.row(i-1).head(2);
		cbpos(3) = 1;
		capos.head(2) = proj.row(i+1).head(2);
		capos(3) = 1;
		cbpos -= cpos;
		capos -= cpos;
		if (cbpos.norm() < capos.norm())
			error(i,2) = cbpos.norm() / 2;
		else
			error(i,2) = capos.norm() / 2;
		// up
		capos = cpos;
		capos(0) += alpha*error(i,2);
		capos(1) += alpha*error(i,2);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,0) = skdist->getPoseDistance(sk0, sk1);
		error(i,0) -= cerr(i);
		// down
		cbpos = cpos;
		cbpos(0) -= alpha*error(i,2);
		cbpos(1) -= alpha*error(i,2);
		skfanim->setController(cbpos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,1) = skdist->getPoseDistance(sk0, sk1);
		error(i,1) -= cerr(i);
	}
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		grad(i,0) = 0;
		grad(i,1) = 0;
		if ((error(i,0) < 0) || (error(i,1) < 0)) {
			grad(i,0) = error(i,1) - error(i,0);
			grad(i,1) = error(i,1) - error(i,0);
		}
	}
	for (unsigned int i=1; i < grad.rows() - 1; ++i) {
		if (proj(i,2) == 1)
			grad.row(i) *= 0;
		if ((grad(i,0) != 0) || (grad(i,1) != 0)) {
			grad.row(i).normalize();
			grad.row(i) *= error(i,2);
		}
	}
	return grad;
}

/**
 * @brief improve a given single dimension projection matrix
 * @param proj pose projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @return improved frame projection matrix
 */
Eigen::MatrixXd improve1DProjection(Eigen::MatrixXd const & proj, MocapReader * rd, SkeletonAnalizer * skdist) {
	Eigen::MatrixXd grad;
	Eigen::VectorXd cerr;
	Eigen::MatrixXd ret = proj;
	double tini, total0, total1, alpha = 0.5;
	unsigned int i;

	cerr = getErrorVector(ret, rd, skdist);
	tini = total0 = cerr.sum();
	//std::cout << "original projection error = " << total0 << std::endl;
	for (i=0; i < 200; ++i) {
		grad = get1DGradientMatrix(ret, rd, skdist, cerr, alpha);
		ret.block(0, 0, ret.rows(), grad.cols()) += alpha * grad;
		cerr = getErrorVector(ret, rd, skdist);
		total1 = cerr.sum();
		//std::cout << "delta projection error = " << total1 << std::endl;
		if (total1 == total0) break;
		total0 = total1;
	}
	total1 = 100 * (tini - total0) / tini;
	tini /= rd->getNumberOfDOFs() * rd->getNumberOfFrames();
	total0 /= rd->getNumberOfDOFs() * rd->getNumberOfFrames();
	std::cout << "original projection error = " << tini << std::endl;
	std::cout << "delta projection error = " << total0 << std::endl;
	std::cout << "total of iterations = " << i << std::endl;
	std::cout << "error gain = " << total1 << " %" << std::endl;
	return ret;
}

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset bvh input file: -f" << std::endl;
	std::cout << "\tset projection input file: -p" << std::endl;
	std::cout << "\tset projection output file: -o" << std::endl << std::endl;
    std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset single dimension projection algorithm : -1" << std::endl << std::endl;
	std::cout << name << " -f [BHV file] -p [2D input file] -o [2D output file]" << std::endl;
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param fmat matrix with 2D projected frames
 * @param kfvec key frame ids vector
 */
void dumpFrameProjection(std::string const & pname, Eigen::MatrixXd const & fmat) {
    std::ofstream outfile;

    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i)
			outfile << fmat(i,0) << " " << fmat(i,1) << " " << fmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

int main(int argc, char** argv) {
    int opt;
	std::string inbvh;
	std::string inproj;
	std::string outproj;
	bool emask[5] = {false, false, false, false, true};
	MocapReader * mocap;
	//RootlessEucSkeletonAnalizer skan;
	GeodesicSkeletonAnalizer skan;

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "fpo1")) != -1 ) {
        switch (opt) {
            case 'f':
				inbvh = argv[optind];
				emask[0] = true;
                break;
            case 'o':
				outproj = argv[optind];
				emask[1] = true;
                break;
            case 'p':
				inproj = argv[optind];
				emask[2] = true;
                break;
            case '1':
				emask[3] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[4] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1] || !emask[2]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* input BVH file" << std::endl;
		if (!emask[1]) std::cout << "* output projection file" << std::endl;
		if (!emask[2]) std::cout << "* input projection file" << std::endl;
		exit(0);
	}
	if (!emask[4]) exit(0);
	//
	mocap = new BVHReader();
	mocap->readFile(inbvh);
	Eigen::MatrixXd fproj = readProjMatrixAndLoadAnimation(inproj, mocap);
	Eigen::MatrixXd iproj;
	if (emask[3])
		iproj = improve1DProjection(fproj, mocap, &skan);
	else
		iproj = improveProjection(fproj, mocap, &skan);
	rescaleMarkers(iproj);
	dumpFrameProjection(outproj, iproj);
	if (skfanim != NULL) delete skfanim;
	if (mocap != NULL) delete mocap;
    return 0;
}
