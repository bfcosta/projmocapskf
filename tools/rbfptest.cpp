#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>
#include <Eigen/Dense>
#include <algorithm>
#include <vector>
#include <unistd.h>

#define NEARZERO 0.000001
#define MAXVAL 1.0e+12

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input file: -i" << std::endl;
	std::cout << "\tset input metadata file: -m" << std::endl;
	std::cout << "\tset projection type: -p [force|mds|isomap|lle|lamp|pca|tsne]" << std::endl;
	std::cout << "\tset output projection file: -f" << std::endl;
	std::cout << "\tset output rbfp projection file: -r" << std::endl;
	std::cout << "\tset output optimized projection file: -o" << std::endl;
	std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset number of control points as a percentage {0-100}: -c" << std::endl;
	std::cout << "\tset number of learning iterations: -l" << std::endl << std::endl;
	std::cout << name << " -i [input file] -m [metadata file] -c [control points ratio] -l [iteration learning parameter] -p [force|mds|lle|lamp|pca|tsne|isomap] -o [optimized projection output file] -r [RBFP projection output file] -f [output projection file]" << std::endl;
}

/**
 * @brief get projection strategy code by a command line parameter
 * @param name projection specification
 * @return internal code
 */
int getProjectionStrategy(std::string const & name) {
	int ret = -1;
	
	if (strcasecmp(name.c_str(), "force") == 0) {
		ret = 0;
	} else if (strcasecmp(name.c_str(), "mds") == 0) {
		ret = 1;
	} else if (strcasecmp(name.c_str(), "lle") == 0) {
		ret = 2;
	}  else if (strcasecmp(name.c_str(), "lamp") == 0) {
		ret = 3;
	} else if (strcasecmp(name.c_str(), "pca") == 0) {
		ret = 4;
	} else if (strcasecmp(name.c_str(), "tsne") == 0) {
		ret = 5;
	} else if (strcasecmp(name.c_str(), "isomap") == 0) {
		ret = 6;
	}
	return ret;
}

/**
 * @brief read projection matrix from file
 * @param fname file name to be read
 * @param fmeta metadata file name to be read
 * @return projection matrix
 */
Eigen::MatrixXd readProjection(std::string const & fname, 
	std::string const & fmeta) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	unsigned int cols = 0, skip0 = 0, skip1 = 0, counter;

	// read metadata
	infile.open(fmeta.c_str());	
	if (infile.is_open()) {
		if (std::getline(infile, line)) {
			cols = std::atoi(line.c_str());
		}
		if (std::getline(infile, line)) {
			skip1 = skip0 = std::atoi(line.c_str());
		}
		if (std::getline(infile, line)) {
			skip1 = std::atoi(line.c_str());
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fmeta << std::endl;
		return ret;
	}

	//~ std::cout << "li metadados" << std::endl;
	//~ std::cout << "cols = " << cols << std::endl;
	//~ std::cout << "skip0 = " << skip0 << std::endl;
	//~ std::cout << "skip1 = " << skip1 << std::endl;
	// read matrix
	infile.open(fname.c_str());
	counter = 0;
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			replace(line.begin(), line.end(), ',', ' ' );
			iss.str(line);
			while (iss >> buffer) {
				if (((counter % cols) != (skip0 - 1)) && ((counter % cols) != (skip1 - 1)))
					data.push_back(std::atof(buffer.c_str()));
				++counter;
			}
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}
	if (skip0 != 0) --cols;
	if (skip1 != skip0) --cols;
	//~ std::cout << "cols = " << cols << std::endl;
	//~ std::cout << "counter = " << counter << std::endl;
	//~ std::cout << "data size = " << data.size() << std::endl;
	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	
	return ret;
}

/**
 * @brief get distance matrix by data matrix where points are rowwise displayed.
 * @param mat data matrix
 * @return distance matrix
 */
Eigen::MatrixXd getDistanceMatrix(Eigen::MatrixXd & mat) {
    Eigen::MatrixXd dist = Eigen::ArrayXXd::Zero(mat.rows(), mat.rows());
    Eigen::VectorXd v0 = Eigen::ArrayXd::Zero(mat.cols());
    Eigen::VectorXd v1 = Eigen::ArrayXd::Zero(mat.cols());

    for (unsigned int i=0; i < mat.rows(); ++i) {
        for (unsigned int j=i+1; j < mat.rows(); ++j) {
            v0 = mat.row(i);
            v1 = mat.row(j);
            v0 -= v1; 
            dist(i,j) = dist(j,i) = std::sqrt(v0.dot(v0));
        }
    }   
    return dist;
}

/**
 * @brief normalize projection to square [0, 1]
 * @param ret projection matrix to be normalized
 */
void normalizeProjection(Eigen::MatrixXd & ret) {
	double min0, max0;
	double min1, max1;
	// normalize projection coordinates
	min0 = max0 = ret(0,0);
	min1 = max1 = ret(0,1);
	for (unsigned int i=1; i < ret.rows(); ++i) {
		if (min0 > ret(i,0)) min0 = ret(i,0);
		else if (max0 < ret(i,0)) max0 = ret(i,0);
		if (min1 > ret(i,1)) min1 = ret(i,1);
		else if (max1 < ret(i,1)) max1 = ret(i,1);
	}
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,0) = (ret(i,0) - min0) / (max0 - min0);
		ret(i,1) = (ret(i,1) - min1) / (max1 - min1);
	}
}
/**
 * @brief get a 2D projection of a distance matrix by successively approximating
 * a random sample to the skeleton distance distribution.
 * @param dmat distance matrix
 * @param train number of approximation iterations
 * @param stepsize fraction of delta approximation
 * @return projected distance matrix into 2D
 */
Eigen::MatrixXd getForceProjection(Eigen::MatrixXd const & dmat, unsigned int train = 50, double stepsize = 0.125) {
	
	Eigen::MatrixXd ospdist = dmat;
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(ospdist.rows(), 4);
	Eigen::VectorXd v;
	double delta, min0, max0 = RAND_MAX;
	//double min1, max1;
	double x0,x1;

	// initialize 2D data with random vectors
	std::srand(time(NULL));		
	for (unsigned int i=0; i < ret.rows(); ++i) {
		do {
			x0 = (double) std::rand() / max0;
		} while (x0 == 0);
		x1 = (double) std::rand() / max0;
		ret(i,0) = std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		//ret(i,0) = std::rand() / max0;
		do {
			x0 = (double) std::rand() / max0;
		} while (x0 == 0);
		x1 = (double) std::rand() / max0;
		ret(i,1) = std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		//ret(i,1) = std::rand() / max0;
		ret(i,2) = 0;
		ret(i,3) = 0;
	}
	// discover skeleton space distances and normalize it
	min0 = max0 = ospdist(0,1);
	for (unsigned int i=0; i < ospdist.rows(); ++i)
		for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
			if (ospdist(i,j) > max0) max0 = ospdist(i,j);
			else if (ospdist(i,j) < min0) min0 = ospdist(i,j);
		}
	for (unsigned int i=0; i < ospdist.rows(); ++i)
		for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
			ospdist(i,j) = (ospdist(i,j) - min0) / (max0 - min0);
			ospdist(j,i) = ospdist(i,j);
		}
	
	// training session
	for (unsigned int k=0; k < train; ++k) {
		// discover delta and update
		for (unsigned int i=0; i < ospdist.rows(); ++i)
			for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
				v = ret.row(i) - ret.row(j);
				delta = stepsize * (ospdist(i,j) - std::sqrt(v.dot(v)));
				v *= delta;
				ret.row(i) += v;
			}
	}
	normalizeProjection(ret);
	// reshape for 3D homogeneous coordinate
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,2) = 0;
		ret(i,3) = 1;
	}
    //std::cout << "markers" << std::endl;
    //std::cout << ret << std::endl;
	return ret;
}

/**
 * @brief get a 2D projection given a frame distance matrix with MDS strategy
 * @param dmat distance matrix
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd getMdsProjection(Eigen::MatrixXd const & dmat) {
	Eigen::MatrixXd adist = dmat;
	Eigen::MatrixXd bdist = Eigen::ArrayXXd::Zero(adist.rows(), adist.cols());
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(adist.rows(), 4);
	Eigen::VectorXd rowavg, colavg;
	double distavg;
		
	for (unsigned int i=0; i < adist.rows(); ++i)
		for (unsigned int j=i+1; j < adist.cols(); ++j) {
			adist(i,j) = -0.5 * adist(i,j) * adist(i,j); // \frac{-1}{2} d_{ij}^2
			adist(j,i) = adist(i,j);
		}
	// build distance B matrix
	rowavg = adist.rowwise().sum();
	colavg = adist.colwise().sum();
	distavg = rowavg.sum();
	rowavg /= adist.rows();
	colavg /= adist.cols();
	distavg /= (adist.rows() * adist.cols());
	for (unsigned int i=0; i < adist.rows(); ++i)
		for (unsigned int j=0; j < adist.cols(); ++j) {
			bdist(i,j) = adist(i,j) + distavg - rowavg(i) - colavg(j);
		}
    
    // get most significant B matrix eigenvalues and eigenvectors
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> esolver(bdist);
    Eigen::VectorXd maxevs = esolver.eigenvalues();
    Eigen::MatrixXd evectors = esolver.eigenvectors();
    const int pdim = 2; // 2D projection
    unsigned int pp = 0, col[pdim];
    double mval;
    bool isin;
    while (pp < pdim) {
        mval = -1 * maxevs.dot(maxevs);
        for (unsigned int i=0; i < maxevs.size(); ++i) {
            isin = false;
            if (mval < (maxevs(i) * maxevs(i))) {
                for (unsigned int j=0; j < pp; ++j)
                    if (i == col[j]) isin = true;
                if (!isin) {
                    col[pp] = i;
                    mval = maxevs(col[pp]) * maxevs(col[pp]);
                }
            }
        }
        ++pp;
    }
    // normalize eigenvectors
    for (unsigned int j=0; j < pdim; ++j) {
        mval = std::sqrt(maxevs(col[j])) / evectors.col(col[j]).norm();
        proj.col(j) = evectors.col(col[j]) * mval;
    }
    // normalize to 1x1 square
    double mmin1, mmin2, mmax, mdiff1, mdiff2;
    mmin1 = mmax = proj(0,0);
    for (unsigned int j=1; j < proj.rows(); ++j) 
		if (mmin1 > proj(j,0)) mmin1 = proj(j,0);
		else if (mmax < proj(j,0)) mmax = proj(j,0);
	mdiff1 = mmax - mmin1;
    mmin2 = mmax = proj(0,1);
    for (unsigned int j=1; j < proj.rows(); ++j) 
		if (mmin2 > proj(j,1)) mmin2 = proj(j,1);
		else if (mmax < proj(j,1)) mmax = proj(j,1);	
	mdiff2 = mmax - mmin2;
	if (mdiff1 > mdiff2) mdiff2 = mdiff1;
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,0) = (proj(j,0) - mmin1) / mdiff2;
    	proj(j,1) = (proj(j,1) - mmin2) / mdiff2;
    }
    // homogeneous coordinates
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,3) = 1;
	}
    //std::cout << "markers" << std::endl;
    //std::cout << proj << std::endl;
    return proj;
}

/**
 * @brief tests if two matrices have equal dimensions and values
 * @param mat1 first matrix to be compared
 * @param mat2 second matrix to be compared
 * @return whether matrices are equal
 */
bool sameMatrices(Eigen::MatrixXd & mat1, Eigen::MatrixXd & mat2) {

	if (mat1.cols() != mat2.cols()) return false;
	if (mat1.rows() != mat2.rows()) return false;
	for (unsigned int i=0; i < mat1.rows(); ++i)
		for (unsigned int j=0; j < mat1.cols(); ++j) 
			if (mat1(i,j) != mat2(i,j))
				return false;
	return true;
}

/**
 * @brief update graph matrix using Floyd-Warshall algorithm
 * @param fmat matrix to be updated
 */
void runFloydWarshallUpdate(Eigen::MatrixXd & fmat) {
	double temp;
	for (unsigned int i=0; i < fmat.rows(); ++i)
		for (unsigned int j=0; j < fmat.cols(); ++j)
			for (unsigned int k=0; k < fmat.cols(); ++k) {
				temp = fmat(i,k) + fmat(k,j);
				if (fmat(i, j) > temp) 
					fmat(i,j) = temp;
			}
}

/**
 * @brief get the euclidean geodesic distance matrix from an ordinary distance matrix
 * @param dmat ordinary distance matrix
 * @param nn number of nearest neighbors
 * @return distance matrix
 */
Eigen::MatrixXd getIsomapDistanceMatrix(Eigen::MatrixXd const & dmat, unsigned int nn = 14) {
	double dmax = 1000000000;
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Constant(dmat.rows(), dmat.cols(), dmax);
	Eigen::MatrixXd temp;
	Eigen::VectorXd dvec = dmat.row(0);
	Eigen::VectorXd p0, p1;
	Eigen::VectorXi nneigh = Eigen::ArrayXi::Zero(nn);
	unsigned int min, max;

	for (unsigned int i=0; i < fmat.rows(); ++i)
		fmat(i,i) = 0;
	for (unsigned int i=0; i < dmat.rows(); ++i) {
		dvec = dmat.row(i);
		// get nearest neighbors
		max = i;
		for (unsigned int k=0; k < dvec.size(); ++k) 
			if (dvec(k) > dvec(max))
				max = k;
		dvec(i) = dvec(max);
		for (unsigned int j=0; j < nneigh.size(); ++j) {
			min = 0;
			for (unsigned int k=1; k < dvec.size(); ++k) 
				if (dvec(k) < dvec(min))
					min = k;
			nneigh(j) = min;
			dvec(min) = dvec(max);
		}
		// get distance to nearest neighbors
		for (unsigned int j=0; j < nneigh.size(); ++j) {
			fmat(i, nneigh(j)) = fmat(nneigh(j), i) = dmat(i, nneigh(j));
		}
	}
	do {
		temp = fmat;
		runFloydWarshallUpdate(fmat);
	} while (!sameMatrices(fmat,temp));
    return fmat;
}

/**
 * @brief get isomap projection
 * @param dmat ordinary distance matrix
 * @return isomap projection
 */
Eigen::MatrixXd getIsomapProjection(Eigen::MatrixXd const & dmat) {
	Eigen::MatrixXd isomat = getIsomapDistanceMatrix(dmat);
	Eigen::MatrixXd ret = getMdsProjection(isomat);
	normalizeProjection(ret);
	return ret;
}

/**
 * @brief get the 2D Principal Components of a given covariance matrix
 * @param datmat rowwise data matrix
 * @return 2D projection
 */
Eigen::MatrixXd getPcaProjection(Eigen::MatrixXd const & datmat) {
	
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(datmat, Eigen::ComputeFullU);
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(datmat.rows(), 4);

    // get most significant matrix eigenvalues and eigenvectors
    Eigen::MatrixXd eigVectors = svd.matrixU();
    proj.block(0, 0, datmat.rows(), 2) = eigVectors.block(0, 0, eigVectors.rows(), 2);
    Eigen::VectorXd sv = svd.singularValues();
    proj.col(0) *= sv(0);
    proj.col(1) *= sv(1);
    //Eigen::MatrixXd eigVectors = svd.matrixV();
    //Eigen::MatrixXd rmat = eigVectors.block(0, 0, eigVectors.rows(), 2);
    //proj.block(0, 0, datmat.rows(), 2) = datmat * rmat;

    // normalize to 1x1 square
    double mmin1, mmin2, mmax, mdiff1, mdiff2;
    mmin1 = mmax = proj(0,0);
    for (unsigned int j=1; j < proj.rows(); ++j)
		if (mmin1 > proj(j,0)) mmin1 = proj(j,0);
		else if (mmax < proj(j,0)) mmax = proj(j,0);
	mdiff1 = mmax - mmin1;
    mmin2 = mmax = proj(0,1);
    for (unsigned int j=1; j < proj.rows(); ++j)
		if (mmin2 > proj(j,1)) mmin2 = proj(j,1);
		else if (mmax < proj(j,1)) mmax = proj(j,1);
	mdiff2 = mmax - mmin2;
	if (mdiff1 > mdiff2) mdiff2 = mdiff1;
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,0) = (proj(j,0) - mmin1) / mdiff2;
		proj(j,1) = (proj(j,1) - mmin2) / mdiff2;
    }
    // homogeneous coordinates
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,3) = 1;
	}
    return proj;
}

/**
 * @brief get nearest neigbors index vector
 * @param dvec distance vector
 * @param knn number of nearest neighbors
 * @param curr current pivot
 * @return index vector with nearest neighbors
 */
Eigen::VectorXi getKnnIndices(Eigen::VectorXd const & dvec, unsigned int knn, unsigned int curr) {
    Eigen::VectorXi ret = Eigen::ArrayXi::Zero(knn);
    bool wasSeen;

    // put random indices
    for (unsigned int i=0; i < ret.size(); ++i) 
        ret(i) = (curr + 1 + i) % dvec.size();
    // find minimum
    for (unsigned int j=0; j < dvec.size(); ++j)
        if (j == curr) continue; else
        if (dvec(j) < dvec(ret(0)))
            ret(0) = j;
    // find next unseen minimums
    for (unsigned int i=1; i < knn; ++i) {
        for (unsigned int j=0; j < dvec.size(); ++j)
            if (j == curr) continue; else
            if (dvec(j) < dvec(ret(i))) {
                // check if it was seen
                wasSeen = false;
                for (unsigned int k=0; k < i; ++k)
                    wasSeen = wasSeen || (ret(k) == (int) j); 
                if (!wasSeen) 
                    ret(i) = j;
            }
    }
    return ret;
}

/**
 * @brief get local linear embedding weight reconstruction matrix for a given data matrix
 * @param dist frame distance matrix
 * @param fref keyframe index vector
 * @param knn size of nearest neighborhood
 * @return weight matrix
 */
Eigen::MatrixXd getLLEWeightMatrix(Eigen::MatrixXd const & dist, unsigned int knn) {
	
    Eigen::MatrixXd wmat = Eigen::ArrayXXd::Zero(dist.rows(), dist.cols());
    Eigen::MatrixXi nbmat = Eigen::ArrayXXi::Zero(dist.rows(), knn);
    Eigen::VectorXd rdist = Eigen::ArrayXd::Zero(dist.rows());

    // get knn minimum values
    for (unsigned int i=0; i < dist.rows(); ++i) {
        rdist = dist.row(i);
        nbmat.row(i) = getKnnIndices(rdist, knn, i);
    }
    // calculate weights
    Eigen::MatrixXd cmat = Eigen::ArrayXXd::Zero(knn, knn);
    Eigen::VectorXd ones = Eigen::ArrayXd::Constant(knn, 1.0);
    Eigen::VectorXd sol = Eigen::ArrayXd::Zero(knn);
    double sum;
    for (unsigned int i=0; i < nbmat.rows(); ++i) {
		for (unsigned int j=0; j < cmat.rows(); ++j)
			for (unsigned int k=0; k < cmat.cols(); ++k) 
				cmat(j,k) = 0.5*(dist(i,nbmat(i,j))*dist(i,nbmat(i,j)) + 
						dist(i,nbmat(i,k))*dist(i,nbmat(i,k)) - 
						dist(nbmat(i,j), nbmat(i,k))*dist(nbmat(i,j), nbmat(i,k)));

        sol = cmat.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(ones);
        //sol = cmat.fullPivLu().solve(ones);
        sum = sol.sum();
        if (sum == 0) {
            // same weigths for everybody
			for (unsigned int j=0; j < sol.size(); ++j) sol(j) = 1;
            sum = sol.cwiseAbs().sum();
        }
        for (unsigned int j=0; j < nbmat.cols(); ++j) {
            wmat(i, nbmat(i,j)) = sol(j) / sum;
        }
    }
    return wmat;
}

/**
 * @brief get embedding coordinates for LLE given weight matrix
 * @param wmat weighht matrix
 * @param dim dimension of embedding coordinates
 * @return matrix with eigenvectors displayed columnwise
 */
Eigen::MatrixXd getEmbeddingCoordinateMatrix(Eigen::MatrixXd const & wmat, unsigned int dim) {
	
    Eigen::MatrixXd imat = Eigen::MatrixXd::Identity(wmat.rows(), wmat.cols());
    Eigen::MatrixXd iwmat = imat - wmat;
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(iwmat, Eigen::ComputeThinV);
    Eigen::MatrixXd vmat = svd.matrixV();
    assert((vmat.cols() - dim) > 0);
    Eigen::MatrixXd sol = vmat.block(0, vmat.cols() - dim - 1, vmat.rows(), dim);
	//Eigen::VectorXd sval = svd.singularValues();
	//for (unsigned int i=0; i < sol.cols(); ++i)
	//	sol.col(i) /= sval(vmat.cols() - dim - 1 + i);
     return sol;
}

/**
 * @brief get the 2D LLE projection given a frame distance matrix and K nearest neighbors
 * @param dmat frame distance matrix
 * @param knn number of nearest neighbors
 * @return LLE 2D projection
 */
Eigen::MatrixXd getLleProjection(Eigen::MatrixXd const & dmat, unsigned int knn = 15) {
	
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(dmat.rows(), 4);
	Eigen::MatrixXd llew = getLLEWeightMatrix(dmat, knn);
	proj.block(0, 0, dmat.rows(), 2) = getEmbeddingCoordinateMatrix(llew, 2);
	normalizeProjection(proj);
	for (unsigned int i=0; i < proj.rows(); ++i) proj(i,3) = 1;
	return proj;
}

/**
 * @brief return a matrix with random elements by a white noise with \mu and \sigma.
 * @param rows number of rows
 * @param cols number of cols
 * @param mu average of the white noise
 * @param sigma standard deviation of the white noise
 * @return random matrix
 */
Eigen::MatrixXd getRandomMatrix(unsigned int rows, unsigned int cols,
	double mu = 0, double sigma = 1) {

	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(rows, cols);
	double max = RAND_MAX;
	double x0, x1;

	std::srand(time(NULL));
	for (unsigned int i=0; i < rows; ++i)
		for (unsigned int j=0; j < cols; ++j) {
			do {
				x0 = (double) std::rand() / max;
			} while (x0 == 0);
			x1 = (double) std::rand() / max;
			ret(i,j) = mu + sigma * std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		}
	return ret;
}

/**
 * @brief get the conditional probability vector given a standard deviation. Also, return the perplexity.
 * @param dvec distance vector
 * @param sigma standard deviation multiplier (x * \sigma) where x=1 means \sigma=sqrt(2)/2.
 * @param pivot element in array which conditional probabilities are referenced
 * @param h (output) log of the perplexity
 * @return conditional probability vector
 */
Eigen::VectorXd getProbabilitiesBySigma(Eigen::VectorXd const & dvec,
	double sigma, unsigned int pivot, double *h) {

	Eigen::VectorXd pvec = dvec;

	// calculate probability vector
	for (unsigned int i=0; i < pvec.size(); ++i)
		pvec(i) = std::exp(-1 * sigma * dvec(i));
	pvec(pivot) = 0;
	double sum = pvec.sum();
	pvec /= sum;
	// calculate Shannon entropy
	*h = 0;
	pvec(pivot) = 1; // avoid NaN at std::log()
	for (unsigned int i=0; i < pvec.size(); ++i)
		*h += -1 * pvec(i) * std::log(pvec(i));
	pvec(pivot) = 0;
	return pvec;
}

/**
 * @brief get the pairwise affinity (p_{ij}) matrix given a perplexity value and the distance matrix
 * @param dmat distance matrix
 * @param perplexity given perplexity value
 * @return non-symetric conditional probability matrix
 */
Eigen::MatrixXd getPairwiseAffinities(Eigen::MatrixXd const & dmat,
	double perplexity = 30.0) {

	Eigen::MatrixXd pijmat = Eigen::ArrayXXd::Zero(dmat.rows(), dmat.rows());
	Eigen::VectorXd beta = Eigen::ArrayXd::Zero(dmat.rows());
	Eigen::VectorXd tmp0, tmp1;
	double bmin = 0, bmax = 0, h, hdiff, logp;
	bool hasMin, hasMax;

	logp = std::log(perplexity);
	for (unsigned int i=0; i < beta.size(); ++i) {
		tmp0 = dmat.row(i);
		beta(i) = 1; // one means $\sigma = 1 / sqrt(2)$ as $beta = 1 / (2 * \sigma^2)$
		tmp1 = getProbabilitiesBySigma(tmp0, beta(i), i, &h);
		hdiff = h - logp;
		hasMin = hasMax = false;
		for (unsigned int j=0; j < 50; ++j)
			if (std::abs(hdiff) <= 0.00001)
				break;
			else {
				if (hdiff > 0) {
					bmin = beta(i);
					hasMin = true;
					if ((hasMin) && (hasMax))
						beta(i) = (beta(i) + bmax) / 2;
					else
						beta(i) *= 2;
				} else {
					bmax = beta(i);
					hasMax = true;
					if ((hasMin) && (hasMax))
						beta(i) = (beta(i) + bmin) / 2;
					else
						beta(i) /= 2;
				}
				tmp1 = getProbabilitiesBySigma(tmp0, beta(i), i, &h);
				hdiff = h - logp;
			}
		pijmat.row(i) = tmp1;
	}
	//std::cout << "beta" << std::endl << beta << std::endl;
	// do p_{ij} = ( p_{ij} + p_{ji} ) / 2*n
	Eigen::MatrixXd ret = pijmat + pijmat.transpose();
	ret /= 2 * pijmat.rows();
	return ret;
}

/**
 * @brief get low dimensional affinities (q_{ij}) given by a t-student statistical distribution
 * @param smat reduced dimension data
 * @return probability matrix
 */
Eigen::MatrixXd getLowDimensionalAffinities(Eigen::MatrixXd const & smat) {
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(smat.rows(), smat.rows());
	Eigen::VectorXd d2vec = Eigen::ArrayXd::Zero(smat.rows());
	Eigen::VectorXd tmp0, tmp1;

	for (unsigned int i=0; i < smat.rows(); ++i) {
		tmp0 = smat.row(i);
		for (unsigned int j=0; j < smat.rows(); ++j) {
			tmp1 = smat.row(j);
			tmp1 -= tmp0;
			d2vec(j) = 1 / (1 + tmp1.dot(tmp1));
		}
		d2vec(i) = 0;
		ret.row(i) = d2vec;
	}
	ret /= ret.colwise().sum().sum();
	return ret;
}

/**
 * @brief get Kullback-Liebler divergence between both affinity matrices as the t-SNE gradient function
 * @param pmat high dimensional affinity matrix
 * @param qmat low dimensional affinity matrix
 * @param ymat low dimensional data
 * @return gradient matrix
 */
Eigen::MatrixXd getKLGradient(Eigen::MatrixXd const & pmat,
	Eigen::MatrixXd const & qmat, Eigen::MatrixXd const & ymat) {

	Eigen::MatrixXd pqmat = pmat - qmat;
	Eigen::MatrixXd dymat = ymat;
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(ymat.rows(), ymat.cols());
	Eigen::VectorXd tmp0, tmp1;

	for (unsigned int i=0; i < ymat.rows(); ++i) {
		tmp0 = ymat.row(i);
		for (unsigned int j=0; j < ymat.rows(); ++j) {
			tmp1 = ymat.row(j);
			tmp1 = tmp0 - tmp1;
			tmp1 /= (1 + tmp1.dot(tmp1));
			dymat.row(j) = tmp1;
		}
		tmp0 = pqmat.col(i);
		ret.row(i) = tmp0.transpose() * dymat;
	}
    ret *= 4;
	return ret;
}

/**
 * @brief calculate the Kullback-Lieber divergence between two conditional
 * probabilities as the error on multidimensional projection
 * @param pmat high dimensional affinity matrix
 * @param qmat low dimensional afifnity matrix
 * @return Kullback-Lieber divergence
 */
double getKLDivergence(Eigen::MatrixXd const & pmat,
	Eigen::MatrixXd const & qmat) {

    Eigen::MatrixXd cmat = qmat;
    for (unsigned int i=0; i < cmat.rows(); ++i)
        cmat(i,i) = 1;
    for (unsigned int i=0; i < cmat.rows(); ++i)
        for (unsigned int j=0; j < cmat.cols(); ++j)
            cmat(i,j) = pmat(i,j) / cmat(i,j);
    for (unsigned int i=0; i < cmat.rows(); ++i)
        cmat(i,i) = 1;
    for (unsigned int i=0; i < cmat.rows(); ++i)
        for (unsigned int j=0; j < cmat.cols(); ++j)
            cmat(i,j) = pmat(i,j) * std::log(cmat(i,j));
    return cmat.colwise().sum().sum();
}

/**
 * @brief get gradient vector applied by local gain adaptation
 * @param grad input gradient vector
 * @param gains gain vector (output)
 * @param deltaY last variation on Y data
 * @param ming minumum gain for gain vector
 * @return gradient vector with gain applied
 */
Eigen::MatrixXd applyAndUpdateGain(Eigen::MatrixXd const & grad,
	Eigen::MatrixXd & gains, Eigen::MatrixXd const & deltaY, double ming = 0.01) {

    Eigen::MatrixXd ret = grad;
    for (unsigned int i=0; i < ret.rows(); ++i)
        for (unsigned int j=0; j < ret.cols(); ++j) {
            ret(i,j) = deltaY(i,j) * grad(i,j);
            // test if both gradient and delta have the same signal
            if (ret(i,j) > 0) ret(i,j) = gains(i,j) * 0.8;
            else ret(i,j) = gains(i,j) + 0.2;
            if (ret(i,j) < ming) ret(i,j) = ming;
            gains(i,j) = ret(i,j);
            ret(i,j) *= grad(i,j);
        }
    return ret;
}

/**
 * @brief centralize matrix with rowwise data
 * @param data input matrix
 */
void centralizeData(Eigen::MatrixXd & data) {
    Eigen::VectorXd avg = data.colwise().sum() / data.rows();
    for (unsigned int i=0; i < data.rows(); ++i)
        data.row(i) -= avg;
}

/**
 * @brief get low dimensional (2D) representation by
 * t-distributed Stochastic Neighborhood Embedding projecion strategy
 * @param pij high dimensional affinity matrix
 * @param ymat low dimensional initial value
 * @param train number of training iterations
 * @param learn learning coefficient for gradient descendant
 * @param momentum momentum coefficient for gradient descendant
 * @return low dimensional data
 */
Eigen::MatrixXd getTsneDimensionReduction(Eigen::MatrixXd const & pij,
	Eigen::MatrixXd const & ymat, unsigned int train = 1000, double learn = 500,
			double momentum = 0.5) {

    Eigen::MatrixXd ret = ymat;
    Eigen::MatrixXd yprev = Eigen::ArrayXXd::Constant(ymat.rows(), ymat.cols(), 1);
    Eigen::MatrixXd yyprev = Eigen::ArrayXXd::Constant(ymat.rows(), ymat.cols(), 1);
    Eigen::MatrixXd gains = Eigen::ArrayXXd::Constant(ymat.rows(), ymat.cols(), 1);
	Eigen::MatrixXd qij = getLowDimensionalAffinities(ret);
    Eigen::MatrixXd eepij = 4*pij;
	Eigen::MatrixXd grad = getKLGradient(eepij, qij, ret);
    unsigned int change0 = train * 0.1;
    unsigned int change1 = train * 0.25;

    for (unsigned int i=0; i < change0; ++i) {
        ret = yprev - learn * grad + momentum * (yprev - yyprev);
        centralizeData(ret);
        qij = getLowDimensionalAffinities(ret);
	    grad = getKLGradient(eepij, qij, ret);
        yyprev = yprev - yyprev;
	    grad = applyAndUpdateGain(grad, gains, yyprev);
        yyprev = yprev;
        yprev = ret;
    }
    eepij /= 4;
    for (unsigned int i=change0; i < change1; ++i) {
        ret = yprev - learn * grad + momentum * (yprev - yyprev);
        centralizeData(ret);
        qij = getLowDimensionalAffinities(ret);
	    grad = getKLGradient(eepij, qij, ret);
        yyprev = yprev - yyprev;
	    grad = applyAndUpdateGain(grad, gains, yyprev);
        yyprev = yprev;
        yprev = ret;
    }
    momentum += 0.3;
    for (unsigned int i=change1; i < train; ++i) {
        ret = yprev - learn * grad + momentum * (yprev - yyprev);
        centralizeData(ret);
        qij = getLowDimensionalAffinities(ret);
	    grad = getKLGradient(eepij, qij, ret);
        yyprev = yprev - yyprev;
	    grad = applyAndUpdateGain(grad, gains, yyprev);
        yyprev = yprev;
        yprev = ret;
    }
    //double error = getKLDivergence(pij, qij);
    //std::cout << "t-SNE error = " << error << std::endl;
    Eigen::MatrixXd ret4d = Eigen::ArrayXXd::Zero(ret.rows(), 4);
    ret4d.block(0, 0, ret.rows(), ret.cols()) = ret;
    normalizeProjection(ret4d);
    for (unsigned int i=0; i < ret4d.rows(); ++i)
		ret4d(i,3) = 1;
    return ret4d;
}

/**
 * @brief get markers position for given frame ids with t-SNE strategy for
 * spreading markers on a 2D space.
 * @param dmat distance matrix
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd getTsneProjection(Eigen::MatrixXd const & dmat) {

	Eigen::MatrixXd pij = getPairwiseAffinities(dmat, 50.0); // perplexity = 50
	Eigen::MatrixXd ymat = getRandomMatrix(pij.rows(), 2);
	return getTsneDimensionReduction(pij, ymat);
}

/**
 * @brief get a projection data from a d1 dimension to d2 where d1 > d2. Usually, d2 = 2.
 * @param mpose data to be projected row-wise
 * @param yproj control points on d2 space dimension
 * @param xproj control points on d1 space dimension
 * @return d2 projected data
 */
Eigen::VectorXd getLampProjection(Eigen::VectorXd const & mpose, 
	Eigen::MatrixXd const & yproj, Eigen::MatrixXd const & xproj) {
	
	Eigen::VectorXd ret = Eigen::ArrayXd::Zero(yproj.cols());
	Eigen::VectorXd alpha = Eigen::ArrayXd::Zero(xproj.rows());
	Eigen::MatrixXd mmat, amat, bmat, atbmat;
	Eigen::VectorXd xt, yt, xh, yh;
	double sa;
	
	amat = Eigen::ArrayXXd::Zero(xproj.rows(), xproj.cols());
	bmat = Eigen::ArrayXXd::Zero(yproj.rows(), yproj.cols());
	
	// calculate alphas
	for (unsigned int j=0; j < xproj.rows(); ++j) {
		alpha(j) = mpose.dot(xproj.row(j));
		if (alpha(j) > NEARZERO) alpha(j) = 1 / (alpha(j) * alpha(j));
		else alpha(j) = MAXVAL;
	}
	// get x~, y~
	sa = 0;
	xt = Eigen::ArrayXd::Zero(xproj.cols());
	yt = Eigen::ArrayXd::Zero(yproj.cols());
	xh = Eigen::ArrayXd::Zero(xproj.cols());
	yh = Eigen::ArrayXd::Zero(yproj.cols());
	for (unsigned int j=0; j < alpha.size(); ++j) {
		sa += alpha(j);
		xt += alpha(j) * xproj.row(j);
		yt += alpha(j) * yproj.row(j);
	}
	xt /= sa;
	yt /= sa;
	// build matrix M
	sa = std::sqrt(sa);
	for (unsigned int j=0; j < alpha.size(); ++j) {
		xh = xproj.row(j);
		yh = yproj.row(j);
		amat.row(j) = sa * (xh - xt);
		bmat.row(j) = sa * (yh - yt);
	}
	atbmat = amat.transpose() * bmat;
	Eigen::JacobiSVD<Eigen::MatrixXd> svdatb(atbmat, Eigen::ComputeThinU | Eigen::ComputeThinV);
	mmat = svdatb.matrixU() * svdatb.matrixV();		
	// save projection: y = (x - x~) * M + y~
	xh = mpose;
	xh -= xt;
	yh = xh.transpose() * mmat;
	yh += yt;
	ret = yh;
	
	return ret;
}

/**
 * @brief get markers position for given frame ids with LAMP strategy for
 * spreading markers on a 2D space.
 * @param data original rowwise data matrix
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd getLampProjection(Eigen::MatrixXd const & data) {
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(data.rows(), 4);
	Eigen::MatrixXd fss;
	Eigen::VectorXi ids;
	int sss, pos;
	
	// get force projection on subset
	sss = data.rows() * 0.08;
	sss = std::floor(sss);
	fss = Eigen::ArrayXXd::Zero(sss, data.cols());
	ids = Eigen::ArrayXi::Zero(sss);
	float step = data.rows() - 1;
	step /= (sss - 1);
	for (int i=0; i < sss; ++i) {
		pos = step * i;
		ids(i) = pos;
		fss.row(i) = data.row(pos);
	}
	fss.row(fss.size() - 1) = data.row(data.size() - 1);
	ids(sss - 1) = data.rows() - 1;
	Eigen::MatrixXd fproj = getForceProjection(fss, 50, 0.125);
	//std::cout << "Force projection" << std::endl << fproj << std::endl;
	Eigen::MatrixXd lowdim = Eigen::ArrayXXd::Zero(fss.rows(), 2);
	Eigen::MatrixXd highdim = Eigen::ArrayXXd::Zero(fss.rows(), data.cols());
	sss = 0;
	for (unsigned int i=0; i < ids.size(); ++i) {
		proj.row(ids(i)) = fproj.row(sss);
		lowdim.row(sss) = fproj.row(sss).head(2);
		highdim.row(sss) = data.row(ids(i));
		++sss;
	}
	
	// build remainder projections
	sss = 0;
	for (int i=0; i < data.rows(); ++i) {
		if (ids(sss) == i) {
			++sss;
			continue;
		}
		Eigen::VectorXd dvec = data.row(i);
		Eigen::VectorXd lvec = getLampProjection(dvec, lowdim, highdim);
		proj(i,0) = lvec(0);
		proj(i,1) = lvec(1);
		proj(i,3) = 1;
	}
	// rescale for [0,1] on (x,y)
	normalizeProjection(proj);
	return proj;
}

/**
 * @brief get projection data by heuristic
 * @param res projection heuristic
 * @param data original data
 * @param dmat distance matrix
 * @return low dimension projection matrix
 */
Eigen::MatrixXd getProjectionData(int res, Eigen::MatrixXd const & data,
	Eigen::MatrixXd const & dmat) {
	Eigen::MatrixXd proj;
	
	switch (res) {
		case 0:
			proj = getForceProjection(dmat);
		break;
		case 1:
			proj = getMdsProjection(dmat);
		break;
		case 2:
			proj = getLleProjection(dmat);
		break;
		case 3:
			proj = getLampProjection(data);
		break;
		case 4:
			proj = getPcaProjection(data);
		break;		
		case 5:
			proj = getTsneProjection(dmat);
		break;
		case 6:
			proj = getIsomapProjection(dmat);
		break;
		default:
			std::cout << "Unknown projection is not implemented." << std::endl;
			exit(0);
		break;
	}
	return proj;
}

/**
 * @brief return the implemented radial basis function kernel as multiquadrics
 * with c and epsilon equals one.
 * @param d2 distance as input
 * @return function output
 */
double phi_rols(double d2) {
	// gaussian kernel, $\epsilon = 1$
	//return std::exp(-1*d2*d2);
	// multiquadrics kernel, c=1 and $\epsilon = 1$
	return std::sqrt(1 + d2*d2);
	// inverse multiquadrics kernel, c=1 and $\epsilon = 1$
	//return std::sqrt(1/(1+d2*d2));
}

/**
 * @brief choose control points with ROLS algorithm
 * @param sample vector containing subsample ids
 * @param phimat rbf kernel matrix
 * @param ssproj subsample projection matrix
 * @param nkf stop criteria: number of control points
 * @param reg regularization parameter
 * @return keyframe vector
 */
Eigen::VectorXi chooseControlPoints(Eigen::VectorXi & sample, Eigen::MatrixXd & phimat, 
Eigen::MatrixXd & ssproj, unsigned int nkf, double reg = 0.00001) {
    
    Eigen::VectorXi cref = Eigen::ArrayXi::Zero(sample.size());
    Eigen::VectorXi ret = Eigen::ArrayXi::Zero(nkf);
    Eigen::VectorXd rvec = Eigen::ArrayXd::Zero(cref.size());
    Eigen::MatrixXd utmp, hvec, ovec;
    Eigen::VectorXd v0, tvec;
    double max = 0;
    unsigned int last = cref.size();

    // build phi matrix
    //~ for (int i=0; i < sample.size(); ++i)
        //~ for (int j=i; j < sample.size(); ++j)
            //~ phimat(i, j) = phimat(j, i) = phi_rols(phimat(i, j));
    utmp = phimat;
    hvec = Eigen::ArrayXXd::Zero(2, utmp.cols());
    ovec = Eigen::ArrayXXd::Zero(2, utmp.cols());

    //std::cout << "choose control points" << std::endl;
    for (unsigned int i=0; i < nkf; ++i) {
        // calculate error
        //std::cout << "calculate error" << std::endl;
        for (int j=0; j < hvec.cols(); ++j) {
            if (cref(j) != 0) {
                ovec(0,j) = hvec(0,j) = 0;
                ovec(1,j) = hvec(1,j) = 0;
            } else {
                v0 = utmp.col(j);
                if (v0.dot(v0) == 0) {
					ovec(0,j) = hvec(0,j) = 0;
					ovec(1,j) = hvec(1,j) = 0;
				} else {
					tvec = ssproj.col(0);
					hvec(0,j) = v0.dot(tvec) / v0.dot(v0);
					ovec(0,j) = (hvec(0,j)*hvec(0,j) + reg)*v0.dot(v0) / tvec.dot(tvec);
					tvec = ssproj.col(1);
					hvec(1,j) = v0.dot(tvec) / v0.dot(v0);
					ovec(1,j) = (hvec(1,j)*hvec(1,j) + reg)*v0.dot(v0) / tvec.dot(tvec);
				}
            }
        }
        // get max error
        //std::cout << "get max error" << std::endl;
        v0 = ovec.colwise().sum();
        for (int j=0; j < cref.size(); ++j)
            if (cref(j) == 0) {
                last = j;
                max = v0(j);
                break;
            }
        for (int j=0; j < v0.size(); ++j)
            if ((cref(j) == 0) && (max < v0(j))) {
                max = v0(j);
                last = j;
            }
        //std::cout << "kf selected = " << sample(last) << std::endl;
        cref(last) = 1;
        ret(i) = sample(last);
        // orthogonalize matrix
        //std::cout << "orthogonalize matrix" << std::endl;
		v0 = utmp.col(last);
		// find rks
		for (int k=0; k < cref.size(); ++k)
			if (cref(k) == 0)
				rvec(k) = v0.dot(utmp.col(k)) / v0.dot(v0);
		// orthogonalize column
		for (int k=0; k < cref.size(); ++k) {
			if (cref(k) == 0) {
				v0 = utmp.col(k);
				v0 -= rvec(k) * utmp.col(last);
				utmp.col(k) = v0;
			}
		}
    }

    int discard = 0;
	// dummy sort with bubble sort
	for (unsigned int i=0; i < nkf; ++i)
		for (unsigned int j=i+1; j < nkf; ++j)
			if (ret(i) >= ret(j)) {
				discard = ret(i);
				ret(i) = ret(j);
				ret(j) = discard;
			}
	return ret;
}

/**
 * @brief get control points given by ROLS algorithm
 * @param data original data rowwise matrix
 * @param proj projected data rowwise matrix
 * @param pc percentage of control points
 * @return keyframe vector
 */
Eigen::VectorXi getRolsControlPoints(Eigen::MatrixXd & data, 
	Eigen::MatrixXd & proj, double pc = 0.03) {

	unsigned int nkf = pc*data.rows();
	// get subsample
    unsigned int size = 0.5 * data.rows();
    Eigen::VectorXi sample = Eigen::ArrayXi::Zero(size);
    size = data.rows() / size;

    for (unsigned int i=0; i < data.rows() - 1; i += size)
        sample(i / size) = i;
    sample(sample.size() - 1) = data.rows() - 1;
	// build phimat
	Eigen::MatrixXd phimat = Eigen::ArrayXXd::Zero(sample.size(), sample.size());
	Eigen::VectorXd v0, v1;
	for (unsigned int i=0; i < phimat.rows(); ++i) {
		v0 = data.row(sample(i));
		for (unsigned int j=i+1; j < phimat.cols(); ++j) {
			v1 = data.row(sample(j));
			v1 -= v0;
			phimat(i,j) = phimat(j,i) = phi_rols(v1.norm());
		}
	}
	// build projection
	Eigen::MatrixXd ssproj = Eigen::ArrayXXd::Zero(sample.size(), proj.cols());
	for (unsigned int i=0; i < ssproj.rows(); ++i) {
		ssproj.row(i) = proj.row(sample(i));
	}
	// get control points
	return chooseControlPoints(sample, phimat, ssproj, nkf);
}

/**
 * @brief return the implemented radial basis function kernel as quadratic multiquadrics
 * with c and epsilon equals one.
 * @param d2 distance as input
 * @return function output
 */
double phi_proj(double d2) {
	return std::sqrt(1 + d2*d2);  // multiquadrics - elisa
	//return (1 + std::pow(d2, 2.5));
}


/**
 * @brief get RBF projection onto a 2D plane given the control points 
 * and their corresponding projections
 * @param dmat distance matrix in original dimension
 * @param pmat projection matrix
 * @param kfs control points vector
 * @return a projection matrix with all poses
 */
Eigen::MatrixXd getRbfProjection(Eigen::MatrixXd & dmat,
Eigen::MatrixXd & pmat, Eigen::VectorXi & kfs) {
	
	Eigen::MatrixXd proj = pmat;
	// find lambda
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Zero(kfs.size(), kfs.size());
	Eigen::MatrixXd ymat = Eigen::ArrayXXd::Zero(kfs.size(), 2);
	
	for (int i=0; i < fmat.rows(); ++i)
		for (int j=i+1; j < fmat.cols(); ++j)
			fmat(j,i) = fmat(i,j) = phi_proj(dmat(kfs(i), kfs(j)));
	for (int i=0; i < ymat.rows(); ++i) {
		ymat(i,0) = proj(kfs(i),0);
		ymat(i,1) = proj(kfs(i),1);
	}
	Eigen::MatrixXd lmat = fmat.inverse() * ymat;
	
	// project remainder frames by RBF
	Eigen::VectorXd rbfvec = Eigen::ArrayXd::Zero(kfs.size());
	int c = 0;
	for (int i=0; i < dmat.rows(); ++i) {
		if ((i == kfs(c)) && (c < (kfs.size() - 1))) ++c;
		else {
			for (int j=0; j < rbfvec.size(); ++j)
				rbfvec(j) = phi_proj(dmat(i, kfs(j)));
			proj.row(i).head(2) = rbfvec.transpose() * lmat;
		}
	}
	normalizeProjection(proj);
	//std::cout << "RBF projection" << std::endl << proj << std::endl;
	return proj;
}

/**
 * @brief return the implemented radial basis function kernel as L2 distance
 * @param d2 distance as input
 * @return function output
 */
double phi_gd(double d2) {
	return std::fabs(d2);
}

/**
 * @brief get backward projection by an RBF function
 * @param val low dimension value to be reprojected
 * @param proj low dimension projection
 * @param lambda lambda coefficient matrix to solve RBF
 * @param ctps control points
 * @param ldim number of components of low dimension
 * @return high dimension back projection
 */
Eigen::VectorXd getBackProjection(Eigen::VectorXd const & val, Eigen::MatrixXd const & proj, 
Eigen::MatrixXd const & lambda, Eigen::VectorXi const &  ctps, unsigned int ldim) {
	Eigen::VectorXd v0, v1;
	Eigen::VectorXd lvec = Eigen::ArrayXd::Zero(lambda.rows());
	
	v0 = val.head(ldim);
	//~ std::cout << "input back projection = " << val.size() << std::endl;
	for (unsigned int j=0; j < ctps.size(); ++j) {
		v1 = proj.row(ctps(j)).head(ldim);
		v1 -= v0;
		lvec(j) = phi_gd(v1.norm());
	}
	lvec(ctps.size()) = 1;
	for (unsigned int j=0; j < ldim; ++j) {
		lvec(ctps.size() + 1 + j) = v0(j);
	}
	// error
	//~ std::cout << "lambda back projection = " << lvec.size() << std::endl;
	//~ std::cout << "lambda matrix = " << lambda.rows() << " x " << lambda.cols() << std::endl;
	v0 = lvec.transpose() * lambda;
	//~ std::cout << "output back projection = " << v0.size() << std::endl;
	return v0;
}

/**
 * @brief get the error vector of a reconstructed RBF/SKF animation for a given
 * frame projection matrix
 * @param proj frame projection matrix
 * @return error vector
 */

Eigen::VectorXd getErrorVector(Eigen::MatrixXd const & proj, Eigen::MatrixXd const & data, 
Eigen::MatrixXd const & lambda, Eigen::VectorXi const &  ctps, unsigned int ldim) {
	
	Eigen::VectorXd cerr = Eigen::ArrayXd::Zero(proj.rows());
	Eigen::VectorXd v0, v1;
	
	for (unsigned int i=0; i < proj.rows(); ++i) {
		v0 = proj.row(i);
		v0 = getBackProjection(v0, proj, lambda, ctps, ldim);
		v1 = data.row(i);
		v1 -= v0;
		cerr(i) = v1.norm();
	}
	for (unsigned int j=0; j < ctps.size(); ++j)
		cerr(ctps(j)) = 0;
	return cerr;
}

/**
 * @brief get the gradient matrix of a 2D projection with inverse projection
 * given by RBF.
 * @param proj low dimension projection matrix
 * @param nmat nearest neighbors matrix
 * @param data original dimension data matrix
 * @param ctps control point matrix
 * @param lambda RBF coefficient matrix
 * @param cerr error vector
 * @param alpha gradient descent parameter
 * @param ldim number of components of low dimension
 * @return gradient matrix
 */
Eigen::MatrixXd getGradientMatrix(Eigen::MatrixXd const & proj, Eigen::MatrixXi const & nmat, 
Eigen::MatrixXd const & data, Eigen::VectorXi const &  ctps, Eigen::MatrixXd const & lambda, 
Eigen::VectorXd const & cerr, double alpha, unsigned int ldim) {
		
	Eigen::MatrixXd error = Eigen::ArrayXXd::Zero(proj.rows(), 5);
	Eigen::MatrixXd grad = Eigen::ArrayXXd::Zero(proj.rows(), 2);
	Eigen::VectorXd v0, v1;
	Eigen::Vector4d cpos, cbpos, capos;

	// get NSEW delta error
	cpos(2) = 0;
	capos(2) = 0;
	cbpos(2) = 0;
	for (unsigned int i=0; i < error.rows(); ++i) {
		// delta east error
		v0 = data.row(i);
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		cbpos.head(2) = proj.row(nmat(i,0)).head(2);
		cbpos(3) = 1;
		capos.head(2) = proj.row(nmat(i,1)).head(2);
		capos(3) = 1;
		cbpos -= cpos;
		capos -= cpos;
		error(i,4) = (cbpos.norm() + capos.norm()) / 2; // circle radius
		capos = cpos;
		capos(0) += alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,0) = v1.norm();
		error(i,0) -= cerr(i);
		// delta west error
		capos = cpos;
		capos(0) -= alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,1) = v1.norm();
		error(i,1) -= cerr(i);
		// delta north error
		capos = cpos;
		capos(1) += alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,2) = v1.norm();
		error(i,2) -= cerr(i);
		// delta south error
		capos = cpos;
		capos(1) -= alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,3) = v1.norm();
		error(i,3) -= cerr(i);
	}
	for (unsigned int j=0; j < ctps.size(); ++j) {
		error(ctps(j),0) = error(ctps(j),1) = error(ctps(j),2) = error(ctps(j),3) =0;
	}
	for (unsigned int i=0; i < error.rows(); ++i) {
		grad(i,0) = 0;
		grad(i,1) = 0;
		if ((error(i,0) < 0) || (error(i,1) < 0)) {
			grad(i,0) = error(i,1) - error(i,0);
		}
		if ((error(i,2) < 0) || (error(i,3) < 0)) {
			grad(i,1) = error(i,3) - error(i,2);
		}
	}
	for (unsigned int i=0; i < grad.rows(); ++i) {
		if ((grad(i,0) != 0) || (grad(i,1) != 0)) {
			grad.row(i).normalize();
			grad.row(i) *= error(i,4);
		}
	}
	return grad;
}

/**
 * @brief get the nearest neighbor matrix given by index reference
 * @param proj rowwise projection matrix
 * @param nn number of nearest neighbors
 * @return neighbor matrix
 */
Eigen::MatrixXi getNNeighborMatrix(Eigen::MatrixXd const &  proj, unsigned int nn = 2) {
	Eigen::MatrixXi ret = Eigen::ArrayXXi::Zero(proj.rows(), nn);
	Eigen::VectorXd v0, v1, error;
	Eigen::VectorXi min = Eigen::ArrayXi::Zero(proj.rows());
	double mv;
	unsigned int mi, aux;
	
	error = proj.col(0);
	for (unsigned int i=0; i < ret.rows(); ++i) {
		v0 = proj.row(i);
		// calculate error to every point
		for (unsigned int j=0; j < error.size(); ++j) {
			v1 = proj.row(j);
			v1 -= v0;
			error(j) = v1.norm();
		}
		// get minimuns errors
		for (unsigned int j=0; j < min.size(); ++j) {
			min(j) = j;
		}
		for (unsigned int j=0; j < nn+1; ++j) {
			mi = j;
			mv = error(j);
			for (unsigned int k=j+1; k < error.size(); ++k) {
				if (mv > error(k)) {
					mi = k;
					mv = error(k);
				}			
			}
			mv = error(mi);
			error(mi) = error(j);
			error(j) = mv;
			aux = min(j);
			min(j) = min(mi);
			min(mi) = aux;
		}
		for (unsigned int j=0; j < ret.cols(); ++j) {
			ret(i,j) = min(j+1);
		}
	}
	return ret;	
}

/**
 * @brief get RBF coefficients in form of a matrix given input and output
 * @param data output values for RBF
 * @param proj input values for RBF
 * @param ctps control points: values to build RBF
 * @param ldim number of components of input dimension
 * @return RBF coefficient matrix
 */
Eigen::MatrixXd getLambdaMatrix(Eigen::MatrixXd const & data, Eigen::MatrixXd const &  proj, 
Eigen::VectorXi const &  ctps, unsigned int ldim) {
	
	Eigen::MatrixXd cmat = Eigen::ArrayXXd::Zero(ctps.size() + 1 + ldim, ctps.size() + 1 + ldim);
	Eigen::VectorXd v0, v1;
	for (unsigned int i=0; i < ctps.size(); ++i) {
		v0 = proj.row(ctps(i));
		for (unsigned int j=i+1; j < ctps.size(); ++j) {
			v1 = proj.row(ctps(j));
			v1 -= v0;
			cmat(i,j) = cmat(j,i) = phi_gd(v1.norm());
		}
		cmat(i,ctps.size()) = cmat(ctps.size(),i) = 1;
		for (unsigned int j=0; j < ldim; ++j) {
			cmat(i,ctps.size()+1+j) = cmat(ctps.size()+1+j,i) = proj(ctps(i),j);
		}
	}
	
	Eigen::MatrixXd ymat = Eigen::ArrayXXd::Zero(ctps.size() + 1 + ldim, data.cols());
	for (unsigned int i=0; i < ctps.size(); ++i) {
		ymat.row(i) = data.row(ctps(i));
	}
	Eigen::MatrixXd lmat = cmat.inverse() * ymat;
	return lmat;
}


/**
 * @brief improve a given projection matrix
 * @param data high dimension data matrix
 * @param proj projection matrix
 * @param ctps control point reference
 * @return improved frame projection matrix
 */
Eigen::MatrixXd getImprovedProjection(Eigen::MatrixXd const & data, Eigen::MatrixXd const &  proj, 
Eigen::VectorXi const &  ctps, unsigned int ldim = 2) {
	
	Eigen::MatrixXd grad;
	Eigen::VectorXd cerr;
	Eigen::MatrixXd ret = proj;
	double total0, total1, alpha = 0.5;
	unsigned int i;

	Eigen::MatrixXd lambda = getLambdaMatrix(data, proj, ctps, ldim);
	//~ std::cout << "get lambda matrix = " << lambda.rows() << " x " << lambda.cols() << std::endl;
	cerr = getErrorVector(ret, data, lambda, ctps, ldim);
	//~ std::cout << "get error vector = " << cerr.size() << std::endl;
	Eigen::MatrixXi nmat = getNNeighborMatrix(ret);
	//~ std::cout << "get nn matrix = " << nmat.rows() << " x " << nmat.cols() << std::endl;
	total0 = cerr.sum();
	//std::cout << "original projection error = " << total0 << std::endl;
	for (i=0; i < 200; ++i) {
		grad = getGradientMatrix(ret, nmat, data, ctps, lambda, cerr, alpha, ldim);
		//~ std::cout << "get gradient matrix = " << grad.rows() << " x " << grad.cols() << std::endl;
		ret.block(0, 0, ret.rows(), grad.cols()) += alpha * grad;
		cerr = getErrorVector(ret, data, lambda, ctps, ldim);	
		total1 = cerr.sum();
		//std::cout << "delta projection error = " << total1 << std::endl;
		if (total1 == total0) break;
		total0 = total1;
	}
	//~ total1 = 100 * (tini - total0) / tini;
	//~ tini /= data.rows();
	//~ total0 /= data.rows();
	//~ std::cout << "original projection error = " << tini << std::endl;
	//~ std::cout << "delta projection error = " << total0 << std::endl;
	//~ std::cout << "total of iterations = " << i << std::endl;
	//~ std::cout << "error gain = " << total1 << " %" << std::endl;
	return ret;
}

/**
 * @brief calculates stress function for multidimensional projection
 * @param dmat distance matrix in high dimension
 * @param proj projected data in low dimension
 * @return calculated stress
 */
double getStress(Eigen::MatrixXd const & dmat, Eigen::MatrixXd const & proj) {
	double s0 = 0, s1 = 0, ldist;
	Eigen::VectorXd v0, v1;
	for (unsigned int i=0; i < dmat.rows(); ++i) {
		v0 = proj.row(i).head(2);
		for (unsigned int j=i+1; j < dmat.rows(); ++j) {
			v1 = proj.row(j).head(2);
			v1 -= v0;
			ldist = v1.norm();
			s0 += (ldist - dmat(i,j))*(ldist - dmat(i,j));
            s1 += ldist*ldist;
		}
	}
    s0 = s0 / s1;
	return s0;
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param proj matrix with 2D projected frames
 * @param ctps key frame ids vector
 */
void saveProjection(std::string const & pname, Eigen::MatrixXd const & proj, Eigen::VectorXi const & ctps) {
    std::ofstream outfile;
	Eigen::MatrixXd fmat = proj;

	for (unsigned int i=0; i < ctps.size(); ++i)
		fmat(ctps(i),2) = 1;
    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i)
			outfile << fmat(i,0) << " " << fmat(i,1) << " " << fmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

/**
 * @brief get random unused keyframes/control points for learning iteration
 * @param frames array with number of times a frame was used as keyframe/control point
 * @param marker current iteration
 * @param size number of keyframes/control point to be generated
 * @return keyframe/control point array
 */
Eigen::VectorXi getRandomKeyFrames(Eigen::VectorXi & frames, int marker, unsigned int size) {
	unsigned int c = 0, i = 0;
	// build vector
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(size);
	for (i=0; i < frames.size(); ++i)
		if (frames(i) == marker) c++;
	if (c < size) c = size;
	// build sample
	Eigen::VectorXi sample = Eigen::ArrayXi::Zero(c);
	c = 0;
	for (i=0; i < frames.size(); ++i)
		if (frames(i) == marker) sample(c++) = i;
	for (i=0; (i < frames.size()) && (c < size); ++i)
		if (frames(i) != marker) sample(c++) = i;
	// sort
	c = 0;
	while (c < size) {
		i = (unsigned int) std::rand();
		i = i % (sample.size() - c);
		ret(c++) = sample(i);
		sample(i) = sample(sample.size() - c);
	}
	return ret;
}

/**
 * @brief apply k-fold learning over data using forward and backward optimization
 * @param data high dimension data matrix
 * @param iproj projection matrix (updated)
 * @param ctps control point reference
 * @return improved frame projection matrix
 * @param pass number of iterations through data set
 */
void learn(Eigen::MatrixXd & data, Eigen::MatrixXd & iproj, Eigen::VectorXi & ctps, unsigned int pass) {
	Eigen::MatrixXd lproj = iproj;
	Eigen::VectorXi kfs = ctps;
	Eigen::VectorXi frames = Eigen::ArrayXi::Zero(iproj.rows());
	unsigned int fold = iproj.rows();
	fold = fold / ctps.size();
	for (unsigned int i=0; i < pass; ++i) {
		for (unsigned int k=0; k < fold; ++k) {
			for (unsigned int j=0; j < kfs.size(); ++j) 
				frames(kfs(j)) += 1;
			kfs = getRandomKeyFrames(frames, i, kfs.size());
			lproj = getImprovedProjection(data, lproj, kfs);
		}
	}
	iproj = lproj;
}

int main(int argc, char ** argv) {
    int opt;
    float cpratio = 3.0f;
    unsigned int lpar = 0;
	std::string infile, inmeta, outfile, outfrbf, outfopt, ptype;
	bool emask[9] = {false, false, false, false, false, false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "climpfor")) != -1 ) {
        switch (opt) {
            case 'i':
				infile = argv[optind];
				emask[0] = true;
                break;
            case 'm':
				inmeta = argv[optind];
				emask[1] = true;
                break;
            case 'r':
				outfrbf = argv[optind];
				emask[5] = true;
                break;
            case 'f':
				outfile = argv[optind];
				emask[3] = true;
                break;
            case 'o':
				outfopt = argv[optind];
				emask[4] = true;
                break;
            case 'p':
				ptype = argv[optind];
				emask[2] = true;
                break;
            case 'c':
				cpratio = atof(argv[optind]);
				emask[6] = true;
                break;
            case 'l':
				lpar = atoi(argv[optind]);
				emask[7] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[8] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1] || !emask[2] || !emask[3] || !emask[4] || !emask[5]) {
		if (!emask[0]) std::cout << "* input file" << std::endl;
		if (!emask[1]) std::cout << "* input metadata file" << std::endl;
		if (!emask[2]) std::cout << "* projection type" << std::endl;
		if (!emask[3]) std::cout << "* output projection file" << std::endl;
		if (!emask[4]) std::cout << "* output optimized projection file" << std::endl;
		if (!emask[5]) std::cout << "* output rbf projection file" << std::endl;
		exit(0);
	}
	if (!emask[8]) exit(0);
	if ((cpratio > 100) || (cpratio < 0)) {
		std::cout << "Invalid percentage of control points. Range must be [0-100]." << std::endl;
		exit(0);		
	}
	cpratio /= 100;

	Eigen::MatrixXd data = readProjection(infile, inmeta);
	//~ std::cout << "get data matrix = " << data.rows() << " x " << data.cols() << std::endl;
	int res = getProjectionStrategy(ptype);
	if (res < 0) {
		std::cout << "Invalid projection strategy" << std::endl;
		exit(0);
	}
	Eigen::MatrixXd dmat = getDistanceMatrix(data);
	//~ std::cout << "get distance matrix = " << dmat.rows() << " x " << dmat.cols() << std::endl;
	Eigen::MatrixXd proj = getProjectionData(res, data, dmat);
	//~ std::cout << "get projection data = " << proj.rows() << " x " << proj.cols() << std::endl;
	Eigen::VectorXi ctps = getRolsControlPoints(data, proj, cpratio);
	//~ std::cout << "get rols control points = " << ctps.size() << std::endl;
	Eigen::MatrixXd rproj = getRbfProjection(dmat, proj, ctps);
	//~ std::cout << "get rols/rbf projection = " << rproj.rows() << " x " << rproj.cols() << std::endl;
	Eigen::MatrixXd iproj = getImprovedProjection(data, rproj, ctps);
	//~ std::cout << "get optimized points = " << iproj.rows() << " x " << iproj.cols() << std::endl;
	// get stress
	learn(data, iproj, ctps, lpar);
	double stress = getStress(dmat,proj);
	std::cout << "Orig stress = " << stress << std::endl;
	stress = getStress(dmat,rproj);
	std::cout << "RBFP stress = " << stress << std::endl;
	stress = getStress(dmat,iproj);
	std::cout << "Opt stress = " << stress << std::endl;
	saveProjection(outfile, proj, ctps);
	saveProjection(outfrbf, rproj, ctps);
	saveProjection(outfopt, iproj, ctps);
	return 0;
}
