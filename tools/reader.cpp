#include <bvhreader.h>
#include <asfreader.h>
#include <skeleton.h>
#include <iostream>
#include <libgen.h>
#include <cstdlib>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <renderhelper.h>

Skeleton *skel;
MocapReader *mocap;
enum Control { PLAY, PAUSE, FORWARD, REWIND };
Control pp;
int height;
Eigen::Vector3d eye, center, up;
int readfr;
unsigned int nf, fi;
double zn,zf;

/**
 * \param value frame id to be rendered
 * 
 * loads given frame id into the rendered skeleton
 */	
void getFrame(int value) {
	Skeleton fs = mocap->getFrameSkeleton(value);

	skel->setSkeleton(fs);
	glutPostRedisplay();
	switch (pp) {
		case PLAY:
		case FORWARD:
			glutTimerFunc(fi, getFrame, (value + 1) % nf);
		break;
		case REWIND:
			glutTimerFunc(fi, getFrame, (value - 1) % nf);
		break;
		case PAUSE:
		default:
		break;
	}
	readfr = value;
}

/**
 * glut reshape handling
 */	
void reshape(int w, int h) {
    height = h;
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glViewport(0, 0, w, h); 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective (45.0, ((float) w)/ h, zn, zf);
    gluLookAt(eye(0), eye(1), eye(2), 
		center(0), center(1), center(2), 
		up(0), up(1), up(2));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

/**
 * glut display handling
 */	
void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//drawGround(100);
	drawAxis();
	if (skel != NULL) skel->draw(GL_RENDER);
	glutSwapBuffers();
}

/**
 * \param key ascii value for the pressed key
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * glut keyboard handler
 */	
void keyboard(unsigned char key, int x, int y) {
	
	switch (key) {
		case 27: // esc
			exit(0);
			break;
		case 80:
		case 112: // P ou p
			if (pp == PAUSE) 
				glutTimerFunc(fi, getFrame, readfr % nf);
			pp = PLAY;
			break;
		case 70:
		case 102: // F ou f
			pp = FORWARD;
			break;
		case 82:
		case 114: // R ou r
			pp = REWIND;
			break;
		default:
			pp = PAUSE;
			std::cout << "current frame is " << (readfr + 2) << std::endl;
			getFrame((readfr + 1) % nf);
			skel->debug();
	}
	
}

int main(int argc, char ** argv) {
	Skeleton s;

	if (argc < 2) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	if (ext.compare("bvh") == 0) {
		mocap = new BVHReader();
		mocap->readFile(fname);
		s = mocap->getFrameSkeleton(0);
	} else if (ext.compare("asf") == 0) {
		mocap = new ASFReader();
		mocap->readFile(fname);
		s = mocap->getSkeleton();
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				mocap->readFile(fname);
				s = mocap->getFrameSkeleton(0);
			}
		}
	}
	skel = &s;
	Eigen::MatrixXd dofs = mocap->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd skpos = dofs.block(0, 0, dofs.rows(), 3);
	setMocapPerspective(skpos);
	pp = PAUSE;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(400, 400);
	glutCreateWindow("Animation stub");
	readfr = 0;
	nf = mocap->getNumberOfFrames();
	fi = mocap->getFrameInterval();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
    glutMainLoop();
    delete mocap;
	return 0;
}

