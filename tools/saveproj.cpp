#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <Eigen/Dense>
#include <bvhreader.h>
#include <animation.h>
#include <fstream>
#include <kfselector.h>

double wtime;

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input file: -f" << std::endl;
	std::cout << "\tset key frame selection algorithm: -s" << std::endl;
	std::cout << "\tset projection strategy: -p" << std::endl;
	std::cout << "\tset projection output file: -o" << std::endl;
	std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset BVH interpolated output file: -b" << std::endl;
	std::cout << "\tset number of key frames as a percentage {0-100}: -k" << std::endl;
	std::cout << "\tset time distance weight {0-...}: -w" << std::endl << std::endl;
	std::cout << name << " -f [BHV file] -s [pb|scs|rols|rbfp|fwbk] -w [time distance weight] -k [key frame ratio] -p [1D|force|mds|lle|lamp|pca|tsne|isomap] -o [2D output file] -b [BHV interpolation file]" << std::endl;
}

/**
 * @brief get selection algorithm code by a command line parameter
 * @param name algorithm specification
 * @return internal code
 */
int getSelectionAlgorithm(std::string const & name) {
	int ret = -1;
	if (strcasecmp(name.c_str(), "pb") == 0) {
		ret = 0;
	} else if (strcasecmp(name.c_str(), "scs") == 0) {
		ret = 1;
	} else if (strcasecmp(name.c_str(), "rols") == 0) {
		ret = 2;
	} else if (strcasecmp(name.c_str(), "rbfp") == 0) {
		ret = 3;
	} else if (strcasecmp(name.c_str(), "fwbk") == 0) {
		ret = 4;
	}
	return ret;
}

/**
 * @brief get projection strategy code by a command line parameter
 * @param name projection specification
 * @return internal code
 */
int getProjectionStrategy(std::string const & name) {
	int ret = -1;
	
	if (strcasecmp(name.c_str(), "force") == 0) {
		ret = 0;
	} else if (strcasecmp(name.c_str(), "mds") == 0) {
		ret = 1;
	} else if (strcasecmp(name.c_str(), "lle") == 0) {
		ret = 2;
	}  else if (strcasecmp(name.c_str(), "lamp") == 0) {
		ret = 3;
	} else if (strcasecmp(name.c_str(), "pca") == 0) {
		ret = 4;
	} else if (strcasecmp(name.c_str(), "tsne") == 0) {
		ret = 5;
	} else if (strcasecmp(name.c_str(), "isomap") == 0) {
		ret = 6;
	} else if (strcasecmp(name.c_str(), "1D") == 0) {
		ret = 7;
	}
	return ret;
}

/**
 * @brief run Force projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithForce(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucForceRbfSelector posforce(mocap, ratio, wtime);
	std::cout << "Position-based selector with 2D SKF interpolation and force spread approach" << std::endl;
	posforce.printStats();
	prmat = posforce.getFramesProjection();
	kfvec = posforce.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run Force projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithForce(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucForceRbfSelector scsforce(mocap, ratio, wtime);		
	std::cout << "SCS selector with 2D SKF interpolation and force spread approach" << std::endl;
	scsforce.printStats();
	prmat = scsforce.getFramesProjection();
	kfvec = scsforce.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run Force projection with Rols selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWithForce(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucForceRbfSelector rolsforce(mocap, ratio, wtime);
	std::cout << "ROLS selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsforce.printStats();
	prmat = rolsforce.getFramesProjection();
	kfvec = rolsforce.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run Force projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWithForce(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucForceRbfSelector rbfpforce(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and force spread approach" << std::endl;
	rbfpforce.printStats();
	prmat = rbfpforce.getFramesProjection();
	kfvec = rbfpforce.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run Force projection with forward and backward selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runFwdBkwdWithForce(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	FwdBkwdEucForceRbfSelector fwdbkwdforce(mocap, ratio, wtime);
	std::cout << "FwdBkwd selector with 2D SKF interpolation and force spread approach" << std::endl;
	fwdbkwdforce.printStats();
	prmat = fwdbkwdforce.getFramesProjection();
	kfvec = fwdbkwdforce.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run MDS projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithMds(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucMdsRbfSelector posmds(mocap, ratio, wtime);	
	std::cout << "Position-based selector with 2D SKF interpolation and MDS approach" << std::endl;
	posmds.printStats();
	prmat = posmds.getFramesProjection();
	kfvec = posmds.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run MDS projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithMds(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucMdsRbfSelector scsmds(mocap, ratio, wtime);
	std::cout << "SCS selector with 2D SKF interpolation and MDS approach" << std::endl;
	scsmds.printStats();
	prmat = scsmds.getFramesProjection();
	kfvec = scsmds.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run MDS projection with Rols selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWithMds(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucMdsRbfSelector rolsmds(mocap, ratio, wtime);
	std::cout << "ROLS selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsmds.printStats();
	prmat = rolsmds.getFramesProjection();
	kfvec = rolsmds.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run MDS projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWithMds(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucMdsRbfSelector rbfpmds(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and MDS approach" << std::endl;
	rbfpmds.printStats();
	prmat = rbfpmds.getFramesProjection();
	kfvec = rbfpmds.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run MDS projection with forward and backward selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runFwdBkwdWithMds(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	FwdBkwdEucMdsRbfSelector fwbkwdmds(mocap, ratio, wtime);
	std::cout << "FwdBkwd selector with 2D SKF interpolation and MDS approach" << std::endl;
	fwbkwdmds.printStats();
	prmat = fwbkwdmds.getFramesProjection();
	kfvec = fwbkwdmds.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LLE projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithLle(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucLleRbfSelector poslle(mocap, ratio, wtime);
	std::cout << "Position-based selector with 2D SKF interpolation and LLE approach" << std::endl;
	poslle.printStats();
	prmat = poslle.getFramesProjection();
	kfvec = poslle.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LLE projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithLle(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucLleRbfSelector scslle(mocap, ratio, wtime);
	std::cout << "SCS selector with 2D SKF interpolation and LLE approach" << std::endl;
	scslle.printStats();
	prmat = scslle.getFramesProjection();
	kfvec = scslle.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LLE projection with Rols selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWithLle(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucLleRbfSelector rolslle(mocap, ratio, wtime);
	std::cout << "ROLS selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolslle.printStats();
	prmat = rolslle.getFramesProjection();
	kfvec = rolslle.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LLE projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWithLle(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucLleRbfSelector rbfplle(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and LLE approach" << std::endl;
	rbfplle.printStats();
	prmat = rbfplle.getFramesProjection();
	kfvec = rbfplle.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LLE projection with forward and backward selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runFwdBkwdWithLle(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	FwdBkwdEucLleRbfSelector fwdbkwdlle(mocap, ratio, wtime);
	std::cout << "FwdBkwd selector with 2D SKF interpolation and LLE approach" << std::endl;
	fwdbkwdlle.printStats();
	prmat = fwdbkwdlle.getFramesProjection();
	kfvec = fwdbkwdlle.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LAMP projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithLamp(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {
		
	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucLampRbfSelector poslamp(mocap, ratio, wtime);
	std::cout << "Position-based selector with 2D SKF interpolation and LAMP approach" << std::endl;
	poslamp.printStats();
	prmat = poslamp.getFramesProjection();
	kfvec = poslamp.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run LAMP projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithLamp(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucLampRbfSelector scslamp(mocap, ratio, wtime);
	std::cout << "SCS selector with 2D SKF interpolation and LAMP approach" << std::endl;
	scslamp.printStats();
	prmat = scslamp.getFramesProjection();
	kfvec = scslamp.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run PCA projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithPca(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucPcaRbfSelector pospca(mocap, ratio, wtime);
	std::cout << "Position-based selector with 2D SKF interpolation and PCA approach" << std::endl;
	pospca.printStats();
	prmat = pospca.getFramesProjection();
	kfvec = pospca.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run PCA projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithPca(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucPcaRbfSelector scspca(mocap, ratio, wtime);
	std::cout << "SCS selector with 2D SKF interpolation and PCA approach" << std::endl;
	scspca.printStats();
	prmat = scspca.getFramesProjection();
	kfvec = scspca.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run PCA projection with Rols selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWithPca(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucPcaRbfSelector rolspca(mocap, ratio, wtime);
	std::cout << "ROLS selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolspca.printStats();
	prmat = rolspca.getFramesProjection();
	kfvec = rolspca.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run PCA projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWithPca(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucPcaRbfSelector rbfppca(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and PCA approach" << std::endl;
	rbfppca.printStats();
	prmat = rbfppca.getFramesProjection();
	kfvec = rbfppca.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run PCA projection with forward and backward selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runFwdBkwdWithPca(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	FwdBkwdEucPcaRbfSelector fwdbkwdpca(mocap, ratio, wtime);
	std::cout << "FwdBkwd selector with 2D SKF interpolation and PCA approach" << std::endl;
	fwdbkwdpca.printStats();
	prmat = fwdbkwdpca.getFramesProjection();
	kfvec = fwdbkwdpca.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run t-SNE projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithTsne(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucTsneRbfSelector postsne(mocap, ratio, wtime);
	std::cout << "Position-based selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	postsne.printStats();
	prmat = postsne.getFramesProjection();
	kfvec = postsne.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run t-SNE projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithTsne(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucTsneRbfSelector scstsne(mocap, ratio, wtime);
	std::cout << "SCS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	scstsne.printStats();
	prmat = scstsne.getFramesProjection();
	kfvec = scstsne.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run t-SNE projection with Rols selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWithTsne(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucTsneRbfSelector rolstsne(mocap, ratio, wtime);
	std::cout << "ROLS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolstsne.printStats();
	prmat = rolstsne.getFramesProjection();
	kfvec = rolstsne.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run t-SNE projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWithTsne(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucTsneRbfSelector rbfptsne(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rbfptsne.printStats();
	prmat = rbfptsne.getFramesProjection();
	kfvec = rbfptsne.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run t-SNE projection with forward and backward selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runFwdBkwdWithTsne(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	FwdBkwdEucTsneRbfSelector fwdbkwdtsne(mocap, ratio, wtime);
	std::cout << "FwdBkwd selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	fwdbkwdtsne.printStats();
	prmat = fwdbkwdtsne.getFramesProjection();
	kfvec = fwdbkwdtsne.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with PB selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWithIsomap(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucIsomapRbfSelector posisomap(mocap, ratio, wtime);
	std::cout << "Position-based selector with 2D SKF interpolation and Isomap approach" << std::endl;
	posisomap.printStats();
	prmat = posisomap.getFramesProjection();
	kfvec = posisomap.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with SCS selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWithIsomap(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucIsomapRbfSelector scsisomap(mocap, ratio, wtime);
	std::cout << "SCS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	scsisomap.printStats();
	prmat = scsisomap.getFramesProjection();
	kfvec = scsisomap.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with Rols selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWithIsomap(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucIsomapRbfSelector rolsisomap(mocap, ratio, wtime);
	std::cout << "ROLS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsisomap.printStats();
	prmat = rolsisomap.getFramesProjection();
	kfvec = rolsisomap.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWithIsomap(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucIsomapRbfSelector rbfpisomap(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rbfpisomap.printStats();
	prmat = rbfpisomap.getFramesProjection();
	kfvec = rbfpisomap.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with forward and backward selection algorithm to get key frames
 * and frame 2D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runFwdBkwdWithIsomap(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	FwdBkwdEucIsomapRbfSelector fwdbkwdisomap(mocap, ratio, wtime);
	std::cout << "FwdBkwd selector with 2D SKF interpolation and Isomap approach" << std::endl;
	fwdbkwdisomap.printStats();
	prmat = fwdbkwdisomap.getFramesProjection();
	kfvec = fwdbkwdisomap.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with PB selection algorithm to get key frames
 * and frame 1D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runPbWith1D(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucRbfSelector pos(mocap, ratio, wtime);
	std::cout << "Position-based selector with 1D SKF interpolation" << std::endl;
	pos.printStats();
	prmat = pos.getFramesProjection();
	kfvec = pos.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with SCS selection algorithm to get key frames
 * and frame 1D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runScsWith1D(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucRbfSelector scs(mocap, ratio, wtime);
	std::cout << "SCS selector with 1D SKF interpolation" << std::endl;
	scs.printStats();
	prmat = scs.getFramesProjection();
	kfvec = scs.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with Rols selection algorithm to get key frames
 * and frame 1D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRolsWith1D(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsEucRbfSelector rols(mocap, ratio, wtime);
	std::cout << "ROLS selector with 1D SKF interpolation" << std::endl;
	rols.printStats();
	prmat = rols.getFramesProjection();
	kfvec = rols.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run isomap projection with Rols/Rbfp selection algorithm to get key frames
 * and frame 1D projection
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runRbfpWith1D(std::string const & bvhfile, float ratio,
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	RolsRbfpEucRbfSelector rbfp(mocap, ratio, wtime);
	std::cout << "ROLS/RBFP selector with 1D SKF interpolation" << std::endl;
	rbfp.printStats();
	prmat = rbfp.getFramesProjection();
	kfvec = rbfp.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run the specified algorithm for key frame selection and projection
 * and get its results.
 * @param option internal code for selection/projection. Selection code is 
 * {0,1,2,3,4} for {pb, scs, rols, rbfp, fwbk}. Projection code is {0,5,10,15,20,25,30,35} for
 * {force, mds, lle, lamp, pca, tsne, isomap, 1d}. Internal code is the sum of both.
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 * @param prmat frame projection matrix on 2D (output)
 */
void runCompressionScheme(int option, std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec, Eigen::MatrixXd & prmat) {
	
	switch (option) {
		case 0:
			runPbWithForce(bvhfile, ratio, kfvec, prmat);
		break;
		case 1:
			runScsWithForce(bvhfile, ratio, kfvec, prmat);
		break;
		case 2:
			runRolsWithForce(bvhfile, ratio, kfvec, prmat);
		break;
		case 3:
			runRbfpWithForce(bvhfile, ratio, kfvec, prmat);
		break;
		case 4:
			runFwdBkwdWithForce(bvhfile, ratio, kfvec, prmat);
		break;		
		case 5:
			runPbWithMds(bvhfile, ratio, kfvec, prmat);
		break;
		case 6:
			runScsWithMds(bvhfile, ratio, kfvec, prmat);
		break;
		case 7:
			runRolsWithMds(bvhfile, ratio, kfvec, prmat);
		break;
		case 8:
			runRbfpWithMds(bvhfile, ratio, kfvec, prmat);
		break;
		case 9:
			runFwdBkwdWithMds(bvhfile, ratio, kfvec, prmat);
		break;
		case 10:
			runPbWithLle(bvhfile, ratio, kfvec, prmat);
		break;
		case 11:
			runScsWithLle(bvhfile, ratio, kfvec, prmat);
		break;
		case 12:
			runRolsWithLle(bvhfile, ratio, kfvec, prmat);
		break;
		case 13:
			runRbfpWithLle(bvhfile, ratio, kfvec, prmat);
		break;
		case 14:
			runFwdBkwdWithLle(bvhfile, ratio, kfvec, prmat);
		break;
		case 15:
			runPbWithLamp(bvhfile, ratio, kfvec, prmat);
		break;
		case 16:
			runScsWithLamp(bvhfile, ratio, kfvec, prmat);
		break;
		case 17:
		case 18:
		case 19:
			std::cout << "ROLS, ROLS/RBFP or FwdBkwd selector with 2D SKF interpolation and LAMP approach is not implemented." << std::endl;
			exit(0);
		break;
		case 20:
			runPbWithPca(bvhfile, ratio, kfvec, prmat);
		break;
		case 21:
			runScsWithPca(bvhfile, ratio, kfvec, prmat);
		break;
		case 22:
			runRolsWithPca(bvhfile, ratio, kfvec, prmat);
		break;
		case 23:
			runRbfpWithPca(bvhfile, ratio, kfvec, prmat);
		break;
		case 24:
			runFwdBkwdWithPca(bvhfile, ratio, kfvec, prmat);
		break;		
		case 25:
			runPbWithTsne(bvhfile, ratio, kfvec, prmat);
		break;
		case 26:
			runScsWithTsne(bvhfile, ratio, kfvec, prmat);
		break;
		case 27:
			runRolsWithTsne(bvhfile, ratio, kfvec, prmat);
		break;
		case 28:
			runRbfpWithTsne(bvhfile, ratio, kfvec, prmat);
		break;
		case 29:
			runFwdBkwdWithTsne(bvhfile, ratio, kfvec, prmat);
		break;
		case 30:
			runPbWithIsomap(bvhfile, ratio, kfvec, prmat);
		break;
		case 31:
			runScsWithIsomap(bvhfile, ratio, kfvec, prmat);
		break;
		case 32:
			runRolsWithIsomap(bvhfile, ratio, kfvec, prmat);
		break;
		case 33:
			runRbfpWithIsomap(bvhfile, ratio, kfvec, prmat);
		break;
		case 34:
			runFwdBkwdWithIsomap(bvhfile, ratio, kfvec, prmat);
		break;
		case 35:
			runPbWith1D(bvhfile, ratio, kfvec, prmat);
		break;
		case 36:
			runScsWith1D(bvhfile, ratio, kfvec, prmat);
		break;
		case 37:
			runRolsWith1D(bvhfile, ratio, kfvec, prmat);
		break;
		case 38:
			runRbfpWith1D(bvhfile, ratio, kfvec, prmat);
		break;
		case 39:
			std::cout << "FwdBkwd selector with 1D SKF interpolation is not implemented." << std::endl;
			exit(0);
		default:
			std::cout << "Unknown scheme is not implemented." << std::endl;
			exit(0);
		break;
	}
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param fmat matrix with 2D projected frames
 * @param kfvec key frame ids vector
 */
void dumpFrameProjection(std::string const & pname, Eigen::MatrixXd const & fmat, Eigen::VectorXi const & kfvec) {
    std::ofstream outfile;
    Eigen::MatrixXd dumpmat = Eigen::ArrayXXd::Zero(fmat.rows(), 3);

    dumpmat.block(0, 0, fmat.rows(), 2) = fmat.block(0, 0, fmat.rows(), 2);
    for (unsigned int i=0; i < kfvec.size(); ++i)
		dumpmat(kfvec(i), 2) = 1;

    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < dumpmat.rows(); ++i)
			outfile << dumpmat(i,0) << " " << dumpmat(i,1) << " " << dumpmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

/**
 * @brief dump non animation BVH contents from one file to another
 * @param bvhin input file name
 * @param bvhout output file name
 */
void dumpBvhStructure(std::string const & bvhin, std::string const & bvhout) {
    std::ifstream infile;
    std::ofstream outfile;
    std::string line;
    
    infile.open(bvhin.c_str());
    outfile.open(bvhout.c_str(), std::ios::trunc);
    if (infile.is_open() && outfile.is_open()) {
        while (std::getline(infile, line)) {
			if (strncasecmp(line.c_str(), "frame time", 10) == 0) {
				outfile << line << std::endl;
				break;
			} else {
				outfile << line << std::endl;
			}
        }
        infile.close();
        outfile.close();

    } else {
        std::cout << "Error while opening files " << bvhin << " and/or " << bvhout << std::endl;
    }   
}

/**
 * @brief dump reconstruced animation of BVH contents to a file
 * @param bvhin original bvh file name
 * @param bvhout output file name
 * @param fmat matrix of 2D frame projection
 * @param kfvec key frame ids vector
 */
void dumpBvhAnimation(std::string const & bvhin, std::string const & bvhout, 
	Eigen::MatrixXd const & fmat, Eigen::VectorXi const & kfvec) {

	Eigen::Vector4d cpos;
	unsigned int id;
	
	// construct animation
	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhin);	

    unsigned int totframes = mocap->getNumberOfFrames();
	Skeleton ms = mocap->getFrameSkeleton(0);
	Animation * skfanim = new Animation();
	skfanim->setSkeleton(ms);
	cpos << 0,0,0,1;
	for (unsigned int i=0; i < kfvec.size(); ++i) {
		cpos(0) = fmat(kfvec(i), 0);
		cpos(1) = fmat(kfvec(i), 1);
		id = (unsigned int) kfvec(i);
		ms = mocap->getFrameSkeleton(id);	
		skfanim->addNewKeyFrame(cpos, ms);
	}
	cpos(0) = fmat(0,0);
	cpos(1) = fmat(0,1);
	skfanim->setController(cpos);
	skfanim->setContext();

	// flush animation
    std::ofstream outfile;
    std::string line;
   	Eigen::VectorXd v0;

    outfile.open(bvhout.c_str(), std::ios::app);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < totframes; i++) {
			ms = mocap->getFrameSkeleton(i);
			v0 = ms.getJointPosition(0);
			cpos = fmat.row(i);
			skfanim->setController(cpos);
			skfanim->animate();
			ms = skfanim->getSkeleton();
			ms.setRootPosition(v0);
			mocap->printPose(ms, outfile);
		}
        outfile.close();
    } else {
        std::cout << "Error while opening file " << bvhout << std::endl;
    }
    if (skfanim) delete skfanim;
    if (mocap) delete mocap;
}

int main(int argc, char** argv) {
    int opt, res;
	std::string inbvh;
	std::string outbvh;
	std::string outproj;
	std::string selalg;
	std::string projalg;
	float kfratio = 5.0f;
	wtime = 0;
	bool emask[8] = {false, false, false, false, false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "bfkopsw")) != -1 ) {
        switch (opt) {
            case 'b':
				outbvh = argv[optind];
				emask[5] = true;
                break;
            case 'f':
				inbvh = argv[optind];
				emask[0] = true;
                break;
            case 'k':
				kfratio = atof(argv[optind]);
				emask[1] = true;
                break;
            case 'o':
				outproj = argv[optind];
				emask[2] = true;
                break;
            case 'p':
				projalg = argv[optind];
				emask[3] = true;
                break;
            case 's':
				selalg = argv[optind];
				emask[4] = true;
                break;
            case 'w':
				wtime = atof(argv[optind]);
				emask[6] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[7] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[2] || !emask[3] || !emask[4]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* input BVH file" << std::endl;
		//if (!emask[1]) std::cout << "* key frame ratio" << std::endl;
		if (!emask[2]) std::cout << "* output projection file" << std::endl;
		if (!emask[3]) std::cout << "* projection strategy" << std::endl;
		if (!emask[4]) std::cout << "* selection algorithm" << std::endl;
		exit(0);
	}
	if (!emask[7]) exit(0);
	if ((kfratio > 100) || (kfratio < 0)) {
		std::cout << "Invalid percentage os key frames. Range must be [0-100]." << std::endl;
		exit(0);		
	}
	kfratio /= 100;
	res = getSelectionAlgorithm(selalg);
	if (res < 0) {
		std::cout << "Invalid selection algorithm" << std::endl;
		exit(0);
	}
	opt = res;
	res = getProjectionStrategy(projalg);
	if (res < 0) {
		std::cout << "Invalid projection strategy" << std::endl;
		exit(0);
	}
	opt += 5*res;
	Eigen::VectorXi kfvec;
	Eigen::MatrixXd prmat;
	runCompressionScheme(opt, inbvh, kfratio, kfvec, prmat);
	dumpFrameProjection(outproj, prmat, kfvec);
	if (emask[5]) {
		dumpBvhStructure(inbvh, outbvh);
		dumpBvhAnimation(inbvh, outbvh, prmat, kfvec);
	}
    return 0;
}
