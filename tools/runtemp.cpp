#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <Eigen/Dense>
#include <bvhreader.h>
#include <animation.h>
#include <fstream>
#include <kfselector.h>

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input file: -f" << std::endl;
	std::cout << "\tset key frame selection algorithm: -s" << std::endl;
	std::cout << "\tset key frame interpolation algorithm: -i" << std::endl;
	std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset number of key frames as a percentage {0-100}: -k" << std::endl << std::endl;
	std::cout << name << " -f [BHV file] -s [us|pb|scs] -i [lin|slerp|her] -k [key frame ratio]" << std::endl;
}

/**
 * @brief get selection algorithm code by a command line parameter
 * @param name algorithm specification
 * @return internal code
 */
int getSelectionAlgorithm(std::string const & name) {
	int ret = -1;
	if (strcasecmp(name.c_str(), "pb") == 0) {
		ret = 0;
	} else if (strcasecmp(name.c_str(), "scs") == 0) {
		ret = 1;
	} else if (strcasecmp(name.c_str(), "us") == 0) {
		ret = 2;
	}
	return ret;
}

/**
 * @brief get interpolation strategy code by a command line parameter
 * @param name interpolation specification
 * @return internal code
 */
int getInterpolationStrategy(std::string const & name) {
	int ret = -1;
	
	if (strcasecmp(name.c_str(), "lin") == 0) {
		ret = 0;
	} else if (strcasecmp(name.c_str(), "slerp") == 0) {
		ret = 1;
	} else if (strcasecmp(name.c_str(), "her") == 0) {
		ret = 2;
	}
	return ret;
}

/**
 * @brief run linear interpolation with PB selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runPbWithLin(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucLinSelector poslin(mocap, ratio);
	std::cout << "Position-based selector with linear interpolation" << std::endl;
	poslin.printStats();
	kfvec = poslin.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run linear interpolation with SCS selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runScsWithLin(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucLinSelector scslin(mocap, ratio);		
	std::cout << "SCS selector with linear interpolation" << std::endl;
	scslin.printStats();
	kfvec = scslin.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run linear interpolation with US selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runUsWithLin(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	USEucLinSelector uslin(mocap, ratio);		
	std::cout << "US selector with linear interpolation" << std::endl;
	uslin.printStats();
	kfvec = uslin.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run slerp interpolation with PB selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runPbWithSlerp(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucSlerpSelector posslerp(mocap, ratio);
	std::cout << "Position-based selector with slerp interpolation" << std::endl;
	posslerp.printStats();
	kfvec = posslerp.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run slerp interpolation with SCS selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runScsWithSlerp(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucSlerpSelector scsslerp(mocap, ratio);		
	std::cout << "SCS selector with slerp interpolation" << std::endl;
	scsslerp.printStats();
	kfvec = scsslerp.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run slerp interpolation with US selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runUsWithSlerp(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	USEucSlerpSelector usslerp(mocap, ratio);		
	std::cout << "US selector with slerp interpolation" << std::endl;
	usslerp.printStats();
	kfvec = usslerp.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run hermitian interpolation with PB selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runPbWithHer(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	PBEucHerSelector posher(mocap, ratio);
	std::cout << "Position-based selector with hermitian interpolation" << std::endl;
	posher.printStats();
	kfvec = posher.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run hermitian interpolation with SCS selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runScsWithHer(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	SCSEucHerSelector scsher(mocap, ratio);		
	std::cout << "SCS selector with hermitian interpolation" << std::endl;
	scsher.printStats();
	kfvec = scsher.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run hermitian interpolation with US selection algorithm to get key frames
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runUsWithHer(std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {

	MocapReader * mocap = new BVHReader();
	mocap->readFile(bvhfile);
	USEucHerSelector usher(mocap, ratio);		
	std::cout << "US selector with hermitian interpolation" << std::endl;
	usher.printStats();
	kfvec = usher.getKeyframeVector();
	delete mocap;
}

/**
 * @brief run the specified algorithm for key frame selection and projection
 * and get its results.
 * @param option internal code for selection/projection. Selection code is 
 * {0,1} for {pb, scs}. Interpolation code is {0,2,4} for
 * {linear, slerp, hermitian}. Internal code is the sum of both.
 * @param bvhfile BVH input file name
 * @param ratio number of key frames as a percentage of total of frames
 * @param kfvec vector containing key frame ids (output)
 */
void runCompressionScheme(int option, std::string const & bvhfile, float ratio,  
	Eigen::VectorXi & kfvec) {
	
	switch (option) {
		case 0:
			runPbWithLin(bvhfile, ratio, kfvec);
		break;
		case 1:
			runScsWithLin(bvhfile, ratio, kfvec);
		break;
		case 2:
			runUsWithLin(bvhfile, ratio, kfvec);
		break;
		case 3:
			runPbWithSlerp(bvhfile, ratio, kfvec);
		break;
		case 4:
			runScsWithSlerp(bvhfile, ratio, kfvec);
		break;
		case 5:
			runUsWithSlerp(bvhfile, ratio, kfvec);
		break;
		case 6:
			runPbWithHer(bvhfile, ratio, kfvec);
		break;
		case 7:
			runScsWithHer(bvhfile, ratio, kfvec);
		break;
		case 8:
			runUsWithHer(bvhfile, ratio, kfvec);
		break;
		default:
			std::cout << "Unknown scheme is not implemented." << std::endl;
			exit(0);
		break;
	}
}

int main(int argc, char** argv) {
    int opt, res;
	std::string inbvh;
	std::string selalg;
	std::string intalg;
	float kfratio = 5.0f;
	bool emask[5] = {false, false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "fkis")) != -1 ) {
        switch (opt) {
            case 'f':
				inbvh = argv[optind];
				emask[0] = true;
                break;
            case 'k':
				kfratio = atof(argv[optind]);
				emask[1] = true;
                break;
            case 'i':
				intalg = argv[optind];
				emask[2] = true;
                break;
            case 's':
				selalg = argv[optind];
				emask[3] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[4] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[2] || !emask[3]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* input BVH file" << std::endl;
		//if (!emask[1]) std::cout << "* key frame ratio" << std::endl;
		if (!emask[2]) std::cout << "* interpolation algorithm" << std::endl;
		if (!emask[3]) std::cout << "* selection algorithm" << std::endl;
		exit(0);
	}
	if (!emask[4]) exit(0);
	if ((kfratio > 100) || (kfratio < 0)) {
		std::cout << "Invalid percentage os key frames. Range must be [0-100]." << std::endl;
		exit(0);		
	}
	kfratio /= 100;
	res = getSelectionAlgorithm(selalg);
	if (res < 0) {
		std::cout << "Invalid selection algorithm" << std::endl;
		exit(0);
	}
	opt = res;
	res = getInterpolationStrategy(intalg);
	if (res < 0) {
		std::cout << "Invalid projection strategy" << std::endl;
		exit(0);
	}
	opt += 3*res;
	Eigen::VectorXi kfvec;
	runCompressionScheme(opt, inbvh, kfratio, kfvec);
	std::cout << "keyframes = " <<  kfvec.transpose() << std::endl;
    return 0;
}
