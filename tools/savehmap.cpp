#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>
#include <Eigen/Dense>
#include <bvhreader.h>
#include <animation.h>
#include <skanalizer.h>

/**
 * @brief save matrix as a PPM heatmap image file
 * @param mat matrix with values to be saved
 * @param fname file name
 * @param width pixel image width
 * @param height pixel image height
 * @param min minimal value in heatmap (blue)
 * @param max maximal value in heatmap (red)
 * @see http://paulbourke.net/dataformats/ppm/
 */
void saveImage(Eigen::MatrixXd const & mat, const char * fname, 
	unsigned int width, unsigned int height, double min, double max) {
		
	std::ofstream outfile;
	outfile.open(fname);
	unsigned char r,g,b;
    double med, ws, hs, wc, hc, q00, q01, q10, q11, cval;
	unsigned int wif, hif, wic, hic;
	
	// get max, min and med
    /*
	max = min = mat(0,0);
	for (unsigned int i=0; i < mat.rows(); ++i) {
		for (unsigned int j=0; j < mat.cols(); ++j)
			if (max < mat(i,j)) max = mat(i,j);
			else if (min > mat(i,j)) min = mat(i,j);
	}
	std::cout << "max = " << max << std::endl;
	std::cout << "min = " << min << std::endl;
    */
    med = (max + min) / 2;
	// magical PPM number: P6 stands for byte format
	outfile << "P6" << std::endl; 
	// image size
	outfile << width << " " << height << std::endl; 
	// max value for byte
	outfile << "255" << std::endl; 
	// raw data: width <-> cols and height <-> rows
	ws = ((double) (mat.cols() - 1)) / (width - 1);
	hs = ((double) (mat.rows() - 1)) / (height - 1);
	//for (unsigned int j=0; j < height; ++j) { 
	for (int j=height - 1; j >=0; --j) { // y axis is reversed
	    for (unsigned int i=0; i < width; ++i) {
			// bilienar color interpolation
			wc = ws * i;
			wic = std::ceil(wc);
			wif = std::floor(wc);
			hc = hs * j;
			hic = std::ceil(hc);
			hif = std::floor(hc);
			q00 = mat(hif, wif);
			q01 = mat(hic, wif);
			q10 = mat(hif, wic);
			q11 = mat(hic, wic);
			wc = wc - wif;
			hc = hc - hif;
			// f(x,y) = f(0,0)*(1 - x)*(1 - y) + f(1,0)*x*(1 - y) + f(0,1)*(1 - x)*y + f(1,1)*x*y
			cval = q00*(1 - wc)*(1 - hc) + q10*wc*(1 - hc) + q01*(1 - wc)*hc + q11*wc*hc;
            cval = std::min(max, cval);
            cval = std::max(min, cval);
			if (cval > med) {
				cval = std::abs( (cval - med) / (max - med) );
				if (cval < 0.5) {
					r = 255 * 2 * cval;
					g = 255;
				} else {
					r = 255;
					g = 255 * (1 - 2 * (cval - 0.5));
				}
				b = 0;
			} else {
				cval = std::abs( (cval - min) / (med - min) );
				r = 0;
				if (cval < 0.5) {
					g = 255 * 2 * cval ;
					b = 255;
				} else {
					g = 255;
					b = 255 * (1 - 2 * (cval - 0.5));
				}
			}
			outfile << r << g << b;
		}
	}
	outfile.close();
}

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset BVH input file: -b" << std::endl;
	std::cout << "\tset projection file: -p" << std::endl;
	std::cout << "\tset output file prefix: -o" << std::endl;
	std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset stride step: -s (default = 20)" << std::endl;
	std::cout << "\tset single heatmap: -m" << std::endl;
	std::cout << name << " -b [BHV input file] -p [projection file] -o [output file prefix] -s [stride step] -m" << std::endl;
}

/**
 * @brief read projection matrix from file and load animation
 * @param fname file name to be read
 * @param rd mocap session handler
 * @param skfanim SKF animation handler
 * @param cols number of columns of given matrix
 * @return projection matrix
 */
Eigen::MatrixXd readProjMatrixAndLoadAnimation(std::string const & fname, 
	MocapReader * rd, Animation ** skfanim, unsigned int cols = 3) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	Eigen::Vector4d v;
	Skeleton s;

	// read matrix
	infile.open(fname.c_str());
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				data.push_back(std::atof(buffer.c_str()));
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}

	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	
	// load animation
	s = rd->getFrameSkeleton(0);
	*skfanim = new Animation();
	(*skfanim)->setSkeleton(s);
	v << 0,0,0,1;
	for (unsigned int i=0; i < ret.rows(); ++i) {
		if (ret(i,2) != 0) {
			v(0) = ret(i,0);
			v(1) = ret(i,1);
			s = rd->getFrameSkeleton(i);	
			(*skfanim)->addNewKeyFrame(v, s);
		}
	}
	v(0) = ret(0,0);
	v(1) = ret(0,1);
	(*skfanim)->setController(v);
	(*skfanim)->setContext();
	return ret;
}

/**
 * @brief produces a rowwise controller matrix for a grid on 2D
 * @param sstep 
 * @return controller matrix
 */
Eigen::MatrixXd getControllerMatrix(unsigned int sstep) {
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(sstep*sstep, 4);
	double yy, xx;
	
	xx = yy = 1.0;
	xx /= (sstep - 1);
	yy /= (sstep - 1);
	for (unsigned int i=0; i < sstep; ++i) {
		for (unsigned int j=0; j < sstep; ++j) {
			ret(i*sstep + j, 0) = xx*j;
			ret(i*sstep + j, 1) = 1 - yy*i;
			ret(i*sstep + j, 3) = 1;
		}
	}
	return ret;
}

/**
 * @brief save heatmap matrix
 * @param hmat heatmap matrix to be saved
 * @param fname filename for saving data
 */
void saveHmap(Eigen::MatrixXd & hmat, std::string fname) {
    std::ofstream savedfile;

    savedfile.open(fname.c_str());
    savedfile << hmat << std::endl;
    savedfile.close();
}


int main(int argc, char ** argv) {
    int opt;
    unsigned int sstep = 20;
	std::string inbvh, inproj, outppm;
	bool emask[6] = {false, false, false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "bmpos")) != -1 ) {
        switch (opt) {
            case 'b':
				inbvh = argv[optind];
				emask[0] = true;
                break;
            case 'p':
				inproj = argv[optind];
				emask[1] = true;
                break;
            case 'o':
				outppm = argv[optind];
				emask[2] = true;
                break;
            case 's':
				sstep = atoi(argv[optind]);
				emask[3] = true;
                break;
            case 'm':
				emask[4] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[5] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1] || !emask[2]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* BVH input file" << std::endl;
		if (!emask[1]) std::cout << "* projection file" << std::endl;
		if (!emask[2]) std::cout << "* output file prefix" << std::endl;
		exit(0);
	}
	if (!emask[5]) exit(0);
	//
	GeodesicSkeletonAnalizer skan;
	SkeletonAnalizer * skdist = &skan;
	MocapReader * mocap = new BVHReader();
	Animation * skfanim = NULL;
	mocap->readFile(inbvh);
	unsigned int tf = mocap->getNumberOfFrames();
	Eigen::MatrixXd proj = readProjMatrixAndLoadAnimation(inproj, mocap, &skfanim);
	Eigen::MatrixXd cmat = getControllerMatrix(sstep);
	Eigen::VectorXd evec = Eigen::ArrayXd::Zero(cmat.rows()*tf);
	Skeleton sk0, sk1;
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos;
	double emax, emin;
	cpos << 0,0,0,1;
	// get error
	//std::cout << "controller matrix" << std::endl;
	//std::cout << cmat << std::endl;
	for (unsigned int i=0; i < tf; ++i) {
		//std::cout << "calculating heatmap for frame " << i << std::endl;
		sk0 = mocap->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		for (unsigned int j=0; j < cmat.rows(); ++j) {
			// current error
			cpos.head(2) = cmat.row(j).head(2);
			cpos(3) = 1;
			skfanim->setController(cpos);
			skfanim->animate();
			sk1 = skfanim->getSkeleton();
			sk1.setRootPosition(v0);
			evec(i*cmat.rows() + j) = skdist->getPoseDistance(sk0, sk1);
		}
	}
	
	// save files
	Eigen::MatrixXd emat = Eigen::ArrayXXd::Zero(sstep, sstep);
	std::string fname;
	std::ostringstream oss;
	if (emask[4]) {
		Eigen::VectorXi kfs = Eigen::ArrayXi::Zero(proj.col(2).sum());
		opt = 0;
		for (unsigned int i=0; i < proj.rows(); ++i)
			if (proj(i,2) == 1) 
				kfs(opt++) = i;
		// get first keyframe error
		for (unsigned int j=0; j < sstep; ++j) 
			for (unsigned int k=0; k < sstep; ++k) 
				emat(j,k) = evec(kfs(0)*cmat.rows() + (j+1)*k);
		// remainder keyframe error
		for (unsigned int i=1; i < kfs.size(); ++i) {
			for (unsigned int j=0; j < sstep; ++j) 
				for (unsigned int k=0; k < sstep; ++k) {
					emax = evec(kfs(i)*cmat.rows() + (j+1)*k);
					if (emax < emat(j,k))
						emat(j,k) = emax;
				}
		}
		emax = emin = emat(0,0);
		for (unsigned int j=0; j < sstep; ++j) 
			for (unsigned int k=0; k < sstep; ++k) {
				if (emax < emat(j,k))
					emax = emat(j,k);
				else if (emin > emat(j,k))
					emin = emat(j,k);
			}
		fname = outppm;
		fname += ".ppm";
		std::cout << "emin = " << emin << " emax = " << emax << std::endl;
		std::cout << "saving file " << fname << std::endl;
		saveImage(emat, fname.c_str(), 400, 400, emin, emax);
		saveHmap(emat,"hmap.txt");
		fname.clear();
	} else {
		// get min and max err
		emax = emin = evec(0);
		for (unsigned int i=1; i < evec.size(); ++i) { 
			if (emax < evec(i)) {
				emax = evec(i);
			} else if (emin > evec(i)) {
				emin = evec(i);
			}
		}
		for (unsigned int i=0; i < tf; ++i) {
			for (unsigned int j=0; j < sstep; ++j) 
				for (unsigned int k=0; k < sstep; ++k) 
					emat(j,k) = evec(i*cmat.rows() + (j+1)*k);
			fname = outppm;
			oss.str("");
			oss << i;
			fname += oss.str();
			fname += ".ppm";
			std::cout << "emin = " << emin << " emax = " << emax << std::endl;
			std::cout << "saving file " << fname << std::endl;
			saveImage(emat, fname.c_str(), 400, 400, emin, emax);
			fname.clear();
		}
	}
	if (skfanim != NULL) delete skfanim;
	if (mocap != NULL) delete mocap;
	return 0;
}
