#include <iostream>
#include <cstdlib>
#include <Eigen/Dense>
#include <asfreader.h>
#include <bvhreader.h>
#include <renderhelper.h>

// nao usado
Eigen::Vector3d eye, center, up;
double zn,zf;

Eigen::VectorXi getUSKeyframes(unsigned int tf, unsigned int nkf) {
	assert(nkf > 2);
	
	Eigen::VectorXi keyframes = Eigen::ArrayXi::Zero(nkf);
	float step = tf - 1;
	
	step = step / (nkf - 1);
	for (unsigned int i=1; i < nkf; ++i)
		keyframes(i) = step*i;
	keyframes(nkf - 1) = tf - 1;
	return keyframes;
}

/**
 * @brief get the linear interpolated vector of two poses and save it in 
 * the interpolated dofs matrix.
 * @param kfs key frames vector
 * @param pos index indicator of pair of key frames. Interpolation is done
 * between pos index and its predecessor (pos - 1).
 * @param dofs mocap pose matrix as rowwise dofs.
 * @param idofs interpolated pose matrix as rowwise dofs.
 * @param ref current frame being interpolated
 * @return interpolated pose as a dof vector
 */
Eigen::VectorXd getLinearInterpolatedVector(Eigen::VectorXi const & kfs, int pos,
	Eigen::MatrixXd const & dofs, Eigen::MatrixXd & idofs, unsigned int ref) {

	Eigen::VectorXd tmp0, tmp1;
	double w;

	w = kfs(pos) - kfs(pos - 1);
	w = (ref - kfs(pos - 1)) / w;
	tmp0 = (1 - w) * dofs.row(kfs(pos - 1));
	tmp1 = w * dofs.row(kfs(pos));
	idofs.row(ref) = tmp0 + tmp1;
	tmp0 += tmp1;
	return tmp0;
}

/**
 * @brief get the spherical linear interpolated vector of two poses and save it in 
 * the interpolated dofs matrix.
 * @param kfs key frames vector
 * @param pos index indicator of pair of key frames. Interpolation is done
 * between pos index and its predecessor (pos - 1).
 * @param dofs mocap pose matrix as rowwise dofs.
 * @param idofs interpolated pose matrix as rowwise dofs.
 * @param ref current frame being interpolated
 * @return interpolated pose as a dof vector
 */
Eigen::VectorXd getSphericLinearInterpolatedVector(Eigen::VectorXi const & kfs, int pos,
	Eigen::MatrixXd const & dofs, Eigen::MatrixXd & idofs, unsigned int ref) {

	Eigen::VectorXd tmp0, tmp1, tmp2;
	Eigen::Quaterniond qp, qn, qc;
	Eigen::Vector3d euler;
	double rad = M_PI / 180;
	double w;

	w = kfs(pos) - kfs(pos - 1);
	w = (ref - kfs(pos - 1)) / w;
	tmp0 = (1 - w) * dofs.row(kfs(pos - 1));
	tmp1 = w * dofs.row(kfs(pos));
	tmp2 = tmp0 + tmp1;
	// recalculate angles with slerp
	for (unsigned int i=3 ; i < tmp0.size(); i += 3) {
		// using ZYX rotation from bvh
		qp = Eigen::AngleAxisd(tmp0(i+2)*rad, Eigen::Vector3d::UnitX()) * 
			Eigen::AngleAxisd(tmp0(i+1)*rad, Eigen::Vector3d::UnitY()) * 
			Eigen::AngleAxisd(tmp0(i)*rad, Eigen::Vector3d::UnitZ());
		qn = Eigen::AngleAxisd(tmp1(i+2)*rad, Eigen::Vector3d::UnitX()) * 
			Eigen::AngleAxisd(tmp1(i+1)*rad, Eigen::Vector3d::UnitY()) * 
			Eigen::AngleAxisd(tmp1(i)*rad, Eigen::Vector3d::UnitZ());
		qc = qp.slerp(w, qn);
		euler = qc.toRotationMatrix().eulerAngles(2,1,0);
		euler /= rad;
		tmp2(i) = euler(0);
		tmp2(i+1) = euler(1);
		tmp2(i+2) = euler(2);
	}
	idofs.row(ref) = tmp2;
	return tmp2;
}


int main(int argc, char ** argv) {
	MocapReader *mocap = NULL;
	
	// read files
	if ((argc < 2) || (argc > 3)) {
		std::cout << "syntax: " << argv[0] << 
			" [mocap file [animation file]]" << std::endl;
		exit(1);
	}
    std::string fname(argv[1]);
    std::string bname(basename(argv[1]));
    std::string ext = bname.substr(bname.length() - 3, 3);	
	if (ext.compare("bvh") == 0) {
        mocap = new BVHReader();
        mocap->readFile(fname);
    } else if (ext.compare("asf") == 0) {
        mocap = new ASFReader();
        mocap->readFile(fname);
        if (argc > 2) {
            fname = argv[2];
            bname = basename(argv[2]);
            ext = bname.substr(bname.length() - 3, 3); 
            if (ext.compare("amc") == 0) {
                mocap->readFile(fname);
            }
        }
    }

	unsigned int totframes = mocap->getNumberOfFrames();
	Eigen::MatrixXd dofs = mocap->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd idofs = Eigen::ArrayXXd::Zero(dofs.rows(), dofs.cols());
	Eigen::VectorXd tmp0;
	Eigen::VectorXi kfs = getUSKeyframes(totframes, totframes*0.05);
	int pos = 1;
	Skeleton ms;
	
	if (argc == 3) {
		std::cout << ":FULLY-SPECIFIED" << std::endl;
		std::cout << ":DEGREES" << std::endl;
	}
	for (unsigned int i=0; i < totframes; ++i) {
		if (argc == 3) std::cout << i+1 << std::endl;
		if ((i == (unsigned int) kfs(pos)) && (pos < kfs.size() - 1)) ++pos;
		tmp0 = getSphericLinearInterpolatedVector(kfs, pos, dofs, idofs, i);
		//tmp0 = getLinearInterpolatedVector(kfs, pos, dofs, idofs, i);
		ms = mocap->getFrameSkeleton(tmp0);
		mocap->printPose(ms, std::cout);
	}
	if (mocap) delete mocap;
	return 0;
}
