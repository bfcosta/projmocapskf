#ifndef RENDERHELPER_H
#define RENDERHELPER_H

#include <GL/gl.h>
#include <Eigen/Dense>
#include <skeleton.h>
#include <ostream>

void drawAxis(float dsize = 40.0f);
void drawFilledCircle(GLfloat x, GLfloat y, GLfloat z, GLfloat radius);
void drawGround(int sq);

void setDefaultPerspective(Eigen::Vector4d const & cc);
void setCameraPosition(const Eigen::Vector4d & cpos, const Eigen::VectorXd & tv);
void setMocapPerspective(const Eigen::MatrixXd & pos);
void setMocapFrontPerspective(const Eigen::MatrixXd & pos);

void readMarkersPosition(char * fname, Eigen::MatrixXd & ret, Eigen::MatrixXd & ret2);

extern Eigen::Vector3d eye, center, up;
extern double zn, zf;

#endif
