#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <vector>
#include <Eigen/Dense>
#include <skeleton.h>
#include <animation.h>
#include <bvhreader.h>

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input projection file: -p" << std::endl;
	std::cout << "\tset input original BVH file: -i" << std::endl;
	std::cout << "\tset output reconstructed BVH file: -o" << std::endl;
	std::cout << name << " -p [projection file] -i [original BVH file] -o [reconstructed BHV file]" << std::endl;
}

/**
 * @brief read projection file and extract markers and controller positions
 * @param fproj projection filename
 * @param ctmat rowwise controller matrix (output)
 * @param mkmat rowwise marker matrix (output)
 */
void readProjectionFile(std::string const & fproj, Eigen::MatrixXd & ctmat, 
	Eigen::MatrixXd & mkmat) {
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> row;
	unsigned int cnum, mnum;

	infile.open(fproj.c_str());
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				row.push_back(std::atof(buffer.c_str()));
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fproj << std::endl;
	}
	cnum = row.size() / 3;
	mnum = 0;
	ctmat = Eigen::ArrayXXd::Zero(cnum, 4);
	for (unsigned int i=0; i < ctmat.rows(); ++i) {
		ctmat(i,0) = row.at(3*i);
		ctmat(i,1) = row.at(3*i + 1);
		mnum += row.at(3*i + 2);
		ctmat(i,3) = 1;
	}
	mkmat = Eigen::ArrayXXd::Zero(mnum, 3);
	cnum = 0;
	for (unsigned int i=0; i < ctmat.rows(); ++i) {
		if (row.at(3*i + 2) != 0) {
			mkmat(cnum,0) = row.at(3*i);
			mkmat(cnum,1) = row.at(3*i + 1);
			mkmat(cnum,2) = i;
			++cnum;
		}
	}	
}

/**
 * @brief print bvh header section describing a skeleton structure
 * @param skel given skeleton
 * @param ost output file stream
 */
void printSkeleton(Skeleton & skel, std::ostream & ost) {

    std::vector<Eigen::Matrix4d> jmats =  skel.getJointMatrices();
    std::vector<std::string> names = skel.getBoneNames();
    std::vector<unsigned int> eeff = skel.getEndEffectorsIdx();
    std::vector<unsigned int> parent =  skel.getIndexHierarchy();
    std::vector<bool> iseeff(names.size(),false);
    std::vector<unsigned int> level(parent.size(),0);
    std::string tab;
    Eigen::Vector4d jvec, zvec;
    unsigned int clevel;
    
    for (unsigned int i=0; i < eeff.size(); ++i) {
		iseeff.at(eeff.at(i)) = true;
	}
    for (unsigned int i=1; i < level.size(); ++i) {
		level.at(i) = level.at(parent.at(i)) + 2;
	}
	zvec << 0,0,0,1;
	jvec = jmats.at(0) * zvec;
	clevel = level.at(0);
	tab.clear();
	for (unsigned int j=0; j < clevel; ++j)
		tab.push_back(' ');
    ost << tab << "HIERARCHY" << std::endl;
    ost << tab << "ROOT " << names.at(0) << std::endl << tab << "{" << std::endl;
	tab.push_back(' ');
	tab.push_back(' ');
	clevel += 2;
    ost << tab << "OFFSET 0 0 0" << std::endl;
    ost << tab << "CHANNELS 6 Xposition Yposition Zposition Zrotation Yrotation Xrotation" << std::endl;
    for (unsigned int i=1; i < names.size(); ++i) {
		for (unsigned int j=clevel; j > level.at(i); j -= 2) {
			tab.erase(tab.size() - 2, 2);
			ost << tab << "}" << std::endl;
		}
		jvec = jmats.at(i) * zvec;
		clevel = level.at(i);
		tab.clear();
		for (unsigned int j=0; j < clevel; ++j)
			tab.push_back(' ');
		if (iseeff.at(i)) 
			ost << tab << "End Site" << std::endl << tab << "{" << std::endl;
		else
			ost << tab << "JOINT " << names.at(i) << std::endl << tab << "{" << std::endl;
		tab.push_back(' ');
		tab.push_back(' ');
		clevel += 2;
		ost << tab << "OFFSET " << jvec(0) << " " << jvec(1) <<  " " << jvec(2) << std::endl;
		if (!iseeff.at(i))
			ost << tab << "CHANNELS 3 Zrotation Xrotation Yrotation" << std::endl;
	}
	for (unsigned int j=clevel; j > level.at(0); j -= 2) {
		tab.erase(tab.size() - 2, 2);
		ost << tab << "}" << std::endl;
	}
}

/**
 * @brief break rotation matrix in euler angles where rotation order is ZXY
 * @param rot rotation matrix
 * @return vector with euler angles
 */
Eigen::Vector3d getZXYRotation(Eigen::Matrix4d & rot) {
	Eigen::Vector3d ret;
	double cos0;

	// rotation order is ZXY
	// get x rotation
	ret(0) = std::asin(rot(2,1));
	if (ret(0) > M_PI / 2)
		ret(0) = M_PI - ret(0);
	cos0 = std::cos(ret(0));
	if ((rot(2,1) == 1) || (rot(2,1) == -1)) {
		// get y rotation, z = 0
		ret(1) = std::atan2(rot(1,0), rot(0,0));
	} else {
		// get y and z rotation
		ret(2) = std::atan2(-1*rot(0,1) / cos0 , rot(1,1) / cos0);
		ret(1) = std::atan2(-1*rot(2,0) / cos0 , rot(2,2) / cos0);
	}
	ret *= 180/M_PI;
	return ret;
}

/**
 * @brief break rotation matrix in euler angles where rotation order is ZYX
 * @param rot rotation matrix
 * @return vector with euler angles
 */
Eigen::Vector3d getZYXRotation(Eigen::Matrix4d & rot) {
	Eigen::Vector3d ret;
	double cos0;

	// rotation order is ZYX
	// get y rotation
	ret(1) = std::asin(-1*rot(2,0));
	if (ret(1) > M_PI / 2)
		ret(1) = M_PI - ret(1);
	cos0 = std::cos(ret(1));
	if ((rot(2,0) == 1) || (rot(2,0) == -1)) {
		// get x rotation, z = 0
		ret(0) = std::atan2(rot(0,1), rot(0,2));
	} else {
		// get x and z rotation
		ret(2) = std::atan2(rot(1,0) / cos0 , rot(0,0) / cos0);
		ret(0) = std::atan2(rot(2,1) / cos0 , rot(2,2) / cos0);
	}
	ret *= 180/M_PI;
	return ret;
}

/**
 * @brief print bvh frame data a skeleton pose
 * @param skel given skeleton
 * @param ost output file stream
 */
void printPose(Skeleton & skel, std::ostream & ost) {
	Eigen::Matrix4d tmat;
    Eigen::Vector3d tvec;
    std::vector<Eigen::Matrix4d> bvec = skel.getBoneMatrices();
    std::vector<unsigned int> par = skel.getIndexHierarchy();
    std::vector<bool> hasSeen(par.size(), false);

    tmat = skel.getSkeletonRootTranslation();
    //std::cout << "translation matrix = " << std::endl << tmat << std::endl;
    ost << tmat(0,3) << " " << tmat(1,3) << " " << tmat(2,3);
    // first rotation is ZYX
    tmat = bvec.at(0);
    tvec = getZYXRotation(tmat);
    hasSeen.at(par.at(1)) = !hasSeen.at(par.at(1));
    ost << " " << tvec(2) << " " << tvec(1) << " " << tvec(0);
    // all others are ZXY
    for (unsigned int i=1; i < bvec.size(); ++i) {
        if (hasSeen.at(par.at(i+1))) continue;
        tmat = bvec.at(i);
        tvec = getZXYRotation(tmat);
        hasSeen.at(par.at(i+1)) = !hasSeen.at(par.at(i+1));
        ost << " " << tvec(2) << " " << tvec(0) << " " << tvec(1);
    }
    ost << std::endl;
}

/**
 * @brief dump SKF animation into a file with bvh format
 * @param outbvh output bvh filename
 * @param mocap mocap read handler
 * @param skfanim SKF animation
 * @param ctmat rowwise controller matrix
 */
void saveBvhFile(std::string const & outbvh, MocapReader * mocap, Animation * skfanim, Eigen::MatrixXd const & ctmat) {
    Eigen::Vector4d cpos;
    Skeleton ms, os;
    Eigen::VectorXd rpos;
	std::ofstream savedfile;
	
	cpos << 0,0,0,1;
	savedfile.open(outbvh.c_str());
    cpos = ctmat.row(0);
	skfanim->setController(cpos);
	skfanim->animate();
	ms = skfanim->getSkeleton();
    printSkeleton(ms, savedfile);
    savedfile << "MOTION" << std::endl;
    savedfile << "Frames: " << ctmat.rows() << std::endl;
    savedfile << "Frame Time: 0.00833333" << std::endl;
    printPose(ms, savedfile);
    for (unsigned int i=1; i < ctmat.rows(); i++) {
		cpos = ctmat.row(i);
		os = mocap->getFrameSkeleton(i);
		rpos = os.getJointPosition(0);
		skfanim->setController(cpos);
		skfanim->animate();
		ms = skfanim->getSkeleton();
		ms.setRootPosition(rpos);
		printPose(ms, savedfile);
	}
	savedfile.close();
}

int main(int argc, char ** argv) {
    int opt;
	std::string inbvh;
	std::string inproj;
	std::string outbvh;
	bool emask[4] = {false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "pio")) != -1 ) {
        switch (opt) {
            case 'p':
				inproj = argv[optind];
				emask[0] = true;
                break;
            case 'i':
				inbvh = argv[optind];
				emask[1] = true;
                break;
            case 'o':
				outbvh = argv[optind];
				emask[2] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[3] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* input projection file" << std::endl;
		if (!emask[1]) std::cout << "* input original BVH file" << std::endl;
		if (!emask[2]) std::cout << "* output reconstructed BVH file" << std::endl;
		exit(0);
	}
	if (!emask[3]) exit(0);

	Eigen::MatrixXd ctmat, mkmat;
    Skeleton sk;
	unsigned int id;
	Eigen::Vector4d v;
	//std::cout << "read projection file" << std::endl;
	readProjectionFile(inproj, ctmat, mkmat);
	//std::cout << "read bvh file" << std::endl;
	MocapReader *mocap = new BVHReader();
    mocap->readFile(inbvh);
    sk = mocap->getFrameSkeleton(0);
    Animation *skfanim = new Animation();
    skfanim->setSkeleton(sk);
	v << 0,0,0,1;
	for (unsigned int i=0; i < mkmat.rows(); ++i) {
		v(0) = mkmat(i,0);
		v(1) = mkmat(i,1);
		id = (unsigned int) mkmat(i,2);
		sk = mocap->getFrameSkeleton(id);	
		skfanim->addNewKeyFrame(v, sk);
	}
	v(0) = mkmat(0,0);
	v(1) = mkmat(0,1);
	skfanim->setController(v);
	skfanim->setContext();
	//std::cout << "save bvh file" << std::endl;
	saveBvhFile(outbvh, mocap, skfanim, ctmat);
	if (mocap != NULL) delete mocap;
	if (skfanim != NULL) delete skfanim;
    return 0;
}
