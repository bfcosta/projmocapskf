#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>
#include <Eigen/Dense>
#include <vector>
#include <asfreader.h>
#include <bvhreader.h>
#include <animation.h>
#include <skanalizer.h>
#include <unistd.h>

#define NEARZERO 0.000001
#define MAXVAL 1.0e+12

Animation *skfanim = NULL;

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input projection file: -i" << std::endl;
	std::cout << "\tset input bvh file: -b" << std::endl;
	std::cout << "\tset output rbfp projection file: -r" << std::endl;
	std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset compact input projection file: -c" << std::endl;
	std::cout << "\tset output optimized projection file: -o" << std::endl;
	std::cout << name << " -i [input file] -b [input bvh file] -r [RBFP projection output file] -c -o [optimized projection output file] " << std::endl;
}

/**
 * @brief read projection matrix from file
 * @param fname file name to be read
 * @param cols number of columns
 * @return projection matrix
 */
Eigen::MatrixXd readProjection(std::string const & fname, unsigned int cols = 3) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	unsigned int counter;

	// read matrix
	infile.open(fname.c_str());
	counter = 0;
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) {
				data.push_back(std::atof(buffer.c_str()));
				++counter;
			}
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}
	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	return ret;
}

/**
 * @brief read projection matrix from file and load animation
 * @param fname file name to be read
 * @param rd mocap session handler
 * @param isComp tells whether input projection file is in compact format
 * @param cols number of columns of given matrix
 * @return projection matrix
 */
Eigen::MatrixXd readProjMatrixAndLoadAnimation(std::string const & fname, 
	MocapReader * rd, bool isComp, unsigned int cols = 3) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	Eigen::Vector4d v;
	Skeleton s;

	ret = readProjection(fname, cols);
	if (isComp) {
		Eigen::MatrixXd pmat = Eigen::ArrayXXd::Zero(rd->getNumberOfFrames(),3);
		for (unsigned int i=0; i < ret.rows(); ++i) {
			pmat(ret(i,0),0) = ret(i,1);
			pmat(ret(i,0),1) = ret(i,2);
		}
		ret = pmat;
	}
	// load animation
	s = rd->getFrameSkeleton(0);
	skfanim = new Animation();
	skfanim->setSkeleton(s);
	v << 0,0,0,1;
	//~ for (unsigned int i=0; i < ret.rows(); ++i) {
		//~ v(0) = ret(i,1);
		//~ v(1) = ret(i,2);
		//~ s = rd->getFrameSkeleton(ret(i,0));	
		//~ skfanim->addNewKeyFrame(v, s);
	//~ }
	//~ v(0) = ret(0,1);
	//~ v(1) = ret(0,2);
	for (unsigned int i=0; i < ret.rows(); ++i) {
		if (ret(i,2) != 0) {
			v(0) = ret(i,0);
			v(1) = ret(i,1);
			s = rd->getFrameSkeleton(i);	
			skfanim->addNewKeyFrame(v, s);
		}
	}
	v(0) = ret(0,0);
	v(1) = ret(0,1);
	skfanim->setController(v);
	skfanim->setContext();
	return ret;
}

/**
 * @brief get distance matrix by data matrix where points are rowwise displayed.
 * @param mat data matrix
 * @return distance matrix
 */
Eigen::MatrixXd getDistanceMatrix(Eigen::MatrixXd & mat) {
    Eigen::MatrixXd dist = Eigen::ArrayXXd::Zero(mat.rows(), mat.rows());
    Eigen::VectorXd v0 = Eigen::ArrayXd::Zero(mat.cols());
    Eigen::VectorXd v1 = Eigen::ArrayXd::Zero(mat.cols());

    for (unsigned int i=0; i < mat.rows(); ++i) {
        for (unsigned int j=i+1; j < mat.rows(); ++j) {
            v0 = mat.row(i);
            v1 = mat.row(j);
            v0 -= v1; 
            dist(i,j) = dist(j,i) = std::sqrt(v0.dot(v0));
        }
    }   
    return dist;
}

/**
 * @brief normalize projection to square [0, 1]
 * @param ret projection matrix to be normalized
 */
void normalizeProjection(Eigen::MatrixXd & ret) {
	double min0, max0;
	double min1, max1;
	// normalize projection coordinates
	min0 = max0 = ret(0,0);
	min1 = max1 = ret(0,1);
	for (unsigned int i=1; i < ret.rows(); ++i) {
		if (min0 > ret(i,0)) min0 = ret(i,0);
		else if (max0 < ret(i,0)) max0 = ret(i,0);
		if (min1 > ret(i,1)) min1 = ret(i,1);
		else if (max1 < ret(i,1)) max1 = ret(i,1);
	}
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,0) = (ret(i,0) - min0) / (max0 - min0);
		ret(i,1) = (ret(i,1) - min1) / (max1 - min1);
	}
}

/**
 * @brief get control points
 * @param proj projected data rowwise matrix
 * @return keyframe vector
 */
Eigen::VectorXi getControlPoints(Eigen::MatrixXd & proj) {

    //~ Eigen::VectorXi sample = Eigen::ArrayXi::Zero(proj.rows());
    //~ for (unsigned int i=0; i < proj.rows(); ++i)
		//~ sample(i) = proj(i,0);
	//~ return sample;
    Eigen::VectorXi sample = Eigen::ArrayXi::Zero(proj.col(2).sum());
    unsigned int idx = 0;
    for (unsigned int i=0; i < proj.rows(); ++i)
		if (proj(i,2) != 0)
			sample(idx++) = i;
	return sample;
}

/**
 * @brief return the implemented radial basis function kernel as quadratic multiquadrics
 * with c and epsilon equals one.
 * @param d2 distance as input
 * @return function output
 */
double phi_proj(double d2) {
	//return std::sqrt(1 + d2*d2);  // multiquadrics - elisa
	return (1 + std::pow(d2, 2.5));
}

/**
 * @brief get RBF projection onto a 2D plane given the control points 
 * and their corresponding projections
 * @param mocap mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @param pmat projection matrix
 * @param kfs control points vector
 * @return a projection matrix with all poses
 */
Eigen::MatrixXd getRbfProjection(MocapReader * mocap, SkeletonAnalizer * skdist, 
	Eigen::MatrixXd & pmat, Eigen::VectorXi & kfs) {
	
	Eigen::MatrixXd proj = pmat;
	//Eigen::MatrixXd proj = pmat.block(0, 0, pmat.rows(), pmat.cols()-1);
	int frames = mocap->getNumberOfFrames();
	//~ Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(frames, pmat.cols());
	//~ for (int i=0; i < pmat.rows(); ++i) {
		//~ proj(pmat(i,0),0) = pmat(i,1);
		//~ proj(pmat(i,0),1) = pmat(i,2);
		//~ proj(pmat(i,0),2) = 1;
	//~ }
	// find lambda
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Zero(kfs.size(), kfs.size());
	Eigen::MatrixXd ymat = Eigen::ArrayXXd::Zero(kfs.size(), 2);
	Skeleton sk0, sk1;
	double dist;
	
	for (int i=0; i < fmat.rows(); ++i)
		for (int j=i+1; j < fmat.cols(); ++j) {
			sk0 = mocap->getFrameSkeleton(kfs(i));
			sk1 = mocap->getFrameSkeleton(kfs(j));
			dist = std::sqrt(skdist->getPoseDistance(sk0, sk1));
			fmat(j,i) = fmat(i,j) = phi_proj(dist);
		}
	for (int i=0; i < ymat.rows(); ++i) {
		ymat(i,0) = proj(kfs(i),0);
		ymat(i,1) = proj(kfs(i),1);
	}
	Eigen::MatrixXd lmat = fmat.inverse() * ymat;
	Eigen::VectorXd rbfvec = Eigen::ArrayXd::Zero(kfs.size());
	int c = 0;
	for (int i=0; i < frames; ++i) {
		if (i == kfs(c)) {
			if (c != (kfs.size() - 1)) ++c;
		} else {
		//if (proj(i,2) == 0) {
			for (int j=0; j < rbfvec.size(); ++j) {
				sk0 = mocap->getFrameSkeleton(i);
				sk1 = mocap->getFrameSkeleton(kfs(j));
				dist = std::sqrt(skdist->getPoseDistance(sk0, sk1));
				rbfvec(j) = phi_proj(dist);
			}
			proj.row(i).head(2) = rbfvec.transpose() * lmat;
		}
	}
	normalizeProjection(proj);
	//std::cout << "RBF projection" << std::endl << proj << std::endl;
	return proj;
}

/**
 * @brief get the error vector of a reconstructed RBF/SKF animation for a given
 * frame projection matrix
 * @param proj frame projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @return error vector
 */
Eigen::VectorXd getErrorVector(Eigen::MatrixXd const & proj, MocapReader * rd, SkeletonAnalizer * skdist) {
	Eigen::VectorXd cerr = Eigen::ArrayXd::Zero(proj.rows());
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos;
	Skeleton sk0, sk1;
	
	cpos(2) = 0;
	for (unsigned int i=1; i < cerr.size() - 1; ++i) {
		sk0 = rd->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		// current error
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		skfanim->setController(cpos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		cerr(i) = skdist->getPoseDistance(sk0, sk1);
	}
	return cerr;
}

/**
 * @brief get the gradient matrix of a 2D pose projection with inverse projection
 * given by RBF/SKF.
 * @param proj pose projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @param cerr error vector
 * @param alpha gain parameter
 * @return gradient matrix
 */
Eigen::MatrixXd getGradientMatrix(Eigen::MatrixXd const & proj, MocapReader * rd, 
	SkeletonAnalizer * skdist, Eigen::VectorXd const & cerr, double alpha) {
		
	Eigen::MatrixXd error = Eigen::ArrayXXd::Zero(proj.rows(), 5);
	Eigen::MatrixXd grad = Eigen::ArrayXXd::Zero(proj.rows(), 2);
	Skeleton sk0, sk1;
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos, cbpos, capos;

	// get NSEW delta error
	cpos(2) = 0;
	capos(2) = 0;
	cbpos(2) = 0;
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		// delta east error
		sk0 = rd->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		cbpos.head(2) = proj.row(i-1).head(2);
		cbpos(3) = 1;
		capos.head(2) = proj.row(i+1).head(2);
		capos(3) = 1;
		cbpos -= cpos;
		capos -= cpos;
		error(i,4) = (cbpos.norm() + capos.norm()) / 2; // circle radius
		capos = cpos;
		capos(0) += alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,0) = skdist->getPoseDistance(sk0, sk1);
		error(i,0) -= cerr(i);
		// delta west error
		capos = cpos;
		capos(0) -= alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,1) = skdist->getPoseDistance(sk0, sk1);
		error(i,1) -= cerr(i);
		// delta north error
		capos = cpos;
		capos(1) += alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,2) = skdist->getPoseDistance(sk0, sk1);
		error(i,2) -= cerr(i);
		// delta south error
		capos = cpos;
		capos(1) -= alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,3) = skdist->getPoseDistance(sk0, sk1);
		error(i,3) -= cerr(i);
	}
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		grad(i,0) = 0;
		grad(i,1) = 0;
		if ((error(i,0) < 0) || (error(i,1) < 0)) {
			grad(i,0) = error(i,1) - error(i,0);
		}
		if ((error(i,2) < 0) || (error(i,3) < 0)) {
			grad(i,1) = error(i,3) - error(i,2);
		}
	}
	for (unsigned int i=1; i < grad.rows() - 1; ++i) {
		if (proj(i,2) == 1)
			grad.row(i) *= 0;
		if ((grad(i,0) != 0) || (grad(i,1) != 0)) {
			grad.row(i).normalize();
			grad.row(i) *= error(i,4);
		}
	}
	return grad;
}

/**
 * @brief improve a given projection matrix
 * @param proj pose projection matrix
 * @param rd mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @return improved frame projection matrix
 */
Eigen::MatrixXd improveProjection(Eigen::MatrixXd const & proj, MocapReader * rd, SkeletonAnalizer * skdist) {
	Eigen::MatrixXd grad;
	Eigen::VectorXd cerr;
	Eigen::MatrixXd ret = proj;
	double tini, total0, total1, alpha = 0.5;
	unsigned int i;
	
	cerr = getErrorVector(ret, rd, skdist);
	tini = total0 = cerr.sum();
	//std::cout << "original projection error = " << total0 << std::endl;
	for (i=0; i < 200; ++i) {
		grad = getGradientMatrix(ret, rd, skdist, cerr, alpha);
		ret.block(0, 0, ret.rows(), grad.cols()) += alpha * grad;
		cerr = getErrorVector(ret, rd, skdist);	
		total1 = cerr.sum();
		//std::cout << "delta projection error = " << total1 << std::endl;
		if (total1 == total0) break;
		total0 = total1;
	}
	total1 = 100 * (tini - total0) / tini;
	tini /= rd->getNumberOfDOFs() * rd->getNumberOfFrames();
	total0 /= rd->getNumberOfDOFs() * rd->getNumberOfFrames();
	//~ std::cout << "original projection error = " << tini << std::endl;
	//~ std::cout << "delta projection error = " << total0 << std::endl;
	//~ std::cout << "total of iterations = " << i << std::endl;
	//~ std::cout << "error gain = " << total1 << " %" << std::endl;
	//normalizeProjection(ret);
	return ret;
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param fmat matrix with 2D projected frames
 * @param kfvec key frame ids vector
 */
void dumpFrameProjection(std::string const & pname, Eigen::MatrixXd const & fmat) {
    std::ofstream outfile;

    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i)
			outfile << fmat(i,0) << " " << fmat(i,1) << " " << fmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param proj matrix with 2D projected frames
 * @param ctps key frame ids vector
 */
void saveProjection(std::string const & pname, Eigen::MatrixXd const & proj, Eigen::VectorXi const & ctps) {
    std::ofstream outfile;
	Eigen::MatrixXd fmat = proj;

	for (unsigned int i=0; i < ctps.size(); ++i)
		fmat(ctps(i),2) = 1;
    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i)
			outfile << fmat(i,0) << " " << fmat(i,1) << " " << fmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}


int main(int argc, char ** argv) {
    int opt;
	std::string inproj, inbvh, outfrbf, outfopt;
	bool emask[6] = {false, false, false, false, false, true};
	MocapReader * mocap;
	//RootlessEucSkeletonAnalizer skan;
	GeodesicSkeletonAnalizer skan;

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "biroc")) != -1 ) {
        switch (opt) {
            case 'i':
				inproj = argv[optind];
				emask[0] = true;
                break;
            case 'b':
				inbvh = argv[optind];
				emask[1] = true;
                break;
            case 'r':
				outfrbf = argv[optind];
				emask[2] = true;
                break;
            case 'o':
				outfopt = argv[optind];
				emask[3] = true;
                break;
            case 'c':
				emask[4] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[5] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1] || !emask[2]) {
		if (!emask[0]) std::cout << "* input projection file" << std::endl;
		if (!emask[1]) std::cout << "* input bvh file" << std::endl;
		if (!emask[2]) std::cout << "* output rbfp projection file" << std::endl;
		//if (!emask[3]) std::cout << "* output optimized projection file" << std::endl;
		exit(0);
	}
	if (!emask[5]) exit(0);
	mocap = new BVHReader();
	mocap->readFile(inbvh);

	//Eigen::MatrixXd proj = readProjection(inproj);
	Eigen::MatrixXd proj = readProjMatrixAndLoadAnimation(inproj, mocap, emask[4]);
	//~ std::cout << "proj" << std::endl;
	//~ std::cout << proj << std::endl;
	//~ std::cout << "get projection data = " << proj.rows() << " x " << proj.cols() << std::endl;
	Eigen::VectorXi ctps = getControlPoints(proj);
	//~ std::cout << "control points = " << ctps.transpose() << std::endl;
	//~ std::cout << "get rols control points = " << ctps.size() << std::endl;
	Eigen::MatrixXd rproj = getRbfProjection(mocap, &skan, proj, ctps);
	//~ std::cout << "get rols/rbf projection = " << rproj.rows() << " x " << rproj.cols() << std::endl;
	saveProjection(outfrbf, rproj, ctps);
	if (emask[3]) {
		Eigen::MatrixXd iproj = improveProjection(rproj, mocap, &skan);
		//~ std::cout << "get optimized points = " << iproj.rows() << " x " << iproj.cols() << std::endl;
		saveProjection(outfopt, iproj, ctps);
	}
	return 0;
}
