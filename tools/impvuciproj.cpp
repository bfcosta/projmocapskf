#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cmath>
#include <Eigen/Dense>
#include <algorithm>
#include <vector>
#include <unistd.h>

#define NEARZERO 0.000001
#define MAXVAL 1.0e+12

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input file: -i" << std::endl;
	std::cout << "\tset input metadata file: -m" << std::endl;
	std::cout << "\tset input projection file: -p" << std::endl;
	std::cout << "\tset output projection file: -f" << std::endl;
	std::cout << name << " -i [input file] -m [metadata file] -p [input projection file] -f [output projection file]" << std::endl;
}

/**
 * @brief dump matrix in a text file
 * @param pname output file name
 * @param proj matrix to be dumped
 */
void saveMatrix(std::string const & pname, Eigen::MatrixXd const & proj) {
    std::ofstream outfile;
	Eigen::MatrixXd fmat = proj;

    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i) {
			outfile << fmat(i,0);
			for (unsigned int j=1; j < fmat.cols(); ++j)
				outfile << " " << fmat(i,j);
			outfile << std::endl;
		}
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

/**
 * @brief read projection matrix from file
 * @param fname file name to be read
 * @param fmeta metadata file name to be read
 * @return projection matrix
 */
Eigen::MatrixXd readData(std::string const & fname, 
	std::string const & fmeta) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	unsigned int cols = 0, skip0 = 0, skip1 = 0, counter;

	// read metadata
	infile.open(fmeta.c_str());	
	if (infile.is_open()) {
		if (std::getline(infile, line)) {
			cols = std::atoi(line.c_str());
		}
		if (std::getline(infile, line)) {
			skip1 = skip0 = std::atoi(line.c_str());
		}
		if (std::getline(infile, line)) {
			skip1 = std::atoi(line.c_str());
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fmeta << std::endl;
		return ret;
	}

	//~ std::cout << "li metadados" << std::endl;
	//~ std::cout << "cols = " << cols << std::endl;
	//~ std::cout << "skip0 = " << skip0 << std::endl;
	//~ std::cout << "skip1 = " << skip1 << std::endl;
	// read matrix
	infile.open(fname.c_str());
	counter = 0;
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			replace(line.begin(), line.end(), ',', ' ' );
			iss.str(line);
			while (iss >> buffer) {
				if (((counter % cols) != (skip0 - 1)) && ((counter % cols) != (skip1 - 1)))
					data.push_back(std::atof(buffer.c_str()));
				++counter;
			}
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}
	if (skip0 != 0) --cols;
	if (skip1 != skip0) --cols;
	//~ std::cout << "cols = " << cols << std::endl;
	//~ std::cout << "counter = " << counter << std::endl;
	//~ std::cout << "data size = " << data.size() << std::endl;	
	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	
	return ret;
}

/**
 * @brief read projection matrix from file
 * @param fname file name to be read
 * @return projection matrix
 */
Eigen::MatrixXd readProjection(std::string const & fname) {
		
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;
	unsigned int cols, rows;

	infile.open(fname.c_str());
	rows = 0;
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				data.push_back(std::atof(buffer.c_str()));
			++rows;
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}
	cols = data.size() / rows;
	ret = Eigen::ArrayXXd::Zero(rows, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	
	return ret;
}

/**
 * @brief normalize projection to square [0, 1]
 * @param ret projection matrix to be normalized
 */
void normalizeProjection(Eigen::MatrixXd & ret) {
	double min0, max0;
	double min1, max1;
	// normalize projection coordinates
	min0 = max0 = ret(0,0);
	min1 = max1 = ret(0,1);
	for (unsigned int i=1; i < ret.rows(); ++i) {
		if (min0 > ret(i,0)) min0 = ret(i,0);
		else if (max0 < ret(i,0)) max0 = ret(i,0);
		if (min1 > ret(i,1)) min1 = ret(i,1);
		else if (max1 < ret(i,1)) max1 = ret(i,1);
	}
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,0) = (ret(i,0) - min0) / (max0 - min0);
		ret(i,1) = (ret(i,1) - min1) / (max1 - min1);
	}
}

/**
 * @brief return the implemented radial basis function kernel as L2 distance
 * @param d2 distance as input
 * @return function output
 */
double phi_gd(double d2) {
	return std::fabs(d2);
}

/**
 * @brief get backward projection by an RBF function
 * @param val low dimension value to be reprojected
 * @param proj low dimension projection
 * @param lambda lambda coefficient matrix to solve RBF
 * @param ctps control points
 * @param ldim number of components of low dimension
 * @return high dimension back projection
 */
Eigen::VectorXd getBackProjection(Eigen::VectorXd const & val, Eigen::MatrixXd const & proj, 
Eigen::MatrixXd const & lambda, Eigen::VectorXi const &  ctps, unsigned int ldim) {
	Eigen::VectorXd v0, v1;
	Eigen::VectorXd lvec = Eigen::ArrayXd::Zero(lambda.rows());
	
	v0 = val.head(ldim);
	//~ std::cout << "input back projection = " << val.size() << std::endl;
	for (unsigned int j=0; j < ctps.size(); ++j) {
		v1 = proj.row(ctps(j)).head(ldim);
		v1 -= v0;
		lvec(j) = phi_gd(v1.norm());
	}
	lvec(ctps.size()) = 1;
	for (unsigned int j=0; j < ldim; ++j) {
		lvec(ctps.size() + 1 + j) = v0(j);
	}
	// error
	//~ std::cout << "lambda back projection = " << lvec.size() << std::endl;
	//~ std::cout << "lambda matrix = " << lambda.rows() << " x " << lambda.cols() << std::endl;
	v0 = lvec.transpose() * lambda;
	//~ std::cout << "output back projection = " << v0.size() << std::endl;
	return v0;
}

/**
 * @brief get the error vector of a reconstructed RBF/SKF animation for a given
 * frame projection matrix
 * @param proj frame projection matrix
 * @return error vector
 */

Eigen::VectorXd getErrorVector(Eigen::MatrixXd const & proj, Eigen::MatrixXd const & data, 
Eigen::MatrixXd const & lambda, Eigen::VectorXi const &  ctps, unsigned int ldim) {
	
	Eigen::VectorXd cerr = Eigen::ArrayXd::Zero(proj.rows());
	Eigen::VectorXd v0, v1;
	
	for (unsigned int i=0; i < proj.rows(); ++i) {
		v0 = proj.row(i);
		v0 = getBackProjection(v0, proj, lambda, ctps, ldim);
		v1 = data.row(i);
		v1 -= v0;
		cerr(i) = v1.norm();
	}
	for (unsigned int j=0; j < ctps.size(); ++j)
		cerr(ctps(j)) = 0;
	return cerr;
}

/**
 * @brief get the gradient matrix of a 2D projection with inverse projection
 * given by RBF.
 * @param proj low dimension projection matrix
 * @param nmat nearest neighbors matrix
 * @param data original dimension data matrix
 * @param ctps control point matrix
 * @param lambda RBF coefficient matrix
 * @param cerr error vector
 * @param alpha gradient descent parameter
 * @param ldim number of components of low dimension
 * @return gradient matrix
 */
Eigen::MatrixXd getGradientMatrix(Eigen::MatrixXd const & proj, Eigen::MatrixXi const & nmat, 
Eigen::MatrixXd const & data, Eigen::VectorXi const &  ctps, Eigen::MatrixXd const & lambda, 
Eigen::VectorXd const & cerr, double alpha, unsigned int ldim) {
		
	Eigen::MatrixXd error = Eigen::ArrayXXd::Zero(proj.rows(), 5);
	Eigen::MatrixXd grad = Eigen::ArrayXXd::Zero(proj.rows(), 2);
	Eigen::VectorXd v0, v1;
	Eigen::Vector4d cpos, cbpos, capos;

	// get NSEW delta error
	cpos(2) = 0;
	capos(2) = 0;
	cbpos(2) = 0;
	for (unsigned int i=0; i < error.rows(); ++i) {
		// delta east error
		v0 = data.row(i);
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		cbpos.head(2) = proj.row(nmat(i,0)).head(2);
		cbpos(3) = 1;
		capos.head(2) = proj.row(nmat(i,1)).head(2);
		capos(3) = 1;
		cbpos -= cpos;
		capos -= cpos;
		error(i,4) = (cbpos.norm() + capos.norm()) / 2; // circle radius
		capos = cpos;
		capos(0) += alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,0) = v1.norm();
		error(i,0) -= cerr(i);
		// delta west error
		capos = cpos;
		capos(0) -= alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,1) = v1.norm();
		error(i,1) -= cerr(i);
		// delta north error
		capos = cpos;
		capos(1) += alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,2) = v1.norm();
		error(i,2) -= cerr(i);
		// delta south error
		capos = cpos;
		capos(1) -= alpha*error(i,4);
		v1 = getBackProjection(capos, proj, lambda, ctps, ldim);
		v1 -= v0;
		error(i,3) = v1.norm();
		error(i,3) -= cerr(i);
	}
	for (unsigned int j=0; j < ctps.size(); ++j) {
		error(ctps(j),0) = error(ctps(j),1) = error(ctps(j),2) = error(ctps(j),3) =0;
	}
	for (unsigned int i=0; i < error.rows(); ++i) {
		grad(i,0) = 0;
		grad(i,1) = 0;
		if ((error(i,0) < 0) || (error(i,1) < 0)) {
			grad(i,0) = error(i,1) - error(i,0);
		}
		if ((error(i,2) < 0) || (error(i,3) < 0)) {
			grad(i,1) = error(i,3) - error(i,2);
		}
	}
	for (unsigned int i=0; i < grad.rows(); ++i) {
		if ((grad(i,0) != 0) || (grad(i,1) != 0)) {
			grad.row(i).normalize();
			grad.row(i) *= error(i,4);
		}
	}
	return grad;
}

/**
 * @brief get the nearest neighbor matrix given by index reference
 * @param proj rowwise projection matrix
 * @param nn number of nearest neighbors
 * @return neighbor matrix
 */
Eigen::MatrixXi getNNeighborMatrix(Eigen::MatrixXd const &  proj, unsigned int nn = 2) {
	Eigen::MatrixXi ret = Eigen::ArrayXXi::Zero(proj.rows(), nn);
	Eigen::VectorXd v0, v1, error;
	Eigen::VectorXi min = Eigen::ArrayXi::Zero(proj.rows());
	double mv;
	unsigned int mi, aux;
	
	error = proj.col(0);
	for (unsigned int i=0; i < ret.rows(); ++i) {
		v0 = proj.row(i);
		// calculate error to every point
		for (unsigned int j=0; j < error.size(); ++j) {
			v1 = proj.row(j);
			v1 -= v0;
			error(j) = v1.norm();
		}
		// get minimuns errors
		for (unsigned int j=0; j < min.size(); ++j) {
			min(j) = j;
		}
		for (unsigned int j=0; j < nn+1; ++j) {
			mi = j;
			mv = error(j);
			for (unsigned int k=j+1; k < error.size(); ++k) {
				if (mv > error(k)) {
					mi = k;
					mv = error(k);
				}			
			}
			mv = error(mi);
			error(mi) = error(j);
			error(j) = mv;
			aux = min(j);
			min(j) = min(mi);
			min(mi) = aux;
		}
		for (unsigned int j=0; j < ret.cols(); ++j) {
			ret(i,j) = min(j+1);
		}
	}
	return ret;
}

/**
 * @brief get RBF coefficients in form of a matrix given input and output
 * @param data output values for RBF
 * @param proj input values for RBF
 * @param ctps control points: values to build RBF
 * @param ldim number of components of input dimension
 * @return RBF coefficient matrix
 */
Eigen::MatrixXd getLambdaMatrix(Eigen::MatrixXd const & data, Eigen::MatrixXd const &  proj, 
Eigen::VectorXi const &  ctps, unsigned int ldim) {
	
	Eigen::MatrixXd cmat = Eigen::ArrayXXd::Zero(ctps.size() + 1 + ldim, ctps.size() + 1 + ldim);
	Eigen::VectorXd v0, v1;
	for (unsigned int i=0; i < ctps.size(); ++i) {
		v0 = proj.row(ctps(i));
		for (unsigned int j=i+1; j < ctps.size(); ++j) {
			v1 = proj.row(ctps(j));
			v1 -= v0;
			cmat(i,j) = cmat(j,i) = phi_gd(v1.norm());
		}
		cmat(i,ctps.size()) = cmat(ctps.size(),i) = 1;
		for (unsigned int j=0; j < ldim; ++j) {
			cmat(i,ctps.size()+1+j) = cmat(ctps.size()+1+j,i) = proj(ctps(i),j);
		}
	}
	
	Eigen::MatrixXd ymat = Eigen::ArrayXXd::Zero(ctps.size() + 1 + ldim, data.cols());
	for (unsigned int i=0; i < ctps.size(); ++i) {
		ymat.row(i) = data.row(ctps(i));
	}
	Eigen::MatrixXd lmat = cmat.inverse() * ymat;
	return lmat;
}


/**
 * @brief improve a given projection matrix
 * @param data high dimension data matrix
 * @param proj projection matrix
 * @param ctps control point reference
 * @return improved frame projection matrix
 */
Eigen::MatrixXd getImprovedProjection(Eigen::MatrixXd const & data, Eigen::MatrixXd const &  proj, 
Eigen::VectorXi const &  ctps, unsigned int ldim = 2) {
	
	Eigen::MatrixXd grad;
	Eigen::VectorXd cerr;
	Eigen::MatrixXd ret = proj;
	double total0, total1, alpha = 0.5;
	unsigned int i;

	Eigen::MatrixXd lambda = getLambdaMatrix(data, proj, ctps, ldim);
	//~ std::cout << "get lambda matrix = " << lambda.rows() << " x " << lambda.cols() << std::endl;
	cerr = getErrorVector(ret, data, lambda, ctps, ldim);
	//~ std::cout << "get error vector = " << cerr.size() << std::endl;
	Eigen::MatrixXi nmat = getNNeighborMatrix(ret);
	//~ std::cout << "get nn matrix = " << nmat.rows() << " x " << nmat.cols() << std::endl;
	total0 = cerr.sum();
	//std::cout << "original projection error = " << total0 << std::endl;
	for (i=0; i < 200; ++i) {
		grad = getGradientMatrix(ret, nmat, data, ctps, lambda, cerr, alpha, ldim);
		//~ saveMatrix("grad_cpp.txt", grad);
		//~ std::cout << "get gradient matrix = " << grad.rows() << " x " << grad.cols() << std::endl;
		ret.block(0, 0, ret.rows(), grad.cols()) += alpha * grad;
		cerr = getErrorVector(ret, data, lambda, ctps, ldim);	
		total1 = cerr.sum();
		//~ std::cout << "delta projection error = " << total1 << std::endl;
		if (total1 == total0) break;
		total0 = total1;
	}
	//~ total1 = 100 * (tini - total0) / tini;
	//~ tini /= data.rows();
	//~ total0 /= data.rows();
	//~ std::cout << "original projection error = " << tini << std::endl;
	//~ std::cout << "delta projection error = " << total0 << std::endl;
	//~ std::cout << "total of iterations = " << i << std::endl;
	//~ std::cout << "error gain = " << total1 << " %" << std::endl;
	return ret;
}

/**
 * @brief calculates stress function for multidimensional projection
 * @param dmat distance matrix in high dimension
 * @param proj projected data in low dimension
 * @return calculated stress
 */
double getStress(Eigen::MatrixXd const & dmat, Eigen::MatrixXd const & proj) {
	double s0 = 0, s1 = 0, ldist;
	Eigen::VectorXd v0, v1;
	for (unsigned int i=0; i < dmat.rows(); ++i) {
		v0 = proj.row(i).head(2);
		for (unsigned int j=i+1; j < dmat.rows(); ++j) {
			v1 = proj.row(j).head(2);
			v1 -= v0;
			ldist = v1.norm();
			s0 += (ldist - dmat(i,j))*(ldist - dmat(i,j));
            s1 += ldist*ldist;
		}
	}
    s0 = s0 / s1;
	return s0;
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param proj matrix with 2D projected frames
 * @param ctps key frame ids vector
 */
void saveProjection(std::string const & pname, Eigen::MatrixXd const & proj, Eigen::VectorXi const & ctps) {
    std::ofstream outfile;
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Zero(proj.rows(), proj.cols()+1);
	fmat.block(0,0,proj.rows(),proj.cols()) = proj;

	for (unsigned int i=0; i < ctps.size(); ++i)
		fmat(ctps(i),2) = 1;
    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i)
			outfile << fmat(i,0) << " " << fmat(i,1) << " " << fmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

int main(int argc, char ** argv) {
    int opt;
	std::string infile, inmeta, inproj, outfile;
	bool emask[5] = {false, false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "fimp")) != -1 ) {
        switch (opt) {
            case 'i':
				infile = argv[optind];
				emask[0] = true;
                break;
            case 'm':
				inmeta = argv[optind];
				emask[1] = true;
                break;
            case 'f':
				outfile = argv[optind];
				emask[2] = true;
                break;
            case 'p':
				inproj = argv[optind];
				emask[3] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[4] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1] || !emask[2]) {
		if (!emask[0]) std::cout << "* input file" << std::endl;
		if (!emask[1]) std::cout << "* input metadata file" << std::endl;
		if (!emask[2]) std::cout << "* input projection file" << std::endl;
		if (!emask[3]) std::cout << "* output projection file" << std::endl;
		exit(0);
	}
	if (!emask[4]) exit(0);
	Eigen::MatrixXd data = readData(infile, inmeta);
	Eigen::MatrixXd proj = readProjection(inproj);
	Eigen::VectorXd iscp = proj.col(2);
	Eigen::VectorXi ctps = Eigen::ArrayXi::Zero(iscp.sum());
	opt = 0;
	for (unsigned int i=0; i < iscp.size(); ++i)
		if (iscp(i) != 0) 
			ctps(opt++) = i;
	//~ std::cout << "get rols/rbf projection = " << rproj.rows() << " x " << rproj.cols() << std::endl;
	Eigen::MatrixXd iproj = getImprovedProjection(data, proj, ctps);
	//~ std::cout << "get optimized points = " << iproj.rows() << " x " << iproj.cols() << std::endl;
	saveProjection(outfile, iproj, ctps);
	return 0;
}

