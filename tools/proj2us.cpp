#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <Eigen/Dense>

/**
 * @brief get keyframes for mocap data given by uniform sampling algorithm
 * @param nkf number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi getUSKeyframes(unsigned int tf, unsigned int nkf) {
	Eigen::VectorXi keyframes = Eigen::ArrayXi::Zero(nkf);
	float step = tf - 1;

	step = step / (nkf - 1);
	for (unsigned int i=1; i < nkf; ++i)
		keyframes(i) = step*i;
	keyframes(nkf - 1) = tf - 1;
	return keyframes;
}

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset projection input file: -i" << std::endl;
	std::cout << "\tset projection output file: -o" << std::endl << std::endl;
	std::cout << name << " -i [2D input file] -o [2D output file]" << std::endl;
	std::cout << "optional parameters:" << std::endl;
	std::cout << "\tset number of key frames as a percentage {0-100}: -k" << std::endl << std::endl;

}

/**
 * @brief read projection matrix from file and load animation
 * @param fname file name to be read
 * @param cols number of columns of given matrix
 * @return projection matrix
 */
Eigen::MatrixXd readProjMatrix(std::string const & fname, unsigned int cols = 3) {

	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> data;
	Eigen::MatrixXd ret;

	// read matrix
	infile.open(fname.c_str());
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				data.push_back(std::atof(buffer.c_str()));
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
		data.clear();
		return ret;
	}

	ret = Eigen::ArrayXXd::Zero(data.size() / cols, cols);
	for (unsigned int i=0; i < ret.rows(); ++i)
		for (unsigned int j=0; j < ret.cols(); ++j)
			ret(i,j) = data.at(cols*i + j);
	return ret;
}

/**
 * @brief dump projected frames in a text file
 * @param pname output file name
 * @param fmat matrix with 2D projected frames
 * @param kfvec key frame ids vector
 */
void dumpFrameProjection(std::string const & pname, Eigen::MatrixXd const & fmat) {
    std::ofstream outfile;

    outfile.open(pname.c_str(), std::ios::trunc);
    if (outfile.is_open()) {
		for (unsigned int i=0; i < fmat.rows(); ++i)
			outfile << fmat(i,0) << " " << fmat(i,1) << " " << fmat(i,2) << std::endl;
        outfile.close();
    } else {
        std::cout << "Error while opening file " << pname << std::endl;
    }   
}

int main(int argc, char** argv) {
    int opt;
	std::string inproj;
	std::string outproj;
	float kfratio = 3.0f;
	bool emask[4] = {false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "iko")) != -1 ) {
        switch (opt) {
            case 'i':
				inproj = argv[optind];
				emask[0] = true;
                break;
            case 'o':
				outproj = argv[optind];
				emask[1] = true;
                break;
			case 'k':
				kfratio = atof(argv[optind]);
				emask[2] = true;
				break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[3] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* output projection file" << std::endl;
		if (!emask[1]) std::cout << "* input projection file" << std::endl;
		exit(0);
	}
	if (!emask[3]) exit(0);
	// do US selector
	Eigen::MatrixXd fproj = readProjMatrix(inproj);
	unsigned int frames = fproj.rows();
	unsigned int nkf = frames * kfratio / 100;
	if (nkf < 3) nkf = 3;
	Eigen::VectorXi kfs = getUSKeyframes(frames, nkf);
	Eigen::VectorXd uskf = Eigen::ArrayXd::Zero(frames);
	for (unsigned int i=0; i < kfs.size(); ++i) 
		uskf(kfs(i)) = 1;
	std::cout << "keyframes = " << kfs.transpose() << std::endl;
	fproj.col(2) = uskf;
	dumpFrameProjection(outproj, fproj);
    return 0;
}
