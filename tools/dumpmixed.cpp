#include <iostream>
#include <cstdlib>
#include <Eigen/Dense>
#include <asfreader.h>
#include <bvhreader.h>
#include <animation.h>
#include <renderhelper.h>
#include <fstream>

Animation *skfanim = NULL;
// nao usado
Eigen::Vector3d eye, center, up;
double zn,zf;

/**
 * @brief load a SKF animation given by a mocap file reader and its key frames.
 * @param line rowwise key frame matrix with 2D projection and key frames.
 * @param rd mocap handler for file reading.
 */
void loadAnimation(Eigen::MatrixXd const & line, MocapReader * rd) {
	unsigned int id;
	Eigen::Vector4d v;
	Skeleton s;
	
	v << 0,0,0,1;
	//skfanim->clearAnimation();
	for (unsigned int i=0; i < line.rows(); ++i) {
		v(0) = line(i,0);
		v(1) = line(i,1);
		id = (unsigned int) line(i,2);
		s = rd->getFrameSkeleton(id);	
		skfanim->addNewKeyFrame(v, s);
	}
	v(0) = line(0,0);
	v(1) = line(0,1);
	skfanim->setController(v);
	skfanim->setContext();
}

/**
 * @brief load mocap file(s) in its reader handler
 * @param fn number of parameters already read
 * @param argc number of command line parameters
 * @param argv char (string) array of command line parameters
 * @param mc mocap reader handler reference
 * @return number of files read
 */
int loadFile(int fn, int argc, char ** argv , MocapReader ** mc) {
	int ret = 1;
	if (argc <= (fn + ret)) {
		std::cout << "expected a mocap file to read." << std::endl;
		exit(1);
	}
	
	std::string fname(argv[fn + ret]);
	std::string bname(basename(argv[fn + ret]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	
	if (ext.compare("bvh") == 0) {
		*mc = new BVHReader();
		(*mc)->readFile(fname);
	} else if (ext.compare("asf") == 0) {
		*mc = new ASFReader();
		(*mc)->readFile(fname);
		++ret;
		if (argc > (fn + ret)) {
			fname = argv[fn + ret];
			bname = basename(argv[fn + ret]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				(*mc)->readFile(fname);
			} else {
				std::cout << "no amc file given." << std::endl;
				exit(1);
			}
		} else {
			std::cout << "expected one amc file." << std::endl;
			exit(1);
		}
	} else {
		std::cout << "unrecognized format." << std::endl;
		exit(1);
	}
	return ret;
}

/**
 * @brief get a mixed curve by a junction of two curves where the junction is
 * the closest point
 * @param proj1 matrix with first curve projections
 * @param proj2 matrix with second curve projections
 * @param ref1 frame change inside first curve (output)
 * @param ref2 frame change inside second curve (output)
 * @param sm number of smoothing frames
 * @return matrix mixed curve
 */
Eigen::MatrixXd getMixedCurve(Eigen::MatrixXd & proj1, 
	Eigen::MatrixXd & proj2, unsigned int * ref1, unsigned int * ref2,
	unsigned int sm = 0) {

	unsigned int e1, e2;
	double dist, mindist;
	Eigen::VectorXd vec1, vec2;
	
	e1 = proj1.rows() / 20;
	e2 = proj2.rows() / 20;
	*ref1 = e1;
	*ref2 = e2;
	vec1 = proj1.row(*ref1);
	vec2 = proj2.row(*ref2);
	vec1 -= vec2;
	mindist = dist = vec1.norm();
	for (unsigned int i=e1; i < proj1.rows() - e1; ++i)
		for (unsigned int j=e2; j < proj2.rows() - e2; ++j) {
				vec1 = proj1.row(i);
				vec2 = proj2.row(j);
				vec1 -= vec2;
				dist = vec1.norm();
				if (mindist > dist) {
					*ref1 = i;
					*ref2 = j;
					mindist = dist;
				}
		}
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(*ref1 + sm + 1 + proj2.rows() - *ref2, proj1.cols());
	vec1 = proj1.row(*ref1);
	vec2 = proj2.row(*ref2);
	vec2 -= vec1;
	vec2 /= (sm + 1);
	for (unsigned int i=0; i < ret.rows(); ++i)
		if (i <= *ref1) {
			ret.row(i) = proj1.row(i);
		} else if (i <= (*ref1 + sm)) {
			ret.row(i) = vec1 + (i - *ref1)*vec2;
		} else {
			ret.row(i) = proj2.row(*ref2 - sm + i - *ref1 - 1);
		}
	return ret;
}

/**
 * @brief the the key frames of a mixed curve
 * @param kfmat1 key frame matrix of the first curve
 * @param kfmat2 key frame matrix of the second curve
 * @param ref1 change point inside first curve
 * @param ref2 change point inside second curve
 * @return mixed key frame matrix
 */
Eigen::MatrixXd getMixedKeyFrames(Eigen::MatrixXd & kfmat1, 
	Eigen::MatrixXd & kfmat2, unsigned int ref1, unsigned int ref2) {
	unsigned int rows = 0;
	
	for (unsigned int i=0; i < kfmat1.rows(); ++i)
		if (kfmat1(i,2) <= ref1) 
			++rows;
		else
			break;
	for (int i=kfmat2.rows() - 1; i >= 0; --i)
		if (kfmat2(i,2) >= ref2) 
			++rows;
		else
			break;
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(rows, kfmat1.cols());
	rows = 0;
	for (unsigned int i=0; i < kfmat1.rows(); ++i)
		if (kfmat1(i,2) <= ref1) 
			ret.row(rows++) = kfmat1.row(i);
		else
			break;
	for (unsigned int i=0; i < kfmat2.rows(); ++i)
		if (kfmat2(i,2) >= ref2) {
			ret.row(rows) = kfmat2.row(i);
			ret(rows,2) = ret(rows,2) - ref2 + ref1 + 1;
			++rows;
		}
	return ret;
}

/**
 * @brief save new curve for playback
 * @param kfmat rowwise key frame matrix for new curve
 * @param curve rowwise matrix of 2D projections
 * @param fname filename for saving data
 */
void saveCurve(Eigen::MatrixXd & kfmat, Eigen::MatrixXd & curve, std::string fname) {
	
	std::ofstream savedfile;
	savedfile.open(fname.c_str());
	savedfile << kfmat << std::endl;
	savedfile << curve << std::endl;
	savedfile.close();
}


int main(int argc, char ** argv) {
	MocapReader *mocap, *sib;
	Eigen::MatrixXd mpos, fproj;
	Eigen::MatrixXd mpos2, fproj2;
	unsigned int totframes;
	Skeleton ms;
	Eigen::Vector4d v0;
	Eigen::Vector4d cpos;
	
	// read files
	if (argc < 3) {
		std::cout << "syntax: " << argv[0] << 
			" [mocap file 1] [spatial keyframe file] [mocap file 2] [spatial keyframe file]" << std::endl;
		exit(1);
	}
	int fn = loadFile(0, argc, argv, &mocap);
	++fn;
	readMarkersPosition(argv[fn], mpos, fproj);
	fn += loadFile(fn, argc, argv, &sib);
	++fn;
	readMarkersPosition(argv[fn], mpos2, fproj2);
	ms = mocap->getFrameSkeleton(0);
	skfanim = new Animation();
	skfanim->setSkeleton(ms);
	loadAnimation(mpos, mocap);
	loadAnimation(mpos2, sib);
	Eigen::MatrixXd dofmat = mocap->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd mcdat1 = dofmat.block(0,0,dofmat.rows(),3);
	for (unsigned int i = mcdat1.rows() - 1; i > 0; --i)
		mcdat1.row(i) -= mcdat1.row(i-1);
	dofmat = sib->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd mcdat2 = dofmat.block(0,0,dofmat.rows(),3);
	for (unsigned int i = mcdat2.rows() - 1; i > 0; --i)
		mcdat2.row(i) -= mcdat2.row(i-1);
	v0 << 0,0,0,1;
	//mocap->print();
    totframes = mocap->getNumberOfFrames();
    std::cout << "first mocap file." << std::endl;
	if ((argc == 4)||(argc == 7)) {
		std::cout << ":FULLY-SPECIFIED" << std::endl;
		std::cout << ":DEGREES" << std::endl;
	}
    for (unsigned int i=0; i < totframes; i++) {
		if ((argc == 4)||(argc == 7)) std::cout << i+1 << std::endl;
		ms = mocap->getFrameSkeleton(i);
		//v0 = ms.getJointPosition(0);
		v0.head(3) += mcdat1.row(i);
		cpos = fproj.row(i);
		skfanim->setController(cpos);
		skfanim->animate();
		ms = skfanim->getSkeleton();
		ms.setRootPosition(v0);
		mocap->printPose(ms, std::cout);
	}
	v0 << 0,0,0,1;
	std::cout << "second mocap file" << std::endl;
	totframes = sib->getNumberOfFrames();
	if ((argc == 4)||(argc == 7)) {
		std::cout << ":FULLY-SPECIFIED" << std::endl;
		std::cout << ":DEGREES" << std::endl;
	}
    for (unsigned int i=0; i < totframes; i++) {
		if ((argc == 4)||(argc == 7)) std::cout << i+1 << std::endl;
		ms = sib->getFrameSkeleton(i);
		//v0 = ms.getJointPosition(0);
		v0.head(3) += mcdat2.row(i);
		cpos = fproj2.row(i);
		skfanim->setController(cpos);
		skfanim->animate();
		ms = skfanim->getSkeleton();
		ms.setRootPosition(v0);
		sib->printPose(ms, std::cout);
	}
	//
	unsigned int ref1, ref2;
	unsigned int sm = 5;
	Eigen::MatrixXd curve1 = getMixedCurve(fproj, fproj2, &ref1, &ref2, sm);
	Eigen::MatrixXd kfmat = getMixedKeyFrames(mpos, mpos2, ref1, ref2);
	saveCurve(kfmat, curve1, "curve12.txt");
	std::cout << "mixed 1+2 mocap file" << std::endl;
	v0 << 0,0,0,1;
	if ((argc == 4)||(argc == 7)) {
		std::cout << ":FULLY-SPECIFIED" << std::endl;
		std::cout << ":DEGREES" << std::endl;
	}
    for (unsigned int i=0; i < curve1.rows(); i++) {
		if ((argc == 4)||(argc == 7)) std::cout << i+1 << std::endl;
		if (i <= ref1) {
			ms = mocap->getFrameSkeleton(i);
			v0.head(3) += mcdat1.row(i);
		} else if (i <= (ref1 + sm)) {

		} else {
			ms = sib->getFrameSkeleton(ref2 - sm + i - ref1 - 1);
			v0.head(3) += mcdat2.row(ref2 -sm + i - ref1 - 1);
		}
		cpos = curve1.row(i);
		skfanim->setController(cpos);
		skfanim->animate();
		ms = skfanim->getSkeleton();
		ms.setRootPosition(v0);
		mocap->printPose(ms, std::cout);
	}
	Eigen::MatrixXd curve2 = getMixedCurve(fproj2, fproj, &ref2, &ref1, sm);
	kfmat = getMixedKeyFrames(mpos2, mpos, ref2, ref1);
	saveCurve(kfmat, curve2, "curve21.txt");
	std::cout << "mixed 2+1 mocap file" << std::endl;
	v0 << 0,0,0,1;
	if ((argc == 4)||(argc == 7)) {
		std::cout << ":FULLY-SPECIFIED" << std::endl;
		std::cout << ":DEGREES" << std::endl;
	}
    for (unsigned int i=0; i < curve2.rows(); i++) {
		if ((argc == 4)||(argc == 7)) std::cout << i+1 << std::endl;
		if (i <= ref2) {
			ms = sib->getFrameSkeleton(i);
			v0.head(3) += mcdat2.row(i);
		} else if (i <= (ref2 + sm)) {

		} else {
			ms = mocap->getFrameSkeleton(ref1 - sm + i - ref2 - 1);
			v0.head(3) += mcdat1.row(ref1 - sm + i - ref2 - 1);
		}
		cpos = curve2.row(i);
		skfanim->setController(cpos);
		skfanim->animate();
		ms = skfanim->getSkeleton();
		ms.setRootPosition(v0);
		sib->printPose(ms, std::cout);
	}
	if (skfanim != NULL) delete skfanim;
	return 0;
}
