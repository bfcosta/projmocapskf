#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <pugixml.hpp>
#include <vector>
#include <Eigen/Dense>
#include <skeleton.h>
#include <animation.h>

/**
 * @brief print help message to the user
 * @param name command name
 */
void printHelp(char const * name) {
	std::cout << "mandatory parameters:" << std::endl;
	std::cout << "\tset input animation file: -a" << std::endl;
	std::cout << "\tset input XML file: -x" << std::endl;
	std::cout << "\tset output BVH file: -o" << std::endl;
	std::cout << name << " -a [animation file] -x [XML file] -o [BHV file]" << std::endl;
}

/**
 * @brief read and extract skeleton data from xml
 * @param node xml element describing T pose or marker pose
 * @param position flag of xml tag, T pose or marker pose
 * @param name vector of bone names (output)
 * @param par_id vector of bone parent ids (output)
 * @param bone_pos vector of bone positions
 */
void readPose(pugi::xml_node & node, char const * position,
	std::vector<std::string> & name, std::vector<unsigned int> & par_id, 
	std::vector<Eigen::Vector3d> & bone_pos) {
		
	pugi::xml_node::iterator root = node.child("joint");
	pugi::xml_node::iterator next = root;
	std::vector<std::string> parent;
	Eigen::Vector3d v;
	std::istringstream iss;
	std::string buffer, tpose("position");
	
	if (tpose.compare(position) == 0) {
		par_id.clear();
		name.clear();		
	}
	bone_pos.clear();
	// traverse inorder
	while (true) {
		if (tpose.compare(position) == 0) {
			name.push_back(next->attribute("name").value());
			parent.push_back(next->parent().attribute("name").value());
		}
		iss.str(next->attribute(position).value());
		if (iss >> buffer) v(2) = std::atof(buffer.c_str()); // z
		if (iss >> buffer) v(0) = std::atof(buffer.c_str()); // x
		if (iss >> buffer) v(1) = std::atof(buffer.c_str()); // y
		bone_pos.push_back(v);
		iss.clear();
		if (next->first_child() != NULL) next = next->first_child();
		else {
			while ((next->next_sibling() == NULL) && (next != root)) 
				next = next->parent();
			if (next == root) break;
			next = next->next_sibling();
		}
	}
	// center data: second bone is not an offset
	v = bone_pos.at(1);
	v = v - bone_pos.at(0);
	bone_pos.at(1) = v;
	// build hierarchy
	if (tpose.compare(position) == 0) {
		par_id.push_back(0);
		for (unsigned int i=1; i < parent.size(); ++i) {
			for (unsigned int j=0; j < name.size(); ++j)
				if (parent.at(i).compare(name.at(j)) == 0) {
					par_id.push_back(j);
					break;
				}
		}
	}
	//~ for (unsigned int i=0; i < bone_pos.size(); ++i) {
		//~ std::cout << name.at(i) << " = " << bone_pos.at(i).transpose() << std::endl;
	//~ }
}

/**
 * @brief read xml file and build a correspondent SKF animation
 * @param fxml xml filename
 * @param marker rowwise marker matrix
 * @return SKF animation
 */
Animation readXmlFile(std::string const & fxml, Eigen::MatrixXd const & marker) {
	pugi::xml_document doc;
	std::vector<std::string> name;
	std::vector<unsigned int> par_id;
	std::vector<Eigen::Vector3d> bone_pos1, bone_pos2;
	std::vector<Eigen::Matrix4d> bone_rot;
	Eigen::Matrix4d rmat, jroot;
	Eigen::Vector3d v0, v1, axis;
	Eigen::Vector4d mkvec;
	double ang;
	Animation skfanim;
	Skeleton sk;
	unsigned int mcnt = 0;
	
	mkvec << 0,0,0,1;
	if (doc.load_file(fxml.c_str())) {
		pugi::xml_node skel = doc.child("root").child("skeleton");
		readPose(skel, "position", name, par_id, bone_pos1);
		rmat = Eigen::Matrix4d::Identity();
		v0 = bone_pos1.at(0);
		sk.setRoot(v0(0), v0(1), v0(2), name.at(0));
		sk.setOrientation(0, 0, 0, "ZYX");
		for (unsigned int i=1; i < bone_pos1.size(); ++i) {
			v0 = bone_pos1.at(i);
			sk.addJoint(v0(0), v0(1), v0(2), 0, 0, 0, "ZXY", par_id.at(i), name.at(i));
		}
		skfanim.setSkeleton(sk);
		pugi::xml_node anim = doc.child("root").child("animation");
		for (pugi::xml_node pose = anim.child("pose"); pose != NULL; pose = pose.next_sibling()) {
			readPose(pose, "rotated_position", name, par_id, bone_pos2);
			bone_rot.clear();
			rmat = Eigen::Matrix4d::Identity();
			bone_rot.push_back(rmat); // global rotation
			//std::cout << "root pos = " << bone_pos2.at(0).transpose() << std::endl;
			//rmat = Eigen::Matrix4d::Identity();
			//std::cout << "Rotation matrix:" << std::endl << rmat << std::endl;
			for (unsigned int i=1; i < bone_pos2.size(); ++i) {
				v0 = bone_pos1.at(i);
				v1 = bone_pos2.at(i);
				v0.normalize();
				v1.normalize();
			    axis = v0.cross(v1);
				ang = v0.dot(v1) / (v0.norm() * v1.norm());
				if (ang > 1) ang = 1;  
				else if (ang < -1) ang = -1; 
				ang = std::acos(ang);
				if (v0(1) > 0) ang *= -1; // ??
				rmat.block(0,0,3,3) = Eigen::AngleAxisd(ang, axis).toRotationMatrix();
				bone_rot.push_back(rmat); // global rotation
				//std::cout << "joint(" << i << "): " << bone_pos2.at(i).transpose() << std::endl;
				//std::cout << "Rotation matrix:" << std::endl << rmat << std::endl;
			}
			// local rotation transformation
			for (unsigned int i=par_id.size()-1; i > 0; --i) {
				jroot = bone_rot.at(par_id.at(i)).transpose();
				rmat = bone_rot.at(i);
				rmat = jroot * rmat;
				bone_rot.at(i) = rmat;
			}
			rmat = bone_rot.at(0);
			bone_rot.erase(bone_rot.begin());
			jroot = Eigen::Matrix4d::Identity();
			jroot.col(3).head(3) = bone_pos2.at(0);
			sk.setPose(jroot, rmat, bone_rot);
			//std::cout << "key frame = " << mcnt << std::endl;
			mkvec.head(3) = marker.row(mcnt++);
			//std::cout << "marker = " << mkvec.transpose() << std::endl;
			skfanim.addNewKeyFrame(mkvec, sk);
		}
		// calculate data offset and rotation
		skfanim.setContext();
	}
	return skfanim;
}

/**
 * @brief read animation file and extract markers and controller positions
 * @param fanim animation filename
 * @param ctmat rowwise controller matrix (output)
 * @param mkmat rowwise marker matrix (output)
 */
void readAnimationFile(std::string const & fanim, Eigen::MatrixXd & ctmat, 
	Eigen::MatrixXd & mkmat) {
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> row;
	unsigned int cnum, mnum;
	//unsigned int anum;

	infile.open(fanim.c_str());
	if (infile.is_open()) {
		if (std::getline(infile, line)) {
			if (line.compare("#Animation Sequence#") == 0) {
				std::cout << "header ok!" << std::endl;
			} else {
				std::cout << "no header!" << std::endl;
				std::cout << "first line = " << line << std::endl;
			}
		}
		if (std::getline(infile, line)) {
			cnum = std::atoi(line.c_str());
			ctmat = Eigen::ArrayXXd::Zero(cnum,3);
		}
		if (std::getline(infile, line)) {
			//anum = std::atoi(line.c_str());
		}
		if (std::getline(infile, line)) {
			mnum = std::atoi(line.c_str());
			mkmat = Eigen::ArrayXXd::Zero(mnum, 3);
		}
		while (std::getline(infile, line)) {
			iss.str(line);
			while (iss >> buffer) 
				row.push_back(std::atof(buffer.c_str()));
			iss.clear();
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fanim << std::endl;
	}
	cnum = 0;
	for (unsigned int i=0; i < ctmat.rows(); ++i)
		for (unsigned int j=0; j < ctmat.cols(); ++j)
			ctmat(i,j) = row.at(cnum++);
	for (unsigned int i=0; i < mkmat.rows(); ++i)
		for (unsigned int j=0; j < mkmat.cols(); ++j)
			mkmat(i,j) = row.at(cnum++);	
}

/**
 * @brief print bvh header section describing a skeleton structure
 * @param skel given skeleton
 * @param ost output file stream
 */
void printSkeleton(Skeleton & skel, std::ostream & ost) {

    std::vector<Eigen::Matrix4d> jmats =  skel.getJointMatrices();
    std::vector<std::string> names = skel.getBoneNames();
    std::vector<unsigned int> eeff = skel.getEndEffectorsIdx();
    std::vector<unsigned int> parent =  skel.getIndexHierarchy();
    std::vector<bool> iseeff(names.size(),false);
    std::vector<unsigned int> level(parent.size(),0);
    std::string tab;
    Eigen::Vector4d jvec, zvec;
    unsigned int clevel;
    
    for (unsigned int i=0; i < eeff.size(); ++i) {
		iseeff.at(eeff.at(i)) = true;
	}
    for (unsigned int i=1; i < level.size(); ++i) {
		level.at(i) = level.at(parent.at(i)) + 2;
	}
	zvec << 0,0,0,1;
	jvec = jmats.at(0) * zvec;
	clevel = level.at(0);
	tab.clear();
	for (unsigned int j=0; j < clevel; ++j)
		tab.push_back(' ');
    ost << tab << "HIERARCHY" << std::endl;
    ost << tab << "ROOT " << names.at(0) << std::endl << tab << "{" << std::endl;
	tab.push_back(' ');
	tab.push_back(' ');
	clevel += 2;
    ost << tab << "OFFSET 0 0 0" << std::endl;
    ost << tab << "CHANNELS 6 Xposition Yposition Zposition Zrotation Yrotation Xrotation" << std::endl;
    for (unsigned int i=1; i < names.size(); ++i) {
		for (unsigned int j=clevel; j > level.at(i); j -= 2) {
			tab.erase(tab.size() - 2, 2);
			ost << tab << "}" << std::endl;
		}
		jvec = jmats.at(i) * zvec;
		clevel = level.at(i);
		tab.clear();
		for (unsigned int j=0; j < clevel; ++j)
			tab.push_back(' ');
		if (iseeff.at(i)) 
			ost << tab << "End Site" << std::endl << tab << "{" << std::endl;
		else
			ost << tab << "JOINT " << names.at(i) << std::endl << tab << "{" << std::endl;
		tab.push_back(' ');
		tab.push_back(' ');
		clevel += 2;
		ost << tab << "OFFSET " << jvec(0) << " " << jvec(1) <<  " " << jvec(2) << std::endl;
		if (!iseeff.at(i))
			ost << tab << "CHANNELS 3 Zrotation Xrotation Yrotation" << std::endl;
	}
	for (unsigned int j=clevel; j > level.at(0); j -= 2) {
		tab.erase(tab.size() - 2, 2);
		ost << tab << "}" << std::endl;
	}
}

/**
 * @brief break rotation matrix in euler angles where rotation order is ZXY
 * @param rot rotation matrix
 * @return vector with euler angles
 */
Eigen::Vector3d getZXYRotation(Eigen::Matrix4d & rot) {
	Eigen::Vector3d ret;
	double cos0;

	// rotation order is ZXY
	// get x rotation
	ret(0) = std::asin(rot(2,1));
	if (ret(0) > M_PI / 2)
		ret(0) = M_PI - ret(0);
	cos0 = std::cos(ret(0));
	if ((rot(2,1) == 1) || (rot(2,1) == -1)) {
		// get y rotation, z = 0
		ret(1) = std::atan2(rot(1,0), rot(0,0));
	} else {
		// get y and z rotation
		ret(2) = std::atan2(-1*rot(0,1) / cos0 , rot(1,1) / cos0);
		ret(1) = std::atan2(-1*rot(2,0) / cos0 , rot(2,2) / cos0);
	}
	ret *= 180/M_PI;
	return ret;
}

/**
 * @brief break rotation matrix in euler angles where rotation order is ZYX
 * @param rot rotation matrix
 * @return vector with euler angles
 */
Eigen::Vector3d getZYXRotation(Eigen::Matrix4d & rot) {
	Eigen::Vector3d ret;
	double cos0;

	// rotation order is ZYX
	// get y rotation
	ret(1) = std::asin(-1*rot(2,0));
	if (ret(1) > M_PI / 2)
		ret(1) = M_PI - ret(1);
	cos0 = std::cos(ret(1));
	if ((rot(2,0) == 1) || (rot(2,0) == -1)) {
		// get x rotation, z = 0
		ret(0) = std::atan2(rot(0,1), rot(0,2));
	} else {
		// get x and z rotation
		ret(2) = std::atan2(rot(1,0) / cos0 , rot(0,0) / cos0);
		ret(0) = std::atan2(rot(2,1) / cos0 , rot(2,2) / cos0);
	}
	ret *= 180/M_PI;
	return ret;
}

/**
 * @brief print bvh frame data a skeleton pose
 * @param skel given skeleton
 * @param ost output file stream
 */
void printPose(Skeleton & skel, std::ostream & ost) {
	Eigen::Matrix4d tmat;
    Eigen::Vector3d tvec;
    std::vector<Eigen::Matrix4d> bvec = skel.getBoneMatrices();
    std::vector<unsigned int> par = skel.getIndexHierarchy();
    std::vector<bool> hasSeen(par.size(), false);

    tmat = skel.getSkeletonRootTranslation();
    //std::cout << "translation matrix = " << std::endl << tmat << std::endl;
    ost << tmat(0,3) << " " << tmat(1,3) << " " << tmat(2,3);
    // first rotation is ZYX
    tmat = bvec.at(0);
    tvec = getZYXRotation(tmat);
    hasSeen.at(par.at(1)) = !hasSeen.at(par.at(1));
    ost << " " << tvec(2) << " " << tvec(1) << " " << tvec(0);
    // all others are ZXY
    for (unsigned int i=1; i < bvec.size(); ++i) {
        if (hasSeen.at(par.at(i+1))) continue;
        tmat = bvec.at(i);
        tvec = getZXYRotation(tmat);
        hasSeen.at(par.at(i+1)) = !hasSeen.at(par.at(i+1));
        ost << " " << tvec(2) << " " << tvec(0) << " " << tvec(1);
    }
    ost << std::endl;
}

/**
 * @brief dump SKF animation into a file with bvh format
 * @param outbvh output bvh filename
 * @param skfanim SKF animation
 * @param ctmat rowwise controller matrix
 */
void saveBvhFile(std::string const & outbvh, Animation & skfanim, Eigen::MatrixXd const & ctmat) {
    Eigen::Vector4d cpos;
    Skeleton ms;
	std::ofstream savedfile;
	
	cpos << 0,0,0,1;
	savedfile.open(outbvh.c_str());
    cpos.head(3) = ctmat.row(0);
    //std::cout << "controller = " << cpos.transpose() << std::endl;
	skfanim.setController(cpos);
	skfanim.animate();
	ms = skfanim.getSkeleton();
    printSkeleton(ms, savedfile);
    savedfile << "MOTION" << std::endl;
    savedfile << "Frames: " << ctmat.rows() << std::endl;
    savedfile << "Frame Time: 0.00833333" << std::endl;
    printPose(ms, savedfile);
    for (unsigned int i=1; i < ctmat.rows(); i++) {
		cpos.head(3) = ctmat.row(i);
		//std::cout << "controller = " << cpos.transpose() << std::endl;
		skfanim.setController(cpos);
		skfanim.animate();
		ms = skfanim.getSkeleton();
		printPose(ms, savedfile);
	}
	savedfile.close();
}

int main(int argc, char ** argv) {
    int opt;
	std::string fanim;
	std::string fxml;
	std::string outbvh;
	bool emask[4] = {false, false, false, true};

    // Retrieve the (non-option) argument:
    if ((argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-')) {
		printHelp(argv[0]);
        return 0;
    } 

    // Shut GetOpt error messages down (return '?'): 
    opterr = 0;
    while ((opt = getopt(argc, argv, "axo")) != -1 ) {
        switch (opt) {
            case 'a':
				fanim = argv[optind];
				emask[0] = true;
                break;
            case 'x':
				fxml = argv[optind];
				emask[1] = true;
                break;
            case 'o':
				outbvh = argv[optind];
				emask[2] = true;
                break;
            case '?':
				std::cerr << "Unknown option: '-" << char(optopt) << "'!" << std::endl;
				emask[3] = false;
				printHelp(argv[0]);
                break;
        }
    }
	if (!emask[0] || !emask[1]) {
		std::cout << "Missing parameters:" << std::endl;
		if (!emask[0]) std::cout << "* input animation file" << std::endl;
		if (!emask[1]) std::cout << "* input XML file" << std::endl;
		if (!emask[2]) std::cout << "* output BVH file" << std::endl;
		exit(0);
	}
	if (!emask[3]) exit(0);
	Eigen::MatrixXd ctmat, mkmat;
	//std::cout << "read animation file" << std::endl;
	readAnimationFile(fanim, ctmat, mkmat);
	//std::cout << "read xml file" << std::endl;
	Animation skfanim = readXmlFile(fxml, mkmat);
	//std::cout << "save bvh file" << std::endl;
	saveBvhFile(outbvh, skfanim, ctmat);	
    return 0;
}
