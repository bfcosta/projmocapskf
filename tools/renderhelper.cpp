#include <renderhelper.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <iostream>
#include <cmath>

/**
 * draw RGB axis to debug orientation
 * 
 * \param dsize axis size
 */	
void drawAxis(float dsize) {
	glColor3f(1,0,0);
    glBegin(GL_LINES);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(dsize, 0.0f, 0.0f);
    glEnd();
    glColor3f(0,1,0);
    glBegin(GL_LINES);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(0.0f, dsize, 0.0f);
    glEnd();
    glColor3f(0,0,1);
    glBegin(GL_LINES);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(0.0f, 0.0f, dsize);
    glEnd();
}

/**
 * draw a solid circle given a center and a radius
 * 
 * \param x center in x axis
 * \param y center in y axis
 * \param z center in z axis
 * \param radius radius circle
 */
void drawFilledCircle(GLfloat x, GLfloat y, GLfloat z, GLfloat radius){
	int i;
	int triangleAmount = 20; //# of triangles used to draw circle
	GLfloat twicePi = 2.0f * M_PI;
	
	glBegin(GL_TRIANGLE_FAN);
		glVertex3f(x, y, z); // center of circle
		for(i = 0; i <= triangleAmount;i++) { 
			glVertex3f(x + (radius * cos(i *  twicePi / triangleAmount)), 
			   y + (radius * sin(i * twicePi / triangleAmount)), z);
		}
	glEnd();
}

/**
 * \param sq number of floor squares
 * 
 * draws a ground floor
 */	
void drawGround(int sq) {
	glPushMatrix();
	glColor3f(0.9294f, 0.7882f, 0.6863f);
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0,1.0);
	float planeAmbient[4] = { 0.081f, 0.081f, 0.055f, 1.0f};
	float planeDiffuse[4] = { 0.729, 0.729f, 0.495f, 1.0f};
	float planeSpecular[4] = { 0.081f, 0.081f, 0.055f, 1.0f};
	float planeShininess = 120.0f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, planeAmbient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, planeDiffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, planeSpecular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, planeShininess);
	glNormal3f(0,1,0);
	float planeAmbientAct[4] = { 0.081f, 0.081f, 0.055f, 1.0f};
	for(int i=0; i < 2*sq; i++)
		for(int j=0; j < 2*sq; j++) {
			float factor = (((i+j) % 2) == 0) ? 0.5f : 1.0f;
			planeAmbientAct[0] = factor * 0.081f;
			planeAmbientAct[1] = factor * 0.081f;
			planeAmbientAct[2] = factor * 0.055f;
			planeAmbientAct[3] = factor;
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, planeAmbientAct);
			glBegin(GL_QUADS);
			glVertex3f(-sq + i * 2, 0, -sq + j * 2);
			glVertex3f(-sq + i * 2, 0, -sq + (j+1) * 2);
			glVertex3f(-sq + (i+1) * 2, 0, -sq + (j+1) * 2);
			glVertex3f(-sq + (i+1) * 2, 0, -sq + j * 2);
			glEnd();
		}
	glDisable(GL_POLYGON_OFFSET_FILL);
	glPopMatrix();
}

/**
 * set opengl initializarioon with the simplest perspective camera setup 
 * \param cc center of the skeleton
 */
void setDefaultPerspective(Eigen::Vector4d const & cc) {
    Eigen::Matrix4d mx = Eigen::Affine3d(Eigen::AngleAxisd(-15 * M_PI / 180, 
		Eigen::Vector3d::UnitX())).matrix();
    Eigen::Matrix4d my = Eigen::Affine3d(Eigen::AngleAxisd(25 * M_PI / 180, 
		Eigen::Vector3d::UnitY())).matrix();
	Eigen::Vector4d dist;
	dist << 0,0,240,0;
	dist += cc/2;
	Eigen::Matrix4d im = Eigen::Affine3d(
		Eigen::Translation3d(dist(0), dist(1), dist(2))).matrix();
	Eigen::Vector4d v;

	// up vector
	up << 0,1,0;
	// center
	center = cc.head(3);
	v = my * mx * im * cc;
	// eye
	eye = v.head(3);
    zn = 0.01;
    zf = 300.0;
	glDisable(GL_TEXTURE_2D);
}

/**
 * opengl initialization with default perspective
 * 
 * \param cpos camera aim position in homogeneous coordinates
 * \param tv translation vector
 */	
void setCameraPosition(const Eigen::Vector4d & cpos, const Eigen::VectorXd & tv) {
	Eigen::Matrix4d im = Eigen::Affine3d(
		Eigen::Translation3d(tv(0), tv(1), tv(2))).matrix();
	double yrot = 25 * tv(2) / std::abs(tv(2));
    Eigen::Matrix4d mx = Eigen::Affine3d(Eigen::AngleAxisd(-15 * M_PI / 180, 
		Eigen::Vector3d::UnitX())).matrix();
    Eigen::Matrix4d my = Eigen::Affine3d(Eigen::AngleAxisd(yrot * M_PI / 180, 
		Eigen::Vector3d::UnitY())).matrix();
	Eigen::Vector4d v;

	// up vector
	up << 0,1,0;
	// center
	center = cpos.head(3);
	v = my * mx * im * cpos;
	// eye
	eye = v.head(3);
}

/**
 * opengl initialization setting animation perspective parameters to make it visible
 * 
 * \param pos matrix with root joint positions in animation
 */
void setMocapPerspective(const Eigen::MatrixXd & pos) {
	Eigen::MatrixXd xzbbox = Eigen::ArrayXXd::Zero(9, pos.cols());
	Eigen::VectorXd xzdiag = Eigen::ArrayXd::Zero(pos.cols());
	Eigen::VectorXd cpos = Eigen::ArrayXd::Zero(4);
	Eigen::VectorXd trvec = Eigen::ArrayXd::Zero(3);
	double d0, d1;

	cpos << 0, 0, 0, 1;
	// get x,y,z max and min
	for (unsigned int i=0; i < xzbbox.rows(); ++i) 
		xzbbox.row(i) = pos.row(0);
	for (unsigned int i=1; i < pos.rows(); ++i) {
		if (pos(i,0) < xzbbox(0,0)) {
			xzbbox.row(0) = pos.row(i);
		} else if (pos(i,0) > xzbbox(1,0)) {
			xzbbox.row(1) = pos.row(i);
		}
		if (pos(i,1) < xzbbox(2,1)) {
			xzbbox.row(2) = pos.row(i);
		} else if (pos(i,1) > xzbbox(3,1)) {
			xzbbox.row(3) = pos.row(i);
		}
		if (pos(i,2) < xzbbox(4,2)) {
			xzbbox.row(4) = pos.row(i);
		} else if (pos(i,2) > xzbbox(5,2)) {
			xzbbox.row(5) = pos.row(i);
		}
	}

	// camera aim position
	xzbbox(6,0) = (xzbbox(0,0) + xzbbox(1,0)) / 2;
	xzbbox(6,1) = (xzbbox(2,1) + xzbbox(3,1)) / 2;
	xzbbox(6,2) = (xzbbox(4,2) + xzbbox(5,2)) / 2;
	cpos(0) = xzbbox(6,0);
	cpos(1) = xzbbox(6,1);
	cpos(2) = xzbbox(6,2);

	// camera eye position, Z near and Z far
	xzdiag(0) = xzbbox(1,0) - xzbbox(0,0);
	xzdiag(1) = 0;
	xzdiag(2) = xzbbox(5,2) - xzbbox(4,2);
	d0 = xzdiag.norm(); 
	d1 = 2*xzbbox(3,1);
	if (d0 < d1) {
		// skeleton height dominates
		cpos(0) = pos(0,0) / 2;
		cpos(1) = pos(0,1) / 2;
		cpos(2) = pos(0,2) / 2;		
		trvec(2) = 5 * cpos.head(3).norm();
		zn = 0.01;
		zf = 2 * trvec(2);
	} else {
		// shift in XZ plane dominates
		zn = 0.2 * d0; // Z near
		d0 = ((1 + std::sqrt(2.0)) / 2) * d0 + zn;
		zf =  2 * d0; // Z far
		xzdiag.normalize();
		// get orthogonal vector
		xzdiag(1) = xzdiag(0);
		xzdiag(0) = - xzdiag(2);
		xzdiag(2) = xzdiag(1);
		xzdiag(1) = 0;
		xzbbox.row(7) = xzbbox.row(6);
		xzbbox.row(8) = xzbbox.row(6);
		trvec(0) = d0 * xzdiag(0);
		trvec(1) = cpos(2);
		trvec(2) = d0 * xzdiag(2);
		xzbbox.row(7) += d0 * xzdiag;
		xzbbox.row(8) -= d0 * xzdiag;
		d0 = xzbbox.row(7).norm();
		d1 = xzbbox.row(8).norm();
		if (d0 < d1) {
			trvec(0) = -1 * trvec(0);
			trvec(2) = -1 * trvec(2);
		}
	}
	setCameraPosition(cpos, trvec);
	//std::cout << "camera position = (" << eye(0) << ", " << eye(1) << ", " << eye(2) << ")" << std::endl;
	//std::cout << "camera aim = (" << center(0) << ", " << center(1) << ", " << center(2) << ")" << std::endl;
	//std::cout << "znear = " << zn << " zfar = " << zf << std::endl;
	glDisable(GL_TEXTURE_2D);
}

/**
 * opengl initialization setting animation perspective parameters to make it visible
 * 
 * \param pos matrix with root joint positions in animation
 */
void setMocapFrontPerspective(const Eigen::MatrixXd & pos) {
	Eigen::MatrixXd xzbbox = Eigen::ArrayXXd::Zero(2, pos.cols());
	Eigen::VectorXd cpos = Eigen::ArrayXd::Zero(4);
	Eigen::VectorXd trvec = Eigen::ArrayXd::Zero(3);
	double n, h;

	cpos << 0, 0, 0, 1;
	// get first and last
	xzbbox.row(0) = pos.row(0);
	xzbbox.row(1) = pos.row(pos.rows() - 1);
	// camera aim position
	cpos.head(3) = (xzbbox.row(1) - xzbbox.row(0)) / 2;
	// camera eye position, Z near and Z far
	trvec = cpos.head(3);
	n = trvec.norm();
	zn = 0.01;
	if (n < xzbbox(0,1)) {
		cpos.head(3) = xzbbox.row(0) / 2;
		trvec(0) = trvec(1) = 0;
		trvec(2) = 5 * cpos.head(3).norm();
		zf = 2 * trvec(2);
	} else {
		cpos.head(3) = (xzbbox.row(1) + xzbbox.row(0)) / 2;
		h = xzbbox(0,1) / (2 * std::tan(0.125 * M_PI)); // 0.125 rad = 22.5º
		h += n;
		zf = 2 * h;
		h /= n;
		trvec *= h;			
	}
	setCameraPosition(cpos, trvec);
	//std::cout << "camera position = (" << eye(0) << ", " << eye(1) << ", " << eye(2) << ")" << std::endl;
	//std::cout << "camera aim = (" << center(0) << ", " << center(1) << ", " << center(2) << ")" << std::endl;
	//std::cout << "znear = " << zn << " zfar = " << zf << std::endl;
	glDisable(GL_TEXTURE_2D);	
}

/**
 * read key frames markers and all frame projection file, loading both into matrices
 * \param fname file location to be read
 * \param ret key frame markers matrix
 * \param ret2 all frames projection matrix
 */
void readMarkersPosition(char * fname, Eigen::MatrixXd & ret, Eigen::MatrixXd & ret2) {
	std::ifstream infile;
	std::string line, buffer;
	std::istringstream iss;
	std::vector<double> row;
	std::vector< std::vector<double> > data;
	unsigned int kf=0;

	infile.open(fname);
	if (infile.is_open()) {
		while (std::getline(infile, line)) {
			iss.str(line);
			row.clear();
			while (iss >> buffer) 
				row.push_back(std::atof(buffer.c_str()));
			if (row.size() == 3) ++kf;
			iss.clear();
			data.push_back(row);
		}
		infile.close();

	} else {
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
	}

	ret = Eigen::ArrayXXd::Zero(kf, data.at(0).size());
	if (data.size() > kf)
		ret2 = Eigen::ArrayXXd::Zero(data.size() - kf, data.at(kf).size());
	for (unsigned int i=0; i < kf; ++i)
		for (unsigned int j=0; j < data.at(i).size(); ++j)
			ret(i,j) = data.at(i).at(j);

	for (unsigned int i=kf; i < data.size(); ++i)
		for (unsigned int j=0; j < data.at(i).size(); ++j)
			ret2(i - kf,j) = data.at(i).at(j);
}
