#include <iostream>
#include <string>
#include <kfselector.h>
#include <skanalizer.h>
#include <asfreader.h>
#include <bvhreader.h>

Eigen::MatrixXd distmat;

/**
 * @brief print error and time stats for linear and hermitian interpolation
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void linearAndHermiteRun(MocapReader * mocap) {
	// linear interpolator
	USEucLinSelector naivelin(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with linear interpolation" << std::endl;
	naivelin.printStats();
	PBEucLinSelector poslin(mocap, 0.03f); // 3 % of frames
	std::cout << "Position-based selector with linear interpolation" << std::endl;
	poslin.printStats();
	SCSEucLinSelector scslin(mocap, 0.03f); // 3 % of frames
	std::cout << "SCS selector with linear interpolation" << std::endl;
	scslin.printStats();
	//RolsEucLinSelector rolslin(mocap, 0.03f); // 3 % of frames
	//std::cout << "ROLS selector with linear interpolation" << std::endl;
	//rolslin.printStats();
	// hermite interpolator
	// spheric linear interpolator
	USEucSlerpSelector naiveslerp(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with spherical linear interpolation" << std::endl;
	naiveslerp.printStats();
	PBEucSlerpSelector posslerp(mocap, 0.03f); // 3 % of frames
	std::cout << "Position-based selector with spherical linear interpolation" << std::endl;
	posslerp.printStats();
	SCSEucSlerpSelector scsslerp(mocap, 0.03f); // 3 % of frames
	std::cout << "SCS selector with spherical linear interpolation" << std::endl;
	scsslerp.printStats();
	// hermitian
	USEucHerSelector naiveher(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with hermite interpolation" << std::endl;
	naiveher.printStats();
	PBEucHerSelector posher(mocap, 0.03f); // 3 % of frames
	std::cout << "Position-based selector with hermite interpolation" << std::endl;
	posher.printStats();
	SCSEucHerSelector scsher(mocap, 0.03f); // 3 % of frames
	std::cout << "SCS selector with hermite interpolation" << std::endl;
	scsher.printStats();	
	//RolsEucHerSelector rolsher(mocap, 0.03f); // 3 % of frames
	//std::cout << "ROLS selector with hermite interpolation" << std::endl;
	//rolsher.printStats();
}

/**
 * @brief print error and time stats for single dimension RBF interpolation
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfSingleDimRun(MocapReader * mocap) {
	// spatial keyframe interpolator 1D
	USEucRbfSelector naiverbf(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 1D SKF interpolation" << std::endl;
	naiverbf.printStats();
	distmat = naiverbf.getFrameDistanceMatrix();
	PBEucRbfSelector posrbf(mocap, 0.03f); // 3 % of frames
	std::cout << "Position-based selector with 1D SKF interpolation" << std::endl;
	posrbf.printStats();
	SCSEucRbfSelector scsrbf(mocap, 0.03f); // 3 % of frames
	std::cout << "SCS selector with 1D SKF interpolation" << std::endl;
	scsrbf.printStats();
	RolsEucRbfSelector rols1d(mocap, 0.03f); // 3 % of frames
	rols1d.setFrameDistanceMatrix(distmat);
	std::cout << "ROLS selector with 1D SKF interpolation" << std::endl;
	rols1d.printStats();
	RolsRbfpEucRbfSelector rolsrbfp1d(mocap, 0.03f); // 3 % of frames
	rolsrbfp1d.setFrameDistanceMatrix(distmat);
	std::cout << "ROLS/RBFP selector with 1D SKF interpolation" << std::endl;
	rolsrbfp1d.printStats();

}

/**
 * @brief print error and time stats for 2D RBF interpolation and Force approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfForceRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: force
	USEucForceRbfSelector naiveforce(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and force spread approach" << std::endl;
	naiveforce.printStats();
	Eigen::MatrixXd forceproj = naiveforce.getFramesProjection();
	PBEucForceRbfSelector posforce(mocap, 0.03f); // 3 % of frames
	posforce.setFramesProjection(forceproj);
	std::cout << "Position-based selector with 2D SKF interpolation and force spread approach" << std::endl;
	posforce.printStats();
	SCSEucForceRbfSelector scsforce(mocap, 0.03f); // 3 % of frames
	scsforce.setFramesProjection(forceproj);
	std::cout << "SCS selector with 2D SKF interpolation and force spread approach" << std::endl;
	scsforce.printStats();
	RolsEucForceRbfSelector rolsforce(mocap, 0.03f); // 3 % of frames
	rolsforce.setFrameDistanceMatrix(distmat);
	rolsforce.setFramesProjection(forceproj);
	std::cout << "ROLS selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsforce.printStats();	
	RolsRbfpEucForceRbfSelector rolsrbfpforce(mocap, 0.03f); // 3 % of frames
	rolsrbfpforce.setFrameDistanceMatrix(distmat);
	rolsrbfpforce.setFramesProjection(forceproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsrbfpforce.printStats();	
}

/**
 * @brief print error and time stats for 2D RBF interpolation and MDS approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfMDSRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: MDS
	USEucMdsRbfSelector naivemds(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and MDS approach" << std::endl;
	naivemds.printStats();
	Eigen::MatrixXd mdsproj = naivemds.getFramesProjection();
	PBEucMdsRbfSelector posmds(mocap, 0.03f); // 3 % of frames
	posmds.setFramesProjection(mdsproj);
	std::cout << "Position-based selector with 2D SKF interpolation and MDS approach" << std::endl;
	posmds.printStats();
	SCSEucMdsRbfSelector scsmds(mocap, 0.03f); // 3 % of frames
	scsmds.setFramesProjection(mdsproj);
	std::cout << "SCS selector with 2D SKF interpolation and MDS approach" << std::endl;
	scsmds.printStats();
	RolsEucMdsRbfSelector rolsmds(mocap, 0.03f); // 3 % of frames
	rolsmds.setFrameDistanceMatrix(distmat);
	rolsmds.setFramesProjection(mdsproj);
	std::cout << "ROLS selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsmds.printStats();	
	RolsRbfpEucMdsRbfSelector rolsrbfpmds(mocap, 0.03f); // 3 % of frames
	rolsrbfpmds.setFrameDistanceMatrix(distmat);
	rolsrbfpmds.setFramesProjection(mdsproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsrbfpmds.printStats();	
}

/**
 * @brief print error and time stats for 2D RBF interpolation and PCA approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfPCARun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: MDS
	USEucPcaRbfSelector naivepca(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and PCA approach" << std::endl;
	naivepca.printStats();
	Eigen::MatrixXd pcaproj = naivepca.getFramesProjection();
	PBEucPcaRbfSelector pospca(mocap, 0.03f); // 3 % of frames
	pospca.setFramesProjection(pcaproj);
	std::cout << "Position-based selector with 2D SKF interpolation and PCA approach" << std::endl;
	pospca.printStats();
	SCSEucPcaRbfSelector scspca(mocap, 0.03f); // 3 % of frames
	scspca.setFramesProjection(pcaproj);
	std::cout << "SCS selector with 2D SKF interpolation and PCA approach" << std::endl;
	scspca.printStats();
	RolsEucPcaRbfSelector rolspca(mocap, 0.03f); // 3 % of frames
	rolspca.setFrameDistanceMatrix(distmat);
	rolspca.setFramesProjection(pcaproj);
	std::cout << "ROLS selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolspca.printStats();	
	RolsRbfpEucPcaRbfSelector rolsrbfppca(mocap, 0.03f); // 3 % of frames
	rolsrbfppca.setFrameDistanceMatrix(distmat);
	rolsrbfppca.setFramesProjection(pcaproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolsrbfppca.printStats();	
}

/**
 * @brief print error and time stats for 2D RBF interpolation and LLE approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfLLERun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: LLE
	USEucLleRbfSelector naivelle(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and LLE approach" << std::endl;
	naivelle.printStats();
	Eigen::MatrixXd lleproj = naivelle.getFramesProjection();
	PBEucLleRbfSelector poslle(mocap, 0.03f); // 3 % of frames
	poslle.setFramesProjection(lleproj);
	std::cout << "Position-based selector with 2D SKF interpolation and LLE approach" << std::endl;
	poslle.printStats();
	SCSEucLleRbfSelector scslle(mocap, 0.03f); // 3 % of frames
	scslle.setFramesProjection(lleproj);
	std::cout << "SCS selector with 2D SKF interpolation and LLE approach" << std::endl;
	scslle.printStats();
	RolsEucLleRbfSelector rolslle(mocap, 0.03f); // 3 % of frames
	rolslle.setFrameDistanceMatrix(distmat);
	rolslle.setFramesProjection(lleproj);
	std::cout << "ROLS selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolslle.printStats();	
	RolsRbfpEucLleRbfSelector rolsrbfplle(mocap, 0.03f); // 3 % of frames
	rolsrbfplle.setFrameDistanceMatrix(distmat);
	rolsrbfplle.setFramesProjection(lleproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolsrbfplle.printStats();	
}

/**
 * @brief print error and time stats for 2D RBF interpolation and LAMP approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfLAMPRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: LAMP
	USEucLampRbfSelector naivelamp(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and LAMP approach" << std::endl;
	naivelamp.printStats();
	Eigen::MatrixXd lampproj = naivelamp.getFramesProjection();
	PBEucLampRbfSelector poslamp(mocap, 0.03f); // 3 % of frames
	poslamp.setFramesProjection(lampproj);
	std::cout << "Position-based selector with 2D SKF interpolation and LAMP approach" << std::endl;
	poslamp.printStats();
	SCSEucLampRbfSelector scslamp(mocap, 0.03f); // 3 % of frames
	scslamp.setFramesProjection(lampproj);
	std::cout << "SCS selector with 2D SKF interpolation and LAMP approach" << std::endl;
	scslamp.printStats();	
	//RolsEucLampRbfSelector rolslamp(mocap, 0.03f); // 3 % of frames
	//rolslamp.setFrameDistanceMatrix(distmat);
	//rolslamp.setFramesProjection(lampproj);
	//std::cout << "ROLS selector with 2D SKF interpolation and LAMP approach" << std::endl;
	//rolslamp.printStats();
}

/**
 * @brief print error and time stats for 2D RBF interpolation and t-SNE approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfTsneRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: t-SNE
	USEucTsneRbfSelector naivetsne(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	naivetsne.printStats();
	Eigen::MatrixXd tsneproj = naivetsne.getFramesProjection();
	PBEucTsneRbfSelector postsne(mocap, 0.03f); // 3 % of frames
	postsne.setFramesProjection(tsneproj);
	std::cout << "Position-based selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	postsne.printStats();
	SCSEucTsneRbfSelector scstsne(mocap, 0.03f); // 3 % of frames
	scstsne.setFramesProjection(tsneproj);
	std::cout << "SCS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	scstsne.printStats();
	RolsEucTsneRbfSelector rolstsne(mocap, 0.03f); // 3 % of frames
	rolstsne.setFrameDistanceMatrix(distmat);
	rolstsne.setFramesProjection(tsneproj);
	std::cout << "ROLS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolstsne.printStats();
	RolsRbfpEucTsneRbfSelector rolsrbfptsne(mocap, 0.03f); // 3 % of frames
	rolsrbfptsne.setFrameDistanceMatrix(distmat);
	rolsrbfptsne.setFramesProjection(tsneproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolsrbfptsne.printStats();
}

/**
 * @brief print error and time stats for 2D RBF interpolation and isomap approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfIsomapRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: isomap
	USEucIsomapRbfSelector naiveisomap(mocap, 0.03f); // 3 % of frames
	std::cout << "Naive selector with 2D SKF interpolation and Isomap approach" << std::endl;
	naiveisomap.printStats();
	Eigen::MatrixXd isomapproj = naiveisomap.getFramesProjection();
	PBEucTsneRbfSelector posisomap(mocap, 0.03f); // 3 % of frames
	posisomap.setFramesProjection(isomapproj);
	std::cout << "Position-based selector with 2D SKF interpolation and Isomap approach" << std::endl;
	posisomap.printStats();
	SCSEucIsomapRbfSelector scsisomap(mocap, 0.03f); // 3 % of frames
	scsisomap.setFramesProjection(isomapproj);
	std::cout << "SCS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	scsisomap.printStats();
	RolsEucIsomapRbfSelector rolsisomap(mocap, 0.03f); // 3 % of frames
	rolsisomap.setFrameDistanceMatrix(distmat);
	rolsisomap.setFramesProjection(isomapproj);
	std::cout << "ROLS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsisomap.printStats();
	RolsRbfpEucIsomapRbfSelector rolsrbfpisomap(mocap, 0.03f); // 3 % of frames
	rolsrbfpisomap.setFrameDistanceMatrix(distmat);
	rolsrbfpisomap.setFramesProjection(isomapproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsrbfpisomap.printStats();
}

int main(int argc, char ** argv) {
	MocapReader * mocap;
	
	if (argc < 2) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	if (ext.compare("bvh") == 0) {
		mocap = new BVHReader();
		mocap->readFile(fname);
	} else if (ext.compare("asf") == 0) {
		mocap = new ASFReader();
		mocap->readFile(fname);
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				mocap->readFile(fname);
			} else {
				std::cout << "no amc file given." << std::endl;
				exit(1);
			}
		} else {
			std::cout << "expected one amc file." << std::endl;
			exit(1);
		}
	} else {
		std::cout << "unrecognized format." << std::endl;
		exit(1);
	}

	linearAndHermiteRun(mocap);
	rbfSingleDimRun(mocap);
	rbfForceRun(mocap);
	rbfMDSRun(mocap);
	rbfPCARun(mocap);
	rbfLLERun(mocap);
	rbfLAMPRun(mocap);
	rbfTsneRun(mocap);
	rbfIsomapRun(mocap);
	delete mocap;
	return 0;
}
