#include <iostream>
#include <fstream>
#include <string>
#include <kfselector.h>
#include <asfreader.h>
#include <bvhreader.h>

Eigen::VectorXi kframes;
Eigen::MatrixXd markers;
Eigen::MatrixXd distmat;

/**
 * @brief save controller and keyframes for playback
 * @param kframes keyframe vector
 * @param markers rowwise matrix of controllers
 * @param fname filename for saving data
 */
void saveMarkersForSKF(Eigen::VectorXi & kframes, Eigen::MatrixXd & markers, std::string fname) {
	Eigen::MatrixXd ret = markers.block(0,0,markers.rows(),3);
	for (unsigned int i=0; i < kframes.size(); ++i)
		ret(i,2) = kframes(i);
	std::ofstream savedfile;
	savedfile.open(fname.c_str());
	savedfile << ret << std::endl;
	savedfile.close();
}

/**
 * @brief save projection matrix for each frame
 * @param projmat projection matrix to be saved
 * @param fname filename for saving data
 */
void saveProjection(Eigen::MatrixXd & projmat, std::string fname) {
	std::ofstream savedfile;

	savedfile.open(fname.c_str());
	savedfile << projmat << std::endl;
	savedfile.close();
}

/**
 * @brief print error and time stats for linear and hermitian interpolation
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void linearAndHermiteRun(MocapReader * mocap) {
	PBEucLinSelector pblin(mocap, 0.05f); // 5 % of frames
	std::cout << "position-based selector, linear interpolator" << std::endl;
	pblin.printStats();
	SCSEucLinSelector scslin(mocap, 0.05f); // 5 % of frames
	std::cout << "SCS selector, linear interpolator" << std::endl;
	scslin.printStats();
	PBEucSlerpSelector pbslerp(mocap, 0.05f); // 5 % of frames
	std::cout << "position-based selector, spherical linear interpolator" << std::endl;
	pbslerp.printStats();
	SCSEucSlerpSelector scsslerp(mocap, 0.05f); // 5 % of frames
	std::cout << "SCS selector, spherical linear interpolator" << std::endl;
	scsslerp.printStats();
	PBEucHerSelector pbher(mocap, 0.05f); // 5 % of frames
	std::cout << "position-based selector, hermite interpolator" << std::endl;
	pbher.printStats();
	SCSEucHerSelector scsher(mocap, 0.05f); // 5 % of frames
	std::cout << "SCS selector, hermite interpolator" << std::endl;
	scsher.printStats();
}

/**
 * @brief print error and time stats for single dimension RBF interpolation
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfSingleDimRun(MocapReader * mocap) {
	PBEucRbfSelector pbrbf(mocap, 0.05f); // 5 % of frames
	std::cout << "position-based selector, RBF interpolator, 1D markers" << std::endl;
	pbrbf.printStats();
	distmat = pbrbf.getFrameDistanceMatrix();
	kframes = pbrbf.getKeyframeVector();
	markers = pbrbf.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBrbf.txt");
	SCSEucRbfSelector scsrbf(mocap, 0.05f); // 5 % of frames
	std::cout << "SCS selector, RBF interpolator, 1D markers" << std::endl;
	scsrbf.printStats();
	kframes = scsrbf.getKeyframeVector();
	markers = scsrbf.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSrbf.txt");
	RolsEucRbfSelector rolsrbf(mocap, 0.05f); // 5 % of frames
	rolsrbf.setFrameDistanceMatrix(distmat);
	std::cout << "ROLS selector, RBF interpolation, 1D markers" << std::endl;
	rolsrbf.printStats();
	kframes = rolsrbf.getKeyframeVector();
	markers = rolsrbf.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSrbf.txt");
	RolsRbfpEucRbfSelector rolsrbfprbf(mocap, 0.05f); // 5 % of frames
	rolsrbfprbf.setFrameDistanceMatrix(distmat);
	std::cout << "ROLS/RBFP selector, RBF interpolation, 1D markers" << std::endl;
	rolsrbfprbf.printStats();
	kframes = rolsrbfprbf.getKeyframeVector();
	markers = rolsrbfprbf.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPrbf.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and Force approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfForceRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: Force
	PBEucForceRbfSelector posforce(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and force spread approach" << std::endl;
	posforce.printStats();
	Eigen::MatrixXd forceproj = posforce.getFramesProjection();
	saveProjection(forceproj, "force.txt");
	//Eigen::VectorXi kframes = posforce.getKeyframes(posforce.getNumberOfKeyframes());
	kframes = posforce.getKeyframeVector();
	markers = posforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBforce.txt");
	SCSEucForceRbfSelector scsforce(mocap, 0.05f); // 5 % of frames
	scsforce.setFramesProjection(forceproj);
	std::cout << "SCS selector with 2D SKF interpolation and force spread approach" << std::endl;
	scsforce.printStats();
	kframes = scsforce.getKeyframeVector();
	markers = scsforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSforce.txt");
	RolsEucForceRbfSelector rolsforce(mocap, 0.05f); // 5 % of frames
	rolsforce.setFrameDistanceMatrix(distmat);
	rolsforce.setFramesProjection(forceproj);
	std::cout << "ROLS selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsforce.printStats();
	kframes = rolsforce.getKeyframeVector();
	markers = rolsforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSforce.txt");
	RolsRbfpEucForceRbfSelector rolsrbfpforce(mocap, 0.05f); // 5 % of frames
	rolsrbfpforce.setFrameDistanceMatrix(distmat);
	rolsrbfpforce.setFramesProjection(forceproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsrbfpforce.printStats();
	kframes = rolsrbfpforce.getKeyframeVector();
	markers = rolsrbfpforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPforce.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and MDS approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfMDSRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: MDS
	PBEucMdsRbfSelector posmds(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and MDS approach" << std::endl;
	posmds.printStats();
	Eigen::MatrixXd mdsproj = posmds.getFramesProjection();
	saveProjection(mdsproj, "mds.txt");
	kframes = posmds.getKeyframeVector();
	markers = posmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBmds.txt");
	SCSEucMdsRbfSelector scsmds(mocap, 0.05f); // 5 % of frames
	scsmds.setFramesProjection(mdsproj);
	std::cout << "SCS selector with 2D SKF interpolation and MDS approach" << std::endl;
	scsmds.printStats();
	kframes = scsmds.getKeyframeVector();
	markers = scsmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSmds.txt");
	RolsEucMdsRbfSelector rolsmds(mocap, 0.05f); // 5 % of frames
	rolsmds.setFrameDistanceMatrix(distmat);
	rolsmds.setFramesProjection(mdsproj);
	std::cout << "ROLS selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsmds.printStats();
	kframes = rolsmds.getKeyframeVector();
	markers = rolsmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSmds.txt");
	RolsRbfpEucMdsRbfSelector rolsrbfpmds(mocap, 0.05f); // 5 % of frames
	rolsrbfpmds.setFrameDistanceMatrix(distmat);
	rolsrbfpmds.setFramesProjection(mdsproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsrbfpmds.printStats();
	kframes = rolsrbfpmds.getKeyframeVector();
	markers = rolsrbfpmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPmds.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and PCA approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfPCARun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: PCA
	PBEucPcaRbfSelector pospca(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and PCA approach" << std::endl;
	pospca.printStats();
	Eigen::MatrixXd pcaproj = pospca.getFramesProjection();
	saveProjection(pcaproj, "pca.txt");
	kframes = pospca.getKeyframeVector();
	markers = pospca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBpca.txt");
	SCSEucPcaRbfSelector scspca(mocap, 0.05f); // 5 % of frames
	scspca.setFramesProjection(pcaproj);
	std::cout << "SCS selector with 2D SKF interpolation and PCA approach" << std::endl;
	scspca.printStats();
	kframes = scspca.getKeyframeVector();
	markers = scspca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSpca.txt");
	RolsEucPcaRbfSelector rolspca(mocap, 0.05f); // 5 % of frames
	rolspca.setFrameDistanceMatrix(distmat);
	rolspca.setFramesProjection(pcaproj);
	std::cout << "ROLS selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolspca.printStats();
	kframes = rolspca.getKeyframeVector();
	markers = rolspca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSpca.txt");
	RolsRbfpEucPcaRbfSelector rolsrbfppca(mocap, 0.05f); // 5 % of frames
	rolsrbfppca.setFrameDistanceMatrix(distmat);
	rolsrbfppca.setFramesProjection(pcaproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolsrbfppca.printStats();
	kframes = rolsrbfppca.getKeyframeVector();
	markers = rolsrbfppca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPpca.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and LLE approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfLLERun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: LLE
	PBEucLleRbfSelector poslle(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and LLE approach" << std::endl;
	poslle.printStats();
	Eigen::MatrixXd lleproj = poslle.getFramesProjection();
	saveProjection(lleproj, "lle.txt");
	kframes = poslle.getKeyframeVector();
	markers = poslle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBlle.txt");
	SCSEucLleRbfSelector scslle(mocap, 0.05f); // 5 % of frames
	scslle.setFramesProjection(lleproj);
	std::cout << "SCS selector with 2D SKF interpolation and LLE approach" << std::endl;
	scslle.printStats();
	kframes = scslle.getKeyframeVector();
	markers = scslle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSlle.txt");
	RolsEucLleRbfSelector rolslle(mocap, 0.05f); // 5 % of frames
	rolslle.setFrameDistanceMatrix(distmat);
	rolslle.setFramesProjection(lleproj);
	std::cout << "ROLS selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolslle.printStats();
	kframes = rolslle.getKeyframeVector();
	markers = rolslle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSlle.txt");
	RolsRbfpEucLleRbfSelector rolsrbfplle(mocap, 0.05f); // 5 % of frames
	rolsrbfplle.setFrameDistanceMatrix(distmat);
	rolsrbfplle.setFramesProjection(lleproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolsrbfplle.printStats();
	kframes = rolsrbfplle.getKeyframeVector();
	markers = rolsrbfplle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPlle.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and t-SNE approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfTsneRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: t-SNE
	PBEucTsneRbfSelector postsne(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	postsne.printStats();
	Eigen::MatrixXd tsneproj = postsne.getFramesProjection();
	saveProjection(tsneproj, "tsne.txt");
	kframes = postsne.getKeyframeVector();
	markers = postsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBtsne.txt");
	SCSEucTsneRbfSelector scstsne(mocap, 0.05f); // 5 % of frames
	scstsne.setFramesProjection(tsneproj);
	std::cout << "SCS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	scstsne.printStats();
	kframes = scstsne.getKeyframeVector();
	markers = scstsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCStsne.txt");
	RolsEucTsneRbfSelector rolstsne(mocap, 0.05f); // 5 % of frames
	rolstsne.setFrameDistanceMatrix(distmat);
	rolstsne.setFramesProjection(tsneproj);
	std::cout << "ROLS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolstsne.printStats();
	kframes = rolstsne.getKeyframeVector();
	markers = rolstsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLStsne.txt");
	RolsRbfpEucTsneRbfSelector rolsrbfptsne(mocap, 0.05f); // 5 % of frames
	rolsrbfptsne.setFrameDistanceMatrix(distmat);
	rolsrbfptsne.setFramesProjection(tsneproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolsrbfptsne.printStats();
	kframes = rolsrbfptsne.getKeyframeVector();
	markers = rolsrbfptsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPtsne.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and isomap approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfIsomapRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: t-SNE
	PBEucIsomapRbfSelector posisomap(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and Isomap approach" << std::endl;
	posisomap.printStats();
	Eigen::MatrixXd isomapproj = posisomap.getFramesProjection();
	saveProjection(isomapproj, "isomap.txt");
	kframes = posisomap.getKeyframeVector();
	markers = posisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBisomap.txt");
	SCSEucIsomapRbfSelector scsisomap(mocap, 0.05f); // 5 % of frames
	scsisomap.setFramesProjection(isomapproj);
	std::cout << "SCS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	scsisomap.printStats();
	kframes = scsisomap.getKeyframeVector();
	markers = scsisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSisomap.txt");
	RolsEucIsomapRbfSelector rolsisomap(mocap, 0.05f); // 5 % of frames
	rolsisomap.setFrameDistanceMatrix(distmat);
	rolsisomap.setFramesProjection(isomapproj);
	std::cout << "ROLS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsisomap.printStats();
	kframes = rolsisomap.getKeyframeVector();
	markers = rolsisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSisomap.txt");
	RolsRbfpEucIsomapRbfSelector rolsrbfpisomap(mocap, 0.05f); // 5 % of frames
	rolsrbfpisomap.setFrameDistanceMatrix(distmat);
	rolsrbfpisomap.setFramesProjection(isomapproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsrbfpisomap.printStats();
	kframes = rolsrbfpisomap.getKeyframeVector();
	markers = rolsrbfpisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPisomap.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and LAMP approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfLAMPRun(MocapReader * mocap) {
	// spatial keyframe interpolator 2D: LAMP
	PBEucLampRbfSelector poslamp(mocap, 0.05f); // 5 % of frames
	std::cout << "Position-based selector with 2D SKF interpolation and LAMP approach" << std::endl;
	poslamp.printStats();
	Eigen::MatrixXd lampproj = poslamp.getFramesProjection();
	saveProjection(lampproj, "lamp.txt");
	kframes = poslamp.getKeyframeVector();
	markers = poslamp.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBlamp.txt");
	SCSEucLampRbfSelector scslamp(mocap, 0.05f); // 5 % of frames
	scslamp.setFramesProjection(lampproj);
	std::cout << "SCS selector with 2D SKF interpolation and LAMP approach" << std::endl;
	scslamp.printStats();
	kframes = scslamp.getKeyframeVector();
	markers = scslamp.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSlamp.txt");
}


int main(int argc, char ** argv) {
	MocapReader * mocap;

	if (argc < 2) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	if (ext.compare("bvh") == 0) {
		mocap = new BVHReader();
		mocap->readFile(fname);
	} else if (ext.compare("asf") == 0) {
		mocap = new ASFReader();
		mocap->readFile(fname);
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				mocap->readFile(fname);
			} else {
				std::cout << "no amc file given." << std::endl;
				exit(1);
			}
		} else {
			std::cout << "expected one amc file." << std::endl;
			exit(1);
		}
	} else {
		std::cout << "unrecognized format." << std::endl;
		exit(1);
	}

	linearAndHermiteRun(mocap);
	rbfSingleDimRun(mocap);
	rbfForceRun(mocap);
	rbfMDSRun(mocap);
	rbfPCARun(mocap);
	rbfLLERun(mocap);
	rbfLAMPRun(mocap);
	rbfTsneRun(mocap);
	rbfIsomapRun(mocap);
	delete mocap;
	return 0;
}
