#include <iostream>
#include <fstream>
#include <string>
#include <kfselector.h>
#include <asfreader.h>
#include <bvhreader.h>

Eigen::VectorXi kframes;
Eigen::MatrixXd markers;
Eigen::MatrixXd distmat;

/**
 * @brief rescale markers for [0,1] interval
 * @param proj marker matrix to be updated
 */
void rescaleMarkers(Eigen::MatrixXd & proj) {
	double minx, miny, maxx, maxy;
	minx = maxx = proj(0,0);
	miny = maxy = proj(0,1);
	for (unsigned int i=1; i < proj.rows(); ++i) {
		if (proj(i,0) < minx) minx = proj(i,0);
		else if (proj(i,0) > maxx) maxx = proj(i,0);
		if (proj(i,1) < miny) miny = proj(i,1);
		else if (proj(i,1) > maxy) maxy = proj(i,1);
	}
	maxx -= minx;
	maxy -= miny;
	if (maxy > maxx) maxx = maxy;
	for (unsigned int i=0; i < proj.rows(); ++i) {
		proj(i,0) = (proj(i,0) - minx) / maxx;
		proj(i,1) = (proj(i,1) - miny) / maxx;
	}
}

/**
 * @brief save controller and keyframes for playback
 * @param kframes keyframe vector
 * @param markers rowwise matrix of controllers
 * @param fname filename for saving data
 */
void saveMarkersForSKF(Eigen::VectorXi & kframes, Eigen::MatrixXd & markers, std::string fname) {
	Eigen::MatrixXd ret = markers.block(0,0,markers.rows(),3);
	for (unsigned int i=0; i < kframes.size(); ++i)
		ret(i,2) = kframes(i);
	std::ofstream savedfile;
	savedfile.open(fname.c_str());
	savedfile << ret << std::endl;
	savedfile.close();
}

/**
 * @brief save projection matrix for each frame
 * @param projmat projection matrix to be saved
 * @param fname filename for saving data
 */
void saveProjection(Eigen::MatrixXd & projmat, std::string fname) {
	std::ofstream savedfile;

	savedfile.open(fname.c_str());
	savedfile << projmat << std::endl;
	savedfile.close();
}

/**
 * @brief print error and time stats for 2D RBF interpolation and Force approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfForceRun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: Force
	std::cout << "First mocap handler" << std::endl;
	PBEucForceRbfSelector posforce(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd allforceproj = posforce.getMerged2DProjection(sibling);
	rescaleMarkers(allforceproj);
	Eigen::MatrixXd forceproj = allforceproj.block(0, 0, mocap->getNumberOfFrames(), allforceproj.cols());
	Eigen::MatrixXd forceproj2 = allforceproj.block(forceproj.rows(), 0, allforceproj.rows() - forceproj.rows(), allforceproj.cols());
	//distmat = posforce.getFrameDistanceMatrix();
	posforce.setFramesProjection(forceproj);
	std::cout << "Position-based selector with 2D SKF interpolation and force spread approach" << std::endl;
	posforce.printStats();
	saveProjection(forceproj, "force-first.txt");
	saveProjection(forceproj2, "force-second.txt");
	//Eigen::VectorXi kframes = posforce.getKeyframes(posforce.getNumberOfKeyframes());
	kframes = posforce.getKeyframeVector();
	markers = posforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBforce-first.txt");
	SCSEucForceRbfSelector scsforce(mocap, 0.03f); // 5 % of frames
	//scsforce.setFrameDistanceMatrix(distmat);
	scsforce.setFramesProjection(forceproj);
	std::cout << "SCS selector with 2D SKF interpolation and force spread approach" << std::endl;
	scsforce.printStats();
	kframes = scsforce.getKeyframeVector();
	markers = scsforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSforce-first.txt");
	RolsEucForceRbfSelector rolsforce(mocap, 0.03f); // 5 % of frames
	//rolsforce.setFrameDistanceMatrix(distmat);
	rolsforce.setFramesProjection(forceproj);
	std::cout << "ROLS selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsforce.printStats();
	kframes = rolsforce.getKeyframeVector();
	markers = rolsforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSforce-first.txt");
	RolsRbfpEucForceRbfSelector rolsrbfpforce(mocap, 0.03f); // 5 % of frames
	//rolsrbfpforce.setFrameDistanceMatrix(distmat);
	rolsrbfpforce.setFramesProjection(forceproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsrbfpforce.printStats();
	kframes = rolsrbfpforce.getKeyframeVector();
	markers = rolsrbfpforce.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPforce-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucForceRbfSelector posforce2(sibling, 0.03f); // 5 % of frames
	//posforce2.setFrameDistanceMatrix(distmat);
	posforce2.setFramesProjection(forceproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and force spread approach" << std::endl;
	posforce2.printStats();
	kframes = posforce2.getKeyframeVector();
	markers = posforce2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBforce-second.txt");
	SCSEucForceRbfSelector scsforce2(sibling, 0.03f); // 5 % of frames
	scsforce2.setFramesProjection(forceproj2);
	//scsforce2.setFrameDistanceMatrix(distmat);
	std::cout << "SCS selector with 2D SKF interpolation and force spread approach" << std::endl;
	scsforce2.printStats();
	kframes = scsforce2.getKeyframeVector();
	markers = scsforce2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSforce-second.txt");
	RolsEucForceRbfSelector rolsforce2(sibling, 0.03f); // 5 % of frames
	//rolsforce2.setFrameDistanceMatrix(distmat);
	rolsforce2.setFramesProjection(forceproj2);
	std::cout << "ROLS selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsforce2.printStats();
	kframes = rolsforce2.getKeyframeVector();
	markers = rolsforce2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSforce-second.txt");
	RolsRbfpEucForceRbfSelector rolsrbfpforce2(sibling, 0.03f); // 5 % of frames
	//rolsrbfpforce2.setFrameDistanceMatrix(distmat);
	rolsrbfpforce2.setFramesProjection(forceproj2);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and force spread approach" << std::endl;
	rolsrbfpforce2.printStats();
	kframes = rolsrbfpforce2.getKeyframeVector();
	markers = rolsrbfpforce2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPforce-second.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and MDS approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfMDSRun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: MDS
	std::cout << "First mocap handler" << std::endl;
	PBEucMdsRbfSelector posmds(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd allmdsproj = posmds.getMerged2DProjection(sibling);
	rescaleMarkers(allmdsproj);
	Eigen::MatrixXd mdsproj = allmdsproj.block(0, 0, mocap->getNumberOfFrames(), allmdsproj.cols());
	Eigen::MatrixXd mdsproj2 = allmdsproj.block(mdsproj.rows(), 0, allmdsproj.rows() - mdsproj.rows(), allmdsproj.cols());
	std::cout << "Position-based selector with 2D SKF interpolation and MDS approach" << std::endl;
	//posmds.setFrameDistanceMatrix(distmat);
	posmds.setFramesProjection(mdsproj);
	posmds.printStats();
	saveProjection(mdsproj, "mds-first.txt");
	saveProjection(mdsproj2, "mds-second.txt");
	kframes = posmds.getKeyframeVector();
	markers = posmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBmds-first.txt");
	SCSEucMdsRbfSelector scsmds(mocap, 0.03f); // 5 % of frames
	//scsmds.setFrameDistanceMatrix(distmat);
	scsmds.setFramesProjection(mdsproj);
	std::cout << "SCS selector with 2D SKF interpolation and MDS approach" << std::endl;
	scsmds.printStats();
	kframes = scsmds.getKeyframeVector();
	markers = scsmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSmds-first.txt");
	RolsEucMdsRbfSelector rolsmds(mocap, 0.03f); // 5 % of frames
	//rolsmds.setFrameDistanceMatrix(distmat);
	rolsmds.setFramesProjection(mdsproj);
	std::cout << "ROLS selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsmds.printStats();
	kframes = rolsmds.getKeyframeVector();
	markers = rolsmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSmds-first.txt");
	RolsRbfpEucMdsRbfSelector rolsrbfpmds(mocap, 0.03f); // 5 % of frames
	//rolsrbfpmds.setFrameDistanceMatrix(distmat);
	rolsrbfpmds.setFramesProjection(mdsproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and MDS approach" << std::endl;
	rolsrbfpmds.printStats();
	kframes = rolsrbfpmds.getKeyframeVector();
	markers = rolsrbfpmds.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPmds-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucMdsRbfSelector posmds2(sibling, 0.03f); // 5 % of frames
	//posmds2.setFrameDistanceMatrix(distmat);
	posmds2.setFramesProjection(mdsproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and MDS spread approach" << std::endl;
	posmds2.printStats();
	kframes = posmds2.getKeyframeVector();
	markers = posmds2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBmds-second.txt");
	SCSEucMdsRbfSelector scsmds2(sibling, 0.03f); // 5 % of frames
	//scsmds2.setFrameDistanceMatrix(distmat);
	scsmds2.setFramesProjection(mdsproj2);
	std::cout << "SCS selector with 2D SKF interpolation and MDS spread approach" << std::endl;
	scsmds2.printStats();
	kframes = scsmds2.getKeyframeVector();
	markers = scsmds2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSmds-second.txt");
	RolsEucMdsRbfSelector rolsmds2(sibling, 0.03f); // 5 % of frames
	//rolsmds2.setFrameDistanceMatrix(distmat);
	rolsmds2.setFramesProjection(mdsproj2);
	std::cout << "ROLS selector with 2D SKF interpolation and MDS spread approach" << std::endl;
	rolsmds2.printStats();
	kframes = rolsmds2.getKeyframeVector();
	markers = rolsmds2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSmds-second.txt");
	RolsRbfpEucMdsRbfSelector rolsrbfpmds2(sibling, 0.03f); // 5 % of frames
	//rolsrbfpmds2.setFrameDistanceMatrix(distmat);
	rolsrbfpmds2.setFramesProjection(mdsproj2);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and MDS spread approach" << std::endl;
	rolsrbfpmds2.printStats();
	kframes = rolsrbfpmds2.getKeyframeVector();
	markers = rolsrbfpmds2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPmds-second.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and PCA approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfPCARun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: PCA
	std::cout << "First mocap handler" << std::endl;
	PBEucPcaRbfSelector pospca(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd allpcaproj = pospca.getMerged2DProjection(sibling);
	rescaleMarkers(allpcaproj);
	Eigen::MatrixXd pcaproj = allpcaproj.block(0, 0, mocap->getNumberOfFrames(), allpcaproj.cols());
	Eigen::MatrixXd pcaproj2 = allpcaproj.block(pcaproj.rows(), 0, allpcaproj.rows() - pcaproj.rows(), allpcaproj.cols());
	pospca.setFramesProjection(pcaproj);
	std::cout << "Position-based selector with 2D SKF interpolation and PCA approach" << std::endl;
	//pospca.setFrameDistanceMatrix(distmat);
	pospca.printStats();
	saveProjection(pcaproj, "pca-first.txt");
	saveProjection(pcaproj2, "pca-second.txt");
	//saveProjection(allpcaproj, "pca-all.txt");
	kframes = pospca.getKeyframeVector();
	markers = pospca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBpca-first.txt");
	SCSEucPcaRbfSelector scspca(mocap, 0.03f); // 5 % of frames
	//scspca.setFrameDistanceMatrix(distmat);
	scspca.setFramesProjection(pcaproj);
	std::cout << "SCS selector with 2D SKF interpolation and PCA approach" << std::endl;
	scspca.printStats();
	kframes = scspca.getKeyframeVector();
	markers = scspca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSpca-first.txt");
	RolsEucPcaRbfSelector rolspca(mocap, 0.03f); // 5 % of frames
	//rolspca.setFrameDistanceMatrix(distmat);
	rolspca.setFramesProjection(pcaproj);
	std::cout << "ROLS selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolspca.printStats();
	kframes = rolspca.getKeyframeVector();
	markers = rolspca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSpca-first.txt");
	RolsRbfpEucPcaRbfSelector rolsrbfppca(mocap, 0.03f); // 5 % of frames
	//rolsrbfppca.setFrameDistanceMatrix(distmat);
	rolsrbfppca.setFramesProjection(pcaproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and PCA approach" << std::endl;
	rolsrbfppca.printStats();
	kframes = rolsrbfppca.getKeyframeVector();
	markers = rolsrbfppca.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPpca-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucPcaRbfSelector pospca2(sibling, 0.03f); // 5 % of frames
	//pospca2.setFrameDistanceMatrix(distmat);
	pospca2.setFramesProjection(pcaproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and PCA spread approach" << std::endl;
	pospca2.printStats();
	kframes = pospca2.getKeyframeVector();
	markers = pospca2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBpca-second.txt");
	SCSEucPcaRbfSelector scspca2(sibling, 0.03f); // 5 % of frames
	//scspca2.setFrameDistanceMatrix(distmat);
	scspca2.setFramesProjection(pcaproj2);
	std::cout << "SCS selector with 2D SKF interpolation and PCA spread approach" << std::endl;
	scspca2.printStats();
	kframes = scspca2.getKeyframeVector();
	markers = scspca2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSpca-second.txt");
	RolsEucPcaRbfSelector rolspca2(sibling, 0.03f); // 5 % of frames
	//rolspca2.setFrameDistanceMatrix(distmat);
	rolspca2.setFramesProjection(pcaproj2);
	std::cout << "ROLS selector with 2D SKF interpolation and PCA spread approach" << std::endl;
	rolspca2.printStats();
	kframes = rolspca2.getKeyframeVector();
	markers = rolspca2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSpca-second.txt");
	RolsRbfpEucPcaRbfSelector rolsrbfppca2(sibling, 0.03f); // 5 % of frames
	//rolsrbfppca2.setFrameDistanceMatrix(distmat);
	rolsrbfppca2.setFramesProjection(pcaproj2);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and PCA spread approach" << std::endl;
	rolsrbfppca2.printStats();
	kframes = rolsrbfppca2.getKeyframeVector();
	markers = rolsrbfppca2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPpca-second.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and LLE approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfLLERun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: LLE
	std::cout << "First mocap handler" << std::endl;
	PBEucLleRbfSelector poslle(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd alllleproj = poslle.getMerged2DProjection(sibling);
	rescaleMarkers(alllleproj);
	Eigen::MatrixXd lleproj = alllleproj.block(0, 0, mocap->getNumberOfFrames(), alllleproj.cols());
	Eigen::MatrixXd lleproj2 = alllleproj.block(lleproj.rows(), 0, alllleproj.rows() - lleproj.rows(), alllleproj.cols());
	//poslle.setFrameDistanceMatrix(distmat);
	poslle.setFramesProjection(lleproj);
	std::cout << "Position-based selector with 2D SKF interpolation and LLE approach" << std::endl;
	poslle.printStats();
	saveProjection(lleproj, "lle-first.txt");
	saveProjection(lleproj2, "lle-second.txt");
	kframes = poslle.getKeyframeVector();
	markers = poslle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBlle-first.txt");
	SCSEucLleRbfSelector scslle(mocap, 0.03f); // 5 % of frames
	//scslle.setFrameDistanceMatrix(distmat);
	scslle.setFramesProjection(lleproj);
	std::cout << "SCS selector with 2D SKF interpolation and LLE approach" << std::endl;
	scslle.printStats();
	kframes = scslle.getKeyframeVector();
	markers = scslle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSlle-first.txt");
	RolsEucLleRbfSelector rolslle(mocap, 0.03f); // 5 % of frames
	//rolslle.setFrameDistanceMatrix(distmat);
	rolslle.setFramesProjection(lleproj);
	std::cout << "ROLS selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolslle.printStats();
	kframes = rolslle.getKeyframeVector();
	markers = rolslle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSlle-first.txt");
	RolsRbfpEucLleRbfSelector rolsrbfplle(mocap, 0.03f); // 5 % of frames
	//rolsrbfplle.setFrameDistanceMatrix(distmat);
	rolsrbfplle.setFramesProjection(lleproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and LLE approach" << std::endl;
	rolsrbfplle.printStats();
	kframes = rolsrbfplle.getKeyframeVector();
	markers = rolsrbfplle.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPlle-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucLleRbfSelector poslle2(sibling, 0.03f); // 5 % of frames
	//poslle2.setFrameDistanceMatrix(distmat);
	poslle2.setFramesProjection(lleproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and LLE spread approach" << std::endl;
	poslle2.printStats();
	kframes = poslle2.getKeyframeVector();
	markers = poslle2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBlle-second.txt");
	SCSEucLleRbfSelector scslle2(sibling, 0.03f); // 5 % of frames
	//scslle2.setFrameDistanceMatrix(distmat);
	scslle2.setFramesProjection(lleproj2);
	std::cout << "SCS selector with 2D SKF interpolation and LLE spread approach" << std::endl;
	scslle2.printStats();
	kframes = scslle2.getKeyframeVector();
	markers = scslle2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSlle-second.txt");
	RolsEucLleRbfSelector rolslle2(sibling, 0.03f); // 5 % of frames
	//rolslle2.setFrameDistanceMatrix(distmat);
	rolslle2.setFramesProjection(lleproj2);
	std::cout << "ROLS selector with 2D SKF interpolation and LLE spread approach" << std::endl;
	rolslle2.printStats();
	kframes = rolslle2.getKeyframeVector();
	markers = rolslle2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSlle-second.txt");
	RolsRbfpEucLleRbfSelector rolsrbfplle2(sibling, 0.03f); // 5 % of frames
	//rolsrbfplle2.setFrameDistanceMatrix(distmat);
	rolsrbfplle2.setFramesProjection(lleproj2);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and LLE spread approach" << std::endl;
	rolsrbfplle2.printStats();
	kframes = rolsrbfplle2.getKeyframeVector();
	markers = rolsrbfplle2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPlle-second.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and t-SNE approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfTsneRun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: t-SNE
	std::cout << "First mocap handler" << std::endl;
	PBEucTsneRbfSelector postsne(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd alltsneproj = postsne.getMerged2DProjection(sibling);
	rescaleMarkers(alltsneproj);
	Eigen::MatrixXd tsneproj = alltsneproj.block(0, 0, mocap->getNumberOfFrames(), alltsneproj.cols());
	Eigen::MatrixXd tsneproj2 = alltsneproj.block(tsneproj.rows(), 0, alltsneproj.rows() - tsneproj.rows(), alltsneproj.cols());
	//postsne.setFrameDistanceMatrix(distmat);
	postsne.setFramesProjection(tsneproj);
	std::cout << "Position-based selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	postsne.printStats();
	saveProjection(tsneproj, "tsne-first.txt");
	saveProjection(tsneproj2, "tsne-second.txt");
	kframes = postsne.getKeyframeVector();
	markers = postsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBtsne-first.txt");
	SCSEucTsneRbfSelector scstsne(mocap, 0.03f); // 5 % of frames
	//scstsne.setFrameDistanceMatrix(distmat);
	scstsne.setFramesProjection(tsneproj);
	std::cout << "SCS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	scstsne.printStats();
	kframes = scstsne.getKeyframeVector();
	markers = scstsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCStsne-first.txt");
	RolsEucTsneRbfSelector rolstsne(mocap, 0.03f); // 5 % of frames
	//rolstsne.setFrameDistanceMatrix(distmat);
	rolstsne.setFramesProjection(tsneproj);
	std::cout << "ROLS selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolstsne.printStats();
	kframes = rolstsne.getKeyframeVector();
	markers = rolstsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLStsne-first.txt");
	RolsRbfpEucTsneRbfSelector rolsrbfptsne(mocap, 0.03f); // 5 % of frames
	//rolsrbfptsne.setFrameDistanceMatrix(distmat);
	rolsrbfptsne.setFramesProjection(tsneproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and t-SNE approach" << std::endl;
	rolsrbfptsne.printStats();
	kframes = rolsrbfptsne.getKeyframeVector();
	markers = rolsrbfptsne.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPtsne-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucTsneRbfSelector postsne2(sibling, 0.03f); // 5 % of frames
	//postsne2.setFrameDistanceMatrix(distmat);
	postsne2.setFramesProjection(tsneproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and t-SNE spread approach" << std::endl;
	postsne2.printStats();
	kframes = postsne2.getKeyframeVector();
	markers = postsne2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBtsne-second.txt");
	SCSEucTsneRbfSelector scstsne2(sibling, 0.03f); // 5 % of frames
	//scstsne2.setFrameDistanceMatrix(distmat);
	scstsne2.setFramesProjection(tsneproj2);
	std::cout << "SCS selector with 2D SKF interpolation and t-SNE spread approach" << std::endl;
	scstsne2.printStats();
	kframes = scstsne2.getKeyframeVector();
	markers = scstsne2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCStsne-second.txt");
	RolsEucTsneRbfSelector rolstsne2(sibling, 0.03f); // 5 % of frames
	//rolstsne2.setFrameDistanceMatrix(distmat);
	rolstsne2.setFramesProjection(tsneproj2);
	std::cout << "ROLS selector with 2D SKF interpolation and t-SNE spread approach" << std::endl;
	rolstsne2.printStats();
	kframes = rolstsne2.getKeyframeVector();
	markers = rolstsne2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLStsne-second.txt");
	RolsRbfpEucTsneRbfSelector rolsrbfptsne2(sibling, 0.03f); // 5 % of frames
	//rolsrbfptsne2.setFrameDistanceMatrix(distmat);
	rolsrbfptsne2.setFramesProjection(tsneproj2);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and t-SNE spread approach" << std::endl;
	rolsrbfptsne2.printStats();
	kframes = rolsrbfptsne2.getKeyframeVector();
	markers = rolsrbfptsne2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPtsne-second.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and isomap approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfIsomapRun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: isomap
	std::cout << "First mocap handler" << std::endl;
	PBEucIsomapRbfSelector posisomap(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd allisomapproj = posisomap.getMerged2DProjection(sibling);
	rescaleMarkers(allisomapproj);
	Eigen::MatrixXd isomapproj = allisomapproj.block(0, 0, mocap->getNumberOfFrames(), allisomapproj.cols());
	Eigen::MatrixXd isomapproj2 = allisomapproj.block(isomapproj.rows(), 0, allisomapproj.rows() - isomapproj.rows(), allisomapproj.cols());
	//posisomap.setFrameDistanceMatrix(distmat);
	posisomap.setFramesProjection(isomapproj);
	std::cout << "Position-based selector with 2D SKF interpolation and Isomap approach" << std::endl;
	posisomap.printStats();
	saveProjection(isomapproj, "isomap-first.txt");
	saveProjection(isomapproj2, "isomap-second.txt");
	kframes = posisomap.getKeyframeVector();
	markers = posisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBisomap-first.txt");
	SCSEucIsomapRbfSelector scsisomap(mocap, 0.03f); // 5 % of frames
	//scsisomap.setFrameDistanceMatrix(distmat);
	scsisomap.setFramesProjection(isomapproj);
	std::cout << "SCS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	scsisomap.printStats();
	kframes = scsisomap.getKeyframeVector();
	markers = scsisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSisomap-first.txt");
	RolsEucIsomapRbfSelector rolsisomap(mocap, 0.03f); // 5 % of frames
	//rolsisomap.setFrameDistanceMatrix(distmat);
	rolsisomap.setFramesProjection(isomapproj);
	std::cout << "ROLS selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsisomap.printStats();
	kframes = rolsisomap.getKeyframeVector();
	markers = rolsisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSisomap-first.txt");
	RolsRbfpEucIsomapRbfSelector rolsrbfpisomap(mocap, 0.03f); // 5 % of frames
	//rolsrbfpisomap.setFrameDistanceMatrix(distmat);
	rolsrbfpisomap.setFramesProjection(isomapproj);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and Isomap approach" << std::endl;
	rolsrbfpisomap.printStats();
	kframes = rolsrbfpisomap.getKeyframeVector();
	markers = rolsrbfpisomap.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPisomap-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucIsomapRbfSelector posisomap2(sibling, 0.03f); // 5 % of frames
	//posisomap2.setFrameDistanceMatrix(distmat);
	posisomap2.setFramesProjection(isomapproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and Isomap spread approach" << std::endl;
	posisomap2.printStats();
	kframes = posisomap2.getKeyframeVector();
	markers = posisomap2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBisomap-second.txt");
	SCSEucIsomapRbfSelector scsisomap2(sibling, 0.03f); // 5 % of frames
	//scsisomap2.setFrameDistanceMatrix(distmat);
	scsisomap2.setFramesProjection(isomapproj2);
	std::cout << "SCS selector with 2D SKF interpolation and Isomap spread approach" << std::endl;
	scsisomap2.printStats();
	kframes = scsisomap2.getKeyframeVector();
	markers = scsisomap2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSisomap-second.txt");
	RolsEucIsomapRbfSelector rolsisomap2(sibling, 0.03f); // 5 % of frames
	//rolsisomap2.setFrameDistanceMatrix(distmat);
	rolsisomap2.setFramesProjection(isomapproj2);
	std::cout << "ROLS selector with 2D SKF interpolation and Isomap spread approach" << std::endl;
	rolsisomap2.printStats();
	kframes = rolsisomap2.getKeyframeVector();
	markers = rolsisomap2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSisomap-second.txt");
	RolsRbfpEucIsomapRbfSelector rolsrbfpisomap2(sibling, 0.03f); // 5 % of frames
	//rolsrbfpisomap2.setFrameDistanceMatrix(distmat);
	rolsrbfpisomap2.setFramesProjection(isomapproj2);
	std::cout << "ROLS/RBFP selector with 2D SKF interpolation and Isomap spread approach" << std::endl;
	rolsrbfpisomap2.printStats();
	kframes = rolsrbfpisomap2.getKeyframeVector();
	markers = rolsrbfpisomap2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"ROLSRBFPisomap-second.txt");
}

/**
 * @brief print error and time stats for 2D RBF interpolation and LAMP approach
 * with implemented key frame selection algorithms
 * @param mocap mocap session reader
 */
void rbfLAMPRun(MocapReader * mocap, MocapReader * sibling) {
	// spatial keyframe interpolator 2D: LAMP
	std::cout << "First mocap handler" << std::endl;
	PBEucLampRbfSelector poslamp(mocap, 0.03f); // 5 % of frames
	Eigen::MatrixXd alllampproj = poslamp.getMerged2DProjection(sibling);
	rescaleMarkers(alllampproj);
	Eigen::MatrixXd lampproj = alllampproj.block(0, 0, mocap->getNumberOfFrames(), alllampproj.cols());
	Eigen::MatrixXd lampproj2 = alllampproj.block(lampproj.rows(), 0, alllampproj.rows() - lampproj.rows(), alllampproj.cols());
	poslamp.setFramesProjection(lampproj);
	//poslamp.setFrameDistanceMatrix(distmat);
	std::cout << "Position-based selector with 2D SKF interpolation and LAMP approach" << std::endl;
	poslamp.printStats();
	saveProjection(lampproj, "lamp-first.txt");
	saveProjection(lampproj2, "lamp-second.txt");
	kframes = poslamp.getKeyframeVector();
	markers = poslamp.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBlamp-first.txt");
	SCSEucLampRbfSelector scslamp(mocap, 0.03f); // 5 % of frames
	//scslamp.setFrameDistanceMatrix(distmat);
	scslamp.setFramesProjection(lampproj);
	std::cout << "SCS selector with 2D SKF interpolation and LAMP approach" << std::endl;
	scslamp.printStats();
	kframes = scslamp.getKeyframeVector();
	markers = scslamp.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSlamp-first.txt");
	//
	std::cout << "Second mocap handler" << std::endl;
	PBEucLampRbfSelector poslamp2(sibling, 0.03f); // 5 % of frames
	//poslamp2.setFrameDistanceMatrix(distmat);
	poslamp2.setFramesProjection(lampproj2);
	std::cout << "Position-based selector with 2D SKF interpolation and LAMP spread approach" << std::endl;
	poslamp2.printStats();
	kframes = poslamp2.getKeyframeVector();
	markers = poslamp2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"PBlamp-second.txt");
	SCSEucLampRbfSelector scslamp2(sibling, 0.03f); // 5 % of frames
	//scslamp2.setFrameDistanceMatrix(distmat);
	scslamp2.setFramesProjection(lampproj2);
	std::cout << "SCS selector with 2D SKF interpolation and LAMP spread approach" << std::endl;
	scslamp2.printStats();
	kframes = scslamp2.getKeyframeVector();
	markers = scslamp2.getMarkers(kframes);
	saveMarkersForSKF(kframes,markers,"SCSlamp-second.txt");
}

/**
 * @brief load mocap file(s) in its reader handler
 * @param fn number of parameters already read
 * @param argc number of command line parameters
 * @param argv char (string) array of command line parameters
 * @param mc mocap reader handler reference
 * @return number of files read
 */
int loadFile(int fn, int argc, char ** argv , MocapReader ** mc) {
	int ret = 1;
	if (argc <= (fn + ret)) {
		std::cout << "expected a mocap file to read." << std::endl;
		exit(1);
	}
	
	std::string fname(argv[fn + ret]);
	std::string bname(basename(argv[fn + ret]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	
	if (ext.compare("bvh") == 0) {
		*mc = new BVHReader();
		(*mc)->readFile(fname);
	} else if (ext.compare("asf") == 0) {
		*mc = new ASFReader();
		(*mc)->readFile(fname);
		++ret;
		if (argc > (fn + ret)) {
			fname = argv[fn + ret];
			bname = basename(argv[fn + ret]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				(*mc)->readFile(fname);
			} else {
				std::cout << "no amc file given." << std::endl;
				exit(1);
			}
		} else {
			std::cout << "expected one amc file." << std::endl;
			exit(1);
		}
	} else {
		std::cout << "unrecognized format." << std::endl;
		exit(1);
	}
	return ret;
}

int main(int argc, char ** argv) {
	MocapReader *mocap, *sibling;

	if (argc < 3) {
		std::cout << "syntax: " << argv[0] << " [mocap file 1] [mocap file 2]" << std::endl;
		exit(1);
	}
	int fn = loadFile(0, argc, argv, &mocap);
	fn = loadFile(fn, argc, argv, &sibling);
	rbfForceRun(mocap, sibling);
	rbfMDSRun(mocap, sibling);
	rbfPCARun(mocap, sibling);
	rbfLLERun(mocap, sibling);
	rbfLAMPRun(mocap, sibling);
	rbfTsneRun(mocap, sibling);
	rbfIsomapRun(mocap, sibling);
	delete mocap;
	delete sibling;
	return 0;
}
