#include <iostream>
#include <pca.h>
#include <lle.h>
#include <string>
#include <asfreader.h>
#include <bvhreader.h>

void printPCAKeyframes(Eigen::MatrixXd & input, unsigned int lfid) {
	MMS mms(input, 7);
	mms.doAnalysis();
	mms.debug();
	Eigen::VectorXi fids = mms.getKeyframesByMMS();
	std::cout << "pca keyframes" << std::endl;
	std::cout << "0";
	for (unsigned int i=0; i < fids.size(); ++i)
		std::cout << " " << fids(i);
	std::cout << " " << lfid << std::endl;	
}

void printLLEKeyframes(Eigen::MatrixXd & input, unsigned int lfid) {
	LLE lle(input);
	Eigen::VectorXi fids = lle.getInitialKeyframesSet();
	std::cout << "lle initial keyframes" << std::endl;
	std::cout << "0";
	for (unsigned int i=0; i < fids.size(); ++i)
		std::cout << " " << fids(i);
	std::cout << " " << lfid << std::endl;
	Eigen::VectorXi pfids = lle.getIKFSelection(fids, 10);
	std::cout << "lle keyframes" << std::endl;
	std::cout << "0";
	for (unsigned int i=0; i < pfids.size(); ++i)
		std::cout << " " << pfids(i);
	std::cout << " " << lfid << std::endl;	
}

int main(int argc, char ** argv) {
	Eigen::MatrixXd input = Eigen::MatrixXd::Zero(5,3);
	//input.col(0) << 7,4,6,8,8,7,5,9,7,8;
	//input.col(1) <<       4,1,3,6,5,2,3,5,4,2;
	//input.col(2) <<       3,8,5,1,7,9,3,8,5,2;

	//input << 2.5,2.4,0.5,0.7,2.2,2.9,1.9,2.2,3.1,3.0,2.3,2.7,2,1.6,1,1.1,1.5,1.6,1.1,0.9;

	//input.col(0) << 4.0,4.2,3.9,4.3,4.1;
	//input.col(1) << 2.0,2.1,2.0,2.1,2.2;
	//input.col(2) << 0.60,0.59,0.58,0.62,0.63;

	//input << 0,0,0, 1,0,0, 1,1,0, 1,0,1;

	//std::cout << "input matrix" << std::endl;
	//std::cout << input << std::endl;
	
	if (argc < 2) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	std::string fname(argv[1]);
	std::string bname(basename(argv[1]));
	std::string ext = bname.substr(bname.length() - 3, 3);
	MocapReader *mocap = NULL;
	if (ext.compare("bvh") == 0) {
		mocap = new BVHReader();
		mocap->readFile(fname);
	} else if (ext.compare("asf") == 0) {
		mocap = new ASFReader();
		mocap->readFile(fname);
		if (argc > 2) {
			fname = argv[2];
			bname = basename(argv[2]);
			ext = bname.substr(bname.length() - 3, 3);
			if (ext.compare("amc") == 0) {
				mocap->readFile(fname);
			}
		}
	}
	input = mocap->getEulerAnglesDOFMatrix();
	printPCAKeyframes(input, mocap->getNumberOfFrames() - 1);
	printLLEKeyframes(input, mocap->getNumberOfFrames() - 1);
	return 0;
}
