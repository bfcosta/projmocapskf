#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <kfselector.h>
#include <skanalizer.h>
#include <asfreader.h>
#include <bvhreader.h>
#include <multireader.h>

/**
 * @brief save controller and keyframes for playback
 * @param kframes keyframe vector
 * @param markers rowwise matrix of controllers
 * @param fname filename for saving data
 */
void saveMarkersForSKF(Eigen::VectorXi & kframes, Eigen::MatrixXd & markers, std::string fname) {
	Eigen::MatrixXd ret = markers.block(0,0,markers.rows(),3);
	for (unsigned int i=0; i < kframes.size(); ++i)
		ret(i,2) = kframes(i);
	std::ofstream savedfile;
	savedfile.open(fname.c_str());
	savedfile << ret << std::endl;
	savedfile.close();
}

/**
 * @brief save projection matrix for each frame
 * @param projmat projection matrix to be saved
 * @param fname filename for saving data
 */
void saveProjection(Eigen::MatrixXd & projmat, std::string fname) {
	std::ofstream savedfile;

	savedfile.open(fname.c_str());
	savedfile << projmat << std::endl;
	savedfile.close();
}

/**
 * @brief return the implemented radial basis function kernel as quadratic multiquadrics
 * with c and epsilon equals one.
 * @param d2 distance as input
 * @return function output
 */
double phi_proj(double d2) {
	//return std::sqrt(1 + d2*d2);  // multiquadrics - elisa
	return (1 + std::pow(d2, 2.5));
}

/**
 * @brief normalize projection to square [0, 1]
 * @param ret projection matrix to be normalized
 */
void normalizeProjection(Eigen::MatrixXd & ret) {
	double min0, max0;
	double min1, max1;
	// normalize projection coordinates
	min0 = max0 = ret(0,0);
	min1 = max1 = ret(0,1);
	for (unsigned int i=1; i < ret.rows(); ++i) {
		if (min0 > ret(i,0)) min0 = ret(i,0);
		else if (max0 < ret(i,0)) max0 = ret(i,0);
		if (min1 > ret(i,1)) min1 = ret(i,1);
		else if (max1 < ret(i,1)) max1 = ret(i,1);
	}
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,0) = (ret(i,0) - min0) / (max0 - min0);
		ret(i,1) = (ret(i,1) - min1) / (max1 - min1);
	}
}

/**
 * @brief
 * @param
 * @return
 */
MocapReader * openFile(char * argv) {
	std::string fname(argv);
	std::string bname(basename(argv));
	std::string ext = bname.substr(bname.length() - 3, 3);
	MocapReader * mocap = NULL;

	if (ext.compare("bvh") == 0) {
		mocap = new BVHReader();
		mocap->readFile(fname);
	}
	return mocap;
}

/**
 * @brief get RBFP matrix that outlines the corresponding projections
 * @param mocap mocap session handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @param pmat seed projection matrix
 * @param kfs control points vector
 * @return RBFP matrix
 */
Eigen::MatrixXd getRBFPMatrix(MocapReader * mocap, SkeletonAnalizer * skdist, 
	Eigen::MatrixXd & pmat, Eigen::VectorXi & kfs) {
	
	Eigen::MatrixXd proj = pmat;
	int frames = mocap->getNumberOfFrames();
	// find lambda
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Zero(kfs.size(), kfs.size());
	Eigen::MatrixXd ymat = Eigen::ArrayXXd::Zero(kfs.size(), 2);
	Skeleton sk0, sk1;
	double dist;
	
	for (int i=0; i < fmat.rows(); ++i)
		for (int j=i+1; j < fmat.cols(); ++j) {
			sk0 = mocap->getFrameSkeleton(kfs(i));
			sk1 = mocap->getFrameSkeleton(kfs(j));
			dist = std::sqrt(skdist->getPoseDistance(sk0, sk1));
			fmat(j,i) = fmat(i,j) = phi_proj(dist);
		}
	for (int i=0; i < ymat.rows(); ++i) {
		ymat(i,0) = proj(kfs(i),0);
		ymat(i,1) = proj(kfs(i),1);
	}
	Eigen::MatrixXd lmat = fmat.inverse() * ymat;
	return lmat;	
}

/**
 * @brief get RBF projection for a unit mocap handler
 * @param mocap unit mocap session handler
 * @param idx index of unit mocap handler
 * @param skdist pose distance definition a SkeletonAnalizer object
 * @param lmat RBFP matrix
 * @param pmat seed projection matrix
 * @param kfs control points vector
 * @return projection matrix
 */
Eigen::MatrixXd getRBFP4UnitMocap(MultiReader * mocap, unsigned int idx,
	SkeletonAnalizer * skdist, Eigen::MatrixXd & lmat, 
	Eigen::MatrixXd & pmat, Eigen::VectorXi & lkfs, Eigen::VectorXi & kfs) {
	
	Skeleton sk0, sk1;
	double dist;
	MocapReader * rd = mocap->getReader(idx);
	int frames = rd->getNumberOfFrames();
	Eigen::VectorXi fsample = mocap->getFrameSampleByUnitReader(idx);
	unsigned int fbase = mocap->getFrameBaseByUnitReader(idx);
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(rd->getNumberOfFrames(), pmat.cols());
	
	// find global keyframes in local unit mocap range
	unsigned int lf = 0;
	for (unsigned int i=0; i < kfs.size(); ++i) {
		if ((kfs(i) >= fbase) && (kfs(i) < (fbase + fsample.size()))) {
			++lf;
		}
	}
	lkfs = Eigen::ArrayXi::Zero(lf);
	lf = 0;
	for (unsigned int i=0; i < kfs.size(); ++i) {
		if ((kfs(i) >= fbase) && (kfs(i) < (fbase + fsample.size()))) {
			lkfs(lf) = fsample(kfs(i) - fbase);
			//std::cout << "multi kfs = " << kfs(i) << " local kfs = " << lkfs(lf) << std::endl;
			proj.row(lkfs(lf)) = pmat.row(kfs(i));
			++lf;			
		}
	}
	//std::cout << "local key frames found = " << lkfs.transpose() << std::endl;

	// project remainder frames by RBF
	Eigen::VectorXd rbfvec = Eigen::ArrayXd::Zero(kfs.size());
	int c = 0;
	for (int i=0; i < frames; ++i) {
		if (i == lkfs(c)) {
			if (c != (lkfs.size() - 1)) ++c;
		} else {
			for (int j=0; j < rbfvec.size(); ++j) {
				sk0 = rd->getFrameSkeleton(i);
				sk1 = mocap->getFrameSkeleton(kfs(j));
				dist = std::sqrt(skdist->getPoseDistance(sk0, sk1));
				rbfvec(j) = phi_proj(dist);
			}
			proj.row(i).head(2) = rbfvec.transpose() * lmat;
		}
	}
	//normalizeProjection(proj);
	return proj;
}


int main(int argc, char ** argv) {
	GeodesicSkeletonAnalizer skan;
	MocapReader * mocap;
	MultiReader * multi;
	std::ostringstream name;

	if (argc < 2) {
		std::cout << "filename needed." << std::endl;
		exit(1);
	}
	int nfiles = argc - 1;
	multi = new MultiReader(500);
	while (nfiles > 0) {
		mocap = openFile(argv[argc - nfiles]);
		if (mocap == NULL) {
			std::cout << "unrecognized format." << std::endl;
			exit(1);
		}
		multi->addMocap(mocap);
		--nfiles;
	}
	multi->reassign();
	std::cout << "multi reader built" << std::endl;
	RolsRbfpEucMdsRbfSelector rolsrbfpmds(multi, 0.03);
	rolsrbfpmds.printStats();
	Eigen::MatrixXd proj2 = rolsrbfpmds.getFramesProjection();
	Eigen::MatrixXd proj = proj2.block(0,0,proj2.rows(),3);
	Eigen::VectorXi kfs = rolsrbfpmds.getKeyframeVector();
	for (unsigned int i=0; i < kfs.size(); ++i)
		proj(kfs(i),2) = 1;
	saveProjection(proj, "mainproj.txt");
	std::cout << "multi reader projection saved" << std::endl;
	Eigen::MatrixXd lmat = getRBFPMatrix(multi, &skan, proj, kfs);
	Eigen::VectorXi lkfs;
	Eigen::MatrixXd lproj;
	nfiles = argc - 1;
	for (int id=0; id < nfiles; ++id) {
		lproj = getRBFP4UnitMocap(multi, id, &skan, lmat, proj, lkfs, kfs);
		proj2 = lproj.block(0,0,lproj.rows(),3);
		for (unsigned int i=0; i < lkfs.size(); ++i)
			proj2(lkfs(i),2) = 1;
		name << "proj" << id << ".txt";
		saveProjection(proj2, name.str());
		name.str("");
		std::cout << "unit reader projection " << id << " saved" << std::endl;
	}
	delete multi;
	return 0;
}
