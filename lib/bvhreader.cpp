#include <bvhreader.h>
#include <iostream>
#include <fstream>
#include <stack>
#include <cstdlib>
#include <sstream>

/**
 * creates a BVH reader
 */
BVHReader::BVHReader() {}

/**
 * \param fname filename to be read
 * 
 * method for interfacing file format reading
 */
void BVHReader::readFile(const std::string & fname) {
	readBvh(fname);
}

/**
 * \param fname filename to be read
 * 
 * read BVH file and builds a temporary data structure for skeleton and 
 * animation
 */
void BVHReader::readBvh(const std::string & fname) {
	enum Section { EMPTY, SKEL, MOVE };
	Section st = EMPTY;
	std::ifstream myfile;
	std::string buf,line;
	std::istringstream ss;
	std::vector<std::string> tokens;
	std::vector<float> tmp;
	std::stack<unsigned int> ref;

	mblen = 0;
	myfile.open(fname.c_str());
	if (myfile.is_open()) {
		while (std::getline(myfile, line)) {
			ss.str(line);
			while (ss >> buf) tokens.push_back(buf);
			if (tokens.at(0).compare("#") == 0) {
				// comment
			} else if (tokens.at(0).compare("HIERARCHY") == 0) {
				st = SKEL;
				
			} else if (tokens.at(0).compare("MOTION") == 0) {
				st = MOVE;
				
			} else {
				if (st == MOVE) {
					if (tokens.at(0).compare("Frames:") == 0) {
						nframe = atoi(tokens.at(1).c_str());
					
					} else if ((tokens.at(0).compare("Frame") == 0)&&
								(tokens.at(1).compare("Time:") == 0)) {
						tframe = atof(tokens.at(2).c_str());
						
					} else {
						tmp.clear();
						for (unsigned int i=0; i < tokens.size(); ++i)
							tmp.push_back(atof(tokens.at(i).c_str()));
						motion.push_back(tmp);
					}

				} else if (st == SKEL) {
					if (tokens.at(0).compare("ROOT") == 0) {
						parent.clear();
						name.clear();
						name.push_back(tokens.at(1));
						ref.push(name.size() - 1);
						
					} else if (tokens.at(0).compare("OFFSET") == 0) {
						struct triplef j;
						j.x = atof(tokens.at(1).c_str());
						j.y = atof(tokens.at(2).c_str());
						j.z = atof(tokens.at(3).c_str());
						joint.push_back(j);
						float t = std::sqrt(j.x*j.x + j.y*j.y + j.z*j.z);
						if (mblen < t) mblen = t;
						
					} else if (tokens.at(0).compare("CHANNELS") == 0) {
						struct mask m;
						unsigned int j;
						j = atoi(tokens.at(1).c_str());
						for (unsigned int i=0; i < j; ++i) {
							if (tokens.at(2+i).compare("Xposition") == 0) {
								m.dim[i % 3] = 'X';
								m.rot = false;
							} else if (tokens.at(2+i).compare("Yposition") == 0) {
								m.dim[i % 3] = 'Y';
								m.rot = false;
							} else if (tokens.at(2+i).compare("Zposition") == 0) {
								m.dim[i % 3] = 'Z';
								m.rot = false;
							} else if (tokens.at(2+i).compare("Xrotation") == 0) {
								m.dim[i % 3] = 'X';
								m.rot = true;
							} else if (tokens.at(2+i).compare("Yrotation") == 0) {
								m.dim[i % 3] = 'Y';
								m.rot = true;
							} else if (tokens.at(2+i).compare("Zrotation") == 0) {
								m.dim[i % 3] = 'Z';
								m.rot = true;
							}
							if ((i % 3) == 2) {
								m.ref = ref.top();
								channel.push_back(m);
							}
						}
						
					} else if (tokens.at(0).compare("JOINT") == 0) {
						name.push_back(tokens.at(1));
												
					} else if (tokens.at(0).compare("{") == 0) {
						parent.push_back(ref.top());
						ref.push(name.size() - 1);
						
					} else if (tokens.at(0).compare("}") == 0) {
						ref.pop();
						
					} else {
						//End Site
						name.push_back("leaf");
					}
				}
			}
			tokens.clear();
			ss.clear();
		} 
		myfile.close();
	} else 
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
}

/**
 * method for debugging file loading
 */
void BVHReader::print() {

	std::cout << "Total attribs" << std::endl;
	std::cout << "name = " << name.size() << std::endl;
	std::cout << "parent = " << parent.size() << std::endl;
	std::cout << "offset = " << joint.size() << std::endl;
	std::cout << "channel = " << channel.size() << std::endl;
	std::cout << "joints" << std::endl;
	for (unsigned int i=0,j=0; i < name.size(); ++i) {
		std::cout << "************" << std::endl;
		std::cout << "id = " << i << std::endl;
		std::cout << "name = " << name.at(i) << std::endl;
		std::cout << "parent = " << parent.at(i) << std::endl;
		std::cout << "offset = (" << joint.at(i).x << ", " << joint.at(i).y << 
			", " << joint.at(i).z << ")" << std::endl;
		if (name.at(i).compare("leaf") != 0) {
			if (j == 0) {
				std::cout << "channel = (" << channel.at(j).dim[0] << ", " << 
					channel.at(j).dim[1] << ", " << channel.at(j).dim[2] << ")" << 
					" rot = " << channel.at(i).rot << " related = " << 
					channel.at(j).ref << std::endl;
				++j;
			}
			std::cout << "channel = (" << channel.at(j).dim[0] << ", " << 
				channel.at(j).dim[1] << ", " << channel.at(j).dim[2] << ")" << 
				" rot = " << channel.at(j).rot << " related = " << 
				channel.at(j).ref << std::endl;
			++j;
		}
	}
	std::cout << "************" << std::endl;
	std::cout << "motion" << std::endl;
	std::cout << "number of frames = " << nframe << std::endl;
	std::cout << "frame interval = " << tframe << std::endl;
	for (unsigned int i=0; i < motion.size(); ++i) {
		for (unsigned int j=0; j < motion.at(i).size(); ++j)
			std::cout << motion.at(i).at(j) << " ";
		std::cout << std::endl;	
	}
}

/**
 * \return skeleton on its resting pose
 * 
 * returns the skeleton on its resting pose 
 */
Skeleton BVHReader::getSkeleton() {
	Skeleton skel(mblen / lcratio);
	
	skel.setRoot(joint.at(0).x, joint.at(0).y, joint.at(0).z, name.at(0));
	skel.setOrientation(0, 0, 0, "ZXY");
	for (unsigned int i=1; i < joint.size(); ++i)
		skel.addJoint(joint.at(i).x, joint.at(i).y, joint.at(i).z,
					0,0,0, "ZXY", parent.at(i), name.at(i));
	return skel;
}

/**
 * \param i frame id
 * \return skeleton pose at the specified frame
 * 
 * returns a skeleton pose for a given frame id
 */
Skeleton BVHReader::getFrameSkeleton(unsigned int i) {
	Skeleton skel(mblen / lcratio);
	double x, y, z, deg2rad = M_PI / 180;
	unsigned int ref;
	std::vector<float> & fr = motion.at(i);

	x = y = z = 0;
	for (unsigned int j=0; j < 3; ++j) {
	   if (channel.at(0).dim[j] == 'X') 
			x = fr.at(j);
	   else if (channel.at(0).dim[j] == 'Y')
			y = fr.at(j);
	   else if (channel.at(0).dim[j] == 'Z') 
			z = fr.at(j);
	}
	skel.setRoot(x, y, z, name.at(0));
	skel.setOrientation(0, 0, 0, channel.at(1).dim);
	for (unsigned int i=1; i < joint.size(); ++i) {
		// acha a matriz de rotacao
		ref = channel.size();
		for (unsigned int j=0; j < channel.size(); ++j) {
			if ((channel.at(j).rot) && (channel.at(j).ref == parent.at(i))) {
				ref = j;
				break;
			}
		}
		for (unsigned int j=0; j < 3; ++j) {
		   if (channel.at(ref).dim[j] == 'X') 
				x = fr.at(3*ref + j) * deg2rad;
		   else if (channel.at(ref).dim[j] == 'Y')
				y = fr.at(3*ref + j) * deg2rad;
		   else if (channel.at(ref).dim[j] == 'Z') 
				z = fr.at(3*ref + j) * deg2rad;
		}
		skel.addJoint(joint.at(i).x, joint.at(i).y, joint.at(i).z,
			x, y, z, channel.at(ref).dim, parent.at(i), name.at(i));
	}
	return skel;
}

/**
 * \param dofs
 * \param euler 
 * \return skeleton pose at the specified frame
 * 
 * returns a skeleton pose for a given configuration
 */
Skeleton BVHReader::getFrameSkeleton(Eigen::VectorXd dofs, bool euler) {
	Skeleton skel(mblen / lcratio);
	double x, y, z, deg2rad = M_PI / 180;
	unsigned int ref;
	
	x = y = z = 0;
	for (unsigned int j=0; j < 3; ++j) {
	   if (channel.at(0).dim[j] == 'X') 
			x = dofs(j);
	   else if (channel.at(0).dim[j] == 'Y')
			y = dofs(j);
	   else if (channel.at(0).dim[j] == 'Z') 
			z = dofs(j);
	}
	skel.setRoot(x, y, z, name.at(0));
	skel.setOrientation(0, 0, 0, channel.at(1).dim);
	for (unsigned int i=1; i < joint.size(); ++i) {
		ref = channel.size();
		for (unsigned int j=0; j < channel.size(); ++j) {
			if ((channel.at(j).rot) && (channel.at(j).ref == parent.at(i))) {
				ref = j;
				break;
			}
		}
		if (!euler) {
			std::cout << "to do quaternion conversion" << std::endl;
		}
		for (unsigned int j=0; j < 3; ++j) {
		   if (channel.at(ref).dim[j] == 'X') 
				x = dofs(3*ref + j) * deg2rad;
		   else if (channel.at(ref).dim[j] == 'Y')
				y = dofs(3*ref + j) * deg2rad;
		   else if (channel.at(ref).dim[j] == 'Z') 
				z = dofs(3*ref + j) * deg2rad;
		}
		skel.addJoint(joint.at(i).x, joint.at(i).y, joint.at(i).z,
			x, y, z, channel.at(ref).dim, parent.at(i), name.at(i));
	}
	return skel;
}

/**
 * \return time frame interval at milisseconds
 * 
 * returns the time interval for frame refreshing at milisseconds
 */
unsigned int BVHReader::getFrameInterval() {
	unsigned int r = (unsigned int) (tframe * 1000);
	return r;
}

/**
 * \return number of DOFs in animated skeleton
 * 
 * get number of DOFs in animated skeleton
 */
unsigned int BVHReader::getNumberOfDOFs() {
	return name.size();
}

/**
 * \return total of frames at animation file
 * 
 * gets the number of frames at the animation
 */
unsigned int BVHReader::getNumberOfFrames() {
	return motion.size();
}

/**
 * \return mocap DOF matrix with loaded mocap data
 * 
 * dump all frames as one matrix. Rows are frames and columns are 
 * skeleton DOFs. Joint data are described as euler angles.
 */
Eigen::MatrixXd BVHReader::getEulerAnglesDOFMatrix() {
	unsigned int nframes = getNumberOfFrames();
	unsigned int ndofs = motion.at(0).size();
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(nframes, ndofs);
	std::vector<float> frline;
	
	for (unsigned int i=0; i < nframes; ++i) {
		frline = motion.at(i);
		for (unsigned int j=0; j < 3; ++j) 
			ret(i,j) = frline.at(j);
		for (unsigned int j=3; j < ndofs; ++j) 
			ret(i,j) = frline.at(j);
	}
	return ret;
}

/**
 * \return mocap DOF matrix with loaded mocap data
 * 
 * dump all frames as one matrix. Rows are frames and columns are 
 * skeleton DOFs. Joint data are described as quaternions.
 */
Eigen::MatrixXd BVHReader::getQuaternionDOFMatrix() {
	unsigned int nframes = getNumberOfFrames();
	unsigned int ndofs = 4*joint.size() + 3;
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(nframes, ndofs);
	Skeleton sk;
	std::vector<Eigen::Matrix4d> skjoints;
	Eigen::Matrix3d rot;
	Eigen::Vector4d rpos;
	
	for (unsigned int i=0; i < nframes; ++i) {
		sk = getFrameSkeleton(i);
		rpos = sk.getJointPosition(0);
		ret(i,0) = rpos(0);
		ret(i,1) = rpos(1);
		ret(i,2) = rpos(2);
		rot = sk.getRootOrientation().block(0,0,3,3);
		Eigen::Quaterniond qroot(rot);
		ret(i,3) = qroot.x();
		ret(i,4) = qroot.y();
		ret(i,5) = qroot.z();
		ret(i,6) = qroot.w();
		skjoints = sk.getBoneMatrices();
		for (unsigned int j=0, k=7; j < skjoints.size(); ++j, k += 4) {	
			rot = skjoints.at(j).block(0,0,3,3);
			Eigen::Quaterniond qbone(rot);
			ret(i,k) = qbone.x();
			ret(i,k+1) = qbone.y();
			ret(i,k+2) = qbone.z();
			ret(i,k+3) = qbone.w();
		}
	}
	return ret;
}

/**
 * @brief print skeleton pose as a bvh frame in file
 * @param sk skeleton pose
 * @param ost output stream to print
 */
void BVHReader::printPose(Skeleton & sk, std::ostream & ost) {
	Eigen::Matrix4d tmat;
	Eigen::Vector3d tvec;
	std::string zxy("ZXY"), zyx("ZYX");
	std::vector<Eigen::Matrix4d> bvec = sk.getBoneMatrices();
	std::vector<unsigned int> par = sk.getIndexHierarchy();

    tmat = sk.getSkeletonRootTranslation();
    // translation
    ost << tmat(0,3) << " " << tmat(1,3) << " " << tmat(2,3);
    // first rotation
	tmat = bvec.at(0);
	if (zxy.compare(0,3,channel.at(1).dim) == 0) {
		tvec = getZXYRotation(tmat);
		ost << " " << tvec(2) << " " << tvec(0) << " " << tvec(1);
	} else if (zyx.compare(0,3,channel.at(1).dim) == 0) {
		tvec = getZYXRotation(tmat);
		ost << " " << tvec(2) << " " << tvec(1) << " " << tvec(0);
	} else {
		ost << " 0 0 0";
	}
	tvec = getZYXRotation(tmat);
	// all other rotations
	for (unsigned int i=2; i < channel.size(); ++i) {
		tmat = bvec.at(channel.at(i).ref);
		if (zxy.compare(0,3,channel.at(1).dim) == 0) {
			tvec = getZXYRotation(tmat);
			ost << " " << tvec(2) << " " << tvec(0) << " " << tvec(1);
		} else if (zyx.compare(0,3,channel.at(1).dim) == 0) {
			tvec = getZYXRotation(tmat);
			ost << " " << tvec(2) << " " << tvec(1) << " " << tvec(0);
		} else {
			ost << " 0 0 0";
		}
	}
	ost << std::endl;
}

/**
 * @brief get a DOF vector from a skeleton pose
 * @param sk skeleton pose
 * @return DOF vector
 */
Eigen::VectorXd BVHReader::getEulerAnglesDOFPose(Skeleton & sk) {

	Eigen::Vector3d tvec;
	std::string zxy("ZXY"), zyx("ZYX");
	Eigen::Vector4d jroot = sk.getJointPosition(0);
    Eigen::Matrix4d skrot = sk.getRootOrientation();
    std::vector<Eigen::Matrix4d> brot = sk.getBoneMatrices();
    Eigen::VectorXd vpose = Eigen::ArrayXd::Zero(channel.size()*3);

	vpose(0) = jroot(0);
	vpose(1) = jroot(1);
	vpose(2) = jroot(2);
	if (zxy.compare(0,3,channel.at(1).dim) == 0) {
		tvec = getZXYRotation(skrot);
		vpose(3) = tvec(2);
		vpose(4) = tvec(0);
		vpose(5) = tvec(1);
	} else if (zyx.compare(0,3,channel.at(1).dim) == 0) {
		tvec = getZYXRotation(skrot);
		vpose(3) = tvec(2);
		vpose(4) = tvec(1);
		vpose(5) = tvec(0);
	} else {
		vpose(3) = 0;
		vpose(4) = 0;
		vpose(5) = 0;
	}
	
	for (unsigned int i=2; i < channel.size(); ++i) {
		skrot = brot.at(channel.at(i).ref);
		if (zxy.compare(0,3,channel.at(i).dim) == 0) {
			tvec = getZXYRotation(skrot);
			vpose(3*i) = tvec(2);
			vpose(3*i + 1) = tvec(0);
			vpose(3*i + 2) = tvec(1);
		} else if (zyx.compare(0,3,channel.at(i).dim) == 0) {
			tvec = getZYXRotation(skrot);
			vpose(3*i) = tvec(2);
			vpose(3*i + 1) = tvec(1);
			vpose(3*i + 2) = tvec(0);
		} else {
			vpose(3*i) = 0;
			vpose(3*i + 1) = 0;
			vpose(3*i + 2) = 0;
		}
	}
	return vpose;
}
