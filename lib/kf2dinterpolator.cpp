#include <kf2dinterpolator.h>
#include <cassert>
#include <Eigen/Eigenvalues>
#include <iostream>

#define NEARZERO 0.000001
#define MAXVAL 1.0e+12

/**
 * @brief get the frame distance matrix merged with another mocap session
 * @param sib the merged mocap session
 * @return merged frame distance matrix
 */
Eigen::MatrixXd RBF2DInterpolator::getMergedDistanceMatrix(MocapReader * sib) {
	unsigned int mcnf = mc->getNumberOfFrames();
	unsigned int sibnf = sib->getNumberOfFrames();
    fdist = Eigen::ArrayXXd::Zero(mcnf + sibnf, mcnf + sibnf);
    Skeleton sk0, sk1;

	for (unsigned int i=0; i < mcnf; ++i) {
		for (unsigned int j=i+1; j < mcnf; ++j) {
			sk0 = mc->getFrameSkeleton(i);
			sk1 = mc->getFrameSkeleton(j);
			fdist(i,j) = fdist(j,i) = std::sqrt(skdist->getPoseDistance(sk0, sk1));
		}
	}
	for (unsigned int i=0; i < mcnf; ++i) {
		for (unsigned int j=0; j < sibnf; ++j) {
			sk0 = mc->getFrameSkeleton(i);
			sk1 = sib->getFrameSkeleton(j);
			fdist(i,j + mcnf) = fdist(j + mcnf,i) = std::sqrt(skdist->getPoseDistance(sk0, sk1));
		}
	}
	for (unsigned int i=0; i < sibnf; ++i) {
		for (unsigned int j=i+1; j < sibnf; ++j) {
			sk0 = sib->getFrameSkeleton(i);
			sk1 = sib->getFrameSkeleton(j);
			fdist(i + mcnf,j + mcnf) = fdist(j + mcnf,i + mcnf) = std::sqrt(skdist->getPoseDistance(sk0, sk1));
		}
	}
	return fdist;
}

/**
 * @brief get the projected frames error vector for given keyframes and interval specified
 * @param kfs current keyframes
 * @param start frame id to start rebuild
 * @param len number of frames to rebuild
 * @return projected frame error vector
 */
Eigen::VectorXd RBF2DInterpolator::getProjectionErrorVector(Eigen::VectorXi & kfs, int start, int len) {
	//assert((start + len) <= rdframes.rows());
	unsigned int next = 0;

	while (kfs(next) <= start) ++next;
	--next;
	for (int i=start; i < start + len; ++i) {
		if (kfs(next) == i) {
			evec(i) = 0;
			++next;
		} else {
			evec(i) = getProjectedFrameDistance(kfs(next - 1), i, kfs(next));
		}
	}
	return evec;
}

/**
 * @brief get the projected frame distance between a marker given by frame id
 * and the current interpolation given by a key frame set. The key frame
 * set is given as an ordered vector.
 * @param kfs key frames id set as an ordered vector
 * @param fid projected frame id to be evaluated
 * @return pose distance between projected frame and interpolation reconstruction
 */
double RBF2DInterpolator::getProjectedFrameDistance(Eigen::VectorXi & kfs, int fid) {
	assert((fid >= 0) && (fid <= kfs(kfs.size() - 1)));
	unsigned int i, pf, ef, nf;
	// find
	for (i=1; i < kfs.size(); ++i)
		if (fid < kfs(i)) break;
	pf = kfs(i-1);
	ef = fid;
	nf = kfs(i);
	return getProjectedFrameDistance(pf, ef, nf);
}

/**
 * @brief get the projected frame distance between a marker given by frame id
 * and a linear interpolation between two other frames. Evaluated frame id
 * has to be inside interpolation interval.
 * @param pf projected frame id for interval beginning
 * @param ef projected frame id to be evaluated
 * @param nf projected frame id for interval end
 * @return pose distance between projected and interpolation reconstruction
 */
double RBF2DInterpolator::getProjectedFrameDistance(unsigned int pf, unsigned int ef, unsigned int nf) {
	assert(rdframes.rows() > nf);
	Eigen::VectorXd pdof, ndof, cdof, rdof;
	float t;

	pdof = rdframes.row(pf);
	ndof = rdframes.row(nf);
	rdof = rdframes.row(ef);
	t = nf - pf;
	t = (ef - pf) / t;
	cdof = t*ndof + (1 - t)*pdof;
	rdof -= cdof;
	return rdof.norm();
}

/**
 * @brief get the error vector of a reconstructed RBF/SKF animation for a given
 * frame projection matrix
 * @param proj frame projection matrix
 * @return error vector
 */
Eigen::VectorXd RBF2DInterpolator::getRebuildErrorVector(Eigen::MatrixXd const & proj, Animation * skfanim)  {
	Eigen::VectorXd cerr = Eigen::ArrayXd::Zero(proj.rows());
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos;
	Skeleton sk0, sk1;

	cpos(2) = 0;
	for (unsigned int i=1; i < cerr.size() - 1; ++i) {
		sk0 = mc->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		// current error
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		skfanim->setController(cpos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		cerr(i) = skdist->getPoseDistance(sk0, sk1);
	}
	return cerr;
}

/**
 * @brief get the gradient matrix of a 2D pose projection with inverse projection
 * given by RBF/SKF.
 * @param proj pose projection matrix
 * @param cerr error vector
 * @param alpha gain parameter
 * @return gradient matrix
 */
Eigen::MatrixXd RBF2DInterpolator::getGradientMatrix(Eigen::MatrixXd const & proj,
Animation * skfanim, Eigen::VectorXd const & cerr, double alpha) {
		
	Eigen::MatrixXd error = Eigen::ArrayXXd::Zero(proj.rows(), 5);
	Eigen::MatrixXd grad = Eigen::ArrayXXd::Zero(proj.rows(), 2);
	Skeleton sk0, sk1;
	Eigen::VectorXd v0;
	Eigen::Vector4d cpos, cbpos, capos;

	// get NSEW delta error
	cpos(2) = 0;
	capos(2) = 0;
	cbpos(2) = 0;
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		// delta east error
		sk0 = mc->getFrameSkeleton(i);
		v0 = sk0.getJointPosition(0);
		cpos.head(2) = proj.row(i).head(2);
		cpos(3) = 1;
		cbpos.head(2) = proj.row(i-1).head(2);
		cbpos(3) = 1;
		capos.head(2) = proj.row(i+1).head(2);
		capos(3) = 1;
		cbpos -= cpos;
		capos -= cpos;
		error(i,4) = (cbpos.norm() + capos.norm()) / 2; // circle radius
		capos = cpos;
		capos(0) += alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,0) = skdist->getPoseDistance(sk0, sk1);
		error(i,0) -= cerr(i);
		// delta west error
		capos = cpos;
		capos(0) -= alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,1) = skdist->getPoseDistance(sk0, sk1);
		error(i,1) -= cerr(i);
		// delta north error
		capos = cpos;
		capos(1) += alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,2) = skdist->getPoseDistance(sk0, sk1);
		error(i,2) -= cerr(i);
		// delta south error
		capos = cpos;
		capos(1) -= alpha*error(i,4);
		skfanim->setController(capos);
		skfanim->animate();
		sk1 = skfanim->getSkeleton();
		sk1.setRootPosition(v0);
		error(i,3) = skdist->getPoseDistance(sk0, sk1);
		error(i,3) -= cerr(i);
	}
	for (unsigned int i=1; i < error.rows() - 1; ++i) {
		grad(i,0) = 0;
		grad(i,1) = 0;
		if ((error(i,0) < 0) || (error(i,1) < 0)) {
			grad(i,0) = error(i,1) - error(i,0);
		}
		if ((error(i,2) < 0) || (error(i,3) < 0)) {
			grad(i,1) = error(i,3) - error(i,2);
		}
	}
	for (unsigned int i=1; i < grad.rows() - 1; ++i) {
		if (proj(i,2) == 1)
			grad.row(i) *= 0;
		if ((grad(i,0) != 0) || (grad(i,1) != 0)) {
			grad.row(i).normalize();
			grad.row(i) *= error(i,4);
		}
	}
	return grad;
}

/**
 * @brief improve a given projection matrix by gradient descent.
 * @param proj pose projection matrix
 * @param keyframes mocap key frames
 * @param iter number o gradient descent maximum iterations
 * @param alpha gradient descent parameter
 * @return improved frame projection matrix
 */
Eigen::MatrixXd RBF2DInterpolator::improveProjectionByGD(Eigen::MatrixXd const & proj, 
Eigen::VectorXi & keyframes, unsigned int iter, double alpha) {
	
	Eigen::MatrixXd grad;
	Eigen::VectorXd cerr;
	Eigen::MatrixXd ret = proj;
	double total0, total1;
	unsigned int i, fid;
	Animation localAnimation;
	Skeleton s0, s1;
	Eigen::Vector4d pos;

	// set animation
	pos << 0,0,0,1;
	s0 = mc->getFrameSkeleton(0);
	localAnimation.setSkeleton(s0);
	for (i=0; i < keyframes.size(); ++i) {
		fid = keyframes(i);
		s0 = mc->getFrameSkeleton(fid);
		pos.head(3) = proj.row(fid).head(3);
		localAnimation.addNewKeyFrame(pos, s0);
	}
	localAnimation.setContext();
	// do GD over projection
	cerr = getRebuildErrorVector(ret, &localAnimation);
	total0 = cerr.sum();
	for (i=0; i < iter; ++i) {
		grad = getGradientMatrix(ret, &localAnimation, cerr, alpha);
		ret.block(0, 0, ret.rows(), grad.cols()) += alpha * grad;
		cerr = getRebuildErrorVector(ret, &localAnimation);
		total1 = cerr.sum();
		if (total1 == total0) break;
		total0 = total1;
	}
	return ret;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd ForceRBFInterpolator::getMerged2DProjection(MocapReader * sib) {

	Eigen::MatrixXd dmat = getMergedDistanceMatrix(sib);
	return getDistanceMatrixProjection(dmat, 50, 0.125);
}

/**
 * @brief get markers position for given frame ids with Force strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd ForceRBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {
	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);
	else
		return getForceApproachProjection(fids, 50, 0.125);
}

/**
 * @brief get a 2D projection of many skeletons by successively approximating
 * a random sample to the skeleton distance distribution.
 * @param fref original skeleton frame reference
 * @param train number of approximation iterations
 * @param stepsize fraction of delta approximation
 * @return projected skeleton into 2D
 */
Eigen::MatrixXd ForceRBFInterpolator::getForceApproachProjection(
	Eigen::VectorXi const & fref, unsigned int train, double stepsize) {
		
	assert(fref.size() > 1);
	// simplest case
	if (fref.size() == 2) {
		Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(fref.size(), 4);
		ret(0,3) = 1;
		ret(1,0) = 1;
		ret(1,1) = 1;
		ret(1,3) = 1;
		return ret;
	}
	Eigen::MatrixXd ospdist = getDistanceMatrix(fref);
	return getDistanceMatrixProjection(ospdist, train, stepsize);
}

/**
 * @brief get a 2D projection of a distance matrix by successively approximating
 * a random sample to the skeleton distance distribution.
 * @param ospdist
 * @param train number of approximation iterations
 * @param stepsize fraction of delta approximation
 * @return projected distance matrix into 2D
 */
Eigen::MatrixXd ForceRBFInterpolator::getDistanceMatrixProjection(
	Eigen::MatrixXd & ospdist, unsigned int train, double stepsize) {
		
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(ospdist.rows(), 4);
	Eigen::VectorXd v;
	Skeleton sk0, sk1;
	double delta, min0, max0 = RAND_MAX;
	double min1, max1;
	double x0,x1;

	// initialize 2D data with random vectors
	std::srand(time(NULL));		
	for (unsigned int i=0; i < ret.rows(); ++i) {
		do {
			x0 = (double) std::rand() / max0;
		} while (x0 == 0);
		x1 = (double) std::rand() / max0;
		ret(i,0) = std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		//ret(i,0) = std::rand() / max0;
		do {
			x0 = (double) std::rand() / max0;
		} while (x0 == 0);
		x1 = (double) std::rand() / max0;
		ret(i,1) = std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		//ret(i,1) = std::rand() / max0;
		ret(i,2) = 0;
		ret(i,3) = 0;
	}
	// discover skeleton space distances and normalize it
	min0 = max0 = ospdist(0,1);
	for (unsigned int i=0; i < ospdist.rows(); ++i)
		for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
			if (ospdist(i,j) > max0) max0 = ospdist(i,j);
			else if (ospdist(i,j) < min0) min0 = ospdist(i,j);
		}
	for (unsigned int i=0; i < ospdist.rows(); ++i)
		for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
			ospdist(i,j) = (ospdist(i,j) - min0) / (max0 - min0);
			ospdist(j,i) = ospdist(i,j);
		}
	
	// training session
	for (unsigned int k=0; k < train; ++k) {
		// discover delta and update
		for (unsigned int i=0; i < ospdist.rows(); ++i)
			for (unsigned int j=i+1; j < ospdist.cols(); ++j) {
				v = ret.row(i) - ret.row(j);
				delta = stepsize * (ospdist(i,j) - std::sqrt(v.dot(v)));
				v *= delta;
				ret.row(i) += v;
			}
		// normalize projection coordinates
		min0 = max0 = ret(0,0);
		min1 = max1 = ret(0,1);
		for (unsigned int i=1; i < ret.rows(); ++i) {
			if (min0 > ret(i,0)) min0 = ret(i,0);
			else if (max0 < ret(i,0)) max0 = ret(i,0);
			if (min1 > ret(i,1)) min1 = ret(i,1);
			else if (max1 < ret(i,1)) max1 = ret(i,1);
		}
		for (unsigned int i=0; i < ret.rows(); ++i) {
			ret(i,0) = (ret(i,0) - min0) / (max0 - min0);
			ret(i,1) = (ret(i,1) - min1) / (max1 - min1);
		}
	}
	// reshape for 3D homogeneous coordinate
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,2) = 0;
		ret(i,3) = 1;
	}
    //std::cout << "markers" << std::endl;
    //std::cout << ret << std::endl;
	return ret;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd MDSRBFInterpolator::getMerged2DProjection(MocapReader * sib) {

	Eigen::MatrixXd dmat = getMergedDistanceMatrix(sib);
	return getDistanceMatrixProjection(dmat);
}

/**
 * @brief get markers position for given frame ids with MDS strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd MDSRBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {

	assert(fids.size() > 1);
	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);

	// simplest case
	if (fids.size() == 2) {
		Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(fids.size(), 4);
		proj(0,3) = 1;
		proj(1,0) = 1;
		proj(1,1) = 1;
		proj(1,3) = 1;
		return proj;
	}
	// build distance A matrix
	Eigen::MatrixXd adist = getDistanceMatrix(fids);
	return getDistanceMatrixProjection(adist);
}

/**
 * @brief get a 2D projection given a frame distance matrix with MDS strategy
 * @param adist frame distance matrix
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd MDSRBFInterpolator::getDistanceMatrixProjection(Eigen::MatrixXd & adist) {
	Eigen::MatrixXd bdist = Eigen::ArrayXXd::Zero(adist.rows(), adist.cols());
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(adist.rows(), 4);
	Eigen::VectorXd rowavg, colavg;
	double distavg;
	Skeleton sk0, sk1;
		
	for (unsigned int i=0; i < adist.rows(); ++i)
		for (unsigned int j=i+1; j < adist.cols(); ++j) {
			//sk0 = mc->getFrameSkeleton(fids(i));
			//sk1 = mc->getFrameSkeleton(fids(j));
			//adist(i,j) = -0.5 * skdist->getPoseDistance(sk0, sk1); // \frac{-1}{2} d_{ij}^2
			adist(i,j) = -0.5 * adist(i,j) * adist(i,j); // \frac{-1}{2} d_{ij}^2
			adist(j,i) = adist(i,j);
		}
	// build distance B matrix
	rowavg = adist.rowwise().sum();
	colavg = adist.colwise().sum();
	distavg = rowavg.sum();
	rowavg /= adist.rows();
	colavg /= adist.cols();
	distavg /= (adist.rows() * adist.cols());
	for (unsigned int i=0; i < adist.rows(); ++i)
		for (unsigned int j=0; j < adist.cols(); ++j) {
			bdist(i,j) = adist(i,j) + distavg - rowavg(i) - colavg(j);
		}
    
    // get most significant B matrix eigenvalues and eigenvectors
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> esolver(bdist);
    Eigen::VectorXd maxevs = esolver.eigenvalues();
    Eigen::MatrixXd evectors = esolver.eigenvectors();
    const int pdim = 2; // 2D projection
    unsigned int pp = 0, col[pdim];
    double mval;
    bool isin;
    while (pp < pdim) {
        mval = -1 * maxevs.dot(maxevs);
        for (unsigned int i=0; i < maxevs.size(); ++i) {
            isin = false;
            if (mval < (maxevs(i) * maxevs(i))) {
                for (unsigned int j=0; j < pp; ++j)
                    if (i == col[j]) isin = true;
                if (!isin) {
                    col[pp] = i;
                    mval = maxevs(col[pp]) * maxevs(col[pp]);
                }
            }
        }
        ++pp;
    }
    // normalize eigenvectors
    for (unsigned int j=0; j < pdim; ++j) {
        mval = std::sqrt(maxevs(col[j])) / evectors.col(col[j]).norm();
        proj.col(j) = evectors.col(col[j]) * mval;
    }
    // normalize to 1x1 square
    double mmin1, mmin2, mmax, mdiff1, mdiff2;
    mmin1 = mmax = proj(0,0);
    for (unsigned int j=1; j < proj.rows(); ++j) 
		if (mmin1 > proj(j,0)) mmin1 = proj(j,0);
		else if (mmax < proj(j,0)) mmax = proj(j,0);
	mdiff1 = mmax - mmin1;
    mmin2 = mmax = proj(0,1);
    for (unsigned int j=1; j < proj.rows(); ++j) 
		if (mmin2 > proj(j,1)) mmin2 = proj(j,1);
		else if (mmax < proj(j,1)) mmax = proj(j,1);	
	mdiff2 = mmax - mmin2;
	if (mdiff1 > mdiff2) mdiff2 = mdiff1;
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,0) = (proj(j,0) - mmin1) / mdiff2;
    	proj(j,1) = (proj(j,1) - mmin2) / mdiff2;
    }
    // homogeneous coordinates
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,3) = 1;
	}
    //std::cout << "markers" << std::endl;
    //std::cout << proj << std::endl;
    return proj;
}

/**
 * @brief tests if two matrices have equal dimensions and values
 * @param mat1 first matrix to be compared
 * @param mat2 second matrix to be compared
 * @return whether matrices are equal
 */
bool IsomapRBFInterpolator::sameMatrices(Eigen::MatrixXd & mat1, Eigen::MatrixXd & mat2) {

	if (mat1.cols() != mat2.cols()) return false;
	if (mat1.rows() != mat2.rows()) return false;
	for (unsigned int i=0; i < mat1.rows(); ++i)
		for (unsigned int j=0; j < mat1.cols(); ++j) 
			if (mat1(i,j) != mat2(i,j))
				return false;
	return true;
}

/**
 * @brief update graph matrix using Floyd-Warshall algorithm
 * @param fmat matrix to be updated
 */
void IsomapRBFInterpolator::runFloydWarshallUpdate(Eigen::MatrixXd & fmat) {
	double temp;
	for (unsigned int i=0; i < fmat.rows(); ++i)
		for (unsigned int j=0; j < fmat.cols(); ++j)
			for (unsigned int k=0; k < fmat.cols(); ++k) {
				temp = fmat(i,k) + fmat(k,j);
				if (fmat(i, j) > temp) 
					fmat(i,j) = temp;
			}
}

/**
 * @brief get the euclidean geodesic distance matrix of a given set of frame ids
 * @param fref frame ids set as a vector
 * @param nn number of nearest neighbors for geodesic distance
 * @return distance matrix
 */
Eigen::MatrixXd IsomapRBFInterpolator::getIsomapDistanceMatrix(Eigen::VectorXi const & fref, unsigned int nn) {
	double dmax = 1000000000;
	Eigen::MatrixXd dmat = getDistanceMatrix(fref);
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Constant(dmat.rows(), dmat.cols(), dmax);
	Eigen::MatrixXd temp;
	Eigen::VectorXd dvec = dmat.row(0);
	Eigen::VectorXd p0, p1;
	Eigen::VectorXi nneigh = Eigen::ArrayXi::Zero(nn);
	unsigned int min, max;

	for (unsigned int i=0; i < fmat.rows(); ++i)
		fmat(i,i) = 0;
	for (unsigned int i=0; i < dmat.rows(); ++i) {
		dvec = dmat.row(i);
		// get nearest neighbors
		max = i;
		for (unsigned int k=0; k < dvec.size(); ++k) 
			if (dvec(k) > dvec(max))
				max = k;
		dvec(i) = dvec(max);
		for (unsigned int j=0; j < nneigh.size(); ++j) {
			min = 0;
			for (unsigned int k=1; k < dvec.size(); ++k) 
				if (dvec(k) < dvec(min))
					min = k;
			nneigh(j) = min;
			dvec(min) = dvec(max);
		}
		// get distance to nearest neighbors
		for (unsigned int j=0; j < nneigh.size(); ++j) {
			fmat(i, nneigh(j)) = fmat(nneigh(j), i) = dmat(i, nneigh(j));
		}
	}
	do {
		temp = fmat;
		runFloydWarshallUpdate(fmat);
	} while (!sameMatrices(fmat,temp));
    return fmat;
}

/**
 * @brief get the frame isomap distance matrix merged with another mocap session
 * @param sib the merged mocap session
 * @param nn number of nearest neighbors
 * @return merged frame distance matrix
 */
Eigen::MatrixXd IsomapRBFInterpolator::getMergedIsomapDistanceMatrix(MocapReader * sib, unsigned int nn) {
	double dmax = 1000000000;
	Eigen::MatrixXd dmat = getMergedDistanceMatrix(sib);
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Constant(dmat.rows(), dmat.cols(), dmax);
	Eigen::MatrixXd temp;
	Eigen::VectorXd dvec = dmat.row(0);
	Eigen::VectorXd p0, p1;
	Eigen::VectorXi nneigh = Eigen::ArrayXi::Zero(nn);
	unsigned int min, max;

	for (unsigned int i=0; i < fmat.rows(); ++i)
		fmat(i,i) = 0;
	for (unsigned int i=0; i < dmat.rows(); ++i) {
		dvec = dmat.row(i);
		// get nearest neighbors
		max = i;
		for (unsigned int k=0; k < dvec.size(); ++k) 
			if (dvec(k) > dvec(max))
				max = k;
		dvec(i) = dvec(max);
		for (unsigned int j=0; j < nneigh.size(); ++j) {
			min = 0;
			for (unsigned int k=1; k < dvec.size(); ++k) 
				if (dvec(k) < dvec(min))
					min = k;
			nneigh(j) = min;
			dvec(min) = dvec(max);
		}
		// get distance to nearest neighbors
		for (unsigned int j=0; j < nneigh.size(); ++j) {
			fmat(i, nneigh(j)) = fmat(nneigh(j), i) = dmat(i, nneigh(j));
		}
	}
	do {
		temp = fmat;
		runFloydWarshallUpdate(fmat);
	} while (!sameMatrices(fmat,temp));
    return fmat;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd IsomapRBFInterpolator::getMerged2DProjection(MocapReader * sib) {

	Eigen::MatrixXd dmat = getMergedIsomapDistanceMatrix(sib);
	return getIsomapProjection(dmat);
}

/**
 * @brief get markers position for given frame ids with Isomap strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd IsomapRBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {

	assert(fids.size() > 1);
	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);

	// simplest case
	if (fids.size() == 2) {
		Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(fids.size(), 4);
		proj(0,3) = 1;
		proj(1,0) = 1;
		proj(1,1) = 1;
		proj(1,3) = 1;
		return proj;
	}
	// build distance matrix
	Eigen::MatrixXd adist = getIsomapDistanceMatrix(fids);
	return getIsomapProjection(adist);
}

/**
 * @brief get the least squares projection as a matrix given a distance matrix
 * @param dist distance matrix
 * @param pdim number of dimensions of the projection
 * @return rowwise projection matrix
 */
Eigen::MatrixXd IsomapRBFInterpolator::getIsomapProjection(Eigen::MatrixXd const & dist) {
    Eigen::MatrixXd smat = Eigen::ArrayXXd::Zero(dist.rows(), dist.cols());
    Eigen::MatrixXd hmat = Eigen::ArrayXXd::Constant(dist.rows(), dist.cols(), -1.0 / dist.rows());
    Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(dist.rows(), 4);

    for (unsigned int i=0; i < dist.rows(); ++i)
        for (unsigned int j=0; j < dist.cols(); ++j)
            smat(i,j) = dist(i,j)*dist(i,j);
    for (unsigned int i=0; i < hmat.rows(); ++i)
        hmat(i,i) += 1;

    smat = -0.5 * hmat * smat * hmat;
    // get most significant eigenvalues and eigenvectors
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> esolver(smat);
    Eigen::VectorXd maxevs = esolver.eigenvalues();
    Eigen::MatrixXd evectors = esolver.eigenvectors();
    const unsigned int pdim = 2;
    unsigned int pp = 0, col[pdim];
    double mval;
    bool isin;
    while (pp < pdim) {
        mval = -1 * maxevs.dot(maxevs);
        for (unsigned int i=0; i < maxevs.size(); ++i) {
            isin = false;
            if (mval < (maxevs(i) * maxevs(i))) {
                for (unsigned int j=0; j < pp; ++j)
                    if (i == col[j]) isin = true;
                if (!isin) {
                    col[pp] = i;
                    mval = maxevs(col[pp]) * maxevs(col[pp]);
                }
            }
        }
        ++pp;
    }
    // normalize eigenvectors
    for (unsigned int j=0; j < pdim; ++j) {
        mval = std::sqrt(std::abs(maxevs(col[j]))) / evectors.col(col[j]).norm();
        proj.col(j) = evectors.col(col[j]) * mval; 
    }
    // normalize to 1x1 square
    double mmin1, mmin2, mmax, mdiff1, mdiff2;
    mmin1 = mmax = proj(0,0);
    for (unsigned int j=1; j < proj.rows(); ++j) 
		if (mmin1 > proj(j,0)) mmin1 = proj(j,0);
		else if (mmax < proj(j,0)) mmax = proj(j,0);
	mdiff1 = mmax - mmin1;
    mmin2 = mmax = proj(0,1);
    for (unsigned int j=1; j < proj.rows(); ++j) 
		if (mmin2 > proj(j,1)) mmin2 = proj(j,1);
		else if (mmax < proj(j,1)) mmax = proj(j,1);	
	mdiff2 = mmax - mmin2;
	if (mdiff1 > mdiff2) mdiff2 = mdiff1;
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,0) = (proj(j,0) - mmin1) / mdiff2;
    	proj(j,1) = (proj(j,1) - mmin2) / mdiff2;
    }
    // homogeneous coordinate
    for (unsigned int i=0; i < proj.rows(); ++i) {
		proj(i,2) = 0;
		proj(i,3) = 1;
	}
    return proj;
}

/**
 * @brief get a rowwise data matrix of frames merged with another mocap session
 * @param sib sibling mocap session
 * @return rowwise frame matrix
 */
Eigen::MatrixXd PCARBFInterpolator::getMergedRowwiseDataMatrix(MocapReader * sib) {
	Skeleton sk0;
	
	sk0 = sib->getFrameSkeleton(0);
	Eigen::VectorXd vs = skdist->getMultidimensionalWeightedPoseVector(sk0);
	unsigned int m2 = vs.size();
	sk0 = mc->getFrameSkeleton(0);
	vs = skdist->getMultidimensionalWeightedPoseVector(sk0);
	unsigned int m1 = vs.size();
	assert(m1 == m2);
	
	m1 = mc->getNumberOfFrames();
	m2 = sib->getNumberOfFrames();
	Eigen::MatrixXd datmat = Eigen::ArrayXXd::Zero(m1 + m2, vs.size());
	
	datmat.row(0) = vs;
	for (unsigned int i=1; i < m1; ++i) {
		sk0 = mc->getFrameSkeleton(i);
		vs = skdist->getMultidimensionalWeightedPoseVector(sk0);
		datmat.row(i) = vs;
	}
	for (unsigned int i=0; i < m2; ++i) {
		sk0 = sib->getFrameSkeleton(i);
		vs = skdist->getMultidimensionalWeightedPoseVector(sk0);
		datmat.row(i + m1) = vs;
	}
	vs = datmat.colwise().sum();
	vs /= datmat.rows();
	for (unsigned int i=0; i < datmat.rows(); ++i)
		datmat.row(i) -= vs;
	return datmat;
}

/**
 * @brief get a rowwise frame matrix given a group of frame ids
 * @param fids frame ids vector
 * @return rowwise matrix
 */
Eigen::MatrixXd PCARBFInterpolator::getRowwiseDataMatrix(Eigen::VectorXi const & fids) {
	Skeleton sk0;
	sk0 = mc->getFrameSkeleton(fids(0));
	Eigen::VectorXd vs = skdist->getMultidimensionalWeightedPoseVector(sk0);
	Eigen::MatrixXd datmat = Eigen::ArrayXXd::Zero(fids.size(), vs.size());
	datmat.row(0) = vs;

	for (unsigned int i=1; i < datmat.rows(); ++i) {
		sk0 = mc->getFrameSkeleton(fids(i));
		vs = skdist->getMultidimensionalWeightedPoseVector(sk0);
		datmat.row(i) = vs;
	}
	vs = datmat.colwise().sum();
	vs /= datmat.rows();
	for (unsigned int i=0; i < datmat.rows(); ++i)
		datmat.row(i) -= vs;
	return datmat;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd PCARBFInterpolator::getMerged2DProjection(MocapReader * sib) {

	Eigen::MatrixXd dmat = getMergedRowwiseDataMatrix(sib);
	return getPCAProjection(dmat);
}

/**
 * @brief get markers position for given frame ids with PCA strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd PCARBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {

	assert(fids.size() > 1);
	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);

	// simplest case
	if (fids.size() == 2) {
		Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(fids.size(), 4);
		proj(0,3) = 1;
		proj(1,0) = 1;
		proj(1,1) = 1;
		proj(1,3) = 1;
		return proj;
	}
	// build covariance matrix
	Eigen::MatrixXd datmat = getRowwiseDataMatrix(fids);
	return getPCAProjection(datmat);
}

/**
 * @brief get the 2D Principal Components of a given covariance matrix
 * @param datmat rowwise data matrix
 * @return 2D projection
 */
Eigen::MatrixXd PCARBFInterpolator::getPCAProjection(Eigen::MatrixXd const & datmat) {
	
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(datmat, Eigen::ComputeFullU);
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(datmat.rows(), 4);

    // get most significant matrix eigenvalues and eigenvectors
    Eigen::MatrixXd eigVectors = svd.matrixU();
    proj.block(0, 0, datmat.rows(), 2) = eigVectors.block(0, 0, eigVectors.rows(), 2);
    Eigen::VectorXd sv = svd.singularValues();
    proj.col(0) *= sv(0);
    proj.col(1) *= sv(1);
    //Eigen::MatrixXd eigVectors = svd.matrixV();
    //Eigen::MatrixXd rmat = eigVectors.block(0, 0, eigVectors.rows(), 2);
    //proj.block(0, 0, datmat.rows(), 2) = datmat * rmat;

    // normalize to 1x1 square
    double mmin1, mmin2, mmax, mdiff1, mdiff2;
    mmin1 = mmax = proj(0,0);
    for (unsigned int j=1; j < proj.rows(); ++j)
		if (mmin1 > proj(j,0)) mmin1 = proj(j,0);
		else if (mmax < proj(j,0)) mmax = proj(j,0);
	mdiff1 = mmax - mmin1;
    mmin2 = mmax = proj(0,1);
    for (unsigned int j=1; j < proj.rows(); ++j)
		if (mmin2 > proj(j,1)) mmin2 = proj(j,1);
		else if (mmax < proj(j,1)) mmax = proj(j,1);
	mdiff2 = mmax - mmin2;
	if (mdiff1 > mdiff2) mdiff2 = mdiff1;
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,0) = (proj(j,0) - mmin1) / mdiff2;
		proj(j,1) = (proj(j,1) - mmin2) / mdiff2;
    }
    // homogeneous coordinates
    for (unsigned int j=0; j < proj.rows(); ++j) {
		proj(j,3) = 1;
	}
    return proj;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd LLERBFInterpolator::getMerged2DProjection(MocapReader * sib) {

	unsigned int knn = 15;
	unsigned int m1 = mc->getNumberOfFrames();
	unsigned int m2 = sib->getNumberOfFrames();
	assert(m1 + m2 > knn);
	
	Eigen::MatrixXd dmat = getMergedDistanceMatrix(sib);
	return getDistanceMatrixProjection(dmat, knn);
}

/**
 * @brief get markers position for given frame ids with LLE strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd LLERBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {
	unsigned int knn;
	
	assert(fids.size() > 1);
	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);
	// simplest case
	if (fids.size() == 2) {
		Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(fids.size(), 4);
		proj(0,3) = 1;
		proj(1,0) = 1;
		proj(1,1) = 1;
		proj(1,3) = 1;
		return proj;
	}
	if (fids.size() > 16) knn = 15; else knn = fids.size() - 1;
	Eigen::MatrixXd dist = getDistanceMatrix(fids);
	return getDistanceMatrixProjection(dist, knn);
}

/**
 * @brief get the 2D LLE projection given a frame distance matrix and K nearest neighbors
 * @param dmat frame distance matrix
 * @param knn number of nearest neighbors
 * @return LLE 2D projection
 */
Eigen::MatrixXd LLERBFInterpolator::getDistanceMatrixProjection(Eigen::MatrixXd & dmat, unsigned int knn) {
	
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(dmat.rows(), 4);
	Eigen::MatrixXd llew = getLLEWeightMatrix(dmat, knn);
	proj.block(0, 0, dmat.rows(), 2) = getEmbeddingCoordinateMatrix(llew, 2);
	for (unsigned int i=0; i < proj.rows(); ++i) proj(i,3) = 1;
	return proj;
}

/**
 * @brief get nearest neigbors index vector
 * @param dvec distance vector
 * @param knn number of nearest neighbors
 * @param curr current pivot
 * @return index vector with nearest neighbors
 */
Eigen::VectorXi LLERBFInterpolator::getKnnIndices(Eigen::VectorXd const & dvec, unsigned int knn, unsigned int curr) {
    Eigen::VectorXi ret = Eigen::ArrayXi::Zero(knn);
    bool wasSeen;

    // put random indices
    for (unsigned int i=0; i < ret.size(); ++i) 
        ret(i) = (curr + 1 + i) % dvec.size();
    // find minimum
    for (unsigned int j=0; j < dvec.size(); ++j)
        if (j == curr) continue; else
        if (dvec(j) < dvec(ret(0)))
            ret(0) = j;
    // find next unseen minimums
    for (unsigned int i=1; i < knn; ++i) {
        for (unsigned int j=0; j < dvec.size(); ++j)
            if (j == curr) continue; else
            if (dvec(j) < dvec(ret(i))) {
                // check if it was seen
                wasSeen = false;
                for (unsigned int k=0; k < i; ++k)
                    wasSeen = wasSeen || (ret(k) == (int) j); 
                if (!wasSeen) 
                    ret(i) = j;
            }
    }
    return ret;
}

/**
 * @brief get local linear embedding weight reconstruction matrix for a given data matrix
 * @param dist frame distance matrix
 * @param fref keyframe index vector
 * @param knn size of nearest neighborhood
 * @return weight matrix
 */
Eigen::MatrixXd LLERBFInterpolator::getLLEWeightMatrix(
	Eigen::MatrixXd & dist, unsigned int knn) {
	
    Eigen::MatrixXd wmat = Eigen::ArrayXXd::Zero(dist.rows(), dist.cols());
    Eigen::MatrixXi nbmat = Eigen::ArrayXXi::Zero(dist.rows(), knn);
    Eigen::VectorXd rdist = Eigen::ArrayXd::Zero(dist.rows());

    // get knn minimum values
    for (unsigned int i=0; i < dist.rows(); ++i) {
        rdist = dist.row(i);
        nbmat.row(i) = getKnnIndices(rdist, knn, i);
    }
    // calculate weights
    Eigen::MatrixXd cmat = Eigen::ArrayXXd::Zero(knn, knn);
    Eigen::VectorXd ones = Eigen::ArrayXd::Constant(knn, 1.0);
    Eigen::VectorXd sol = Eigen::ArrayXd::Zero(knn);
    double sum;
    for (unsigned int i=0; i < nbmat.rows(); ++i) {
		for (unsigned int j=0; j < cmat.rows(); ++j)
			for (unsigned int k=0; k < cmat.cols(); ++k) 
				cmat(j,k) = 0.5*(dist(i,nbmat(i,j))*dist(i,nbmat(i,j)) + 
						dist(i,nbmat(i,k))*dist(i,nbmat(i,k)) - 
						dist(nbmat(i,j), nbmat(i,k))*dist(nbmat(i,j), nbmat(i,k)));

        sol = cmat.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(ones);
        //sol = cmat.fullPivLu().solve(ones);
        sum = sol.sum();
        if (sum == 0) {
            // same weigths for everybody
			for (unsigned int j=0; j < sol.size(); ++j) sol(j) = 1;
            sum = sol.cwiseAbs().sum();
        }
        for (unsigned int j=0; j < nbmat.cols(); ++j) {
            wmat(i, nbmat(i,j)) = sol(j) / sum;
        }
    }
    return wmat;
}

/**
 * @brief get embedding coordinates for LLE given weight matrix
 * @param wmat weighht matrix
 * @param dim dimension of embedding coordinates
 * @return matrix with eigenvectors displayed columnwise
 */
Eigen::MatrixXd LLERBFInterpolator::getEmbeddingCoordinateMatrix(Eigen::MatrixXd const & wmat, unsigned int dim) {
	
    Eigen::MatrixXd imat = Eigen::MatrixXd::Identity(wmat.rows(), wmat.cols());
    Eigen::MatrixXd iwmat = imat - wmat;
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(iwmat, Eigen::ComputeThinV);
    Eigen::MatrixXd vmat = svd.matrixV();
    assert((vmat.cols() - dim) > 0);
    Eigen::MatrixXd sol = vmat.block(0, vmat.cols() - dim - 1, vmat.rows(), dim);
	//Eigen::VectorXd sval = svd.singularValues();
	//for (unsigned int i=0; i < sol.cols(); ++i)
	//	sol.col(i) /= sval(vmat.cols() - dim - 1 + i);
     return sol;
}

/**
 * @brief get frame most distant from initial and final frame
 * @param fids vector with frame ids
 * @return most distant frame id
 */
int LAMPRBFInterpolator::getMostDistantFrameFromExtremes(Eigen::VectorXi const & fids) {
	Skeleton sk0, sk1, sk2;
	Eigen::VectorXd dvec = Eigen::ArrayXd::Zero(fids.size());
	double dd;
	unsigned int ret;

	assert(fids.size() > 2);
	// discover skeleton space distances and normalize it
	sk1 = mc->getFrameSkeleton(fids(0));
	sk2 = mc->getFrameSkeleton(fids(fids.size() - 1));
	for (unsigned int i=0; i < fids.size(); ++i) {
		dd = 0;
		sk0 = mc->getFrameSkeleton(fids(i));
		//
		dd += skdist->getPoseDistance(sk0, sk1);
		dd += skdist->getPoseDistance(sk0, sk2);
		dvec(i) = dd;
	}
	ret = 1;
	for (unsigned int i=2; i < dvec.size() - 1; ++i) {
		if (dvec(i) > dvec(ret)) 
			ret = i;
	}
	return fids(ret);
}

/**
 * @brief get marker position/projection for most distant frame (median)
 * @param median most distant frame id
 * @param nf last frame id
 * @return marker position/projection
 */
Eigen::Vector4d LAMPRBFInterpolator::getMedianProjection(unsigned int median, unsigned int nf) {
	Skeleton sk0, sk1, sk2;
	double d0, d1;
	Eigen::Vector4d ret;

	sk0 = mc->getFrameSkeleton(median);
	sk1 = mc->getFrameSkeleton(0);
	sk2 = mc->getFrameSkeleton(nf);
	//
	d0 = skdist->getPoseDistance(sk0, sk1);
	d1 = skdist->getPoseDistance(sk0, sk2);
	if (d1 != 0) {
		d0 = std::atan2(d0, d1); // alpha
	} else {
		d0 = M_PI / 4;
	}
	d1 = M_PI / 4 + d0; // 45 - (90 - alpha)
	
	ret(1) = std::cos(d1) * std::sin(d0) * std::sqrt(2); // cos(45 + alpha) * sin(alpha) * r
	ret(2) = std::sin(d1) * std::sin(d0) * std::sqrt(2); // cos(45 + alpha) * sin(alpha) * r 		
	ret(2) = 0;
	ret(3) = 1;
	return ret;
	
}

/**
 * @brief get a projection data from a d1 dimension to d2 where d1 > d2. Usually, d2 = 2.
 * @param mpose data to be projected row-wise
 * @param yproj control points on d2 space dimension
 * @param xproj control points on d1 space dimension
 * @return d2 projected data
 */
Eigen::VectorXd LAMPRBFInterpolator::getLampProjection(Eigen::VectorXd const & mpose, 
	Eigen::MatrixXd const & yproj, Eigen::MatrixXd const & xproj) {
	
	Eigen::VectorXd ret = Eigen::ArrayXd::Zero(yproj.cols());
	Eigen::VectorXd alpha = Eigen::ArrayXd::Zero(xproj.rows());
	Eigen::MatrixXd mmat, amat, bmat, atbmat;
	Eigen::VectorXd xt, yt, xh, yh;
	double sa;
	
	amat = Eigen::ArrayXXd::Zero(xproj.rows(), xproj.cols());
	bmat = Eigen::ArrayXXd::Zero(yproj.rows(), yproj.cols());
	
	// calculate alphas
	for (unsigned int j=0; j < xproj.rows(); ++j) {
		alpha(j) = mpose.dot(xproj.row(j));
		if (alpha(j) > NEARZERO) alpha(j) = 1 / (alpha(j) * alpha(j));
		else alpha(j) = MAXVAL;
	}
	// get x~, y~
	sa = 0;
	xt = Eigen::ArrayXd::Zero(xproj.cols());
	yt = Eigen::ArrayXd::Zero(yproj.cols());
	xh = Eigen::ArrayXd::Zero(xproj.cols());
	yh = Eigen::ArrayXd::Zero(yproj.cols());
	for (unsigned int j=0; j < alpha.size(); ++j) {
		sa += alpha(j);
		xt += alpha(j) * xproj.row(j);
		yt += alpha(j) * yproj.row(j);
	}
	xt /= sa;
	yt /= sa;
	// build matrix M
	sa = std::sqrt(sa);
	for (unsigned int j=0; j < alpha.size(); ++j) {
		xh = xproj.row(j);
		yh = yproj.row(j);
		amat.row(j) = sa * (xh - xt);
		bmat.row(j) = sa * (yh - yt);
	}
	atbmat = amat.transpose() * bmat;
	Eigen::JacobiSVD<Eigen::MatrixXd> svdatb(atbmat, Eigen::ComputeThinU | Eigen::ComputeThinV);
	mmat = svdatb.matrixU() * svdatb.matrixV();		
	// save projection: y = (x - x~) * M + y~
	xh = mpose;
	xh -= xt;
	yh = xh.transpose() * mmat;
	yh += yt;
	ret = yh;
	
	return ret;
}

/**
 * @brief return a subset of indices equally spaced from given key frame vector
 * @param ref given key frames index vector
 * @param ssetsize size of key frame subset
 * @return subset key frames index vector with chosen subset
 */
Eigen::VectorXi LAMPRBFInterpolator::getForceSubset(Eigen::VectorXi const & ref, int ssetsize) {
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(ssetsize);
	int pos;
	float step = ref.size() - 1;
	step /= (ssetsize - 1);

	for (int i=0; i < ssetsize; ++i) {
		pos = step * i;
		ret(i) = ref(pos);
	}
	ret(ret.size() - 1) = ref(ref.size() - 1);
	return ret;
}

/**
 * @brief get markers position for given frame ids with LAMP strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd LAMPRBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(fids.size(), 4);
	Eigen::VectorXi fss = Eigen::ArrayXi::Zero(3);
	Eigen::VectorXd skvec;
	Skeleton sk0;
	int sss;
	
	assert(fids.size() > 1);
	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);

	// simplest case
	proj(0,3) = 1;
	proj(fids.size() - 1, 0) = 1;
	proj(fids.size() - 1, 1) = 1;
	proj(fids.size() - 1, 3) = 1;
	if (fids.size() == 2) return proj;
	if (fids.size() < 50) {
		// three is enough: get median and project it
		fss(0) = fids(0);
		fss(1) = getMostDistantFrameFromExtremes(fids); // median
		fss(2) = fids(fids.size() - 1);
		Eigen::VectorXd mpos = getMedianProjection(fss(1), fids(fids.size() - 1));
		for (unsigned int i=0; i < fids.size(); ++i) 
			if (fss(1) == fids(i)) {
				proj(i, 0) = mpos(0);
				proj(i, 1) = mpos(1);
				proj(i, 3) = 1;
				break;
			}
	} else {
		// more than three: do Force on subset
		sss = fids.size() * 0.08;
		sss = std::floor(sss);
		fss = getForceSubset(fids, sss);
		Eigen::MatrixXd fproj = getForceApproachProjection(fss, 50, 0.125);
		//std::cout << "Force projection" << std::endl << fproj << std::endl;
		sss = 0;
		for (unsigned int i=0; i < fids.size(); ++i) 
			if (fss(sss) == fids(i)) {
				proj.row(i) = fproj.row(sss);
				++sss;
			}
	}
	
	// build remainder projections
	Eigen::MatrixXd lowdim = Eigen::ArrayXXd::Zero(fss.size(), 2);
	sk0 = mc->getFrameSkeleton(0);
	skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
	Eigen::MatrixXd highdim = Eigen::ArrayXXd::Zero(fss.size(), skvec.size());
	for (unsigned int i=0; i < fss.size(); ++i)
		lowdim.row(i) = proj.row(fss(i)).head(2);
	for (unsigned int i=0; i < fss.size(); ++i) {
		sk0 = mc->getFrameSkeleton(fss(i));
		skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
		highdim.row(i) = skvec;
	}
	sss = 0;
	for (unsigned int i=0; i < fids.size(); ++i) {
		if (fids(i) == fss(sss)) {
			++sss;
			continue;
		}
		sk0 = mc->getFrameSkeleton(fids(i));
		skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
		Eigen::VectorXd lvec = getLampProjection(skvec, lowdim, highdim);
		proj(i,0) = lvec(0);
		proj(i,1) = lvec(1);
		proj(i,3) = 1;
	}
	// rescale for [0,1] on (x,y)
	//rescaleMarkers(proj);
	return proj;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd LAMPRBFInterpolator::getMerged2DProjection(MocapReader * sib) {
	unsigned int m1 = mc->getNumberOfFrames();
	unsigned int m2 = sib->getNumberOfFrames();
	
	Eigen::MatrixXd proj = Eigen::ArrayXXd::Zero(m1 + m2, 4);
	Eigen::VectorXi fss = Eigen::ArrayXi::Zero(3);
	Eigen::VectorXd skvec;
	Skeleton sk0;
	int sss;
	Eigen::VectorXi fids = Eigen::ArrayXi::Zero(m1);
	for (unsigned int i=0; i < fids.size(); ++i) 
		fids(i) = i;
	if (fids.size() < 50) {
		// three is enough: get median and project it
		fss(0) = fids(0);
		fss(1) = getMostDistantFrameFromExtremes(fids); // median
		fss(2) = fids(fids.size() - 1);
		Eigen::VectorXd mpos = getMedianProjection(fss(1), fids(fids.size() - 1));
		for (unsigned int i=0; i < fids.size(); ++i) 
			if (fss(1) == fids(i)) {
				proj(i, 0) = mpos(0);
				proj(i, 1) = mpos(1);
				proj(i, 3) = 1;
				break;
			}
	} else {
		// more than three: do Force on subset
		sss = fids.size() * 0.08;
		sss = std::floor(sss);
		fss = getForceSubset(fids, sss);
		Eigen::MatrixXd fproj = getForceApproachProjection(fss, 50, 0.125);
		//std::cout << "Force projection" << std::endl << fproj << std::endl;
		sss = 0;
		for (unsigned int i=0; i < fids.size(); ++i) 
			if (fss(sss) == fids(i)) {
				proj.row(i) = fproj.row(sss);
				++sss;
			}
	}
	
	// build remainder projections
	Eigen::MatrixXd lowdim = Eigen::ArrayXXd::Zero(fss.size(), 2);
	sk0 = mc->getFrameSkeleton(0);
	skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
	Eigen::MatrixXd highdim = Eigen::ArrayXXd::Zero(fss.size(), skvec.size());
	for (unsigned int i=0; i < fss.size(); ++i)
		lowdim.row(i) = proj.row(fss(i)).head(2);
	for (unsigned int i=0; i < fss.size(); ++i) {
		sk0 = mc->getFrameSkeleton(fss(i));
		skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
		highdim.row(i) = skvec;
	}
	sss = 0;
	for (unsigned int i=0; i < fids.size(); ++i) {
		if (fids(i) == fss(sss)) {
			++sss;
			continue;
		}
		sk0 = mc->getFrameSkeleton(fids(i));
		skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
		Eigen::VectorXd lvec = getLampProjection(skvec, lowdim, highdim);
		proj(i,0) = lvec(0);
		proj(i,1) = lvec(1);
		proj(i,3) = 1;
	}
	//
	for (unsigned int i=0; i < m2; ++i) {
		sk0 = sib->getFrameSkeleton(i);
		skvec = skdist->getMultidimensionalWeightedPoseVector(sk0);
		Eigen::VectorXd lvec = getLampProjection(skvec, lowdim, highdim);
		proj(i + m1,0) = lvec(0);
		proj(i + m1,1) = lvec(1);
		proj(i + m1,3) = 1;
	}
	// rescale for [0,1] on (x,y)
	//rescaleMarkers(proj);
	return proj;
	
}

/**
 * @brief return a matrix with random elements by a white noise with \mu and \sigma.
 * @param rows number of rows
 * @param cols number of cols
 * @param mu average of the white noise
 * @param sigma standard deviation of the white noise
 * @return random matrix
 */
Eigen::MatrixXd TsneRBFInterpolator::getRandomMatrix(unsigned int rows, unsigned int cols,
	double mu, double sigma) {

	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(rows, cols);
	double max = RAND_MAX;
	double x0, x1;

	std::srand(time(NULL));
	for (unsigned int i=0; i < rows; ++i)
		for (unsigned int j=0; j < cols; ++j) {
			do {
				x0 = (double) std::rand() / max;
			} while (x0 == 0);
			x1 = (double) std::rand() / max;
			ret(i,j) = mu + sigma * std::sqrt(-2 * std::log(x0)) * std::cos(2 * M_PI * x1);
		}
	return ret;
}

/**
 * @brief get the conditional probability vector given a standard deviation. Also, return the perplexity.
 * @param dvec distance vector
 * @param sigma standard deviation multiplier (x * \sigma) where x=1 means \sigma=sqrt(2)/2.
 * @param pivot element in array which conditional probabilities are referenced
 * @param h (output) log of the perplexity
 * @return conditional probability vector
 */
Eigen::VectorXd TsneRBFInterpolator::getProbabilitiesBySigma(Eigen::VectorXd const & dvec,
	double sigma, unsigned int pivot, double *h) {

	Eigen::VectorXd pvec = dvec;

	// calculate probability vector
	for (unsigned int i=0; i < pvec.size(); ++i)
		pvec(i) = std::exp(-1 * sigma * dvec(i));
	pvec(pivot) = 0;
	double sum = pvec.sum();
	pvec /= sum;
	// calculate Shannon entropy
	*h = 0;
	pvec(pivot) = 1; // avoid NaN at std::log()
	for (unsigned int i=0; i < pvec.size(); ++i)
		*h += -1 * pvec(i) * std::log(pvec(i));
	pvec(pivot) = 0;
	return pvec;
}

/**
 * @brief get the pairwise affinity (p_{ij}) matrix given a perplexity value and the distance matrix
 * @param dmat distance matrix
 * @param perplexity given perplexity value
 * @return non-symetric conditional probability matrix
 */
Eigen::MatrixXd TsneRBFInterpolator::getPairwiseAffinities(Eigen::MatrixXd const & dmat,
	double perplexity) {

	Eigen::MatrixXd pijmat = Eigen::ArrayXXd::Zero(dmat.rows(), dmat.rows());
	Eigen::VectorXd beta = Eigen::ArrayXd::Zero(dmat.rows());
	Eigen::VectorXd tmp0, tmp1;
	double bmin = 0, bmax = 0, h, hdiff, logp;
	bool hasMin, hasMax;

	logp = std::log(perplexity);
	for (unsigned int i=0; i < beta.size(); ++i) {
		tmp0 = dmat.row(i);
		beta(i) = 1; // one means $\sigma = 1 / sqrt(2)$ as $beta = 1 / (2 * \sigma^2)$
		tmp1 = getProbabilitiesBySigma(tmp0, beta(i), i, &h);
		hdiff = h - logp;
		hasMin = hasMax = false;
		for (unsigned int j=0; j < 50; ++j)
			if (std::abs(hdiff) <= 0.00001)
				break;
			else {
				if (hdiff > 0) {
					bmin = beta(i);
					hasMin = true;
					if ((hasMin) && (hasMax))
						beta(i) = (beta(i) + bmax) / 2;
					else
						beta(i) *= 2;
				} else {
					bmax = beta(i);
					hasMax = true;
					if ((hasMin) && (hasMax))
						beta(i) = (beta(i) + bmin) / 2;
					else
						beta(i) /= 2;
				}
				tmp1 = getProbabilitiesBySigma(tmp0, beta(i), i, &h);
				hdiff = h - logp;
			}
		pijmat.row(i) = tmp1;
	}
	//std::cout << "beta" << std::endl << beta << std::endl;
	// do p_{ij} = ( p_{ij} + p_{ji} ) / 2*n
	Eigen::MatrixXd ret = pijmat + pijmat.transpose();
	ret /= 2 * pijmat.rows();
	return ret;
}

/**
 * @brief get low dimensional affinities (q_{ij}) given by a t-student statistical distribution
 * @param smat reduced dimension data
 * @return probability matrix
 */
Eigen::MatrixXd TsneRBFInterpolator::getLowDimensionalAffinities(Eigen::MatrixXd const & smat) {
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(smat.rows(), smat.rows());
	Eigen::VectorXd d2vec = Eigen::ArrayXd::Zero(smat.rows());
	Eigen::VectorXd tmp0, tmp1;

	for (unsigned int i=0; i < smat.rows(); ++i) {
		tmp0 = smat.row(i);
		for (unsigned int j=0; j < smat.rows(); ++j) {
			tmp1 = smat.row(j);
			tmp1 -= tmp0;
			d2vec(j) = 1 / (1 + tmp1.dot(tmp1));
		}
		d2vec(i) = 0;
		ret.row(i) = d2vec;
	}
	ret /= ret.colwise().sum().sum();
	return ret;
}

/**
 * @brief get Kullback-Liebler divergence between both affinity matrices as the t-SNE gradient function
 * @param pmat high dimensional affinity matrix
 * @param qmat low dimensional affinity matrix
 * @param ymat low dimensional data
 * @return gradient matrix
 */
Eigen::MatrixXd TsneRBFInterpolator::getKLGradient(Eigen::MatrixXd const & pmat,
	Eigen::MatrixXd const & qmat, Eigen::MatrixXd const & ymat) {

	Eigen::MatrixXd pqmat = pmat - qmat;
	Eigen::MatrixXd dymat = ymat;
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(ymat.rows(), ymat.cols());
	Eigen::VectorXd tmp0, tmp1;

	for (unsigned int i=0; i < ymat.rows(); ++i) {
		tmp0 = ymat.row(i);
		for (unsigned int j=0; j < ymat.rows(); ++j) {
			tmp1 = ymat.row(j);
			tmp1 = tmp0 - tmp1;
			tmp1 /= (1 + tmp1.dot(tmp1));
			dymat.row(j) = tmp1;
		}
		tmp0 = pqmat.col(i);
		ret.row(i) = tmp0.transpose() * dymat;
	}
    ret *= 4;
	return ret;
}

/**
 * @brief calculate the Kullback-Lieber divergence between two conditional
 * probabilities as the error on multidimensional projection
 * @param pmat high dimensional affinity matrix
 * @param qmat low dimensional afifnity matrix
 * @return Kullback-Lieber divergence
 */
double TsneRBFInterpolator::getKLDivergence(Eigen::MatrixXd const & pmat,
	Eigen::MatrixXd const & qmat) {

    Eigen::MatrixXd cmat = qmat;
    for (unsigned int i=0; i < cmat.rows(); ++i)
        cmat(i,i) = 1;
    for (unsigned int i=0; i < cmat.rows(); ++i)
        for (unsigned int j=0; j < cmat.cols(); ++j)
            cmat(i,j) = pmat(i,j) / cmat(i,j);
    for (unsigned int i=0; i < cmat.rows(); ++i)
        cmat(i,i) = 1;
    for (unsigned int i=0; i < cmat.rows(); ++i)
        for (unsigned int j=0; j < cmat.cols(); ++j)
            cmat(i,j) = pmat(i,j) * std::log(cmat(i,j));
    return cmat.colwise().sum().sum();
}

/**
 * @brief get gradient vector applied by local gain adaptation
 * @param grad input gradient vector
 * @param gains gain vector (output)
 * @param deltaY last variation on Y data
 * @param ming minumum gain for gain vector
 * @return gradient vector with gain applied
 */
Eigen::MatrixXd TsneRBFInterpolator::applyAndUpdateGain(Eigen::MatrixXd const & grad,
	Eigen::MatrixXd & gains, Eigen::MatrixXd const & deltaY, double ming) {

    Eigen::MatrixXd ret = grad;
    for (unsigned int i=0; i < ret.rows(); ++i)
        for (unsigned int j=0; j < ret.cols(); ++j) {
            ret(i,j) = deltaY(i,j) * grad(i,j);
            // test if both gradient and delta have the same signal
            if (ret(i,j) > 0) ret(i,j) = gains(i,j) * 0.8;
            else ret(i,j) = gains(i,j) + 0.2;
            if (ret(i,j) < ming) ret(i,j) = ming;
            gains(i,j) = ret(i,j);
            ret(i,j) *= grad(i,j);
        }
    return ret;
}

/**
 * @brief centralize matrix with rowwise data
 * @param data input matrix
 */
void TsneRBFInterpolator::centralizeData(Eigen::MatrixXd & data) {
    Eigen::VectorXd avg = data.colwise().sum() / data.rows();
    for (unsigned int i=0; i < data.rows(); ++i)
        data.row(i) -= avg;
}

/**
 * @brief get low dimensional (2D) representation by
 * t-distributed Stochastic Neighborhood Embedding projecion strategy
 * @param pij high dimensional affinity matrix
 * @param ymat low dimensional initial value
 * @param train number of training iterations
 * @param learn learning coefficient for gradient descendant
 * @param momentum momentum coefficient for gradient descendant
 * @return low dimensional data
 */
Eigen::MatrixXd TsneRBFInterpolator::getTsneDimensionReduction(Eigen::MatrixXd const & pij,
	Eigen::MatrixXd const & ymat, unsigned int train, double learn, double momentum) {

    Eigen::MatrixXd ret = ymat;
    Eigen::MatrixXd yprev = Eigen::ArrayXXd::Constant(ymat.rows(), ymat.cols(), 1);
    Eigen::MatrixXd yyprev = Eigen::ArrayXXd::Constant(ymat.rows(), ymat.cols(), 1);
    Eigen::MatrixXd gains = Eigen::ArrayXXd::Constant(ymat.rows(), ymat.cols(), 1);
	Eigen::MatrixXd qij = getLowDimensionalAffinities(ret);
    Eigen::MatrixXd eepij = 4*pij;
	Eigen::MatrixXd grad = getKLGradient(eepij, qij, ret);
    unsigned int change0 = train * 0.1;
    unsigned int change1 = train * 0.25;

    for (unsigned int i=0; i < change0; ++i) {
        ret = yprev - learn * grad + momentum * (yprev - yyprev);
        centralizeData(ret);
        qij = getLowDimensionalAffinities(ret);
	    grad = getKLGradient(eepij, qij, ret);
        yyprev = yprev - yyprev;
	    grad = applyAndUpdateGain(grad, gains, yyprev);
        yyprev = yprev;
        yprev = ret;
    }
    eepij /= 4;
    for (unsigned int i=change0; i < change1; ++i) {
        ret = yprev - learn * grad + momentum * (yprev - yyprev);
        centralizeData(ret);
        qij = getLowDimensionalAffinities(ret);
	    grad = getKLGradient(eepij, qij, ret);
        yyprev = yprev - yyprev;
	    grad = applyAndUpdateGain(grad, gains, yyprev);
        yyprev = yprev;
        yprev = ret;
    }
    momentum += 0.3;
    for (unsigned int i=change1; i < train; ++i) {
        ret = yprev - learn * grad + momentum * (yprev - yyprev);
        centralizeData(ret);
        qij = getLowDimensionalAffinities(ret);
	    grad = getKLGradient(eepij, qij, ret);
        yyprev = yprev - yyprev;
	    grad = applyAndUpdateGain(grad, gains, yyprev);
        yyprev = yprev;
        yprev = ret;
    }
    //double error = getKLDivergence(pij, qij);
    //std::cout << "t-SNE error = " << error << std::endl;
    Eigen::MatrixXd ret4d = Eigen::ArrayXXd::Zero(ret.rows(), 4);
    ret4d.block(0, 0, ret.rows(), ret.cols()) = ret;
    for (unsigned int i=0; i < ret4d.rows(); ++i)
		ret4d(i,3) = 1;
    return ret4d;
}

/**
 * @brief get 2D projection matrix merged with a given mocap session.
 * @param sib sibling mocap session
 * @return 2D projection matrix
 */
Eigen::MatrixXd TsneRBFInterpolator::getMerged2DProjection(MocapReader * sib) {

	Eigen::MatrixXd dmat = getMergedDistanceMatrix(sib);
	Eigen::MatrixXd pij = getPairwiseAffinities(dmat);
	Eigen::MatrixXd ymat = getRandomMatrix(pij.rows(), 2);
	return getTsneDimensionReduction(pij, ymat);
}

/**
 * @brief get markers position for given frame ids with t-SNE strategy for
 * spreading markers on a 2D space.
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd TsneRBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {

	if (rdframes.rows() > 0)
		return getSelectedRdframes(fids);

	assert(fids.size() > 1);
	// simplest case
	if (fids.size() == 2) {
		Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(fids.size(), 4);
		ret(0,3) = 1;
		ret(1,0) = 1;
		ret(1,1) = 1;
		ret(1,3) = 1;
		return ret;
	}
	Eigen::MatrixXd dmat = getDistanceMatrix(fids);
	Eigen::MatrixXd pij = getPairwiseAffinities(dmat, 50.0); // perplexity = 50
	Eigen::MatrixXd ymat = getRandomMatrix(pij.rows(), 2);
	return getTsneDimensionReduction(pij, ymat);
}
