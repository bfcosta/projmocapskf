#include <asfreader.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <sstream>

/**
 * cerates a new ASF reader.
 */
ASFReader::ASFReader() {}

/**
 * \param fname filename to be read
 * 
 * method for interfacing file format reading
 */
void ASFReader::readFile(const std::string & fname) {
	std::string ext = fname.substr(fname.length() - 3, 3);
	
	if (ext.compare("asf") == 0) readAsf(fname);
	else readAmc(fname);
}

/**
 * \param fname filename to be read
 * 
 * read animation file in AMC format  and load its temporary data structure.
 */
void ASFReader::readAmc(const std::string & fname) {
	std::ifstream myfile;
	std::string buf,line;
	std::istringstream ss;
	std::vector<std::string> tokens;
	std::vector<float> vtmp;
	struct amc tmp;
	bool emptyFile = true;
	
	degrees=false;
	myfile.open(fname.c_str());
	if (myfile.is_open()) {
		while (std::getline(myfile, line)) {
			ss.str(line);
			while (ss >> buf) tokens.push_back(buf);
			if (tokens.at(0).compare("#") == 0) {
				// comment
			} else if (tokens.at(0).compare(":DEGREES") == 0) {
				degrees = true;
			} else if (tokens.at(0).compare(":FULLY-SPECIFIED") == 0) {
				// jump
			} else {
				if (tokens.size() == 1) {
					int i = atoi(tokens.at(0).c_str());
					if (i > 1) motion.push_back(tmp);
					tmp.nameref.clear();
					tmp.bhandler.clear();
				} else {
					tmp.nameref.push_back(getBoneId(tokens.at(0)));
					vtmp.clear();
					for (unsigned int i=1; i < tokens.size(); ++i) {
						vtmp.push_back(atof(tokens.at(i).c_str()));
					}
					tmp.bhandler.push_back(vtmp);
				}
			}
			tokens.clear();
			ss.clear();
			emptyFile = false;
		}
		motion.push_back(tmp);
		myfile.close();
		if (!emptyFile) findAmc2AsfJointAssociation(0);
	} else
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
}

/**
 * \param fname filename to be read
 * 
 * read skeleton file on ASF format and load its temporary data structure.
 */
void ASFReader::readAsf(const std::string & fname) {
	enum Section { ROOT, HEADER, NEWBONE, NEWHIER, BDATA, HDATA };
	Section st = HEADER;
    std::ifstream myfile;
	std::string buf,line;
	std::vector<std::string> tokens;
	std::istringstream ss;
	struct tripleb tmpdof;
	struct rtriplef lim;
	unsigned int ll = 0;
	bool hasLimit = false;
	
	myfile.open(fname.c_str());
	tmpdof.rx = tmpdof.ry = tmpdof.rz = false;
	lim.ix = lim.ax = lim.iy = lim.ay = lim.iz = lim.az = 0;
	mblen = 0;
	if (myfile.is_open()) {
        while (std::getline(myfile, line)) {
			ss.str(line);
			while (ss >> buf) tokens.push_back(buf);
			if (tokens.at(0).compare("#") == 0) {
				// ignore comment

			} else if (tokens.at(0).compare(":version") == 0) {
				st = HEADER;

			} else if (tokens.at(0).compare(":name") == 0) {
				st = HEADER;

			} else if (tokens.at(0).compare(":units") == 0) {
				st = HEADER;

			} else if (tokens.at(0).compare(":documentation") == 0) {
				st = HEADER;

			} else if (tokens.at(0).compare(":root") == 0) {
				st = ROOT;

			} else if (tokens.at(0).compare(":bonedata") == 0) {
				st = NEWBONE;
			
			} else if (tokens.at(0).compare(":hierarchy") == 0) {
				st = NEWHIER;

			} else if (tokens.at(0).compare("begin") == 0) {
				if (st == NEWHIER) {
					st = HDATA;
					parent.resize(id.size(), 0);
				} else if (st == NEWBONE) {
					st = BDATA;
					hasLimit = false;
				}

			} else if (tokens.at(0).compare("end") == 0) {
				if (st == HDATA) st = HEADER;
				else if (st == BDATA) {
					st = NEWBONE;
					limit.push_back(lim);
					unbounded.push_back(!hasLimit);
					dof.push_back(tmpdof);
					tmpdof.rx = tmpdof.ry = tmpdof.rz = false;
					lim.ix = lim.ax = lim.iy = lim.ay = lim.iz = lim.az = 0;
					
				} else std::cout << "couldn't find begin" << std::endl;

			} else {
				if (st == BDATA) {
					if (tokens.at(0).compare("id") == 0) {
						id.push_back(atoi(tokens.at(1).c_str()));

					} else if (tokens.at(0).compare("name") == 0) {
						name.push_back(tokens.at(1));
					
					} else if (tokens.at(0).compare("direction") == 0) {
						struct triplef tmp;
						tmp.x = atof(tokens.at(1).c_str());
						tmp.y = atof(tokens.at(2).c_str());
						tmp.z = atof(tokens.at(3).c_str());
						direction.push_back(tmp);
					
					} else if (tokens.at(0).compare("length") == 0) {
						length.push_back(atof(tokens.at(1).c_str()));
						if (mblen < length.back()) mblen = length.back();
					
					} else if (tokens.at(0).compare("axis") == 0) {
						struct triplef tmp;
						float t;
						std::string s = tokens.at(4);
						for (unsigned int i=0; i < s.size(); ++i) {
							t = atof(tokens.at(i+1).c_str());
							if (s.at(i) == 'X') tmp.x = t;
							else if (s.at(i) == 'Y') tmp.y = t;
							else tmp.z = t;
						}
						axis.push_back(tmp);
					
					} else if (tokens.at(0).compare("dof") == 0) {
						for (unsigned int i=1; i < tokens.size(); ++i) {
							if (tokens.at(i).compare("rx") == 0) tmpdof.rx = !tmpdof.rx;
							else if (tokens.at(i).compare("ry") == 0) tmpdof.ry = !tmpdof.ry;
							else tmpdof.rz = !tmpdof.rz;
						}
					
					} else {
						// limits
						float i,a;
						if (tokens.at(0).compare("limits") == 0) {
							ll = 0;
							tokens.at(1).erase(0);
							tokens.at(2).erase(tokens.at(2).size() - 1);
							i = atof(tokens.at(1).c_str());
							a = atof(tokens.at(2).c_str());
							hasLimit = true;
						} else {
							tokens.at(0).erase(0);
							tokens.at(1).erase(tokens.at(1).size() - 1);
							i = atof(tokens.at(0).c_str());
							a = atof(tokens.at(1).c_str());
						}
						if (ll == 0) {
							if (dof.at(dof.size() - 1).rx) {
								lim.ix = i;
								lim.ax = a;
							} else if (dof.at(dof.size() - 1).ry) {
								lim.iy = i;
								lim.ay = a;
							} else {
								lim.iz = i;
								lim.az = a;
							}
						} else if (ll == 1) {
							if (dof.at(dof.size() - 1).ry) {
								lim.iy = i;
								lim.ay = a;
							} else {
								lim.iz = i;
								lim.az = a;
							}
						} else {
							lim.iz = i;
							lim.az = a;
						}
						++ll;					
					}

				} else if (st == HDATA) {
					unsigned int k,i;
					k = getBoneId(tokens.at(0));
					for (unsigned int j=1; j < tokens.size(); ++j) {
						i = getBoneId(tokens.at(j));
						parent.at(i-1) = k;
					}
				} else if (st == ROOT) {
					if (tokens.at(0).compare("position") == 0) {
						rootpos.x = atof(tokens.at(1).c_str());
						rootpos.y = atof(tokens.at(2).c_str());
						rootpos.z = atof(tokens.at(3).c_str());
					} else if (tokens.at(0).compare("orientation") == 0) {
						rootor.x = atof(tokens.at(1).c_str());
						rootor.y = atof(tokens.at(2).c_str());
						rootor.z = atof(tokens.at(3).c_str());						
					} else if (tokens.at(0).compare("axis") == 0) {
						strncpy(axorder, tokens.at(1).c_str(), 3);
					} else if (tokens.at(0).compare("order") == 0) {
						for (unsigned int j=1; (j < tokens.size()) && (j < 7); ++j) {
							strncpy(rootdof[j-1], tokens.at(j).c_str(), 2);
						}	
					}
				}
			}
			tokens.clear();
			ss.clear();
		}
		myfile.close();
		setAxisCorrection();
	} else
		std::cout << "Erro ao tentar abrir o arquivo " << fname << std::endl;
}

/**
 * \param s a valid bone name
 * \return id of the bone
 * 
 * get bone id by its name. 0 is always root. All others have the name
 * vector index plus one.
 */
unsigned int ASFReader::getBoneId(const std::string & s) {
	unsigned int i=0;
	
	if (s.compare("root") == 0) return i;
	else for (; i < name.size(); ++i)
		if (s.compare(name.at(i)) == 0) break;
	return ++i;
}

/**
 * prints loaded structure to std output for debugging purposes.
 */
void ASFReader::print() {
	std::cout << "Total attribs" << std::endl;
	std::cout << "name = " << name.size() << std::endl;
	std::cout << "id = " << id.size() << std::endl;
	std::cout << "parent = " << parent.size() << std::endl;
	std::cout << "direction = " << direction.size() << std::endl;
	std::cout << "unbounded = " << unbounded.size() << std::endl;
	std::cout << "limit = " << limit.size() << std::endl;
	std::cout << "axis = " << axis.size() << std::endl;
	std::cout << "dof = " << dof.size() << std::endl;
	std::cout << "length = " << length.size() << std::endl;
	std::cout << "motion = " << motion.size() << std::endl;
	std::cout << "************" << std::endl;
	std::cout << "root" << std::endl;
	std::cout << "position = (" << rootpos.x << ", " << rootpos.y << 
		", " << rootpos.z << ")" << std::endl;
	std::cout << "orientation = (" << rootor.x << ", " << rootor.y << 
		", " << rootor.z << ")" << std::endl;
	std::cout << "axis order = " << std::string(axorder,3) << std::endl;
	std::cout << "root dof = " << std::string(rootdof[0],2) << " " <<
		std::string(rootdof[1],2) << " " << std::string(rootdof[2],2) << " " <<
		std::string(rootdof[3],2) << " " << std::string(rootdof[4],2) << " " <<
		std::string(rootdof[5],2) << std::endl;
	std::cout << "************" << std::endl;
	std::cout << "bones" << std::endl;
	for (unsigned int i=0; i < id.size(); ++i) {
		std::cout << "************" << std::endl;
		std::cout << "name = " << name.at(i) << std::endl;
		std::cout << "id = " << id.at(i) << std::endl;
		std::cout << "parent = " << parent.at(i) << std::endl;
		std::cout << "unbounded = " << unbounded.at(i) << std::endl;
		std::cout << "direction = (" << direction.at(i).x << ", " << 
			direction.at(i).y << ", " << direction.at(i).z << ")" << std::endl;
		std::cout << "length = " << length.at(i) << std::endl;
		std::cout << "axis = (" << axis.at(i).x << ", " << axis.at(i).y << 
			", " << axis.at(i).z << ")" << std::endl;
		std::cout << "dof = (" << dof.at(i).rx << ", " << dof.at(i).ry << 
			", " << dof.at(i).rz << ")" << std::endl;
		std::cout << "limit = (" << limit.at(i).ix << " "<<limit.at(i).ax << 
			") (" << limit.at(i).iy << " " << limit.at(i).ay << ") ("<<
			limit.at(i).iz << " " << limit.at(i).az << ")" << std::endl;
	}
	std::cout << "************" << std::endl;
	std::cout << "motion" << std::endl;
	for (unsigned int i=0; i < motion.size(); ++i) {
		for (unsigned int j=0; j < motion.at(i).nameref.size(); ++j) {
			if (motion.at(i).nameref.at(j) == 0)
				std::cout << "root: ";
			else
				std::cout << name.at(motion.at(i).nameref.at(j) - 1) << ": ";
			for (unsigned int k=0; k < motion.at(i).bhandler.at(j).size(); ++k) 
				std::cout << " " << motion.at(i).bhandler.at(j).at(k);
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
}

/**
 * calculates conversion matrices for local to global axis conversion and
 * its inverse. 
 */
void ASFReader::setAxisCorrection() {
	Eigen::Matrix4d mx, my, mz;
	Eigen::Vector3d xdir = Eigen::Vector3d::UnitX();
	Eigen::Vector3d ydir = Eigen::Vector3d::UnitY();
	Eigen::Vector3d zdir = Eigen::Vector3d::UnitZ();
	double rx, ry, rz, deg2rad;

	world2local.resize(axis.size() + 1);
	local2world.resize(axis.size() + 1);
	deg2rad = M_PI / 180;
	// root
	rx = rootor.x * deg2rad;
	ry = rootor.y * deg2rad;
	rz = rootor.z * deg2rad;
	mx = Eigen::Affine3d(Eigen::AngleAxisd(rx, xdir)).matrix();
	my = Eigen::Affine3d(Eigen::AngleAxisd(ry, ydir)).matrix();
	mz = Eigen::Affine3d(Eigen::AngleAxisd(rz, zdir)).matrix();
	world2local.at(0) = mz * my * mx;
	rx = -1 * rx;
	ry = -1 * ry;
	rz = -1 * rz;
	mx = Eigen::Affine3d(Eigen::AngleAxisd(rx, xdir)).matrix();
	my = Eigen::Affine3d(Eigen::AngleAxisd(ry, ydir)).matrix();
	mz = Eigen::Affine3d(Eigen::AngleAxisd(rz, zdir)).matrix();
	local2world.at(0) = mx * my * mz;
	// bones
	for (unsigned int i=0; i < axis.size(); ++i) {
		rx = axis.at(i).x * deg2rad;
		ry = axis.at(i).y * deg2rad;
		rz = axis.at(i).z * deg2rad;
		mx = Eigen::Affine3d(Eigen::AngleAxisd(rx, xdir)).matrix();
		my = Eigen::Affine3d(Eigen::AngleAxisd(ry, ydir)).matrix();
		mz = Eigen::Affine3d(Eigen::AngleAxisd(rz, zdir)).matrix();
		world2local.at(i + 1) = mz * my * mx;
		rx = -1 * rx;
		ry = -1 * ry;
		rz = -1 * rz;
		mx = Eigen::Affine3d(Eigen::AngleAxisd(rx, xdir)).matrix();
		my = Eigen::Affine3d(Eigen::AngleAxisd(ry, ydir)).matrix();
		mz = Eigen::Affine3d(Eigen::AngleAxisd(rz, zdir)).matrix();
		local2world.at(i + 1) = mx * my * mz;
	}
}

/**
 * \return skeleton on its resting pose
 * 
 * returns the skeleton on its resting pose
 */
Skeleton ASFReader::getSkeleton() {
	Skeleton skel(mblen / lcratio);
	Eigen::Matrix4d trans;
	double tx, ty, tz, deg2rad;
	
	if (degrees) deg2rad = M_PI / 180; else deg2rad = 1;
	skel.setRoot(rootpos.x, rootpos.y, rootpos.z, "root");
	skel.setOrientation(rootor.x * deg2rad, rootor.y * deg2rad, 
		rootor.z * deg2rad, "ZYX");
	for (unsigned int i=0; i < direction.size(); ++i) {
		tx = direction.at(i).x * length.at(i);
		ty = direction.at(i).y * length.at(i);
		tz = direction.at(i).z * length.at(i);
		trans = Eigen::Affine3d(Eigen::Translation3d(tx, ty, tz)).matrix();
		skel.addJoint(trans, Eigen::Matrix4d::Identity(), parent.at(i), name.at(i));
	}	
	return skel;
}

/**
 * \param a valid frame id
 * \return skeleton pose for the given id
 * 
 * returns the skeleton pose for a given frame id.
 */
Skeleton ASFReader::getFrameSkeleton(unsigned int id) {
	Skeleton skel(mblen / lcratio);
	double tx, ty, tz, rx, ry, rz, deg2rad;
	Eigen::Matrix4d trans, rot;
	unsigned int ref;
	struct amc fr = motion.at(id);
	bool dx, dy, dz;
	
	if (degrees) deg2rad = M_PI / 180; else deg2rad = 1;
	//std::cout << "current frame is " << id << std::endl;
	// root
	ref = drefs.at(0);
	tx = fr.bhandler.at(ref).at(0);
	ty = fr.bhandler.at(ref).at(1);
	tz = fr.bhandler.at(ref).at(2);
	trans = getGlobalOffsetMatrix(tx, ty, tz);
	//trans = getLocalOffsetMatrix(tx, ty, tz, 0);
	skel.setRoot(trans, "root");
	rx = fr.bhandler.at(ref).at(3) * deg2rad;
	ry = fr.bhandler.at(ref).at(4) * deg2rad; 
	rz = fr.bhandler.at(ref).at(5) * deg2rad;
	rot = getGlobalRotationMatrix(rx, ry, rz, 0);
	//rot = world2local.at(0) * getLocalRotationMatrix(rx, ry, rz);
	skel.setOrientation(rot);
	for (unsigned int i=0; i < parent.size(); ++i) {
		ref = drefs.at(i + 1);
		// bones
		tx = direction.at(i).x * length.at(i);
		ty = direction.at(i).y * length.at(i);
		tz = direction.at(i).z * length.at(i);
		trans = getGlobalOffsetMatrix(tx, ty, tz);
		//trans = getLocalOffsetMatrix(tx, ty, tz, i + 1);
		rx = ry = rz = 0;
		dx = dof.at(i).rx;
		dy = dof.at(i).ry;
		dz = dof.at(i).rz;
		if (ref < fr.nameref.size()) {
			for (unsigned int j=0; j < fr.bhandler.at(ref).size(); ++j) {
				if (dx) {
					rx = fr.bhandler.at(ref).at(j) * deg2rad;
					dx = ! dx;
				} else if (dy) {
					ry = fr.bhandler.at(ref).at(j) * deg2rad;
					dy = ! dy;
				} else if (dz) {
					rz = fr.bhandler.at(ref).at(j) * deg2rad;
					dz = ! dz;
				} 
			}
		}
		//rot = local2world.at(parent.at(i)) * world2local.at(i + 1) *
		//		getLocalRotationMatrix(rx, ry, rz);
		rot = getGlobalRotationMatrix(rx, ry, rz, i + 1);
		skel.addJoint(trans, rot, parent.at(i), name.at(i));
	}
	return skel;
}

/**
 * \param dofs
 * \param euler
 * \return skeleton pose at the specified frame
 * 
 * returns a skeleton pose for a given configuration
 */
Skeleton ASFReader::getFrameSkeleton(Eigen::VectorXd dofs, bool euler) {
	Skeleton skel(mblen / lcratio);
	double tx, ty, tz, rx, ry, rz, deg2rad;
	Eigen::Matrix4d trans, rot;
	bool dx, dy, dz;
	
	if (degrees) deg2rad = M_PI / 180; else deg2rad = 1;
	if (!euler) {
		std::cout << "to do quaternion conversion" << std::endl;
	}
	// root joint
	tx = dofs(0);
	ty = dofs(1);
	tz = dofs(2);
	trans = getGlobalOffsetMatrix(tx, ty, tz);
	skel.setRoot(trans, "root");
	rx = dofs(3) * deg2rad;
	ry = dofs(4) * deg2rad; 
	rz = dofs(5) * deg2rad;
	rot = getGlobalRotationMatrix(rx, ry, rz, 0);
	skel.setOrientation(rot);
	// other joints
	for (unsigned int i=0; i < parent.size(); ++i) {
		tx = direction.at(i).x * length.at(i);
		ty = direction.at(i).y * length.at(i);
		tz = direction.at(i).z * length.at(i);
		trans = getGlobalOffsetMatrix(tx, ty, tz);
		rx = ry = rz = 0;
		dx = dof.at(i).rx;
		dy = dof.at(i).ry;
		dz = dof.at(i).rz;
		for (unsigned int j=0; j < 3; ++j) {
			if (dx) {
				rx = dofs(3*i + 6 + j) * deg2rad;
				dx = ! dx;
			} else if (dy) {
				ry = dofs(3*i + 6 + j) * deg2rad;
				dy = ! dy;
			} else if (dz) {
				rz = dofs(3*i + 6 + j) * deg2rad;
				dz = ! dz;
			} 
		}
		rot = getGlobalRotationMatrix(rx, ry, rz, i + 1);
		skel.addJoint(trans, rot, parent.at(i), name.at(i));
	}
	return skel;
}

/**
 * @brief translate joint ids order in amc file to order in asf file.
 */
void ASFReader::findAmc2AsfJointAssociation(unsigned int fid) {
	struct amc frline = motion.at(fid);
	
	for (unsigned int i=0; i < id.size() + 1; ++i) {
		drefs.push_back(frline.nameref.size());
		for (unsigned int j=0; j < frline.nameref.size(); ++j) {
			if (i == frline.nameref.at(j)) {
				drefs.at(i) = j;
				break;
			}
		}
	}
}

/**
 * \param rx rotation angle for X direction. Values should be given on radians.
 * \param ry rotation angle for Y direction. Values should be given on radians.
 * \param rz rotation angle for Z direction. Values should be given on radians.
 * \return a rotation matrix on global axis coordinate system build with the ZYX order.
 * \param i bone recerence id
 * \return
 * 
 * returns the global axis rotation matrix for the given triple of
 * angles and bone id reference.
 */
Eigen::Matrix4d ASFReader::getGlobalRotationMatrix(double rx, double ry,
		double rz, unsigned int i) {
	Eigen::Vector3d xdir = Eigen::Vector3d::UnitX();
	Eigen::Vector3d ydir = Eigen::Vector3d::UnitY();
	Eigen::Vector3d zdir = Eigen::Vector3d::UnitZ();
	Eigen::Matrix4d tmp, mx, my, mz;

	mx = Eigen::Affine3d(Eigen::AngleAxisd(rx, xdir)).matrix();
	my = Eigen::Affine3d(Eigen::AngleAxisd(ry, ydir)).matrix();
	mz = Eigen::Affine3d(Eigen::AngleAxisd(rz, zdir)).matrix();
	tmp = world2local.at(i) * mz * my * mx * local2world.at(i);
	return tmp;	
}

/**
 * \param rx rotation angle for X direction. Values should be given on radians.
 * \param ry rotation angle for Y direction. Values should be given on radians.
 * \param rz rotation angle for Z direction. Values should be given on radians.
 * \return a rotation matrix on local axis coordinate system build with the ZYX order.
 * 
 * returns the local axis rotation matrix for the given triple of angles.
 */
Eigen::Matrix4d ASFReader::getLocalRotationMatrix(double rx, double ry,
		double rz) {
	Eigen::Vector3d xdir = Eigen::Vector3d::UnitX();
	Eigen::Vector3d ydir = Eigen::Vector3d::UnitY();
	Eigen::Vector3d zdir = Eigen::Vector3d::UnitZ();
	Eigen::Matrix4d tmp, mx, my, mz;

	mx = Eigen::Affine3d(Eigen::AngleAxisd(rx, xdir)).matrix();
	my = Eigen::Affine3d(Eigen::AngleAxisd(ry, ydir)).matrix();
	mz = Eigen::Affine3d(Eigen::AngleAxisd(rz, zdir)).matrix();
	tmp = mz * my * mx;
	return tmp;	
}

/**
 * \param tx translation offset for X direction
 * \param ty translation offset for Y direction
 * \param tz translation offset for Z direction
 * \return a translation matrix on global axis
 * 
 * retorna a matriz de trasformacao global de offset em coordenada global.
 */
Eigen::Matrix4d ASFReader::getGlobalOffsetMatrix(double tx, double ty, 
		double tz) {
	Eigen::Matrix4d trans = Eigen::Affine3d(
		Eigen::Translation3d(tx, ty, tz)).matrix();
	return trans;
}

/**
 * \param tx translation offset for X direction on global axis
 * \param ty translation offset for Y direction on global axis
 * \param tz translation offset for Z direction on global axis
 * \param i bone index
 * \return a translation matrix on local axis of given bone
 * 
 * return the translation matrix on local axis coordinate system for 
 * the offset triple given on global axis coordinate system
 */
Eigen::Matrix4d ASFReader::getLocalOffsetMatrix(double tx, double ty, 
		double tz, unsigned int i) {
	Eigen::Matrix4d trans;
	Eigen::Vector4d v;

	v(0) = tx;
	v(1) = ty;
	v(2) = tz;
	v(3) = 1.0;	
	v = local2world.at(i) * v;
	trans = Eigen::Affine3d(Eigen::Translation3d(v(0), v(1), v(2))).matrix();
	return trans;
}

/**
 * \return time frame interval on milisseconds
 * 
 * return the time interval on milisseconds between two consecutive frames
 */
unsigned int ASFReader::getFrameInterval() {
	return 8; // FPS = 125
}

/**
 * \return number of DOFs in animated skeleton
 * 
 * get number of DOFs in animated skeleton
 */
unsigned int ASFReader::getNumberOfDOFs() {
	return id.size()+1;
}

/**
 * \return total of frames in the animation
 * 
 * returns the number of frames in the animation file.
 */
unsigned int ASFReader::getNumberOfFrames() {
	return motion.size();
}

/**
 * \return mocap DOF matrix with loaded mocap data
 * 
 * dump all frames as one matrix. Rows are frames and columns are 
 * skeleton DOFs. Joint data are described as euler angles.
 */
Eigen::MatrixXd ASFReader::getEulerAnglesDOFMatrix() {
	
	unsigned int nframes = getNumberOfFrames();
	unsigned int ndofs = 3*(id.size() + 2);
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(nframes, ndofs);
	struct amc frline;
	std::vector<float> dofs;
	unsigned int jref;
	bool dx, dy, dz;
	
	// frames
	for (unsigned int i=0; i < nframes; ++i) {
		frline = motion.at(i);
		// root joint
		jref = drefs.at(0);
		dofs = frline.bhandler.at(jref);
		for (unsigned int k=0; k < dofs.size(); ++k) 
			ret(i,k) = dofs.at(k);
		// other joints
		for (unsigned int j=0; j < parent.size(); ++j) {
			jref = drefs.at(j+1);
			if (jref < frline.bhandler.size()) {
				dofs = frline.bhandler.at(jref);
				// others
				dx = dof.at(j).rx;
				dy = dof.at(j).ry;
				dz = dof.at(j).rz;
				for (unsigned int k=0; k < dofs.size(); ++k) {
					if (dx) {
						ret(i, 3*j + 6 + k) = dofs.at(k);
						dx = ! dx;
					} else if (dy) {
						ret(i, 3*j + 6 + k) = dofs.at(k);
						dy = ! dy;
					} else if (dz) {
						ret(i, 3*j + 6 + k) = dofs.at(k);
						dz = ! dz;
					} 
				}
			}
		}
	}
	return ret;
}

/**
 * \return mocap DOF matrix with loaded mocap data
 * 
 * dump all frames as one matrix. Rows are frames and columns are 
 * skeleton DOFs. Joint data are described as quaternions.
 */
Eigen::MatrixXd ASFReader::getQuaternionDOFMatrix() {
	unsigned int nframes = getNumberOfFrames();
	unsigned int ndofs = 4*(id.size() + 1) + 3;
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(nframes, ndofs);
	Skeleton sk;
	std::vector<Eigen::Matrix4d> skjoints;
	Eigen::Matrix3d rot;
	Eigen::Vector4d rpos;
	
	for (unsigned int i=0; i < nframes; ++i) {
		sk = getFrameSkeleton(i);
		rpos = sk.getJointPosition(0);
		ret(i,0) = rpos(0);
		ret(i,1) = rpos(1);
		ret(i,2) = rpos(2);
		rot = sk.getRootOrientation().block(0,0,3,3);
		Eigen::Quaterniond qroot(rot);
		ret(i,3) = qroot.x();
		ret(i,4) = qroot.y();
		ret(i,5) = qroot.z();
		ret(i,6) = qroot.w();
		skjoints = sk.getBoneMatrices();
		for (unsigned int j=0, k=7; j < skjoints.size(); ++j, k += 4) {
			rot = skjoints.at(j).block(0,0,3,3);
			Eigen::Quaterniond qbone(rot);
			ret(i,k) = qbone.x();
			ret(i,k+1) = qbone.y();
			ret(i,k+2) = qbone.z();
			ret(i,k+3) = qbone.w();
		}
	}
	return ret;
}

/**
 * @brief print skeleton pose as a amc frame in file
 * @param sk skeleton pose
 * @param ost output stream to print
 */
void ASFReader::printPose(Skeleton & sk, std::ostream & ost) {

	Eigen::Matrix4d tmat;
	Eigen::Vector3d tvec;
	std::vector<Eigen::Matrix4d> bvec = sk.getBoneMatrices();
	std::vector<std::string> bnames = sk.getBoneNames();
	unsigned int boneid;

    tmat = sk.getSkeletonRootTranslation();
    ost << bnames.at(0) << " " << tmat(0,3) << " " << tmat(1,3) << " " << tmat(2,3);
	tmat = sk.getRootOrientation();
	tmat = local2world.at(0) * tmat * world2local.at(0);
	tvec = getZYXRotation(tmat);
	ost << " " << tvec(0) << " " << tvec(1) << " " << tvec(2) << std::endl;
	//for (unsigned int i=1; i < bnames.size(); ++i) {
	//	boneid = i - 1;
	for (unsigned int j=1; j < motion.at(0).nameref.size(); ++j) {
		boneid = motion.at(0).nameref.at(j) - 1;

		tmat = bvec.at(boneid);
		tmat = local2world.at(boneid + 1) * tmat * world2local.at(boneid + 1);
		tvec = getZYXRotation(tmat);
		if (!(dof.at(boneid).rx || dof.at(boneid).ry || dof.at(boneid).rz)) continue;
		ost << bnames.at(boneid + 1);
		if (dof.at(boneid).rx) ost << " " << tvec(0);
		if (dof.at(boneid).ry) ost << " " << tvec(1);
		if (dof.at(boneid).rz) ost << " " << tvec(2);
		ost << std::endl;
	}
}

/**
 * @brief get a DOF vector from a skeleton pose. To be tested.
 * @param sk skeleton pose
 * @return DOF vector
 */
Eigen::VectorXd ASFReader::getEulerAnglesDOFPose(Skeleton & sk) {
	
	Eigen::Vector3d tvec;
	Eigen::Vector4d jroot = sk.getJointPosition(0);
    Eigen::Matrix4d skrot = sk.getRootOrientation();
    std::vector<Eigen::Matrix4d> brot = sk.getBoneMatrices();
    Eigen::VectorXd vpose = Eigen::ArrayXd::Zero(brot.size()*3 + 6);
	//bool dx, dy, dz;

	vpose(0) = jroot(0);
	vpose(1) = jroot(1);
	vpose(2) = jroot(2);
	tvec = getZYXRotation(skrot);
	vpose(3) = tvec(0);
	vpose(4) = tvec(1);
	vpose(5) = tvec(2);
	for (unsigned int i=0; i < brot.size(); ++i) {
		//dx = dof.at(i).rx;
		//dy = dof.at(i).ry;
		//dz = dof.at(i).rz;
		skrot = brot.at(i);
		tvec = getZYXRotation(skrot);
		//if (dx) 
		vpose(3*i) = tvec(0);
		//if (dy) 
		vpose(3*i + 1) = tvec(1);
		//if (dz) 
		vpose(3*i + 2) = tvec(2);
	}
	return vpose;
}
