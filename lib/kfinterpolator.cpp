#include <kfinterpolator.h>
#include <cassert>
#include <Eigen/Eigenvalues>
#include <iostream>

/**
 * @brief get distance matrix by keyframes vector.
 * @param fref frame ids to build matrix
 * @return distance matrix
 */
Eigen::MatrixXd MocapInterpolator::getDistanceMatrix(Eigen::VectorXi const & fref) {
    Eigen::MatrixXd dist = Eigen::ArrayXXd::Zero(fref.size(), fref.size());
    Skeleton sk0, sk1;
    double tdist, tneigh = 5.0;

	if (fdist.rows() == mc->getNumberOfFrames()) {
		if (fref.size() == fdist.rows()) {
			return fdist;
		} else {
			for (unsigned int i=0; i < fref.size(); ++i)
				for (unsigned int j=i+1; j < fref.size(); ++j)
					dist(i, j) = dist(j, i) = fdist(fref(i), fref(j));
		}

	} else {

		for (unsigned int i=0; i < fref.size(); ++i) {
			for (unsigned int j=i+1; j < fref.size(); ++j) {
				sk0 = mc->getFrameSkeleton(fref(i));
				sk1 = mc->getFrameSkeleton(fref(j));
				tdist = j-i;
				tdist = std::min(tdist, tneigh);
				tdist /= tneigh;
				tdist = 3 * std::pow(tdist, 2) - 2 * std::pow(tdist, 3);
				tdist *= tneigh;
				dist(i,j) = dist(j,i) = (1 - wtime)*std::sqrt(skdist->getPoseDistance(sk0, sk1)) + wtime*tdist;
			}
		}
	}
    return dist;
}

/**
 * @brief get frame distance matrix
 * @return frame distance matrix
 */
Eigen::MatrixXd MocapInterpolator::getFrameDistanceMatrix() {
    fdist = Eigen::ArrayXXd::Zero(mc->getNumberOfFrames(), mc->getNumberOfFrames());
    Skeleton sk0, sk1;
    double tdist, tneigh = 5.0;

	for (unsigned int i=0; i < mc->getNumberOfFrames(); ++i) {
		for (unsigned int j=i+1; j < mc->getNumberOfFrames(); ++j) {
			sk0 = mc->getFrameSkeleton(i);
			sk1 = mc->getFrameSkeleton(j);
			tdist = j-i;
			tdist = std::min(tdist, tneigh);
			tdist /= tneigh;
			tdist = 3 * std::pow(tdist, 2) - 2 * std::pow(tdist, 3);
			tdist *= tneigh;
			fdist(i,j) = fdist(j,i) = (1 - wtime)*std::sqrt(skdist->getPoseDistance(sk0, sk1)) + wtime*tdist;
		}
	}
    return fdist;
}

/**
 * @brief set frame distance matrix. Used to speed up computation
 * setting the previously computed matrix.
 * @param dmat frame distance matrix
 */
void MocapInterpolator::setFrameDistanceMatrix(Eigen::MatrixXd & dmat) {
	fdist = dmat;
}

/**
 * @brief get markers position for given frame ids
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd LinearInterpolator::getMarkers(Eigen::VectorXi const & fids) {
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(fids.size(),4);
	
	for (unsigned int i=0; i < ret.rows(); ++i) {
		ret(i,0) = fids(i);
		ret(i,3) = 1;
	}
	return ret;
}

/**
 * @brief rebuilds mocap data for given keyframes and interval specified
 * @param kfs current keyframes
 * @param start frame id to start rebuild
 * @param len number of frames to rebuild
 * @return error vector
 */
Eigen::VectorXd LinearInterpolator::rebuildMocapData(Eigen::VectorXi & kfs, int start, int len) {
	assert((start + len) <= dofs.rows());
	unsigned int next = 0;
	Eigen::VectorXd tmp0;

	while (kfs(next) <= start) ++next;
	--next;
	for (int i=start; i < start + len; ++i) {
		if (kfs(next) == i) {
			tmp0 = dofs.row(i);
			idofs.row(i) = tmp0;
			evec(i) = 0;
			++next;
		} else {
			evec(i) = getFrameDistance(kfs(next - 1), i, kfs(next));
		}
	}
	return evec;
}

/**
 * @brief get the frame distance between a mocap pose given by frame id
 * and a linear interpolation between two other frames. Evaluated frame 
 * id has to be inside interpolation interval.
 * 
 * @param pf frame id for interval beginning
 * @param ef frame id to be evaluated
 * @param nf frame id for interval end
 * @return pose distance between mocap and interpolation reconstruction
 */
double LinearInterpolator::getFrameDistance(unsigned int pf, unsigned int ef, unsigned int nf) {
	Eigen::VectorXd pdof, ndof, cdof;
	Skeleton s0,s1;
	float t;
	
	pdof = dofs.row(pf);
	ndof = dofs.row(nf);
	t = nf - pf;
	t = (ef - pf) / t;
	cdof = t*ndof + (1 - t)*pdof;
	idofs.row(ef) = cdof;
	s0 = mc->getFrameSkeleton(cdof);
	pdof = dofs.row(ef);
	s1 = mc->getFrameSkeleton(pdof);
	return skdist->getPoseDistance(s0,s1);
}

/**
 * @brief get the frame distance between a mocap pose given by frame id
 * and a spherical linear interpolation between two other frames. Evaluated frame 
 * id has to be inside interpolation interval.
 * 
 * @param pf frame id for interval beginning
 * @param ef frame id to be evaluated
 * @param nf frame id for interval end
 * @return pose distance between mocap and interpolation reconstruction
 */
double SphericalLinearInterpolator::getFrameDistance(unsigned int pf, unsigned int ef, unsigned int nf) {
	Eigen::VectorXd pdof, ndof, cdof;
	Eigen::Quaterniond qp, qn, qc;
	Eigen::Vector3d euler;
	Eigen::Matrix4d jroot, skrot;
	Eigen::Matrix3d r0, r1;
	std::vector<Eigen::Matrix4d> brot;
	Skeleton s0,s1;
	double t;
	
	pdof = dofs.row(pf);
	ndof = dofs.row(nf);
	t = nf - pf;
	t = (ef - pf) / t;
	s0 = mc->getFrameSkeleton(pdof);
	s1 = mc->getFrameSkeleton(ndof);
    // orientation
    Eigen::Matrix4d or0 = s0.getRootOrientation();
    r0 = or0.block(0,0,3,3);
    skrot = or0;
    Eigen::Matrix4d or1 = s1.getRootOrientation();
    r1 = or1.block(0,0,3,3);
    qp = Eigen::AngleAxisd(r0);
    qn = Eigen::AngleAxisd(r1);
    qc = qp.slerp(t, qn);
    skrot.block(0,0,3,3) = qc.toRotationMatrix();
    // other bones
	std::vector<Eigen::Matrix4d> bvec0 = s0.getBoneMatrices();
	std::vector<Eigen::Matrix4d> bvec1 = s1.getBoneMatrices();
	for (unsigned int i=0; i < bvec0.size(); ++i) {
		or0 = bvec0.at(i);
		r0 = or0.block(0,0,3,3);
		or1 = bvec1.at(i);
		r1 = or1.block(0,0,3,3);
		qp = Eigen::AngleAxisd(r0);
		qn = Eigen::AngleAxisd(r1);
		qc = qp.slerp(t, qn);
		or0.block(0,0,3,3) = qc.toRotationMatrix();
		brot.push_back(or0);
	}
	jroot = Eigen::Matrix4d::Identity();
	jroot.col(3).head(3) = dofs.row(ef).head(3);
	s0.setPose(jroot, skrot, brot);
	cdof = mc->getEulerAnglesDOFPose(s0);
	idofs.row(ef) = cdof;
	pdof = dofs.row(ef);
	s1 = mc->getFrameSkeleton(pdof);	
	return skdist->getPoseDistance(s0,s1);
}

/**
 * @brief get the frame distance between a mocap pose given by frame id
 * and the current interpolation given by a key frame set. The key frame
 * set is given as an ordered vector.
 *
 * @param kfs key frames id set as an ordered vector
 * @param fid frame id to be evaluated
 * @return pose distance between mocap and interpolation reconstruction
 */
double LinearInterpolator::getFrameDistance(Eigen::VectorXi & kfs, int fid) {
	assert((fid >= 0) && (fid <= kfs(kfs.size() - 1)));
	unsigned int i, pf, ef, nf;
	// find
	for (i=1; i < kfs.size(); ++i)
		if (fid < kfs(i)) break;
	pf = kfs(i-1);
	ef = fid;
	nf = kfs(i);
	return getFrameDistance(pf, ef, nf);
}

/**
 * @brief get the interpolation neighbours frames for a given key frame
 * list and frame id. An interpolation neighbour frame is the one which
 * certainly has to update its importance because of the impact of
 * taking evaluated frame id from key frame list.
 * @param fid evaluated frame id
 * @param kfs key frames as a vector
 * @return candidate frame ids for evaluation
 */
Eigen::VectorXi LinearInterpolator::getINeighbourFrames(
	Eigen::VectorXi const & kfs, unsigned int fid) {

	unsigned int tf = kfs(kfs.size() - 1) + 1;
	assert(fid < tf);
	Eigen::VectorXi disp = Eigen::ArrayXi::Zero(tf);

	for (unsigned int i=0; i < kfs.size(); ++i)
		disp(kfs(i)) = 1;
	disp(fid) = 0;
	disp(0) = 0;
	disp(disp.size() - 1) = 0;
	tf = 0;
	// previous key frame in time
	for (int i = fid - 1; i >= 0; --i)
		if (disp(i) != 0) {
			disp(tf++) = i;
			break;
		}
	// next key frame in time
	for (int i = fid + 1; i < disp.size(); ++i)
		if (disp(i) != 0) {
			disp(tf++) = i;
			break;
		}
	assert(tf != 0);
	return disp.head(tf);
}

/**
 * @brief get interpolated skeleton pose for a given frame id
 * @param fid frame id
 * @return skeleton pose
 */
Skeleton LinearInterpolator::getIFramePose(unsigned int fid) {
	Eigen::VectorXd pvec = idofs.row(fid);
	return mc->getFrameSkeleton(pvec);
}

/**
 * @brief get the frame distance between a mocap pose given by frame id
 * and a linear interpolation between two other frames. Evaluated frame 
 * id has to be inside interpolation interval.
 * 
 * @param pf frame id for interval beginning
 * @param ef frame id to be evaluated
 * @param nf frame id for interval end
 * @return pose distance between mocap and interpolation reconstruction
 */
double HermiteInterpolator::getFrameDistance(unsigned int pf, unsigned int ef, unsigned int nf) {
	Eigen::VectorXd pdof, pd1dof, ndof, nd1dof, cdof;
	Skeleton s0,s1;
	float t, t3, t2, a, b, c, d;
	
	t = nf - pf;
	t = (ef - pf) / t;
	t3 = std::pow(t,3);
	t2 = std::pow(t,2);
	// cubic hermitian coefficients
	a = 2*t3  - 3*t2 + 1;
	b = t3    - 2*t2 + t;
	c = -2*t3 + 3*t2;
	d = t3    - t2;
	pdof = dofs.row(pf);
	ndof = dofs.row(nf);
	pd1dof = d1dofs.row(pf);
	nd1dof = d1dofs.row(nf);
	cdof = a*pdof + b*pd1dof + c*ndof + d*nd1dof;
	idofs.row(ef) = cdof;
	s0 = mc->getFrameSkeleton(cdof);
	pdof = dofs.row(ef);
	s1 = mc->getFrameSkeleton(pdof);
	return skdist->getPoseDistance(s0,s1);
}

/**
 * @brief build D1 derivative of mocap data
 */
void HermiteInterpolator::setDerivative() {
	Eigen::VectorXd f0, f1;
	d1dofs = dofs;
	
	for (unsigned int i = 1; i < dofs.rows() - 1; ++i) {
		f0 = dofs.row(i-1);
		f1 = dofs.row(i+1);
		f1 = 0.5 * (f1 - f0);
		d1dofs.row(i) = f1;
	}
	// undefined values get d1 from nearest frame
	unsigned int last = d1dofs.rows() - 1;
	f1 = d1dofs.row(1);
	d1dofs.row(0) = f1;
	f1 = d1dofs.row(last - 1);
	d1dofs.row(last) = f1;
}

/**
 * @brief rebuild motion capture data with RBF / spatial keyframe interpolation
 * for given keyframes and frame interval
 * @param kfs current keyframes
 * @param start frame id to start rebuild
 * @param len number of frames to rebuild
 * @return error vector
 */
Eigen::VectorXd RBFInterpolator::rebuildMocapData(Eigen::VectorXi & kfs, int start, int len) {
	assert((start + len) <= (int) mc->getNumberOfFrames());
	unsigned int next = 0;
	Skeleton s0;
	Eigen::MatrixXd marker;
	double csr;

	// rebuild animation
	//marker = getMarkers(kfs);
	s0 = mc->getFrameSkeleton(0);
	animHandler.clearAnimation();
	animHandler.setSkeleton(s0);
	//animHandler.addNewKeyFrame(marker.row(0), s0);
	animHandler.addNewKeyFrame(rdframes.row(0), s0);
	for (unsigned int i=1; i < kfs.size(); ++i) {
		if (kfs(i) == 0) break;
		s0 = mc->getFrameSkeleton(kfs(i));
		//animHandler.setSkeleton(s0);
		//animHandler.addNewKeyFrame(marker.row(i), s0);
		animHandler.addNewKeyFrame(rdframes.row(kfs(i)), s0);
	}
	csr = getCSRadius(kfs);
	animHandler.setCsr(csr);
	animHandler.setContext();
	// get begin interval
	while (kfs(next) <= start) ++next;
	if (next != 0) --next;
	// evaluate errors
	for (int i=start; i < start + len; ++i) {
		if (kfs(next) == i) {
			evec(i) = 0;
			if (next < (kfs.size() - 1)) ++next;
		} else {
			evec(i) = getAnimationError(rdframes, i);
		}
	}
	return evec;	
}	

/**
 * @brief get distance between mocap pose and interpolation reconstruction
 * to current elected keyframes without 2D projection.
 * @param marker markers vector as a rowwise matrix
 * @param kfs keyframes indices vector
 * @param ff frame id to be evaluated
 * @return pose distance between mocap and interpolation reconstruction
 */
double RBFInterpolator::getAnimationError(Eigen::MatrixXd const & marker, Eigen::VectorXi const & kfs, int ff) {
	Eigen::Vector4d controller;
	Skeleton s0, s1;
	
	for (unsigned int i=0; i < kfs.size(); ++i)
		if (kfs(i) == ff) {
			controller = marker.row(ff);
			break;
		} else if (kfs(i) > ff) {
			Eigen::Vector4d v0 = marker.row(kfs(i-1));
			Eigen::Vector4d v1 = marker.row(kfs(i));
			double t = ff - kfs(i - 1);
			t =  t / (kfs(i) - kfs(i - 1));
			controller = v0 + t * (v1 - v0);
			break;
		}
	animHandler.setController(controller);
	animHandler.animate();
	s0 = mc->getFrameSkeleton(ff);
	s1 = animHandler.getSkeleton();
	return skdist->getPoseDistance(s0, s1);
}

/**
 * @brief get distance between mocap pose and interpolation reconstruction
 * to current elected keyframes using 2D projection to softening its contour.
 * @param marker markers vector as a rowwise matrix
 * @param ff frame id to be evaluated
 * @return pose distance between mocap and interpolation reconstruction
 */
double RBFInterpolator::getAnimationError(Eigen::MatrixXd const & marker, int ff) {
	Eigen::Vector4d controller;
	Skeleton s0, s1;

	controller = marker.row(ff);
	animHandler.setController(controller);
	animHandler.animate();
	s0 = mc->getFrameSkeleton(ff);
	s1 = animHandler.getSkeleton();
	return skdist->getPoseDistance(s0, s1);
}

/**
 * @brief get the frame distance between a mocap pose given by frame id
 * and a RBF/spatial keyframe interpolation between two other frames. 
 * Evaluated frame id has to be inside interpolation interval.
 * 
 * @param pf frame id for interval beginning
 * @param ef frame id to be evaluated
 * @param nf frame id for interval end
 * @return pose distance between mocap and interpolation reconstruction
 */
double RBFInterpolator::getFrameDistance(unsigned int pf, unsigned int ff, unsigned int nf) {
	Eigen::Vector4d controller;
	Animation localAnimation;
	Skeleton s0, s1;
	Eigen::Vector4d marker;

	// begin interval
	//marker << pf,0,0,1; // change
	marker = rdframes.row(pf);
	//setMarker(marker, pf);
	s0 = mc->getFrameSkeleton(pf);	
	localAnimation.setSkeleton(s0);
	localAnimation.addNewKeyFrame(marker, s0);
	// end interval
	//marker(0) = nf; // change candidate
	marker = rdframes.row(nf);
	s0 = mc->getFrameSkeleton(nf);
	//localAnimation.setSkeleton(s0);
	localAnimation.addNewKeyFrame(marker, s0);
	// build animated skeleton and get distance
	localAnimation.setContext();
	//controller << ff,0,0,1;	// change candidate
	controller = rdframes.row(ff);
	localAnimation.setController(controller);
	localAnimation.animate();
	s0 = mc->getFrameSkeleton(ff);
	s1 = localAnimation.getSkeleton();
	return skdist->getPoseDistance(s0, s1);
}

/**
 * @brief get the frame distance between a mocap pose given by frame id
 * and the current interpolation given by a key frame set. The key frame
 * set is given as an ordered vector.
 *
 * @param kfs key frames id set as an ordered vector
 * @param fid frame id to be evaluated
 * @return pose distance between mocap and interpolation reconstruction
 */
double RBFInterpolator::getFrameDistance(Eigen::VectorXi & kfs, int fid) {
	assert((fid >= 0) && (fid <= kfs(kfs.size() - 1)));

	Eigen::Vector4d controller;
	Animation localAnimation;
	Skeleton s0, s1;
	Eigen::Vector4d marker;
	double csr;

	marker = rdframes.row(kfs(0));
	s0 = mc->getFrameSkeleton(kfs(0));
	localAnimation.setSkeleton(s0);
	localAnimation.addNewKeyFrame(marker, s0);
	for (unsigned int i=1; i < kfs.size(); ++i) {
		marker = rdframes.row(kfs(i));
		s0 = mc->getFrameSkeleton(kfs(i));
		//localAnimation.setSkeleton(s0);
		localAnimation.addNewKeyFrame(marker, s0);
	}
	// build animated skeleton and get distance
	csr = getCSRadius(kfs);
	localAnimation.setCsr(csr);
	localAnimation.setContext();
	//controller << ff,0,0,1;	// change candidate
	controller = rdframes.row(fid);
	localAnimation.setController(controller);
	localAnimation.animate();
	s0 = mc->getFrameSkeleton(fid);
	s1 = localAnimation.getSkeleton();
	return skdist->getPoseDistance(s0, s1);
}

/**
 * @brief get markers position for given frame ids
 * @param fids frame ids vector
 * @return rowwise matrix for markers position vectors
 */
Eigen::MatrixXd RBFInterpolator::getMarkers(Eigen::VectorXi const & fids) {
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(fids.size(), 4);
	for (unsigned int i=0; i < fids.size(); ++i) {
		ret(i,0) = i;
		ret(i,1) = i;
		ret(i,3) = 1;
	}
	return ret;
}

/**
 * @brief rescale markers for [0,1] interval
 * @param proj marker matrix to be updated
 */
void RBFInterpolator::rescaleMarkers(Eigen::MatrixXd & proj) {
	double minx, miny, maxx, maxy;
	minx = maxx = proj(0,0);
	miny = maxy = proj(0,1);
	for (unsigned int i=1; i < proj.rows(); ++i) {
		if (proj(i,0) < minx) minx = proj(i,0);
		else if (proj(i,0) > maxx) maxx = proj(i,0);
		if (proj(i,1) < miny) miny = proj(i,1);
		else if (proj(i,1) > maxy) maxy = proj(i,1);
	}
	maxx -= minx;
	maxy -= miny;
	if (maxy > maxx) maxx = maxy;
	for (unsigned int i=0; i < proj.rows(); ++i) {
		proj(i,0) = (proj(i,0) - minx) / maxx;
		proj(i,1) = (proj(i,1) - miny) / maxx;
	}	
}

/**
 * @brief a time as the third marker's coordinate
 * @param proj marker matrix to be updated
 */
void RBFInterpolator::addTimeCoordinate(Eigen::MatrixXd & proj) {
	double step = proj.rows() - 1;
	step = 1 / step;
	for (unsigned int i=0; i < proj.rows(); ++i) 
		proj(i,2) = i*step;
}

/**
 * @brief set the reduced dimension frame representation. For 1D, frame
 * representation is its id in time line.
 */
void RBFInterpolator::setRdframes() {
	//std::cout << "rd frames: 1D" << std::endl;
	if (rdframes.rows() > 0) return;
	Eigen::VectorXi fids = Eigen::ArrayXi::Zero(mc->getNumberOfFrames());
	for (unsigned int i=0; i < fids.size(); ++i)
		fids(i) = i;
	rdframes = getMarkers(fids);
	//addTimeCoordinate(rdframes);
	rescaleMarkers(rdframes);
}

/**
 * @brief get compact support kernel radius
 * @param kfs ids from key frame set
 * @return compact support kernel radius
 */
double RBFInterpolator::getCSRadius(Eigen::VectorXi & kfs) {
	assert(kfs.size() > 1);
	double ret, max;
	Eigen::VectorXd v0, v1;
	
	v0 = rdframes.row(kfs(0));
	v1 = rdframes.row(kfs(1));
	v0 = v1 - v0;
	max = ret = v0.norm();
	for (unsigned int i=2; i < kfs.size(); ++i) {
		v0 = v1;
		v1 = rdframes.row(kfs(i));
		v0 = v1 - v0;
		ret = v0.norm();
		if (max < ret) {
			max = ret;
		}
	}
	return max;	
}

/**
 * @brief get previously reduced dimension projection of given frame ids
 * @param fids frame ids vector
 * @return selected frames projection matrix
 */
Eigen::MatrixXd RBFInterpolator::getSelectedRdframes(Eigen::VectorXi const & fids) {

	assert(rdframes.rows() > 0);
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(fids.size(), rdframes.cols());

	for (unsigned int i=0; i < fids.size(); ++i)
		ret.row(i) = rdframes.row(fids(i));
	return ret;
}

/**
 * @brief get the interpolation neighbours frames for a given key frame
 * list and frame id. An interpolation neighbour frame is the one which
 * certainly has to update its importance because of the impact of
 * taking evaluated frame id from key frame list.
 * @param fid evaluated frame id
 * @param kfs key frames as a vector
 * @return candidate frame ids for evaluation
 */
Eigen::VectorXi RBFInterpolator::getINeighbourFrames(
	Eigen::VectorXi const & kfs, unsigned int fid) {

	unsigned int tf = kfs(kfs.size() - 1) + 1;
	assert(fid < tf);
	Eigen::VectorXi disp = Eigen::ArrayXi::Zero(tf);
	Eigen::VectorXd p0, p1, p2, pc, v0;
	double ma, mb, d0, d1;

	for (unsigned int i=0; i < kfs.size(); ++i)
		disp(kfs(i)) = 1;
	disp(fid) = 0;
	disp(0) = 0;
	disp(disp.size() - 1) = 0;
	p0 = rdframes.row(0);
	p1 = rdframes.row(rdframes.rows() - 1);
	p2 = rdframes.row(fid);
	// previous key frame in time
	for (int i = fid-1; i >= 0; --i)
		if (disp(i) != 0) {
			p0 = rdframes.row(i);
			break;
		}
	// next key frame in time
	for (int i = fid+1; i < disp.size(); ++i)
		if (disp(i) != 0) {
			p1 = rdframes.row(i);
			break;
		}
	// INeighbour radius (d0) and center (pc)
	v0 = p1 - p2;
	ma = v0.norm();
	v0 = p0 - p2;
	mb = v0.norm();
	if (mb > ma) d0 = mb; else d0 = ma;
	pc = p2;
	/*
	pc = 0.5 * (p0 + p1);
	v0 = p1 - pc;
	d0 = v0.norm();
	v0 = p2 - pc;
	d1 = v0.norm();
	if (d1 > d0) {
		ma = (p1(1) - p0(1)) / (p1(0) - p0(0));
		mb = (p2(1) - p1(1)) / (p2(0) - p1(0));
		pc(0) = ma*mb*(p0(1) - p2(1));
		pc(0) += mb*(p0(0) + p1(0));
		pc(0) -= ma*(p1(0) + p2(0));
		pc(0) /= 2*(mb - ma);
		pc(1) = 0.5*(p0(0) + p1(0)) - pc(0);
		pc(1) /= ma;
		pc(1) += 0.5*(p0(1) + p1(1));
		v0 = p2 - pc;
		d0 = v0.norm();
	}
	*/
	// who is inside radius
	for (unsigned int i=0; i < disp.size(); ++i) {
		if (disp(i) == 0) continue;
		v0 = rdframes.row(i);
		v0 -= pc;
		d1 = v0.norm();
		if ((d1 - d0) > 0.000001) disp(i) = 0;
	}
	tf = disp.sum();
	assert(tf > 0);
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(tf);
	tf = 0;
	for (unsigned int i=1; i < disp.size() - 1; ++i)
		if (disp(i) != 0) ret(tf++) = i;
	return ret;
}

/**
 * @brief get interpolated skeleton pose for a given frame id
 * @param fid frame id
 * @return skeleton pose
 */
Skeleton RBFInterpolator::getIFramePose(unsigned int fid) {
	Eigen::Vector4d controller;

	controller = rdframes.row(fid);
	animHandler.setController(controller);
	animHandler.animate();
	return animHandler.getSkeleton();
}
