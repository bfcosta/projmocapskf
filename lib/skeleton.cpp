#include <skeleton.h>
#include <GL/gl.h>
#include <iostream>
#include <cassert>

/**
 * \param th bone thickness, default equals 1.
 *
 * builds a new empty skeleton
 */
Skeleton::Skeleton(double th) : ShapeDrawer() {
	orientation = Eigen::Matrix4d::Identity();
	setColor(1.0f, 1.0f, 1.0f);
	thick = th;
}

/**
 * \param th new bone thickness
 *
 * set a new thickness value for bone
 */
void Skeleton::setThickness(double th) {
	std::cout << "old thick = " <<  thick << " new thick = " << th << std::endl;
	thick = th;
}

/**
 * \param tx X coordinate translation
 * \param ty Y coordinate translation
 * \param tz Z coordinate translation
 * \param n name for the root joint
 * 
 * defines the root joint by a translation triple and a name
 */
void Skeleton::setRoot(double tx, double ty, double tz, const std::string & n) {
	Eigen::Affine3d tr(Eigen::Translation3d(tx, ty, tz));

	parent.push_back(0);
	name.push_back(n);
	joint.push_back(tr.matrix());
}

/**
 * \param offset translation matrix
 * \param n name for the root joint
 * 
 * defines the root joint by a translation matrix and a name
 */
void Skeleton::setRoot(const Eigen::Matrix4d & offset, const std::string & n) {
	parent.push_back(0);
	name.push_back(n);
	joint.push_back(offset);	
}

/**
 * \param r0 vector for root joint position
 * 
 * set the root joint position
 */
void Skeleton::setRootPosition(const Eigen::Vector4d & r0) {
	Eigen::Matrix4d j0 = Eigen::Matrix4d::Identity();
	j0(0,3) = r0(0);
	j0(1,3) = r0(1);
	j0(2,3) = r0(2);
	joint.at(0) = j0;
}

/**
 * \param rx rotation angle for X direction. Value should be given on radians.
 * \param ry rotation angle for Y direction. Value should be given on radians.
 * \param rz rotation angle for Z direction. Value should be given on radians.
 * \param order array of three chars ('X','Y' or 'Z') describing the order of rotation angles.
 * 
 * defines a rotation orientation for the skeleton
 */
void Skeleton::setOrientation(double rx, double ry, double rz, const char * order) {
	Eigen::Affine3d mx(Eigen::AngleAxisd(rx, Eigen::Vector3d(1, 0, 0)));
	Eigen::Affine3d my(Eigen::AngleAxisd(ry, Eigen::Vector3d(0, 1, 0)));
	Eigen::Affine3d mz(Eigen::AngleAxisd(rz, Eigen::Vector3d(0, 0, 1)));
	orientation = Eigen::Matrix4d::Identity();
	
	for (unsigned int i=0; i < 3; ++i) {
		if (order[i] == 'X') {
			orientation = orientation * mx.matrix();
		} else if (order[i] == 'Y') { 
			orientation = orientation * my.matrix();
		} else if (order[i] == 'Z') { 
			orientation = orientation * mz.matrix();
		}
	}
}

/**
 * \param ori rotation matrix efining the skeleton orientation
 * 
 * defines a rotation orientation for the skeleton
 */
void Skeleton::setOrientation(const Eigen::Matrix4d & ori) {
	orientation = ori;
}

/**
 * \param tx translation offset on X direction.
 * \param ty translation offset on Y direction.
 * \param tz translation offset on Z direction.
 * \param rx rotation angle on X direction. Value should be given on radians.
 * \param ry rotation angle on Y direction. Value should be given on radians.
 * \param rz rotation angle on Z direction. Value should be given on radians.
 * \param order array of three chars ('X','Y' or 'Z')describing the order of rotation angles.
 * \param par index to parent joint
 * \param n name for the added joint
 * 
 * adds a new bone/joint based on the offset, rotation angles, parent
 * index and a name.
 */
void Skeleton::addJoint(double tx, double ty, double tz, 
					double rx, double ry, double rz, const char * order,
					unsigned int par, const std::string & n) {
	Eigen::Affine3d tr(Eigen::Translation3d(tx, ty, tz));
	Eigen::Affine3d mx(Eigen::AngleAxisd(rx, Eigen::Vector3d(1, 0, 0)));
	Eigen::Affine3d my(Eigen::AngleAxisd(ry, Eigen::Vector3d(0, 1, 0)));
	Eigen::Affine3d mz(Eigen::AngleAxisd(rz, Eigen::Vector3d(0, 0, 1)));
	Eigen::Matrix4d rot = Eigen::Matrix4d::Identity();

	parent.push_back(par);
	name.push_back(n);
	joint.push_back(tr.matrix());
	for (unsigned int i=0; i < 3; ++i) {
		if (order[i] == 'X') {
			rot = rot * mx.matrix();
		} else if (order[i] == 'Y') { 
			rot = rot * my.matrix();
		} if (order[i] == 'Z') { 
			rot = rot * mz.matrix();
		}
	}
	bone.push_back(rot);
}

/**
 * \param offset a translation matrix defining bone/joint offset
 * \param pRotation a rotation matrix defining bone/joint rotation
 * \param par index to parent joint
 * \param n name for the added joint
 * 
 * adds a new bone/joint based on the offset and rotation matrices, 
 * and also as parent joint index and a name.
 */
void Skeleton::addJoint(const Eigen::Matrix4d & offset,
		const Eigen::Matrix4d & pRotation, unsigned int par, 
		const std::string & n) {

	parent.push_back(par);
	name.push_back(n);
	joint.push_back(offset);
	bone.push_back(pRotation);			
}

/**
 * \param i skeleton bone index
 * \return global transformation matrix for the given bone
 * 
 * returns the global transformation matrix for a given bone/joint
 * including all transfomations needed in the hierarchy.
 */
Eigen::Matrix4d Skeleton::getBoneMatrix(unsigned int i) {
	
	Eigen::Matrix4d mview = Eigen::Matrix4d::Identity();
	while (i != 0) {
		mview = bone.at(i - 1) * joint.at(i) * mview;
		i = parent.at(i);
	}
	mview = joint.at(i) * orientation * mview;
	return mview;
}

/**
 * \param i index to a skeleton joint
 * \return position of the joint at global homogeneous coordinates
 * 
 * returns the current position at global homogeneous coordinates
 * for the saved configuration of rotations in the skeleton.
 */
Eigen::Vector4d Skeleton::getJointPosition(unsigned int i) {
	Eigen::Vector4d vet;
	
	vet << 0,0,0,1;
	return (getBoneMatrix(i) * vet);
}

/**
 * \return vector with bone's transformation matrices
 * 
 * get current bone's transformation matrices
 */
std::vector<Eigen::Matrix4d> Skeleton::getBonesTransfMatrices() {
	std::vector<Eigen::Matrix4d> ret;
	
	ret.resize(bone.size()+1);
	ret.at(0) = joint.at(0);
	for (unsigned int i=1; i < ret.size(); ++i) 
		ret.at(i) = getBoneMatrix(i);
	return ret;
}

/**
 * \param glmode opengl mode for render selection
 * 
 * draws current skeleton
 */
void Skeleton::draw(unsigned int glmode) {
	std::vector<Eigen::Vector4d> jpos;
	Eigen::Vector4d v1, v2, axis;
	unsigned int par;
	double angle, norm;

    v2 << 0,0,1,0;
	glColor4fv(scolor);
	for (unsigned int i=0; i < joint.size(); ++i) {
		jpos.push_back(getJointPosition(i));
	}
	
    // Junction
    for (unsigned int i=0; i < jpos.size(); ++i) {
		glPushMatrix();
        glTranslatef(jpos.at(i)(0), jpos.at(i)(1), jpos.at(i)(2));
        if (glmode == GL_SELECT) glLoadName(i);
        drawSphere(2* thick, 30, 30);
        glPopMatrix();
    }   
    // Bones
    for (unsigned int i=1; i < jpos.size(); ++i) {
        par = parent.at(i);
        v1 = jpos.at(i) - jpos.at(par);
        norm = v1.norm();
        v1.normalize();
        axis = v2.cross3(v1);
        angle = acos( v2.dot(v1) )*180/M_PI;
        glPushMatrix();
		glTranslatef(jpos.at(par)(0), jpos.at(par)(1), jpos.at(par)(2));
		glRotatef(angle, axis(0), axis(1), axis(2));
		if (glmode == GL_SELECT) glLoadName(i + jpos.size());
		drawCylinder(thick, norm, 10, 4);
        glPopMatrix();
    }
}

/**
 * \param s a target skeleton model
 * 
 * sets the current skeleton to the same configuration of the given
 * skeleton. It is expected that the target skeleton has the same number
 * of bones and joints and the hierarchy between the is the same.
 */
void Skeleton::setSkeleton(const Skeleton & s) {
	assert((this->joint.size() == s.joint.size()) && 
		(this->bone.size() == s.bone.size()) &&
		s.joint.size() != 0);
		
	this->joint.at(0) = s.joint.at(0);
	this->orientation = s.orientation;
	for (unsigned int i=0; i < s.bone.size(); ++i) {
		this->joint.at(i+1) = s.joint.at(i+1);
		this->bone.at(i) = s.bone.at(i);
		this->parent.at(i) = s.parent.at(i);
	}
}

/**
 * shows the skeleton data at std output for debugging purposes.
 */
void Skeleton::debug() {
	Eigen::Vector4d v;

	//std::cout.precision(5);
	//std::cout << std::fixed;
	for (unsigned int i=0; i < joint.size(); ++i) {
        std::cout << "position for bone " << name.at(i) << std::endl;
        v << 0,0,0,1;
		v = getBoneMatrix(i) * v;
        std::cout << v(0) << " " << v(1) << " " << v(2) << std::endl;
	}
	std::cout << std::endl;
}

/**
 * \return average joint position
 * 
 * returns the average joint position on homogeneous coordinates for
 * the current skeleton.
 */
Eigen::Vector4d Skeleton::getCenter() {
	Eigen::Vector4d v1;

	v1 << 0,0,0,0;
	//std::cout << "size = " << joint.size() << std::endl;
	assert(joint.size() > 0);
	for (unsigned int i=0; i < joint.size(); ++i) {
		v1 = v1 + getJointPosition(i);
		//std::cout << "sum = " << v1 << std::endl;
	}
	v1 = v1 / joint.size();
	return v1;	
}

/**
 * \return radius of skeleton circunscribed sphere 
 * 
 * returns the radius of the circunscribed sphere for this skeleton.
 * Namely, the distance between skeleton average joint position and the
 * farthest joint.
 */
double Skeleton::getRadius() {
	std::vector<Eigen::Vector4d> jpos;
	Eigen::Vector4d v1, v2;
	double dist = 0, sqnorm;

	v1 << 0,0,0,0;
	assert(joint.size() > 0);
	for (unsigned int i=0; i < joint.size(); ++i) {
		v1 = v1 + getJointPosition(i);
		jpos.push_back(getJointPosition(i));
	}
	v1 = v1 / joint.size();
	for (unsigned int i=0; i < jpos.size(); ++i) {
		v2 = v1 - jpos.at(i);
		sqnorm = v2.squaredNorm();
		if (sqnorm > dist) dist = sqnorm;
	}
	dist = std::sqrt(dist);
	return dist;	
	
}

/**
 * \brief getarray vector of end effectors indices.
 * \return end effector indices
 */
std::vector<unsigned int> Skeleton::getEndEffectorsIdx() {
	std::vector<bool> endeff;
	endeff.resize(joint.size(), true);
	for (unsigned int i=0; i < endeff.size(); ++i) {
		for (unsigned int j=0; j < parent.size(); ++j) {
			if (parent.at(j) == i) {
				endeff.at(i) = false;
				break;
			}
		}
	}
	std::vector<unsigned int> ret;
	for (unsigned int i=0; i < endeff.size(); ++i) 
		if (endeff.at(i)) ret.push_back(i);
	return ret;
}

/**
 *
 */
void Skeleton::setPose(const Eigen::Matrix4d & jroot, const Eigen::Matrix4d & ori, const std::vector<Eigen::Matrix4d> & brot) {
    assert(brot.size() == bone.size());
    joint.at(0) = jroot;
    orientation = ori;
    bone.clear();
    bone = brot;
}

/**
 * @brief get current skeleton orientation
 * @return rotation matrix for skeleton root orientation
 */
Eigen::Matrix4d Skeleton::getRootOrientation() {
    return orientation;
}

/**
 * @brief get root joint translation matrix
 * @return forst joint translation matrix
 */
Eigen::Matrix4d Skeleton::getSkeletonRootTranslation() {
    return joint.at(0);
}

/**
 * @brief get current joint translation matrices
 * @return array with all joint translation matrices
 */
std::vector<Eigen::Matrix4d> & Skeleton::getJointMatrices() {
	return joint;
}

/**
 * @brief get current bone rotation state
 * @return array with all bones rotation matrices
 */
std::vector<Eigen::Matrix4d> & Skeleton::getBoneMatrices() {
	return bone;
}

/**
 * @brief get current joint position state
 * @return array with all joint's position
 */
std::vector<Eigen::Vector4d> Skeleton::getJointVectors() {
	std::vector<Eigen::Vector4d> ret;
	for (unsigned int i=0; i < joint.size(); ++i) 
		ret.push_back(getJointPosition(i));
	return ret;
}

/**
 * @brief specify skeleton rendering color
 * @param r red color component
 * @param g green color component
 * @param b blue color component
 * @param a alpha color component
 */
void Skeleton::setColor(float r, float g, float b, float a) {
    scolor[0] = r;
    scolor[1] = g;
    scolor[2] = b;
    scolor[3] = a;
}

