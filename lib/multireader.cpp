#include <multireader.h>
#include <kfselector.h>
#include <cassert>
#include <iostream>

/**
 * @brief get frames re-sampled using Uniform Sampling
 * @param mc single format mocap file reader
 * @param pc percentage of keyframes
 * @return frame vector
 */
Eigen::VectorXi MultiReader::getUSKeyframes(MocapReader * mc, float pc) {
	USEucSlerpSelector sel(mc, pc);
	return sel.getKeyframeVector();
}

/**
 * @brief get frames re-sampled using SCS
 * @param mc single format mocap file reader
 * @param pc percentage of keyframes
 * @return frame vector
 */
Eigen::VectorXi MultiReader::getSCSKeyframes(MocapReader * mc, float pc) {
	SCSEucSlerpSelector sel(mc, pc);
	return sel.getKeyframeVector();
}

/**
 * @brief dispose frames from unit mocap readers so that the sum of frames
 * fits in maximum number of frames
 */
void MultiReader::reassign() {
	unsigned int tf = 0;
	float pc = 0;
	Eigen::VectorXi kfs;
	
	for (unsigned int i=0; i < mset.size(); ++i) {
		tf += mset.at(i)->getNumberOfFrames();
	}
	pc = maxframes;
	pc /= tf;
	if (pc >= 0.5) {
		pc = 0.5;	
	} else if (pc >= 0.33) {
		pc = 0.33;		
	}
	mcref.clear();
	kfref.clear();
	for (unsigned int i=0; i < mset.size(); ++i) {
		if (pc < 0.33) {
			kfs = getSCSKeyframes(mset.at(i), pc);
		} else {
			kfs = getUSKeyframes(mset.at(i), pc);
		}
		for (unsigned int j=0; j < kfs.size(); ++j) {
			mcref.push_back(i);
			kfref.push_back(kfs(j));
		}
	}
	//~ std::cout << "Unit mocap # = " << mset.size() << std::endl;
	//~ std::cout << "sample = " << kfref.at(0) << " ";
	//~ for (unsigned int i=1; i < kfref.size(); ++i) {
		//~ if (mcref.at(i-1) != mcref.at(i))
			//~ std::cout << std::endl;
		//~ std::cout << kfref.at(i) << " ";
	//~ }
	//~ std::cout << std::endl;
}

/**
 * @brief get the skeleton resting pose
 * @return skeleton on resting pose
 */
Skeleton MultiReader::getSkeleton() {
	assert(mset.size() > 0);
	return mset.at(0)->getSkeleton();
}

/**
 * @brief get number of DOFs in animated skeleton
 * @return number of DOFs in animated skeleton
 */
unsigned int MultiReader::getNumberOfDOFs() {
	assert(mset.size() > 0);
	Skeleton s = mset.at(0)->getSkeleton();
	return s.getBoneNames().size();
}

/**
 * @brief get number of frames in animation
 * @return number of frames in animation
 */
unsigned int MultiReader::getNumberOfFrames() {
	return kfref.size();
}

/**
 * @brief returns a skeleton at a specific frame id during animation
 * @param i frame id counter
 * @return a skeleton pose
 */
Skeleton MultiReader::getFrameSkeleton(unsigned int i) {
	assert((i < mcref.size()) && (i < kfref.size()));
	return mset.at(mcref.at(i))->getFrameSkeleton(kfref.at(i));
}

/**
 * @brief returns a skeleton at a specific pose
 * @param dofs dof configuration in vector format
 * @param euler format of mocap matrix. True is euler angles and false is quaternion.
 * @return a skeleton pose
 */

Skeleton MultiReader::getFrameSkeleton(Eigen::VectorXd dofs, bool euler) {
	assert(mset.size() > 0);
	return mset.at(0)->getFrameSkeleton(dofs, euler);
}

/**
 * @brief dump all frames as one matrix. Rows are frames and columns are 
 * skeleton DOFs. Joint data are described as euler angles.
 * @return mocap DOF matrix with loaded mocap data
 */
Eigen::MatrixXd MultiReader::getEulerAnglesDOFMatrix() {
	assert(mset.size() > 0);
	Eigen::MatrixXd temp = mset.at(0)->getEulerAnglesDOFMatrix();
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(kfref.size(), temp.cols());
	unsigned int rref = 0;
	
	while (mcref.at(rref) == 0) {
		ret.row(rref) = temp.row(kfref.at(rref));
		++rref;
	}
	for (unsigned int i=1; i < mset.size(); ++i) {
		temp = mset.at(i)->getEulerAnglesDOFMatrix();
		while (mcref.at(rref) == i) {
			ret.row(rref) = temp.row(kfref.at(rref));
			++rref;
		}
	}
	return ret;
}

/**
 * @brief dump skeleton pose as a DOF vector. Joint data are described as euler angles.
 * @param sk skeleton pose
 * @return mocap DOF vector with skeleton pose data
 */
Eigen::VectorXd MultiReader::getEulerAnglesDOFPose(Skeleton & sk) {
	assert(mset.size() > 0);
	return mset.at(0)->getEulerAnglesDOFPose(sk);
}

/**
 * @brief reads file and loads animation data
 * @param fname fname animation file
 */
void MultiReader::readFile(const std::string & fname) {
	std::cout << "This reader can't read file: " << fname << std::endl;
	std::cout << "Please choose ASF or BVH reader." << std::endl;
}

/**
 * @brief get frame time interval in milisseconds
 * @return frame time interval
 */
unsigned int MultiReader::getFrameInterval() {
	assert(mset.size() > 0);
	return mset.at(0)->getFrameInterval();	
}

/**
 * @brief dump structure to stdout for debugging purposes
 */
void MultiReader::print() {
	for (unsigned int i=0; i < mset.size(); ++i) {
		std::cout << "Printing file " << i << std::endl;
		mset.at(i)->print();
	}
}

/**
 * @brief print skeleton pose as a current format frame in file
 * @param sk skeleton pose
 * @param ost output stream to print
 */
void MultiReader::printPose(Skeleton & sk, std::ostream & ost) {
	assert(mset.size() > 0);
	return mset.at(0)->printPose(sk, ost);		
}

/**
 * @brief dump all frames as one matrix. Rows are frames and columns are 
 * skeleton DOFs. Joint data are described as quaternions.
 * @return mocap DOF matrix with loaded mocap data
 */
Eigen::MatrixXd MultiReader::getQuaternionDOFMatrix() {
	assert(mset.size() > 0);
	Eigen::MatrixXd temp = mset.at(0)->getQuaternionDOFMatrix();
	Eigen::MatrixXd ret = Eigen::ArrayXXd::Zero(kfref.size(), temp.cols());
	unsigned int rref = 0;
	
	while (mcref.at(rref) == 0) {
		ret.row(rref) = temp.row(kfref.at(rref));
		++rref;
	}
	for (unsigned int i=1; i < mset.size(); ++i) {
		temp = mset.at(i)->getQuaternionDOFMatrix();
		while (mcref.at(rref) == i) {
			ret.row(rref) = temp.row(kfref.at(rref));
			++rref;
		}
	}
	return ret;
	
}

/**
 * @brief get used frames in internal sampling given a unit mocap reader
 * @param i index of given unit mocap reader
 * @return used frames as vector
 */
Eigen::VectorXi MultiReader::getFrameSampleByUnitReader(unsigned int i) {
	unsigned int nf = 0;
	for (unsigned int j = 0; j < mcref.size(); ++j)
		if (mcref.at(j) == i) 
			++nf;
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(nf);
	nf = 0;
	for (unsigned int j = 0; j < mcref.size(); ++j)
		if (mcref.at(j) == i) {
			ret(nf) = kfref.at(j);
			++nf;
		}
	return ret;
}

/**
 * @brief get used frames in internal sampling given a unit mocap reader
 * @param rd reference of given unit mocap reader
 * @return used frames as vector
 */
Eigen::VectorXi MultiReader::getFrameSampleByUnitReader(MocapReader * rd) {
	unsigned int i, nf = 0;
	for (i=0; i < mset.size(); ++i)
		if (mset.at(i) == rd)
			break;
	for (unsigned int j = 0; j < mcref.size(); ++j)
		if (mcref.at(j) == i) 
			++nf;
	Eigen::VectorXi ret = Eigen::ArrayXi::Zero(nf);
	nf = 0;
	for (unsigned int j = 0; j < mcref.size(); ++j)
		if (mcref.at(j) == i) {
			ret(nf) = kfref.at(j);
			++nf;
		}
	return ret;
	
}

/**
 * @brief get the base/first frame id for a given unit mocap reader
 * @param i index of the given unit mocap reader
 * @return frame id
 */
unsigned int MultiReader::getFrameBaseByUnitReader(unsigned int i) {
	unsigned int ret;
	
	for (ret = 0; ret < mcref.size(); ++ret)
		if (i == mcref.at(ret))
			break;
	return ret;
}

/**
 * @brief get the base/first frame id for a given unit mocap reader
 * @param rd reference of the given unit mocap reader
 * @return frame id
 */
unsigned int MultiReader::getFrameBaseByUnitReader(MocapReader * rd) {
	unsigned int j, ret;

	for (j = 0; j < mset.size(); ++j)
		if (mset.at(j) == rd)
			break;
	for (ret = 0; ret < mcref.size(); ++ret)
		if (j == mcref.at(ret))
			break;
	return ret;
	
}

/**
 * @brief get frame sequential id in database given the sample id
 * @param sample id
 * @return frame id
 */
unsigned int MultiReader::getFrameIdBySampleId(unsigned int i) {
	unsigned int ret = 0;
	
	for (unsigned int j=0; j < mcref.at(i); ++j)
		ret += mset.at(j)->getNumberOfFrames();
	ret += kfref.at(i);
	return ret;
}

/**
 * @brief get the database sample id given the frame sequential id
 * @param frame id
 * @return sample id
 */
unsigned int MultiReader::getSampleIdByFrameId(unsigned int i) {
	unsigned int ret = 0, idx = 0, nf = 0;

	for (unsigned int j=0; j < mset.size(); ++j) {
		nf += mset.at(j)->getNumberOfFrames();
		idx = j;
		if (i < nf) break;
	}
	nf -= mset.at(idx)->getNumberOfFrames();
	nf = i - nf;
	for (unsigned int j=0; j < kfref.size(); ++j) {
		if (mcref.at(j) == idx) {
			ret = j;
			if (kfref.at(j) >= nf) 
				break;		
		}		
	}
	return ret;
}
