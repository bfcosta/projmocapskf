#include <kfselector.h>
#include <vector>
#include <cassert>
#include <iostream>
#include <ctime>

/**
 * @brief get last computed keyframes
 * @return keyframe vector
 */
Eigen::VectorXi USBaseSelector::getKeyframeVector() {
	
	if (keyframes.size() == 0) {
		keyframes = getKeyframes(nkf);
	} 
	return keyframes;
}

/**
 * @brief print keyframe list, total reconstruction error and elapsed 
 * computational time
 */
void USBaseSelector::printStats() {
	clock_t begin, end;
	
	begin = clock();
	ih->setRdframes();
	getKeyframes(nkf);
	end = clock();
	eltime = double(end - begin) / CLOCKS_PER_SEC;
	ih->rebuildMocapData(keyframes, 0, tf);
	double err = ih->getAvgError();
	
	std::cout << "keyframes = " << keyframes.transpose() << std::endl;
	std::cout << "total error = " << err << std::endl;
	std::cout << "elapsed time = " << eltime << std::endl;
}

/**
 * @brief get keyframes for mocap data given by uniform sampling algorithm
 * @param nkf number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi USBaseSelector::getKeyframes(unsigned int nkf) {
	assert(nkf > 2);
	
	keyframes = Eigen::ArrayXi::Zero(nkf);
	float step = tf - 1;
	
	step = step / (nkf - 1);
	for (unsigned int i=1; i < nkf; ++i)
		keyframes(i) = step*i;
	keyframes(nkf - 1) = tf - 1;
	return keyframes;
}

/**
 * @brief get the frame id with minimum error given a set of ids marked 
 * for evaluation
 * @param ev error vector
 * @param iskf boolean vector indicating whether the frame id should be 
 * evaluated (true) or not (false)
 * @return frame id with minimum error
 */
unsigned int PBBaseSelector::getMinErrorFrameId(const Eigen::VectorXd & ev, std::vector<bool> & iskf) {
	unsigned int ret = ev.size();
	double minerr = ev(0);
	
	for (unsigned int i = 1; i < tf - 1; ++i) {
		if (iskf.at(i)) {
			ret = i;
			minerr = ev(i);
			break;
		}
	}
	for (unsigned int i = ret + 1; i < tf - 1; ++i) {
		if (iskf.at(i) && (ev(i) < minerr)) {
			ret = i;
			minerr = ev(i);
		}
	}
	return ret;
}

/**
 * @brief get keyframes for mocap data given by position-based algorithm
 * @param nkf stop criteria: number of keyframes
 * @return keyframe vector
 *
Eigen::VectorXi PBBaseSelector::getKeyframes(unsigned int nkf) {
	assert(nkf > 2);
	
	Eigen::VectorXd evet = Eigen::ArrayXd::Zero(tf);
	std::vector<bool> iskf(tf,true);
	std::vector<unsigned int> frids;
	unsigned int kcn, curr, prev, next;
	
	
	// build error vector
	for (unsigned int i=1; i < tf - 1; ++i) {
		evet(i) = ih->getFrameDistance(i-1, i, i+1); // change
	}
	// find disposable frame
	curr = getMinErrorFrameId(evet, iskf);
	iskf.at(curr) = !iskf.at(curr);
	kcn = tf - 1;
	while (kcn > nkf) {	
		// find candidates for update
		next = curr + 1;
		while ((!iskf.at(next)) && (next < (iskf.size() - 1))) ++next;
		if (next < (iskf.size() - 1)) frids.push_back(next);
		prev = curr - 1;
		while ((!iskf.at(prev)) && (prev > 0)) --prev;
		if (prev > 0) frids.push_back(prev);
		// rebuild neighbor errors
		for (unsigned int i=0; i < frids.size(); ++i) {
			curr = frids.at(i);
			next = curr + 1;
			while ((!iskf.at(next)) && (next < (iskf.size() - 1))) ++next;
			prev = curr - 1;
			while ((!iskf.at(prev)) && (prev > 0)) --prev;
			evet(curr) = ih->getFrameDistance(prev, curr, next); // change
		}
		frids.clear();
		// find disposable frame
		curr = getMinErrorFrameId(evet, iskf);
		iskf.at(curr) = !iskf.at(curr);
		--kcn;
	}
	//std::cout << "error vector = " << evet.transpose() << std::endl;
	// populate output keyframe vector
	kcn = 0;
	keyframes = Eigen::ArrayXi::Zero(nkf);
	for (unsigned int i=0; i < iskf.size(); ++i) {
		if (iskf.at(i)) {
			keyframes(kcn++) = i;
		}
	}
	return keyframes;
}
*/

/**
 * @brief get keyframes for mocap data given by position-based algorithm
 * @param nkf stop criteria: number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi PBBaseSelector::getKeyframes(unsigned int nkf) {
	assert(nkf > 2);
	
	Eigen::VectorXd evet = Eigen::ArrayXd::Zero(tf);
	Eigen::VectorXi tset = Eigen::ArrayXi::Zero(tf);
	Eigen::VectorXi cand = Eigen::ArrayXi::Zero(tf-2);
	Eigen::VectorXi tmp;
	std::vector<bool> iskf(tf,true);
	unsigned int kcn, curr, cc;
	
	tset(0) = 0;
	tset(tf-2) = tf-1;
	kcn = tf;
	for (unsigned int i=1; i < tf-1; ++i)
		cand(i-1) = i;
	while (kcn > nkf) {
		// reset error vector
		for (unsigned int i=0; i < cand.size(); ++i) {
			curr = cand(i);
			cc = 1;
			for (unsigned int j=1; j < tf; ++j)
				if (iskf.at(j) && (j != curr))
					tset(cc++) = j;
			tmp = tset.head(cc);
			evet(curr) = ih->getFrameDistance(tmp, curr);
			//evet(curr) = ih->getProjectedFrameDistance(tmp, curr);
		}
		// find disposable frame
		curr = getMinErrorFrameId(evet, iskf);
		iskf.at(curr) = !iskf.at(curr);	
		// find candidates for error update
		cc = 1;
		for (unsigned int j=1; j < tf; ++j)
			if (iskf.at(j))
				tset(cc++) = j;
		tmp = tset.head(cc);
		cand = ih->getINeighbourFrames(tmp, curr);
		//std::cout << "last disposed = " << curr << std::endl;
		//std::cout << "candidates = " << cand.transpose() << std::endl;
		--kcn;
	}
	// populate output keyframe vector
	cc = 0;
	keyframes = Eigen::ArrayXi::Zero(nkf);
	for (unsigned int i=0; i < iskf.size(); ++i)
		if (iskf.at(i)) 
			keyframes(cc++) = i;
	return keyframes;
}

/**
 * @brief get keyframes for mocap data given by position-based algorithm
 * @param nd stop criteria: minimal distance between neighbor keyframes
 * @return keyframe vector
 */
Eigen::VectorXi PBBaseSelector::getKeyframesByNeigborhood(unsigned int nd) {
	assert(nd < tf);
	Eigen::VectorXd evet = Eigen::ArrayXd::Zero(tf);
	std::vector<bool> iskf(tf,true);
	std::vector<unsigned int> frids;
	bool hasNeighbor = true;
	unsigned int nidx, curr, prev, next;
	

	// build error vector
	for (unsigned int i=1; i < tf - 1; ++i) {
		evet(i) = ih->getFrameDistance(i-1, i, i+1); // change
	}
	// find disposable frame
	curr = getMinErrorFrameId(evet, iskf);
	iskf.at(curr) = !iskf.at(curr);
	// test whether there is a near neighbor
	nidx = 0;
	for (unsigned int i = 1; i < iskf.size(); ++i) {
		if (iskf.at(i)) {
			hasNeighbor = ((i - nidx) <= nd);
			if (hasNeighbor) {
				break;
			} else {
				nidx = i;
			}
		}
	}
	while (hasNeighbor) {	
		// find candidates for update
		next = curr + 1;
		while ((!iskf.at(next)) && (next < (iskf.size() - 1))) ++next;
		if (next < (iskf.size() - 1)) frids.push_back(next);
		prev = curr - 1;
		while ((!iskf.at(prev)) && (prev > 0)) --prev;
		if (prev > 0) frids.push_back(prev);
		// rebuild neighbor errors
		for (unsigned int i=0; i < frids.size(); ++i) {
			curr = frids.at(i);
			next = curr + 1;
			while ((!iskf.at(next)) && (next < (iskf.size() - 1))) ++next;
			prev = curr - 1;
			while ((!iskf.at(prev)) && (prev > 0)) --prev;
			evet(curr) = ih->getFrameDistance(prev, curr, next); // change
		}
		frids.clear();
		// find disposable frame
		curr = getMinErrorFrameId(evet, iskf);
		iskf.at(curr) = !iskf.at(curr);
		// test whether there is a near neighbor
		nidx = 0;
		for (unsigned int i = 1; i < iskf.size(); ++i) {
			if (iskf.at(i)) {
				hasNeighbor = ((i - nidx) <= nd);
				if (hasNeighbor) {
					break;
				} else {
					nidx = i;
				}
			}
		}		
	}
	//std::cout << "error vector = " << evet.transpose() << std::endl;
	// populate output keyframe vector
	nkf = 0;
	for (unsigned int i = 0; i < iskf.size(); ++i) 
		if (iskf.at(i)) ++nkf;
	nidx = 0;
	keyframes = Eigen::ArrayXi::Zero(nkf);
	for (unsigned int i=0; i < iskf.size(); ++i) {
		if (iskf.at(i)) {
			keyframes(nidx++) = i;
		}
	}
	return keyframes;	
}

/**
 * @brief get the frame id with most distant pose from mocap data given an interval
 * @param kfs given keyframes
 * @param start beginning frame id interval
 * @param end ending frame id interval
 * @return frame id
 */
int SCSBaseSelector::getMostDissimilarFrame(Eigen::VectorXi & kfs, int start, int len) {
	int ret = start;
	double maxerr;

	Eigen::VectorXd err = ih->getErrorVector();
	//Eigen::VectorXd err = ih->getProjectionErrorVector(kfs, start, len);

	maxerr = err(ret);
	for (int i=start; i < start + len; ++i) 
		if (maxerr < err(i)) {
			maxerr = err(i);
			ret = i;
		}
	return ret;
}

/**
 * @brief get keyframes for mocap data given by SCS algorithm
 * @param nkf stop criteria: number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi SCSBaseSelector::getKeyframes(unsigned int nkf) {
	assert(nkf > 2);
	Eigen::VectorXi tmprt;
	unsigned int tmp, tkf;
	int start, length, ckf;
	keyframes = Eigen::ArrayXi::Zero(nkf);
	
	tkf	= 2;
	keyframes(tkf - 1) = tf - 1;
	start = 0;
	length = tf;
	while (nkf > tkf) {
		// rebuild rdata
		tmprt = keyframes.head(tkf);
		//std::cout << "rebuild interpolation" << std::endl;
		ih->rebuildMocapData(tmprt, start, length);
		// get max error frame
		//std::cout << "elect key frame" << std::endl;
		keyframes(tkf) = getMostDissimilarFrame(tmprt, 0, tf);
		ckf = tkf;
		for (unsigned int i=tkf; i > 0; --i) {
			if (keyframes(i-1) > keyframes(i)) {
				tmp = keyframes(i);
				keyframes(i) = keyframes(i-1);
				keyframes(i-1) = tmp;
				--ckf;
			}
		}
		// !(rebuild data only of affected interval)
		//length = keyframes(start + 1) - keyframes(start - 1) + 1;
		//start = keyframes(start - 1);
		++tkf;
	}
	return keyframes;
}

/**
 * @brief get keyframes for mocap data given by SCS algorithm
 * @param nkf stop criteria: percentage decrease in error
 * @return keyframe vector
 */
Eigen::VectorXi SCSBaseSelector::getKeyframesByDeltaError(float ep) {
	Eigen::VectorXi tmprt;
	unsigned int tmp, tkf;
	int start, length;
	double e0, e1, delta;
	
	keyframes = Eigen::ArrayXi::Zero(nkf);
	tkf	= 2;
	keyframes(tkf - 1) = tf - 1;
	start = 0;
	length = tf;
	// rebuild rdata
	tmprt = keyframes.head(tkf);
	ih->rebuildMocapData(tmprt, start, length);
	// get max error frame
	keyframes(tkf) = getMostDissimilarFrame(tmprt, 0, tf);
	start = tkf;
	// sort keyframes
	for (unsigned int i=tkf; i > 0; --i) {
		if (keyframes(i-1) > keyframes(i)) {
			tmp = keyframes(i);
			keyframes(i) = keyframes(i-1);
			keyframes(i-1) = tmp;
			--start;
		}
	}
	e0 = ih->getFrameDistance(keyframes(start - 1), keyframes(start) , keyframes(start + 1)); // change
	// rebuild data only of affected interval
	length = keyframes(start + 1) - keyframes(start - 1) + 1;
	start = keyframes(start - 1);
	++tkf;
	do {
		// rebuild rdata
		tmprt = keyframes.head(tkf);
		ih->rebuildMocapData(tmprt, start, length);
		// get max error frame
		keyframes(tkf) = getMostDissimilarFrame(tmprt, 0, tf);
		start = tkf;
		// sort keyframes
		for (unsigned int i=tkf; i > 0; --i) {
			if (keyframes(i-1) > keyframes(i)) {
				tmp = keyframes(i);
				keyframes(i) = keyframes(i-1);
				keyframes(i-1) = tmp;
				--start;
			}
		}
		e1 = ih->getFrameDistance(keyframes(start - 1), keyframes(start) , keyframes(start + 1)); // change
		// rebuild data only of affected interval
		length = keyframes(start + 1) - keyframes(start - 1) + 1;
		start = keyframes(start - 1);
		delta = e1 / e0;
		//std::cout << "delta = " << delta << " e1 = " << e1 << " e0 = " << e0 << std::endl;
		e0 = e1;
		++tkf;
	} while ((delta < ep) || (tkf < nkf));
	return keyframes.head(tkf);	
}

/**
 * @brief return a subset index vector of frames where subset is a
 * uniform sample
 * @param pc percentage of data used to build subset
 * @return subset index vector
 */
Eigen::VectorXi ROLSBaseSelector::getSubsetData(double pc) {
    assert(tf > 0);
    unsigned int size = pc * tf;
    Eigen::VectorXi ret = Eigen::ArrayXi::Zero(size);
    size = tf / size;

    for (unsigned int i=0; i < tf - 1; i += size)
        ret(i / size) = i;
    ret(ret.size() - 1) = tf - 1;
    return ret;
}

/**
 * @brief return the implemented radial basis function kernel as multiquadrics
 * with c and epsilon equals one.
 * @param d2 distance as input
 * @return function output
 */
double ROLSBaseSelector::phi(double d2) {
	// gaussian kernel, $\epsilon = 1$
	//return std::exp(-1*d2*d2);
	// multiquadrics kernel, c=1 and $\epsilon = 1$
	return std::sqrt(1 + d2*d2);
	// inverse multiquadrics kernel, c=1 and $\epsilon = 1$
	//return std::sqrt(1/(1+d2*d2));
}

/**
 * @brief choose keyframes with ROLS algorithm
 * @param nkf stop criteria: number of keyframes
 * @param reg regularization parameter
 * @return keyframe vector
 */
Eigen::VectorXi ROLSBaseSelector::chooseControlPoints(unsigned int nkf, double reg) {
    assert(sample.size() > 0);

    Eigen::VectorXi cref = Eigen::ArrayXi::Zero(sample.size());
    Eigen::VectorXi ret = Eigen::ArrayXi::Zero(nkf);
    Eigen::VectorXd rvec = Eigen::ArrayXd::Zero(cref.size());
    Eigen::MatrixXd utmp, hvec, ovec;
    Eigen::VectorXd v0, tvec;
    double max = 0;
    unsigned int last = cref.size();

    // build phi matrix
    for (int i=0; i < sample.size(); ++i)
        for (int j=i; j < sample.size(); ++j)
            phimat(i, j) = phimat(j, i) = phi(phimat(i, j));
    utmp = phimat;
    hvec = Eigen::ArrayXXd::Zero(2, utmp.cols());
    ovec = Eigen::ArrayXXd::Zero(2, utmp.cols());

    //std::cout << "choose control points" << std::endl;
    for (unsigned int i=0; i < nkf; ++i) {
        // calculate error
        //std::cout << "calculate error" << std::endl;
        for (int j=0; j < hvec.cols(); ++j) {
            if (cref(j) != 0) {
                ovec(0,j) = hvec(0,j) = 0;
                ovec(1,j) = hvec(1,j) = 0;
            } else {
                v0 = utmp.col(j);
                if (v0.dot(v0) == 0) {
					ovec(0,j) = hvec(0,j) = 0;
					ovec(1,j) = hvec(1,j) = 0;
				} else {
					tvec = ssproj.col(0);
					hvec(0,j) = v0.dot(tvec) / v0.dot(v0);
					ovec(0,j) = (hvec(0,j)*hvec(0,j) + reg)*v0.dot(v0) / tvec.dot(tvec);
					tvec = ssproj.col(1);
					hvec(1,j) = v0.dot(tvec) / v0.dot(v0);
					ovec(1,j) = (hvec(1,j)*hvec(1,j) + reg)*v0.dot(v0) / tvec.dot(tvec);
				}
            }
        }
        // get max error
        //std::cout << "get max error" << std::endl;
        v0 = ovec.colwise().sum();
        for (int j=0; j < cref.size(); ++j)
            if (cref(j) == 0) {
                last = j;
                max = v0(j);
                break;
            }
        for (int j=0; j < v0.size(); ++j)
            if ((cref(j) == 0) && (max < v0(j))) {
                max = v0(j);
                last = j;
            }
        //std::cout << "kf selected = " << sample(last) << std::endl;
        cref(last) = 1;
        ret(i) = sample(last);
        // orthogonalize matrix
        //std::cout << "orthogonalize matrix" << std::endl;
		v0 = utmp.col(last);
		// find rks
		for (int k=0; k < cref.size(); ++k)
			if (cref(k) == 0)
				rvec(k) = v0.dot(utmp.col(k)) / v0.dot(v0);
		// orthogonalize column
		for (int k=0; k < cref.size(); ++k) {
			if (cref(k) == 0) {
				v0 = utmp.col(k);
				v0 -= rvec(k) * utmp.col(last);
				utmp.col(k) = v0;
			}
		}
    }

    // change last two key frames if first or last frames was not selected
    int discard = 0;
    /*
    last = nkf;
    for (unsigned int i=0; i < nkf; ++i) {
		if (ret(i) == 0) {
			last = i;
			break;
		}
	}
	if (last >= (nkf - 1)) {
		ret(nkf - 1) = 0;
		++discard;
	}
    last = nkf;
    for (unsigned int i=0; i < nkf; ++i) {
		if (ret(i) == (int) (tf - 1)) {
			last = i;
			break;
		}
	}
	if (last == nkf)
		ret(nkf - discard - 1) = tf - 1;
	*/
	// dummy sort with bubble sort
	for (unsigned int i=0; i < nkf; ++i)
		for (unsigned int j=i+1; j < nkf; ++j)
			if (ret(i) >= ret(j)) {
				discard = ret(i);
				ret(i) = ret(j);
				ret(j) = discard;
			}
	return ret;
}

/**
 * @brief get keyframes for mocap data given by ROLS algorithm
 * @param nkf stop criteria: number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi ROLSBaseSelector::getKeyframes(unsigned int nkf) {

	sample = getSubsetData();
	phimat = ih->getDistanceMatrix(sample);
	ssproj = ((RBFInterpolator *) ih)->getMarkers(sample);
	keyframes = chooseControlPoints(nkf);
	return keyframes;
}

/**
 * @brief get keyframes for mocap data given by ROLS/RBFP algorithm
 * @param nkf stop criteria: number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi ROLSRBFPBaseSelector::getKeyframes(unsigned int nkf) {

	keyframes = ROLSBaseSelector::getKeyframes(nkf);
	Eigen::MatrixXd rbfproj = getRBF2DProjection(keyframes);
	ih->setRdframes(rbfproj);
	return keyframes;
}

/**
 * @brief get RBF projection onto a 2D plane given the key frames as 
 * control points and their corresponding projections
 * @param kfs key frame ids vector
 * @return a projection matrix with all poses
 */
Eigen::MatrixXd ROLSRBFPBaseSelector::getRBF2DProjection(Eigen::VectorXi & kfs) {

	Eigen::VectorXi fids = Eigen::ArrayXi::Zero(tf);
	for (int i=0; i < fids.size(); ++i) 
		fids(i) = i;
	Eigen::MatrixXd dmat = ih->getDistanceMatrix(fids);
	Eigen::MatrixXd proj = ((RBFInterpolator *) ih)->getMarkers(fids);
	//std::cout << "original projection" << std::endl << proj << std::endl;
	
	// find lambda
	Eigen::MatrixXd fmat = Eigen::ArrayXXd::Zero(kfs.size(), kfs.size());
	Eigen::MatrixXd ymat = Eigen::ArrayXXd::Zero(kfs.size(), 2);
	
	for (int i=0; i < fmat.rows(); ++i)
		for (int j=i+1; j < fmat.cols(); ++j)
			fmat(j,i) = fmat(i,j) = projPhi(dmat(kfs(i), kfs(j)));
	for (int i=0; i < ymat.rows(); ++i) {
		ymat(i,0) = proj(kfs(i),0);
		ymat(i,1) = proj(kfs(i),1);
	}
	Eigen::MatrixXd lmat = fmat.inverse() * ymat;
	
	// project remainder frames by RBF
	Eigen::VectorXd rbfvec = Eigen::ArrayXd::Zero(kfs.size());
	int c = 0;
	for (int i=0; i < fids.size(); ++i) {
		if (fids(i) == kfs(c)) {
			if (c != (kfs.size() - 1)) 
				++c;
		} else {
			for (int j=0; j < rbfvec.size(); ++j)
				rbfvec(j) = projPhi(dmat(i, kfs(j)));
			proj.row(i).head(2) = rbfvec.transpose() * lmat;
		}
	}
	((RBFInterpolator *) ih)->rescaleMarkers(proj);
	//std::cout << "RBF projection" << std::endl << proj << std::endl;
	return proj;
}

/**
 * @brief return the implemented radial basis function kernel as quadratic multiquadrics
 * with c and epsilon equals one.
 * @param d2 distance as input
 * @return function output
 */
double ROLSRBFPBaseSelector::projPhi(double d2) {
	//return std::sqrt(1 + d2*d2);  // multiquadrics - elisa
	return (1 + std::pow(d2, 2.5));
}

/**
 * @brief get keyframes for mocap data given by iterative projection improvements
 * @param nkf number of keyframes
 * @return keyframe vector
 */
Eigen::VectorXi FwdBkwdBaseSelector::getKeyframes(unsigned int nkf) {
	unsigned int gdkf, bkf;
	Eigen::MatrixXd proj0, proj1;
	Eigen::VectorXd evec;
	Eigen::VectorXi temp = Eigen::ArrayXi::Zero(nkf);

	bkf = nkf / 2;
	if (bkf < 3) bkf = 3;
	keyframes = ROLSBaseSelector::getKeyframes(bkf);
	proj0 = getRBF2DProjection(keyframes);
	evec = proj0.col(0);
	for (unsigned int i=bkf; i < nkf; ++i) {
		proj1 = ((RBF2DInterpolator *) ih)->improveProjectionByGD(proj0, keyframes);
		proj0 = proj1 - proj0;
		for (unsigned int j=0; j < evec.size(); ++j) 
			evec(j) = proj0.row(j).norm();
		gdkf = 0;
		for (unsigned int j=1; j < evec.size(); ++j) 
			if (evec(gdkf) < evec(j)) 
				gdkf = j;
		temp(0) = gdkf;
		std::cout << "kf selected = " << gdkf << std::endl;
		for (unsigned int j=0; j < keyframes.size(); ++j)
			if (keyframes(j) < temp(j)) {
				temp(j+1) = temp(j);
				temp(j) = keyframes(j);
			} else {
				temp(j+1) = keyframes(j);
			}
		keyframes = temp.head(keyframes.size() + 1);
		proj0 = getRBF2DProjection(keyframes);
	}
	((RBF2DInterpolator *) ih)->rescaleMarkers(proj0);
	ih->setRdframes(proj0);
	return keyframes;
}
