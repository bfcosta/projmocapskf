#ifndef MULTIREADER_H
#define MULTIREADER_H

#include <vector>
#include <mocapreader.h>

/**
 * @class MultiReader
 * @brief A class for reading the contents of many mocaps as a single one.
 *
 * This class implements a reader of many files as a way to mix contents
 * of different files in a single reader. This reader needs to be initialized
 * with a maximum number of frames. As the sum of number of frames of each 
 * file surpasses this maximum, the content is re-sampled to fit in this
 * restriction.
 */

class MultiReader: public MocapReader {

	protected:
		std::vector<MocapReader *> mset;
		std::vector<unsigned int> mcref;
		std::vector<unsigned int> kfref;
		unsigned int maxframes;
		
		Eigen::VectorXi getUSKeyframes(MocapReader * mc, float pc);
		Eigen::VectorXi getSCSKeyframes(MocapReader * mc, float pc);

	public:
		/**
		 * @brief builds new multi file reader with a maximum number of frames
		 * @param nf maximum number of frames
		 */
		MultiReader(unsigned int nf) {
			maxframes = nf;
		}
		/**
		 * @brief clears all data
		 */
		virtual ~MultiReader() {
			removeAll();
		}
		/**
		 * @brief add new mocap file to the reader
		 * @param rd mocap handler reference
		 */
		void addMocap(MocapReader * rd) {
			mset.push_back(rd);
		}
		/**
		 * @brief remove mocap file from the reader
		 * @param rd mocap handler reference
		 */
		void removeMocap(MocapReader * rd) {
			unsigned int i;
			for (i=0; i < mset.size(); ++i) 
				if (rd == mset.at(i))
					break;
			if (i < mset.size()) {
				if (mset.at(i) != NULL)
					delete mset.at(i);
				mset.erase(mset.begin() + i);
			}
		}
		/**
		 * @brief remove mocap file from the reader
		 * @param id mocap file id in vector
		 */
		void removeMocap(unsigned int id) {
			if (id < mset.size()) {
				if (mset.at(id) != NULL) 
					delete mset.at(id);
				mset.erase(mset.begin() + id);
				
			}
		}
		/**
		 * @brief remove all files added to the reader
		 */
		void removeAll() {
			for (unsigned int i=0; i < mset.size(); ++i) {
				delete mset.at(i);
			}
			mset.clear();
		}
		/**
		 * @brief get total of unit mocap readers
		 * @return number of unit readers
		 */
		unsigned int getNumberOfReaders() {
			return mset.size();
		}
		/**
		 * @brief get unit reader
		 * @param i unit mocap reader id
		 * @return reference to unit mocap reader
		 */
		MocapReader * getReader(unsigned int i) {
			return mset.at(i);
		}

		void reassign();
		Eigen::VectorXi getFrameSampleByUnitReader(unsigned int i);
		Eigen::VectorXi getFrameSampleByUnitReader(MocapReader * rd);
		unsigned int getFrameBaseByUnitReader(unsigned int i);
		unsigned int getFrameBaseByUnitReader(MocapReader * rd);
		unsigned int getFrameIdBySampleId(unsigned int i);
		unsigned int getSampleIdByFrameId(unsigned int i);
		//
		virtual Skeleton getSkeleton();
		virtual unsigned int getNumberOfDOFs();
		virtual unsigned int getNumberOfFrames();
		virtual Skeleton getFrameSkeleton(unsigned int i);
		virtual Skeleton getFrameSkeleton(Eigen::VectorXd dofs, bool euler = true);
		virtual Eigen::MatrixXd getEulerAnglesDOFMatrix();
		virtual Eigen::VectorXd getEulerAnglesDOFPose(Skeleton & sk);
		virtual void readFile(const std::string & fname);
		virtual unsigned int getFrameInterval();
		virtual void print();
		virtual void printPose(Skeleton & sk, std::ostream & ost);
		virtual Eigen::MatrixXd getQuaternionDOFMatrix();
};

#endif
