#ifndef ASFREADER_H
#define ASFREADER_H

#include <vector>
#include <mocapreader.h>

/**
 * \class ASFReader
 *
 * \brief A reader for AMC/ASF file format
 *
 * Implements a reader to ASF/AMC file format. This format splits skeleton
 * data and animation data in two different files. ASF file describes
 * the skeleton and AMC file describes the animation. Most important tags
 * on ASF file are direction, length and dof. The product of the first
 * two gives the offset between joints and dof tag interfaces data
 * provided by the AMC file for anlge rotations basically. Details are
 * omitted here. 
 * 
 * \see http://research.cs.wisc.edu/graphics/Courses/cs-838-1999/Jeff/ASF-AMC.html
 */

class ASFReader: public MocapReader {
	protected:
	/**
	 * A structure to read direction and axis tag.
	 */
	struct triplef {
		float x,y,z;
	};

	/**
	 * A structure to read dof tag. 
	 */
	struct tripleb {
		bool rx,ry,rz;
	};

	/**
	 * A structure to read limit tag.
	 */
	struct rtriplef {
		float ix,ax,iy,ay,iz,az;
	};

	/**
	 * A structure to read frame description on AMC files.
	 */
	struct amc {
		std::vector<unsigned int> nameref;
		std::vector< std::vector<float> > bhandler;
	};
	
	/// position of the root skeleton joint on its resting pose
	struct triplef rootpos;
	/// rotation of root joint axis
	struct triplef rootor;
	/// flag telling if angles are described in degrees or radians
	bool degrees;
	/// rotation order between X, Y and Z directions
	char axorder[3];
	/// degrees of freedom for root joint, the only which can have more than 3
	char rootdof[6][2];
	/// bone names in ASF file
	std::vector<std::string> name;
	/// bone ids in ASF file
	std::vector<unsigned int> id;
	/// bone parent id. See :hierachy section on ASF file.
	std::vector<unsigned int> parent;
	/// direction tag in ASF file
	std::vector<struct triplef> direction;
	/// axis tag in ASF file
	std::vector<struct triplef> axis;
	/// degrees of freedom (dof) tag in ASF file
	std::vector<struct tripleb> dof;
	/// flag support for limit tag. If true, limit should be ingored.
	std::vector<bool> unbounded;
	/// limit tag for ASF file.
	std::vector<struct rtriplef> limit;
	/// length of the bone as in ASF file
	std::vector<float> length;
	/// frame data for the given frames, mostly angles.
	std::vector<struct amc> motion;
	/// matrix for converting global axis to local bone axis
	std::vector<Eigen::Matrix4d> world2local;
	/// matrix for converting local bone axis to global axis
	std::vector<Eigen::Matrix4d> local2world;
	/// joint id order translator between amc and asf file
	std::vector<unsigned int> drefs;
	/// bone maximum length
	float mblen;

	unsigned int getBoneId(const std::string &s);
	void readAsf(const std::string & fname);
	void readAmc(const std::string & fname);
	void setAxisCorrection();
	void findAmc2AsfJointAssociation(unsigned int fid);
	Eigen::Matrix4d getGlobalOffsetMatrix(double tx, double ty, 
		double tz);
	Eigen::Matrix4d getLocalOffsetMatrix(double tx, double ty, 
		double tz, unsigned int i);
	Eigen::Matrix4d getGlobalRotationMatrix(double rx, double ry,
		double rz, unsigned int i);
	Eigen::Matrix4d getLocalRotationMatrix(double rx, double ry,
		double rz);

	public:
	ASFReader();
	virtual void readFile(const std::string & fname);
	virtual Skeleton getSkeleton();
	virtual unsigned int getNumberOfDOFs();
	virtual unsigned int getNumberOfFrames();
	virtual Skeleton getFrameSkeleton(unsigned int id);
	virtual Skeleton getFrameSkeleton(Eigen::VectorXd dofs, bool euler = true);
	virtual unsigned int getFrameInterval();
	virtual void print();
	virtual Eigen::MatrixXd getEulerAnglesDOFMatrix();
	virtual Eigen::MatrixXd getQuaternionDOFMatrix();
	virtual void printPose(Skeleton & sk, std::ostream & ost);
	virtual Eigen::VectorXd getEulerAnglesDOFPose(Skeleton & sk);
};

# endif
