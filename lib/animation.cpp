#include <animation.h>
#include <GL/gl.h>
#include <iostream>

/**
 * \return animation current skeleton
 * 
 * get current skeleton
 */    
Skeleton & Animation::getSkeleton() {
    return skel;
}

/**
 * @brief set skeleton to be animated
 * @param s skeleton to be animated
 */
void Animation::setSkeleton(Skeleton & s) {
    skel = s;
    skel.setColor(0.0f, 1.0f, 1.0f);
}

/**
 * \return array of recorded poses
 * 
 * get marked frame poses described by volatile matrices
 */
const std::vector<std::vector<Eigen::Matrix4d> > &Animation::getFrameSet() {
    return frameset;
}

/**
 * \return array with all markers positions
 * 
 * get pose frame markers
 */
std::vector<Eigen::Vector4d> & Animation::getMarkers() {
    return markers;
}

/**
 * \param fid frame id recorded
 * \param sk skeleton pose at the given frame id
 * 
 * add a new Keyframe. Saves current skeleton and current frame id
 * as the marker position.
 */
void Animation::addNewKeyFrame(unsigned int fid, Skeleton & sk) {
    Eigen::Vector4d c;
	
	// add new marker and update controller position
    c(0) = fid;
    c(1) = 0;
    c(2) = 0;
    c(3) = 1;
    addNewKeyFrame(c, sk);
}

/**
 * \param m marker position on 3D, 2D or 1D
 * \param sk skeleton pose at the given frame id
 * 
 * add a new Keyframe. Saves current skeleton for a given marker position.
 */
void Animation::addNewKeyFrame(Eigen::Vector4d const & m, Skeleton &sk) {
    std::vector<Eigen::Matrix4d> frame;
	
	// add new marker and update controller position
	markers.push_back(m);
    frame = sk.getBoneMatrices();
    frame.push_back(sk.getRootOrientation());
    frame.push_back(sk.getSkeletonRootTranslation());
	// record current skeleton
    frameset.push_back(frame);
	
}

/**
 *  update skeleton from controller position
 */
void Animation::animate() {
    std::vector<Eigen::Matrix4d> pose = rbfint.getNewSkel(controller);
    Eigen::Matrix4d ori = pose.at(pose.size() - 2);
    Eigen::Matrix4d jtr = pose.at(pose.size() - 1);
    pose.pop_back();
    pose.pop_back();
    skel.setPose(jtr, ori, pose);
}

/**
 *  prepare data for animation interpolation
 */
void Animation::setContext() {
	if (frameset.size() < 2) {
		std::cout << "Not enough frame poses to interpolate." << std::endl;
		return;
	}
	assert(markers.size() == frameset.size());
	// add markers
	rbfint.reset();
	rbfint.addMarker(markers);
	// add skeletons
	for (unsigned int i=0; i < frameset.size(); ++i) {
        rbfint.addSkel(frameset.at(i));
	}
	rbfint.setCsradius(csr);
	rbfint.solve();
}

/**
 *  function for debugging purposes
 */ 
void Animation::printMarkers() {

    std::cout << "controller = (" << controller(0) << "\t" << controller(1) <<
              "\t" << controller(2) << "\t" << controller(3) << ")\n";
    std::cout << "total of markers = " << markers.size() << "\n";
	for (unsigned int i=0; i < markers.size(); ++i)
        std::cout << "marker " << i << " = (" << markers.at(i)(0) <<
              "\t" << markers.at(i)(1) << "\t" << markers.at(i)(2) << "\t" <<
                  markers.at(i)(3) << "\t" << ")\n";
}

/**
 * \return current controller position
 *
 * get controller position
 */
Eigen::Vector4d & Animation::getController() {
    return controller;
}

/**
 * \param fid a controller position in 1D.
 *
 * set current controller position.
 */
void Animation::setController(unsigned int fid) {
	Eigen::Vector4d c;
	
	c << 0,0,0,1;
    c(0) = (double) fid;
    setController(c);
}

/**
 * \param c a controller position in 3D, 2D or 1D.
 *
 * set current controller position.
 */
void Animation::setController(Eigen::Vector4d const & c) {
	controller = c;
}
