#include <skanalizer.h>
#include <iostream>

/**
 * @brief get the rotation distance represented by a transformation matrix
 * @param m1 bone transformation matrix
 * @param m2 bone transformation matrix
 * @return rotation distance between two points in a rotation sphere with radius equal to one
 */
double EuclideanSkeletonAnalizer::getMatRotDist(Eigen::Matrix4d const & m1, Eigen::Matrix4d const & m2) {
	Eigen::Vector4d v1, v2, v3; 
	double r = std::sqrt(3.0);
	v1 << 1, 1, 1, r;
	v2 << 1, 1, 1, r;
	v1 = m1 * v1; 
	v2 = m2 * v2;
	v3 = v1 - v2;
	return fabs( v3.head(3).dot(v3.head(3)) / 3.0 );
}

/**
 * @brief get the euclidian distance between two points represented both by a transformation matrix
 * @param m1 bone transformation matrix
 * @param m2 bone transformation matrix
 * @return euclidian distance between two points represented by transformation matrices
 */
double EuclideanSkeletonAnalizer::getMatEuclDist(Eigen::Matrix4d const & m1, Eigen::Matrix4d const & m2) {
	Eigen::Vector4d v1, v2, v3; 
	v1 << 0,0,0,1;
	v2 << 0,0,0,1;
	v1 = m1 * v1; 
	v2 = m2 * v2;
	v3 = v1 - v2;
	return fabs( v3.head(3).dot(v3.head(3)) );
}

/**
 * @brief get weight vector for current distance metric. Options 
 * are constant, increasing and decreasing. If increasing, each dof gets 
 * its weight equal to its hierarchy level. If decreasing, each dof gets 
 * weight equal to the inverse of its hierarchy level.
 * 
 * @param s1 skeleton structure
 * @param option flag for weight vector. 0 for constant, 1 for increasing, 
 * 2 for decreasing, 3 for bone length, 4 for hierarchical bone length,
 * 5 for reverse hierarchical bone length. 
 * Any other value stands for constant.
 * 
 * @return weight vector for distance metric
 */
std::vector<double> SkeletonAnalizer::getWeights(Skeleton & s1, unsigned int option) {
	std::vector<Eigen::Matrix4d> jmats =  s1.getJointMatrices();
	std::vector<double> weights(jmats.size(), 1.0);
	std::vector<unsigned int> par = s1.getIndexHierarchy();
		
	switch (option) {
		case 1:
			for (unsigned int i=0; i < par.size(); ++i) 
				weights.at( par.at(i) ) += 1;
			break;
		case 2:
			for (unsigned int i=0; i < par.size(); ++i) 
				weights.at( par.at(i) ) += 1;
			for (unsigned int i=0; i < weights.size(); ++i) 
				weights.at(i) = 1 / weights.at(i);
			break;
		case 3:
			for (unsigned int i=1; i < weights.size(); ++i) 
				weights.at(i) += jmats.at(i).col(3).head(3).norm();
			break;
		case 4:
			for (unsigned int i=1; i < weights.size(); ++i) 
				weights.at(i) += jmats.at(i).col(3).head(3).norm();
			for (unsigned int i=0; i < par.size(); ++i) 
				weights.at( par.at(i) ) += weights.at(i) - 1;
			break;
		case 5:
			for (unsigned int i=1; i < weights.size(); ++i) 
				weights.at(i) += jmats.at(i).col(3).head(3).norm();
			double dd;
			for (unsigned int i=weights.size(); i > 0; --i) {
				dd = 0;
				for (unsigned int j=0; j < par.size(); ++j) {
					if ((par.at(j) == (i-1)) && (dd < weights.at(j)))
						dd = weights.at(j);
				}
				weights.at(i-1) += dd;
			}

			break;
		default:
			break;
	}
	return weights;	
}


/**
 * @brief get the quaternion distance between two angle/axis systems represented by rotation matrices
 * @param m1 bone rotation matrix
 * @param m2 bone rotation matrix
 * @return quaternion distance between two rotation matrices
 */
double QuaternionSkeletonAnalizer::getDistance(Eigen::Matrix4d const & m1, Eigen::Matrix4d const & m2) {
	Eigen::Quaterniond q1(m1.block<3,3>(0,0));
	Eigen::Quaterniond q2(m2.block<3,3>(0,0));
	double ret = q1.vec().dot(q2.vec()) + q1.w()*q2.w();

	if (ret > 1) ret = 1;
	ret = 1 - ret*ret;
	return ret;
}

/**
 * @brief get a distance metric measure between two skeleton poses using euclidean distance
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return sum of weighted squared errors for all skeleton joints
 */
double EuclideanSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<Eigen::Vector4d> b1 = s1.getJointVectors();
	std::vector<Eigen::Vector4d> b2 = s2.getJointVectors();
	Eigen::Vector4d jp;
	assert(b1.size() == b2.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double err = 0;
	for (unsigned int i=0; i < b1.size(); ++i) {
		jp = b1.at(i) - b2.at(i);
		err =  err + weights.at(i) * jp.dot(jp);
	}
	return err;
}

/**
 * @brief get the multidimensional vector for a given pose with current weighted scheme.
 * @param s1 skeleton pose
 * @return pose vector
 */
Eigen::VectorXd EuclideanSkeletonAnalizer::getMultidimensionalWeightedPoseVector(Skeleton & s1) {
	std::vector<Eigen::Vector4d> jpos = s1.getJointVectors();
	std::vector<double> weights = getWeights(s1, wfunc);
	Eigen::VectorXd mvec = Eigen::ArrayXd::Zero(3 * jpos.size());	
	double sqw;
	
	for (unsigned int i=0; i < jpos.size(); ++i) {
		sqw = std::sqrt(weights.at(i));
		mvec(3*i) = sqw * jpos.at(i)(0);
		mvec(3*i + 1) = sqw * jpos.at(i)(1);
		mvec(3*i + 2) = sqw * jpos.at(i)(2);
	}
	return mvec;
}

/**
 * @brief get a distance metric measure between two skeleton poses using
 * translation invariant euclidean distance
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return sum of weighted squared errors for all skeleton joints
 */
double TrInvEucSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<Eigen::Vector4d> b1 = s1.getJointVectors();
	std::vector<Eigen::Vector4d> b2 = s2.getJointVectors();
	Eigen::Vector4d jp, jc1, jc2;
	assert(b1.size() == b2.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double err = 0;
	jc1 = b1.at(0);
	jc2 = b2.at(0);
	for (unsigned int i=1; i < b1.size(); ++i) {
		b1.at(i) -= jc1;
		b2.at(i) -= jc2;
	}
	for (unsigned int i=0; i < b1.size(); ++i) {
		jp = b1.at(i) - b2.at(i);
		err =  err + weights.at(i) * jp.dot(jp);
	}
	return err;
}

/**
 * @brief get the multidimensional vector for a given pose with current weighted scheme.
 * @param s1 skeleton pose
 * @return pose vector
 */
Eigen::VectorXd TrInvEucSkeletonAnalizer::getMultidimensionalWeightedPoseVector(Skeleton & s1) {
	std::vector<Eigen::Vector4d> jpos = s1.getJointVectors();
	Eigen::Vector4d jc;
	std::vector<double> weights = getWeights(s1, wfunc);
	Eigen::VectorXd mvec = Eigen::ArrayXd::Zero(3 * jpos.size());
	double sqw;

	jc = jpos.at(0);
	for (unsigned int i=1; i < jpos.size(); ++i)
		jpos.at(i) -= jc;
	for (unsigned int i=0; i < jpos.size(); ++i) {
		sqw = std::sqrt(weights.at(i));
		mvec(3*i) = sqw * jpos.at(i)(0);
		mvec(3*i + 1) = sqw * jpos.at(i)(1);
		mvec(3*i + 2) = sqw * jpos.at(i)(2);
	}
	return mvec;
}

/**
 * @brief get a distance metric measure between two skeleton poses using
 * euclidean distance without root joint
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return sum of weighted squared errors for all skeleton joints
 */
double RootlessEucSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<Eigen::Vector4d> b1 = s1.getJointVectors();
	std::vector<Eigen::Vector4d> b2 = s2.getJointVectors();
	Eigen::Vector4d jp, jc1, jc2;
	assert(b1.size() == b2.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double err = 0;
	jc1 = b1.at(0);
	jc2 = b2.at(0);
	for (unsigned int i=0; i < b1.size(); ++i) {
		b1.at(i) -= jc1;
		b2.at(i) -= jc2;
	}
	for (unsigned int i=0; i < b1.size(); ++i) {
		jp = b1.at(i) - b2.at(i);
		err =  err + weights.at(i) * jp.dot(jp);
	}
	return err;
}

/**
 * @brief get the multidimensional vector for a given pose with current weighted scheme.
 * @param s1 skeleton pose
 * @return pose vector
 */
Eigen::VectorXd RootlessEucSkeletonAnalizer::getMultidimensionalWeightedPoseVector(Skeleton & s1) {
	std::vector<Eigen::Vector4d> jpos = s1.getJointVectors();
	Eigen::Vector4d jc;
	std::vector<double> weights = getWeights(s1, wfunc);
	Eigen::VectorXd mvec = Eigen::ArrayXd::Zero(3 * jpos.size());
	double sqw;

	jc = jpos.at(0);
	for (unsigned int i=0; i < jpos.size(); ++i)
		jpos.at(i) -= jc;
	for (unsigned int i=0; i < jpos.size(); ++i) {
		sqw = std::sqrt(weights.at(i));
		mvec(3*i) = sqw * jpos.at(i)(0);
		mvec(3*i + 1) = sqw * jpos.at(i)(1);
		mvec(3*i + 2) = sqw * jpos.at(i)(2);
	}
	return mvec;
}

/**
 * @brief get weight vector for current distance metric. Options 
 * are constant, increasing and decreasing. If increasing, each dof gets 
 * its weight equal to its hierarchy level. If decreasing, each dof gets 
 * weight equal to the inverse of its hierarchy level.
 * 
 * @param s1 skeleton structure
 * @param option flag for weight vector. 0 for constant, 1 for increasing, 
 * 2 for decreasing. Any other value stands for increasing.
 * 
 * @return weight vector for distance metric
 *
std::vector<double> QuaternionSkeletonAnalizer::getWeights(Skeleton & s1, unsigned int option) {
	std::vector<double> weights(s1.getBoneMatrices().size() + 1, 1.0);
	std::vector<unsigned int> par = s1.getIndexHierarchy();
	
	if (option != 0) {	
		for (unsigned int i=0; i < par.size(); ++i) 
				weights.at( par.at(i) ) += 1;
		if (option == 2) {
			for (unsigned int i=0; i < weights.size(); ++i) 
				weights.at(i) = 1 / weights.at(i);		
		} 
	}
	return weights;
	
}
*/

/**
 * @brief get a distance metric measure between two skeleton poses using quaternion distance
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return sum of weighted squared errors for all skeleton joints
 */
double QuaternionSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<Eigen::Matrix4d> b1 = s1.getBoneMatrices();
	std::vector<Eigen::Matrix4d> b2 = s2.getBoneMatrices();
	Eigen::Matrix4d jm1 = s1.getJointMatrices().at(0);
	Eigen::Matrix4d jm2 = s2.getJointMatrices().at(0);
	Eigen::Vector4d j1, j2;
	j1 << 0,0,0,1;
	j2 << 0,0,0,1;
	j1 = jm1 * j1;
	j2 = jm2 * j2;
	assert(b1.size() == b2.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double err = 0;
	for (unsigned int i=0; i < b1.size(); ++i) {
		err =  err + weights.at(i+1) * getDistance(b1.at(i), b2.at(i));
	}
	j1 = j1 - j2;
	err =  err + weights.at(0) * (j1.dot(j1));
	return err;
}

/**
 * @brief get the multidimensional vector for a given pose with current weighted scheme.
 * @param s1 skeleton pose
 * @return pose vector
 */
Eigen::VectorXd QuaternionSkeletonAnalizer::getMultidimensionalWeightedPoseVector(Skeleton & s1) {
	std::vector<Eigen::Matrix4d> rmats = s1.getBoneMatrices();
	Eigen::VectorXd mvec = Eigen::ArrayXd::Zero(4 * rmats.size() + 3);	
	Eigen::Matrix4d jm1 = s1.getJointMatrices().at(0);
	std::vector<double> weights = getWeights(s1, wfunc);
	double sqw;
	Eigen::Vector4d j1;
	Eigen::Quaterniond q;

	j1 << 0,0,0,1;
	j1 = jm1 * j1;
	sqw = std::sqrt(weights.at(0));
	mvec(0) = sqw * j1(0);
	mvec(1) = sqw * j1(1);
	mvec(2) = sqw * j1(2);
	for (unsigned int i=0; i < rmats.size(); ++i) {
		q = rmats.at(i).block<3,3>(0,0);
		sqw = std::sqrt(weights.at(i+1));
		mvec(4*i + 3) = sqw * q.w();
		mvec(4*i + 4) = sqw * q.x();
		mvec(4*i + 5) = sqw * q.y();
		mvec(4*i + 6) = sqw * q.z();
	}
	return mvec;
}

/**
 * @brief get a distance metric measure between two skeleton poses using quaternion distance without root joint
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return sum of weighted squared errors for all skeleton joints
 */
double RootlessQuatSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<Eigen::Matrix4d> b1 = s1.getBoneMatrices();
	std::vector<Eigen::Matrix4d> b2 = s2.getBoneMatrices();
	assert(b1.size() == b2.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double err = 0;
	for (unsigned int i=0; i < b1.size(); ++i) {
		err =  err + weights.at(i+1) * getDistance(b1.at(i), b2.at(i));
	}
	return err;
}

/**
 * @brief get the multidimensional vector for a given pose with current weighted scheme.
 * @param s1 skeleton pose
 * @return pose vector
 */
Eigen::VectorXd RootlessQuatSkeletonAnalizer::getMultidimensionalWeightedPoseVector(Skeleton & s1) {
	std::vector<Eigen::Matrix4d> rmats = s1.getBoneMatrices();
	Eigen::VectorXd mvec = Eigen::ArrayXd::Zero(4 * rmats.size() + 3);
	std::vector<double> weights = getWeights(s1, wfunc);
	double sqw;
	Eigen::Quaterniond q;

	for (unsigned int i=0; i < rmats.size(); ++i) {
		q = rmats.at(i).block<3,3>(0,0);
		sqw = std::sqrt(weights.at(i+1));
		mvec(4*i + 3) = sqw * q.w();
		mvec(4*i + 4) = sqw * q.x();
		mvec(4*i + 5) = sqw * q.y();
		mvec(4*i + 6) = sqw * q.z();
	}
	return mvec;
}

/**
 * @brief get the geodesic distance between two angle/axis systems represented by rotation matrices
 * @param m1 bone rotation matrix
 * @param m2 bone rotation matrix
 * @return geodesic distance between two rotation matrices
 */
double GeodesicSkeletonAnalizer::getDistance(Eigen::Matrix4d const & m1, Eigen::Matrix4d const & m2) {

	Eigen::Matrix4d rot = m1 * m2.transpose();
	double ret = (rot(0,0) + rot(1,1) + rot(2,2) - 1) / 2;
	// safe acos
	if (ret > 1) ret = 1; else if (ret < -1) ret = -1;
	ret = std::acos(ret);
	return ret;
}

/**
 * @brief get a distance metric measure between two skeleton poses using geodesic distance
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return sum of weighted squared errors for all skeleton joints
 */
double GeodesicSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<Eigen::Matrix4d> b1 = s1.getBoneMatrices();
	std::vector<Eigen::Matrix4d> b2 = s2.getBoneMatrices();
	assert(b1.size() == b2.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double err = 0;
	for (unsigned int i=0; i < b1.size(); ++i) {
		err =  err + weights.at(i+1) * getDistance(b1.at(i), b2.at(i));
	}
	return err;
}

/**
 * @brief get the multidimensional vector for a given pose with current weighted scheme.
 * @param s1 skeleton pose
 * @return pose vector
 */
Eigen::VectorXd GeodesicSkeletonAnalizer::getMultidimensionalWeightedPoseVector(Skeleton & s1) {
	std::vector<Eigen::Matrix4d> rmats = s1.getBoneMatrices();
	Eigen::VectorXd mvec = Eigen::ArrayXd::Zero(4 * rmats.size());
	std::vector<double> weights = getWeights(s1, wfunc);
	double sqw;
	Eigen::Quaterniond q;

	for (unsigned int i=0; i < rmats.size(); ++i) {
		q = rmats.at(i).block<3,3>(0,0);
		sqw = std::sqrt(weights.at(i+1));
		mvec(4*i) = sqw * q.w();
		mvec(4*i) = sqw * q.x();
		mvec(4*i) = sqw * q.y();
		mvec(4*i) = sqw * q.z();
	}
	return mvec;
}

/**
 * @brief get the area of two bones by triangulating extremities
 * @param b00 first bone's first joint
 * @param b01 first bone's second joint
 * @param b10 second bone's first joint
 * @param b11 second bone's second joint
 * @return area of two triangles
 */
double AreaSkeletonAnalizer::getBoneArea(Eigen::Vector4d const & b00,
	Eigen::Vector4d const & b01, Eigen::Vector4d const & b10,
	Eigen::Vector4d const & b11) {

	double a0, a1;
	Eigen::Vector4d ab, ac;

	// first triangle
	ab = b10 - b00;
	ac = b11 - b00;
	a0 = ab.cross3(ac).norm() / 2;
	// second triangle
	ab = b01 - b00;
	a1 = ab.cross3(ac).norm() / 2;
	return (a0+a1) / 2;
}


/**
 * @brief get a distance metric measure between two skeleton poses as (an attempt to calculate) the area
 * between them
 * @param s1 skeleton pose
 * @param s2 skeleton pose
 * @return area between two given seleton poses
 */
double AreaSkeletonAnalizer::getPoseDistance(Skeleton & s1, Skeleton & s2) {
	std::vector<unsigned int> par1, par2;
	par1 = s1.getIndexHierarchy();
	par2 = s2.getIndexHierarchy();
	
	assert(par1.size() == par2.size());
	std::vector<Eigen::Vector4d> j1, j2;
	j1.resize(par1.size());
	j2.resize(par2.size());

	for (unsigned int i=0; i < j1.size(); ++i) {
		j1.at(i) = s1.getJointPosition(i);
		j2.at(i) = s2.getJointPosition(i);
	}
	std::vector<double> weights = getWeights(s1, wfunc);
	double area = 0;

	Eigen::Vector4d b00, b01, b10, b11;
	// make distance translatio invariant
	b00 = j1.at(0);
	b01 = j2.at(0);
	for (unsigned int i=0; i < j1.size(); ++i) {
		j1.at(i) -= b00;
		j2.at(i) -= b01;
	}
	for (unsigned int i=1; i < j1.size(); ++i) {
		b01 = j1.at(i);
		b00 = j1.at(par1.at(i));
		b11 = j2.at(i);
		b10 = j2.at(par2.at(i));
		area += weights.at(i) * getBoneArea(b00, b01, b10, b11);
	}
	return area;
}

